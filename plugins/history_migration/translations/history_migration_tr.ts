<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.0">
<context>
    <name>HistoryImportWindow</name>
    <message>
        <source>History is now being imported into new format. Please wait until this task is finished.</source>
        <translation>Geçmiş şuan yeni formatında yükleniyor. Görev tamamlanana kadar lütfen bekleyiniz.</translation>
    </message>
    <message>
        <source>Chats progress:</source>
        <translation>Konuşma ilerlemesi:</translation>
    </message>
    <message>
        <source>Messages progress:</source>
        <translation>Mesaj ilerlemesi:</translation>
    </message>
</context>
<context>
    <name>HistoryMigrationActions</name>
    <message>
        <source>Import history...</source>
        <translation>Geçmiş yükleniyor...</translation>
    </message>
</context>
</TS>