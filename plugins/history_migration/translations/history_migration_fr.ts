<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>HistoryImportWindow</name>
    <message>
        <source>History is now being imported into new format. Please wait until this task is finished.</source>
        <translation>L&apos;historique est maintenant importé dans le format. Veuillez patienter jusqu&apos;à ce que cette tâche soit terminée.</translation>
    </message>
    <message>
        <source>Chats progress:</source>
        <translation>Progression de la conversation :</translation>
    </message>
    <message>
        <source>Messages progress:</source>
        <translation>Progression des messages :</translation>
    </message>
</context>
<context>
    <name>HistoryMigrationActions</name>
    <message>
        <source>Import history...</source>
        <translation>Importer l&apos;historique...</translation>
    </message>
</context>
</TS>