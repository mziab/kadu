<?xml version="1.0" ?><!DOCTYPE TS><TS language="cs" version="2.0">
<context>
    <name>HistoryImportWindow</name>
    <message>
        <source>History is now being imported into new format. Please wait until this task is finished.</source>
        <translation>Historie se nyní převádí do nového formátu. Počkejte, prosím, dokud tento úkol nebude dokončen.</translation>
    </message>
    <message>
        <source>Chats progress:</source>
        <translation>Postup rozhovorů:</translation>
    </message>
    <message>
        <source>Messages progress:</source>
        <translation>Postup zpráv:</translation>
    </message>
</context>
<context>
    <name>HistoryMigrationActions</name>
    <message>
        <source>Import history...</source>
        <translation>Zavést historii...</translation>
    </message>
</context>
</TS>