<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.0">
<context>
    <name>ImportProfileWindow</name>
    <message>
        <source>Import external Kadu 0.6.5 profile</source>
        <translation>Harici Kadu 0.6.5 profili yükle</translation>
    </message>
    <message>
        <source>Select profile path</source>
        <translation>Profil yolu seç</translation>
    </message>
    <message>
        <source>Select profile path:</source>
        <translation>Profil yolu seçimi:</translation>
    </message>
    <message>
        <source>Select imported account identity:</source>
        <translation>Yüklenen hesabın kimliğini seçin:</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Yükleme geçmişi</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Yükle</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <source>&lt;b&gt;Identity not selected&lt;/b&gt;</source>
        <translation>&lt;b&gt;Kimlik seçilmedi&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Selected directory does not contain kadu.conf.xml file&lt;/b&gt;</source>
        <translation>&lt;b&gt;Seçilen dizinde kadu.conf.xml dosyasını içermiyor&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Harici profil yükle...</translation>
    </message>
    <message>
        <source>This directory is not a Kadu profile directory.
File kadu.conf.xml not found</source>
        <translation>Bu dizin bir Kadu profil dizini değil.
kadu.conf.xml dosyası bulunamadı</translation>
    </message>
    <message>
        <source>Profile successfully imported!</source>
        <translation>Profil başarıyla yüklendi!</translation>
    </message>
    <message>
        <source>Unable to import profile: %1</source>
        <translation>Profil yüklenemiyor: %1</translation>
    </message>
</context>
<context>
    <name>ImportProfilesWindow</name>
    <message>
        <source>&lt;p&gt;Current version of Kadu does not support user profiles.&lt;br /&gt;Instead, multiple account are supported in one instances of kadu.&lt;/p&gt;&lt;p&gt;Please select profiles that you would like to import as&lt;br /&gt;account into this instance of Kadu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Şuanki Kadu&apos;nun versiyonu kullanıcı profillerini desteklemiyor.&lt;br/&gt;Bunun yerine, tek kadu örneğinde birden fazla hesap desteklemektir.&lt;/p&gt;&lt;p&gt;Lütfen bu Kadu örneğindeki hesaba yüklenecek profilleri seçin.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Yükle</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Yükleme geçmişi</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Harici profil yükle...</translation>
    </message>
    <message>
        <source>Profile %1 successfully imported!</source>
        <translation>%1 profili başarıyla yüklendi!</translation>
    </message>
    <message>
        <source>Import profile...</source>
        <translation>Profil yükle...</translation>
    </message>
    <message>
        <source>Unable to import profile: %1: %2</source>
        <translation>Profil yüklenemiyor: %1: %2</translation>
    </message>
</context>
<context>
    <name>ProfileImporter</name>
    <message>
        <source>Unable to open profile file [%1].</source>
        <translation>[%1] profil dosyası açılamıyor.</translation>
    </message>
    <message>
        <source>Account already exists.</source>
        <translation>Hesap zaten mevcut.</translation>
    </message>
    <message>
        <source>Imported account has no ID</source>
        <translation>Yüklenilen hesabın ID yok</translation>
    </message>
</context>
<context>
    <name>ProfilesImportActions</name>
    <message>
        <source>Import profiles...</source>
        <translation>Profil yükle...</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Harici profil yükle...</translation>
    </message>
</context>
</TS>