<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Show YouTube movies in chat window</source>
        <translation>Show YouTube movies in chat window</translation>
    </message>
    <message>
        <source>Show images in chat window</source>
        <translation>Show images in chat window</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Messages</translation>
    </message>
</context>
</TS>