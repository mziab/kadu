<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Keep contact list size</source>
        <translation>Keep contact list size</translation>
    </message>
    <message>
        <source>Hide scroll bar</source>
        <translation>Hide scroll bar</translation>
    </message>
    <message>
        <source>Extras</source>
        <translation>Extras</translation>
    </message>
    <message>
        <source>Simple View</source>
        <translation>Simple View</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Hide window borders</source>
        <translation>Hide window borders</translation>
    </message>
</context>
<context>
    <name>SimpleView</name>
    <message>
        <source>Simple view</source>
        <translation>Simple view</translation>
    </message>
</context>
</TS>