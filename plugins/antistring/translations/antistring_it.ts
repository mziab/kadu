<?xml version="1.0" ?><!DOCTYPE TS><TS language="it" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Antistring</source>
        <translation>Antistring</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Enable Antistring</source>
        <translation>Abilita Antistring</translation>
    </message>
    <message>
        <source>Block message</source>
        <translation>Blocca messaggio</translation>
    </message>
    <message>
        <source>Admonition</source>
        <translation>Ammonizione</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <source>Write log to file</source>
        <translation>Scrivi il log nel file</translation>
    </message>
    <message>
        <source>Conditions</source>
        <translation>Condizioni</translation>
    </message>
    <message>
        <source>Antistring notifications</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Antistring</name>
    <message>
        <source>     DATA AND TIME      ::   ID   ::    MESSAGE
</source>
        <translation>DATA E TEMPO :: ID :: MESSAGGIO</translation>
    </message>
</context>
<context>
    <name>AntistringConfigurationUiHandler</name>
    <message>
        <source>Condition</source>
        <translation>Condizione</translation>
    </message>
    <message>
        <source>Don&apos;t use</source>
        <translation>Non usare</translation>
    </message>
    <message>
        <source>Factor</source>
        <translation>Fattore</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Cambia</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Cancella</translation>
    </message>
</context>
<context>
    <name>AntistringNotification</name>
    <message>
        <source>Antistring</source>
        <translation>Antistring</translation>
    </message>
    <message>
        <source>Your interlocutor send you love letter</source>
        <translation>Il tuo interlocutore ti invia una lettera d&apos;amore</translation>
    </message>
</context>
</TS>