<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Konuşma</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Şifreleme</translation>
    </message>
    <message>
        <source>Enable encrytpion after receiving encrypted message</source>
        <translation>Şifrelemeyi açtıktan sonra alınan mesaj şifrelenecek</translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteEncryptor</name>
    <message>
        <source>Cannot use public key: not a valid RSA key</source>
        <translation>Public anahtar kullanamazsınız: geçerli bir RSA anahtarı değil</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid BASE64 encoding</source>
        <translation>Public anahtar kullanamazsınız: hatalı BASE64 kodlama</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid PKCS1 certificate</source>
        <translation>Public anahtar kullanamazsınız: hatalı PKCS1 sertifikası</translation>
    </message>
    <message>
        <source>Cannot use public key: this key does not allow encrypttion</source>
        <translation>Public anahtar kullanılamaz: bu anahtar şifrelenmeye izin verilmiyor</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid public key not available</source>
        <translation>Şifrelenemez: geçerli public anahtarı yok</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid blowfish key not available</source>
        <translation>Şifrelenemez: geçerli blowfish anahtarı yok</translation>
    </message>
    <message>
        <source>Cannot encrypt: unknown error</source>
        <translation>Şifreleme hatası: bilinmeyen hata</translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteProvider</name>
    <message>
        <source>Simlite</source>
        <translation>Simlite</translation>
    </message>
</context>
<context>
    <name>SimliteSendPublicKeyActionDescription</name>
    <message>
        <source>Send My Public Key (Simlite)</source>
        <translation>Public Anahtarımı Gönder (Simlite)</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Şifreleme</translation>
    </message>
    <message>
        <source>Cannot send keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>Anahtarlar gönderilemez. encryption_ng_simlite eklentisi yüklü mü kontrol edin</translation>
    </message>
    <message>
        <source>Public key dont exist. Do you want to create new one?</source>
        <translation>Public anahtar bulunmuyor. Yeni bir tane oluşturmak ister misiniz?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Evet</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Hayır</translation>
    </message>
    <message>
        <source>Error generating key</source>
        <translation>Anahtar oluşturma hatası</translation>
    </message>
    <message>
        <source>No public key available</source>
        <translation>Public anahtar yok</translation>
    </message>
</context>
</TS>