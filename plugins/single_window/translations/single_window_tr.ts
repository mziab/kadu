<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Look</source>
        <translation>Görünüm</translation>
    </message>
    <message>
        <source>SingleWindow</source>
        <translation>TekliPencere</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Roster position</source>
        <translation>Görev listesini konumu</translation>
    </message>
    <message>
        <source>Choose position of roster in Single Window mode</source>
        <translation>Tekli Pencere modunda görev listesinin konumunu seçin</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Sol</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Sağ</translation>
    </message>
    <message>
        <source>Show number of messages on tab</source>
        <translation>Sekmede mesajların sayısını göster</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Kısayollar</translation>
    </message>
    <message>
        <source>Switch to previous tab</source>
        <translation>Önceki sekmeye geç</translation>
    </message>
    <message>
        <source>Switch to next tab</source>
        <translation>Sonraki sekmeye geç</translation>
    </message>
    <message>
        <source>Show/hide roster</source>
        <translation>Görev listesini göster/gizle</translation>
    </message>
    <message>
        <source>Switch focus between roster and tabs</source>
        <translation>Görev listesi ve sekmeler arasında geçiş yap</translation>
    </message>
    <message>
        <source>Behavior</source>
        <translation>Davranış</translation>
    </message>
</context>
<context>
    <name>SingleWindow</name>
    <message>
        <source>Conference [%1]</source>
        <translation>Konferans [%1]</translation>
    </message>
</context>
</TS>