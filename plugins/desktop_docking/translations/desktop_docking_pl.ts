<?xml version="1.0" ?><!DOCTYPE TS><TS language="pl" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Tacka systemowa</translation>
    </message>
    <message>
        <source>Desktop Docking</source>
        <translation>Dokowanie na pulpicie</translation>
    </message>
    <message>
        <source>Horizontal position</source>
        <translation>Pozycja pozioma</translation>
    </message>
    <message>
        <source>Vertical position</source>
        <translation>Pozycja pionowa</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation>Przezroczystość</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Kolor tła</translation>
    </message>
    <message>
        <source>Enable Move entry in docklet&apos;s menu</source>
        <translation>Pokaż element &apos;Przesuń&apos; w menu ikony</translation>
    </message>
    <message>
        <source>Move</source>
        <translation>Przesuń</translation>
    </message>
    <message>
        <source>Enable icon moving on desktop. After pressing move cursor over docking icon and then move. Press any mouse key when the icon is in right place.</source>
        <translation>Zezwól na przesuwanie ikony po pulpicie. Po wciśnięciu przycisku myszy zacznj przesuwać kursor. Naciśnij przycisk myszy gdy ikona znajdzie się na pożądanej pozycji.</translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Lista znajomych</translation>
    </message>
</context>
<context>
    <name>DesktopDock</name>
    <message>
        <source>Move</source>
        <translation>Przesuń</translation>
    </message>
</context>
</TS>