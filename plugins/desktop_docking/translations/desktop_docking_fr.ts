<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Zone de notification</translation>
    </message>
    <message>
        <source>Desktop Docking</source>
        <translation>Bureau amarré</translation>
    </message>
    <message>
        <source>Horizontal position</source>
        <translation>Position horizontale</translation>
    </message>
    <message>
        <source>Vertical position</source>
        <translation>Position verticale</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation>Transparent</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Couleur de fond</translation>
    </message>
    <message>
        <source>Enable Move entry in docklet&apos;s menu</source>
        <translation>Activer le déplacement des éléments du menu docklet</translation>
    </message>
    <message>
        <source>Move</source>
        <translation>Déplacer</translation>
    </message>
    <message>
        <source>Enable icon moving on desktop. After pressing move cursor over docking icon and then move. Press any mouse key when the icon is in right place.</source>
        <translation>Activer le déplacement d&apos;icône vers le bureau. Pressez le curseur de déplacement au dessus de l&apos;icône du dock puis déplacez le. Appuyez sur n&apos;importe quelle touche de la souris lorsque l&apos;icône est à la bonne place.</translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Liste d&apos;amis</translation>
    </message>
</context>
<context>
    <name>DesktopDock</name>
    <message>
        <source>Move</source>
        <translation>Déplacer</translation>
    </message>
</context>
</TS>