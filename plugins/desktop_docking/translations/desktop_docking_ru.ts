<?xml version="1.0" ?><!DOCTYPE TS><TS language="ru" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Трей</translation>
    </message>
    <message>
        <source>Desktop Docking</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Horizontal position</source>
        <translation>Позиция по-горизонтали</translation>
    </message>
    <message>
        <source>Vertical position</source>
        <translation>Позиция по-вертикали</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation>Прозрачный</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Цыет фона</translation>
    </message>
    <message>
        <source>Enable Move entry in docklet&apos;s menu</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Move</source>
        <translation>Переместить</translation>
    </message>
    <message>
        <source>Enable icon moving on desktop. After pressing move cursor over docking icon and then move. Press any mouse key when the icon is in right place.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Список контактов</translation>
    </message>
</context>
<context>
    <name>DesktopDock</name>
    <message>
        <source>Move</source>
        <translation>Переместить</translation>
    </message>
</context>
</TS>