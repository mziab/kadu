<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Sistem Tepsisi</translation>
    </message>
    <message>
        <source>Desktop Docking</source>
        <translation>Masaüstü Yerleşimi</translation>
    </message>
    <message>
        <source>Horizontal position</source>
        <translation>Yatay konum</translation>
    </message>
    <message>
        <source>Vertical position</source>
        <translation>Dikey konum</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation>Şeffaf</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Arkaplan rengi</translation>
    </message>
    <message>
        <source>Enable Move entry in docklet&apos;s menu</source>
        <translation>Docklet menüsünü taşımaya izin ver</translation>
    </message>
    <message>
        <source>Move</source>
        <translation>Taşı</translation>
    </message>
    <message>
        <source>Enable icon moving on desktop. After pressing move cursor over docking icon and then move. Press any mouse key when the icon is in right place.</source>
        <translation>Masaüstündeki simgeyi taşımak için tıklayınız. Doğru yere geldiğinizde tekrar tıklayıp simgeyi bırakın.</translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Arkadaş Listesi</translation>
    </message>
</context>
<context>
    <name>DesktopDock</name>
    <message>
        <source>Move</source>
        <translation>Taşı</translation>
    </message>
</context>
</TS>