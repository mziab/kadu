<?xml version="1.0" ?><!DOCTYPE TS><TS language="pt_BR" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Encryption</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Public key has been sent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Error during sending public key</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Encryption error has occured</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>EncryptionNgNotification</name>
    <message>
        <source>Encryption</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Public key has been send to: %1 (%2)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Error sending public key to: %1 (%2)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Error occured during encryption</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>EncryptionNgPlugin</name>
    <message>
        <source>Encryption</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The QCA OSSL plugin for libqca2 is not present!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>EncryptionProviderManager</name>
    <message>
        <source>Buddy %1 is sending you his public key.
Do you want to save it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Encryption</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>EncryptionSetUpActionDescription</name>
    <message>
        <source>Encrypt</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>EncryptionSetUpMenu</name>
    <message>
        <source>No Encryption</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 Encryption</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GenerateKeysActionDescription</name>
    <message>
        <source>Generate Encryption Keys</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Encryption</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Keys have been generated</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Error generating keys</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot generate keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Keys already exist. Do you want to overwrite them?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Overwrite keys</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>