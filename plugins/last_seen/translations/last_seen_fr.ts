<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>Infos</name>
    <message>
        <source>&amp;Show infos about buddies</source>
        <translation>&amp;Afficher les infos à propos des contacts</translation>
    </message>
</context>
<context>
    <name>InfosDialog</name>
    <message>
        <source>Buddies Information</source>
        <translation>Informations sur les amis</translation>
    </message>
    <message>
        <source>Buddy</source>
        <translation>Ami</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation>Protocole</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Pseudo</translation>
    </message>
    <message>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <source>Domain name</source>
        <translation>Nom de domaine</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>State</source>
        <translation>État</translation>
    </message>
    <message>
        <source>Last time seen on</source>
        <translation>Dernière visite sur</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
</context>
</TS>