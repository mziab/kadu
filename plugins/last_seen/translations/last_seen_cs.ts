<?xml version="1.0" ?><!DOCTYPE TS><TS language="cs" version="2.0">
<context>
    <name>Infos</name>
    <message>
        <source>&amp;Show infos about buddies</source>
        <translation>&amp;Ukázat informace o kamarádech</translation>
    </message>
</context>
<context>
    <name>InfosDialog</name>
    <message>
        <source>Buddies Information</source>
        <translation>Informace o kamarádech</translation>
    </message>
    <message>
        <source>Buddy</source>
        <translation>Kamarád</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation>Protokol</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <source>IP</source>
        <translation>Adresa IP</translation>
    </message>
    <message>
        <source>Domain name</source>
        <translation>Název domény</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Popis</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Stav</translation>
    </message>
    <message>
        <source>Last time seen on</source>
        <translation>Naposledy viděn</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Zavřít</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
</context>
</TS>