<?xml version="1.0" ?><!DOCTYPE TS><TS language="pl" version="2.0">
<context>
    <name>Infos</name>
    <message>
        <source>&amp;Show infos about buddies</source>
        <translation>Informacje o znajomych</translation>
    </message>
</context>
<context>
    <name>InfosDialog</name>
    <message>
        <source>Buddies Information</source>
        <translation>Informacje o znajomych</translation>
    </message>
    <message>
        <source>Buddy</source>
        <translation>Znajomy</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation>Protokół</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Pseudonim</translation>
    </message>
    <message>
        <source>IP</source>
        <translation>Adres IP</translation>
    </message>
    <message>
        <source>Domain name</source>
        <translation>Nazwa domeny</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Stan</translation>
    </message>
    <message>
        <source>Last time seen on</source>
        <translation>Ostatnio widziany</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nazwa użytkownika</translation>
    </message>
</context>
</TS>