<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Notification</source>
        <translation>Uyarı</translation>
    </message>
    <message>
        <source>Tray Icon Balloon</source>
        <translation>Sistem Tepsisi Simge Balonu</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Zaman aşımı</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation><numerusform>%n saniye</numerusform><numerusform>%n saniye (ler)</numerusform></translation>
    </message>
    <message>
        <source>Notification icon</source>
        <translation>Uyarı simgesi</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Bilgi</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Uyarı</translation>
    </message>
    <message>
        <source>Critical</source>
        <translation>Kritik</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Başlık</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Sözdizimi</translation>
    </message>
    <message>
        <source>No Icon</source>
        <translation>Simge Yok</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Uyarılar</translation>
    </message>
</context>
<context>
    <name>Qt4NotifyConfigurationWidget</name>
    <message>
        <source>Configure</source>
        <translation>Ayar</translation>
    </message>
    <message>
        <source>Tray icon balloon&apos;s look configuration</source>
        <translation>Sistem tepsisi simge balonu görünüş ayarı</translation>
    </message>
    <message>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>
%&amp;t - başlık (örn. Yeni mesaj) %&amp;m - uyarı yazısı (örn. Ahmet&apos;ten mesaj), %&amp;d - detaylar (örn. mesaj alıntısı),
%&amp;i - uyarı simgesi</translation>
    </message>
</context>
</TS>