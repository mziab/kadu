<?xml version="1.0" ?><!DOCTYPE TS><TS language="de" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Cenzor</source>
        <translation>Zensor</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Enable cenzor</source>
        <translation>Zensor einschalten</translation>
    </message>
    <message>
        <source>Admonition</source>
        <translation>Ermahnung</translation>
    </message>
    <message>
        <source>Swearwords</source>
        <translation>Schimpfwörter</translation>
    </message>
    <message>
        <source>Exclusions</source>
        <translation>Ausnahmen</translation>
    </message>
    <message>
        <source>Message was cenzored</source>
        <translation>Nachricht wurde zensiert</translation>
    </message>
</context>
<context>
    <name>CenzorNotification</name>
    <message>
        <source>Message was cenzored</source>
        <translation>Nachricht wurde zensiert</translation>
    </message>
    <message>
        <source>Your interlocutor used obscene word and became admonished</source>
        <translation>Dein Gegenüber schimpft und wurde auf sein Fehlverhalten hingewiesen</translation>
    </message>
    <message>
        <source>Cenzor</source>
        <translation>Zensor</translation>
    </message>
</context>
<context>
    <name>ListEditWidget</name>
    <message>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
</context>
</TS>