<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Cenzor</source>
        <translation>Cenzor</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Enable cenzor</source>
        <translation>Enable cenzor</translation>
    </message>
    <message>
        <source>Admonition</source>
        <translation>Admonition</translation>
    </message>
    <message>
        <source>Swearwords</source>
        <translation>Swearwords</translation>
    </message>
    <message>
        <source>Exclusions</source>
        <translation>Exclusions</translation>
    </message>
    <message>
        <source>Message was cenzored</source>
        <translation>Message was cenzored</translation>
    </message>
</context>
<context>
    <name>CenzorNotification</name>
    <message>
        <source>Message was cenzored</source>
        <translation>Message was cenzored</translation>
    </message>
    <message>
        <source>Your interlocutor used obscene word and became admonished</source>
        <translation>Your interlocutor used obscene word and became admonished</translation>
    </message>
    <message>
        <source>Cenzor</source>
        <translation>Cenzor</translation>
    </message>
</context>
<context>
    <name>ListEditWidget</name>
    <message>
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Change</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
</context>
</TS>