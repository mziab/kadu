<?xml version="1.0" ?><!DOCTYPE TS><TS language="pl" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>External address</source>
        <translation>Adres zewnętrzny</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Rozłączony</translation>
    </message>
    <message>
        <source>XML Parsing Error</source>
        <translation>Błąd Parsowania XML</translation>
    </message>
    <message>
        <source>XMPP Protocol Error</source>
        <translation>Błąd Protokołu XMPP</translation>
    </message>
    <message>
        <source>Generic stream error</source>
        <translation>Błąd strumienia</translation>
    </message>
    <message>
        <source>Conflict(remote login replacing this one)</source>
        <translation>Konflikt (zdalne logowanie zastępuje bieżące)</translation>
    </message>
    <message>
        <source>Timed out from inactivity</source>
        <translation>Przekroczono limit czasu z powodu nieaktywności</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation>Błąd wewnętrzny serwera</translation>
    </message>
    <message>
        <source>Invalid XML</source>
        <translation>Nieprawidłowy XML</translation>
    </message>
    <message>
        <source>Policy violation</source>
        <translation>Naruszenie zasad</translation>
    </message>
    <message>
        <source>Server out of resources</source>
        <translation>Serwer wyczerpał zasoby</translation>
    </message>
    <message>
        <source>Server is shutting down</source>
        <translation>Serwer został wyłączony</translation>
    </message>
    <message>
        <source>XMPP Stream Error: %1</source>
        <translation>Błąd Strumienia XMPP: %1</translation>
    </message>
    <message>
        <source>Unable to connect to server</source>
        <translation>Nie można połączyć się z serwerem</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Nie odnaleziono hosta</translation>
    </message>
    <message>
        <source>Error connecting to proxy</source>
        <translation>Nie można połączyć się z serwerem proxy</translation>
    </message>
    <message>
        <source>Error during proxy negotiation</source>
        <translation>Błąd podczas negocjacji proxy</translation>
    </message>
    <message>
        <source>Proxy authentication failed</source>
        <translation>Uwierzytelnianie serwera proxy nie powiodło się</translation>
    </message>
    <message>
        <source>Socket/stream error</source>
        <translation>Błąd strumienia/portu</translation>
    </message>
    <message>
        <source>Connection Error: %1</source>
        <translation>Błąd Połączenia: %1</translation>
    </message>
    <message>
        <source>Host no longer hosted</source>
        <translation>Host nie jest już utrzymywany</translation>
    </message>
    <message>
        <source>Host unknown</source>
        <translation>Nieznany host</translation>
    </message>
    <message>
        <source>A required remote connection failed</source>
        <translation>Wymagane zdalne połączenie nie powiodło się</translation>
    </message>
    <message>
        <source>See other host: %1</source>
        <translation>Użyj innego hosta: %1</translation>
    </message>
    <message>
        <source>Server does not support proper XMPP version</source>
        <translation>Serwer nie wspiera wymaganej wersji XMPP</translation>
    </message>
    <message>
        <source>Stream Negotiation Error: %1</source>
        <translation>Błąd Negocjacji Strumienia: %1</translation>
    </message>
    <message>
        <source>Server rejected STARTTLS</source>
        <translation>Serwer odrzucił STARTTLS</translation>
    </message>
    <message>
        <source>TLS handshake error</source>
        <translation>Błąd uzgadniania TLS</translation>
    </message>
    <message>
        <source>Broken security layer (TLS)</source>
        <translation>Niesprawna warstwa zabezpieczeń (TLS)</translation>
    </message>
    <message>
        <source>Unable to login</source>
        <translation>Nie można się zalogować</translation>
    </message>
    <message>
        <source>No appropriate mechanism available for given security settings(e.g. SASL library too weak, or plaintext authentication not enabled)</source>
        <translation>Brak odpowiedniego mechanizmu dla wybranych ustawień zabezpieczeń (np. zbyt słaba biblioteka SASL lub niewłączona autoryzacja otwartym tekstem)</translation>
    </message>
    <message>
        <source>Bad server response</source>
        <translation>Błędna odpowiedź serwera</translation>
    </message>
    <message>
        <source>Server failed mutual authentication</source>
        <translation>Serwer nie przeszedł autentykacji</translation>
    </message>
    <message>
        <source>Encryption required for chosen SASL mechanism</source>
        <translation>Wybrany mechanizm SASL wymaga szyfrowania</translation>
    </message>
    <message>
        <source>Invalid account information</source>
        <translation>Nieprawidłowe informacje o koncie</translation>
    </message>
    <message>
        <source>Invalid SASL mechanism</source>
        <translation>Nieprawidłowy mechanizm SASL</translation>
    </message>
    <message>
        <source>Invalid realm</source>
        <translation>Nieprawidłowa domena</translation>
    </message>
    <message>
        <source>SASL mechanism too weak for this account</source>
        <translation>Mechanizm SASL jest za słaby dla tego konta</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation>Nieautoryzowany</translation>
    </message>
    <message>
        <source>Temporary auth failure</source>
        <translation>Chwilowy problem przy uwierzytelnianiu</translation>
    </message>
    <message>
        <source>Authentication error: %1</source>
        <translation>Błąd uwierzytelniania: %1</translation>
    </message>
    <message>
        <source>Broken security layer (SASL)</source>
        <translation>Niesprawna warstwa zabezpieczeń (SASL)</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Żaden</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Zaawansowane</translation>
    </message>
    <message>
        <source>XMPP/Jabber</source>
        <translation>XMPP/Jabber</translation>
    </message>
    <message>
        <source>Port for data transfers</source>
        <translation>Port transferu danych</translation>
    </message>
</context>
<context>
    <name>CertificateDisplayDialog</name>
    <message>
        <source>Certificate Information</source>
        <translation>Informacje o certyfikacie</translation>
    </message>
    <message>
        <source>Certificate Validation</source>
        <translation>Ważność certyfikatu</translation>
    </message>
    <message>
        <source>Valid From</source>
        <translation>Nieważny przed</translation>
    </message>
    <message>
        <source>Valid Until</source>
        <translation>Wygasa dnia</translation>
    </message>
    <message>
        <source>Serial Number</source>
        <translation>Numer seryjny</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Zamknij_Się</translation>
    </message>
    <message>
        <source>The certificate is valid.</source>
        <translation>Certyfikat jest ważny.</translation>
    </message>
    <message>
        <source>The certificate is NOT valid!</source>
        <translation>Certyfikat NIE jest ważny!</translation>
    </message>
    <message>
        <source>Reason: %1.</source>
        <translation>Powód: %1.</translation>
    </message>
    <message>
        <source>Subject Details:</source>
        <translation>Wystawiony dla:</translation>
    </message>
    <message>
        <source>Issuer Details:</source>
        <translation>Wystawca:</translation>
    </message>
    <message>
        <source>Organization:</source>
        <translation>Organizacja:</translation>
    </message>
    <message>
        <source>Organizational unit:</source>
        <translation>Jednostka organizacyjna:</translation>
    </message>
    <message>
        <source>Locality:</source>
        <translation>Miejscowość:</translation>
    </message>
    <message>
        <source>State:</source>
        <translation>Stan:</translation>
    </message>
    <message>
        <source>Country:</source>
        <translation>Państwo:</translation>
    </message>
    <message>
        <source>Common name:</source>
        <translation>Nazwa serwera:</translation>
    </message>
    <message>
        <source>Domain name:</source>
        <translation>Nazwa domeny:</translation>
    </message>
    <message>
        <source>XMPP name:</source>
        <translation>Nazwa XMPP:</translation>
    </message>
    <message>
        <source>Email:</source>
        <translation>Adres E-mail:</translation>
    </message>
</context>
<context>
    <name>CertificateErrorWindow</name>
    <message>
        <source>Cannot establish secure connection with server &lt;i&gt;%1&lt;/i&gt;.</source>
        <translation>Nie można ustanowić bezpiecznego połączenia z serwerem &lt;i&gt;%1&lt;/i&gt;.</translation>
    </message>
    <message>
        <source>Show certificate...</source>
        <translation>Pokaż certyfikat...</translation>
    </message>
    <message>
        <source>If you do not trust &lt;i&gt;%1&lt;/i&gt;, cancel the connection.</source>
        <translation>Jeśli nie ufasz &lt;i&gt;%1&lt;/i&gt;, zakończ połączenie.</translation>
    </message>
    <message>
        <source>Remember my choice for this certificate</source>
        <translation>Pamiętaj mój wybór dla tego certyfikatu</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Połącz</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>FacebookProtocolFactory</name>
    <message>
        <source>Facebook ID:</source>
        <translation>ID konta Facebook:</translation>
    </message>
    <message>
        <source>Your username is available at &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; under Username field. If this field is empty, you can choose your Username and enter it there.</source>
        <translation>Nazwa użytkownika jest dostępna po adresem &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; w polu Nazwa użytkownika.  Jeśli to pole jest puste, możesz wprowadzić tam wybraną nazwę.</translation>
    </message>
</context>
<context>
    <name>GTalkProtocolFactory</name>
    <message>
        <source>Gmail/Google Talk ID:</source>
        <translation>ID konta Gmail/Google Talk</translation>
    </message>
</context>
<context>
    <name>HttpConnect</name>
    <message>
        <source>Authentication failed</source>
        <translation>Uwierzytelnianie nie powiodło się</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Nie znaleziono serwera</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Odmowa dostępu</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Połączenie odrzucone</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Nieprawidłowa odpowiedź</translation>
    </message>
</context>
<context>
    <name>HttpProxyGetStream</name>
    <message>
        <source>Authentication failed</source>
        <translation>Uwierzytelnianie nie powiodło się</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Nie znaleziono hosta</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Brak dostępu</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Połączenie odrzucone</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Nieprawidłowa odpowiedź</translation>
    </message>
</context>
<context>
    <name>HttpProxyPost</name>
    <message>
        <source>Authentication failed</source>
        <translation>Uwierzytelnianie nie powiodło się</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Nie znaleziono hosta</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Brak dostępu</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Połączenie odrzucone</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Nieprawidłowa odpowiedź</translation>
    </message>
</context>
<context>
    <name>JabberActions</name>
    <message>
        <source>Resend Subscription</source>
        <translation>Ponów subskrypcję</translation>
    </message>
    <message>
        <source>Remove Subscription</source>
        <translation>Anuluj subskrypcję</translation>
    </message>
    <message>
        <source>Ask for Subscription</source>
        <translation>Poproś o subskrypcję</translation>
    </message>
</context>
<context>
    <name>JabberAddAccountWidget</name>
    <message>
        <source>&lt;a href=&apos;#&apos;&gt;What is my username?&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;#&apos;&gt;Jaka jest moja nazwa użytkownika?&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nazwa użytkownika</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <source>Remember Password</source>
        <translation>Zapamiętaj hasło</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Tożsamość konta</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wybierz lub wprowadź tożsamość z która będzie skojarzone konto.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Dodaj konto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>JabberChangePasswordWindow</name>
    <message>
        <source>Change Password</source>
        <translation>Zmień hasło</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation>Stare hasło</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Podaj stare hasło dla twojego konta XMPP/Jabbera.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>New password</source>
        <translation>Nowe hasło</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Podaj nowe hasło dla twojego konta XMPP/Jabbera.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Retype new password</source>
        <translation>Ponów nowe hasło</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Changing password was successful.</source>
        <translation>Hasło zostało pomyślnie zmienione.</translation>
    </message>
    <message>
        <source>An error has occurred. Please try again later.</source>
        <translation>Wystąpił błąd. Spróbuj ponownie później.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;Password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>Wymagane pola wypełniono nieprawidłowo.

Hasło w obu polach (&quot;Hasło&quot; oraz &quot;Powtórz hasło&quot;) musi być identyczne!</translation>
    </message>
</context>
<context>
    <name>JabberContactPersonalInfoWidget</name>
    <message>
        <source>Full Name</source>
        <translation>Imię i nazwisko</translation>
    </message>
    <message>
        <source>Family Name</source>
        <translation>Nazwisko rodowe</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Pseudonim</translation>
    </message>
    <message>
        <source>Birthdate</source>
        <translation>Data urodzenia</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Miasto</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Strona WWW</translation>
    </message>
</context>
<context>
    <name>JabberCreateAccountWidget</name>
    <message>
        <source>Username</source>
        <translation>Nazwa użytkownika</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <source>Retype Password</source>
        <translation>Powtórz hasło</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Zapamiętaj hasło</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Tożsamość konta</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wybierz lub wprowadź tożsamość z która będzie skojarzone konto.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>More options:</source>
        <translation>Więcej opcji:</translation>
    </message>
    <message>
        <source>Connection settings</source>
        <translation>Ustawienia połączenia</translation>
    </message>
    <message>
        <source>Manually Specify Server Host/Port</source>
        <translation>Własna konfiguracja</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Encrypt connection</source>
        <translation>Szyfruj połączenie</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Zawsze</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>Jeśli możliwe</translation>
    </message>
    <message>
        <source>Legacy SSL</source>
        <translation>Legacy SSL</translation>
    </message>
    <message>
        <source>Probe legacy SSL port</source>
        <translation>Port dla Legacy SSL</translation>
    </message>
    <message>
        <source>Register Account</source>
        <translation>Zarejestruj konto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Nie można ustanowić bezpiecznego połączenia. Brak wtyczki SSL/TLS.</translation>
    </message>
    <message>
        <source>Legacy secure connection (SSL) is only available in combination with manual host/port.</source>
        <translation>Bezpieczne połączenie (SSL) jest dostępne tylko przy ręcznym ustawieniu hosta/portu.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;New password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>Wymagane pola wypełniono nieprawidłowo.

Hasło w obu polach (&quot;Nowe Hasło&quot; oraz &quot;Powtórz hasło&quot;) musi być identyczne!</translation>
    </message>
</context>
<context>
    <name>JabberEditAccountWidget</name>
    <message>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Usuń konto</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nazwa użytkownika</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Zapamiętaj hasło</translation>
    </message>
    <message>
        <source>Change your password</source>
        <translation>Zmień hasło</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Tożsamość konta</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wybierz lub wprowadź tożsamość z która będzie skojarzone konto.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Personal Information</source>
        <translation>Informacje osobiste</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Połączenie</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>Nigdy</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Zawsze</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>Jeśli możliwe</translation>
    </message>
    <message>
        <source>Allow plaintext authentication</source>
        <translation>Zezwól na nieszyfrowaną autentykację</translation>
    </message>
    <message>
        <source>Over encrypted connection</source>
        <translation>Przy szyfrowanym połączeniu</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <source>Resource</source>
        <translation>Zasób</translation>
    </message>
    <message>
        <source>Priority</source>
        <translation>Priorytet</translation>
    </message>
    <message>
        <source>Data transfer proxy</source>
        <translation>Serwer proxy dla transmisji danych</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Nie można ustanowić bezpiecznego połączenia. Brak wtyczki SSL/TLS.</translation>
    </message>
    <message>
        <source>Legacy SSL is only available in combination with manual host/port.</source>
        <translation>Legacy SSL jest dostępny tylko w połączeniu z ręczną konfiguracją hosta/portu.</translation>
    </message>
    <message>
        <source>Remove account</source>
        <translation>Usuń konto</translation>
    </message>
    <message>
        <source>Proxy configuration</source>
        <translation>Konfiguracja proxy</translation>
    </message>
    <message>
        <source>Publish system information</source>
        <translation>Udostępniaj informacje systemowe</translation>
    </message>
    <message>
        <source>XMPP Server</source>
        <translation>Serwer XMPP</translation>
    </message>
    <message>
        <source>Use custom server address/port</source>
        <translation>Użyj własnego adresu i portu</translation>
    </message>
    <message>
        <source>Server address</source>
        <translation>Adres serwera</translation>
    </message>
    <message>
        <source>Use encrypted connection</source>
        <translation>Używaj szyfrowanego połączenia</translation>
    </message>
    <message>
        <source>Only in older version</source>
        <translation>Tylko w starszej wersj</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Sieć</translation>
    </message>
    <message>
        <source>Use computer name as a resource</source>
        <translation>Użyj nazwy komputera jako zasobu</translation>
    </message>
    <message>
        <source>Enable composing events</source>
        <translation>Włącz powiadomienia o pisaniu</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</source>
        <translation>Twój rozmówca będzie informowany o tym, że piszesz wiadomość. Działa także w drugą stronę.</translation>
    </message>
    <message>
        <source>Enable chat activity events</source>
        <translation>Włącz powiadomienia o aktywności rozmowy</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you suspend or end conversation. And vice versa.</source>
        <translation>Twój rozmówca będzie informowany o tym, że wstrzymałęs pisanie lub zakończyłeś rozmowę. Działa także w drugą stronę.</translation>
    </message>
    <message>
        <source>Others can see your system name/version</source>
        <translation>Znajomi będą widzieć nazwę i wersję twojego systemu operacyjnego</translation>
    </message>
    <message>
        <source>Confrim Account Removal</source>
        <translation>Potwierdź usunięcie konta</translation>
    </message>
    <message>
        <source>Are you sure do you want to remove account %1 (%2)?</source>
        <translation>Czy na pewno chcesz usunąć konto %1 (%2)?</translation>
    </message>
</context>
<context>
    <name>JabberPersonalInfoWidget</name>
    <message>
        <source>Full name</source>
        <translation>Imię i nazwisko</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Pseudonim</translation>
    </message>
    <message>
        <source>Family name</source>
        <translation>Nazwisko rodowe</translation>
    </message>
    <message>
        <source>Birth year</source>
        <translation>Rok urodzenia</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Miejscowość</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Strona WWW</translation>
    </message>
</context>
<context>
    <name>JabberProtocolFactory</name>
    <message>
        <source>User JID:</source>
        <translation>JID Użytkownika:</translation>
    </message>
</context>
<context>
    <name>JabberServerRegisterAccount</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>This server does not support registration</source>
        <translation>Ten serwer nie wspiera rejestracji</translation>
    </message>
    <message>
        <source>There was an error registering the account.
Reason: %1</source>
        <translation>Wystąpił błąd podczas rejestracji konta.
Powód: %1</translation>
    </message>
</context>
<context>
    <name>JabberWaitForAccountRegisterWindow</name>
    <message>
        <source>Plase wait. New XMPP account is being registered.</source>
        <translation>Proszę czekać.  Trwa rejestracja nowego konta XMPP.</translation>
    </message>
    <message>
        <source>Registration was successful. Your new XMPP username is %1.
Store it in a safe place along with the password.
Now please add your friends to the buddy list.</source>
        <translation>Rejestracja przebiegła pomyślnie. Twoja nowa nazwa użytownika XMPP to %1.
Zachowaj ją wraz z hasłem w bezpiecznym miejscu.
Teraz możesz dodać przyjaciół do listy kontaktów.</translation>
    </message>
    <message>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation>Wystąpił błąd w trakcie rejestracji. Spróbuj ponownie później.</translation>
    </message>
    <message>
        <source>Registering new XMPP account</source>
        <translation>Rejestrowanie nowego konta XMPP</translation>
    </message>
</context>
<context>
    <name>MiniClient</name>
    <message>
        <source>Server Authentication</source>
        <translation>Autentykacja serwera</translation>
    </message>
    <message>
        <source>Server Error</source>
        <translation>Błąd serwera</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>Serwer nie wspiera szyfrowania TLS.</translation>
    </message>
    <message>
        <source>There was an error communicating with the Jabber server.
Details: %1</source>
        <translation>Wystąpił błąd podczas komunikacji z serwerem Jabbera.
Szczegóły: %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>The server did not present a certificate.</source>
        <translation>Serwer nie przedstawił certyfikatu.</translation>
    </message>
    <message>
        <source>Certificate is valid.</source>
        <translation>Certyfikat jest ważny.</translation>
    </message>
    <message>
        <source>The hostname does not match the one the certificate was issued to.</source>
        <translation>Adres hosta nie zgadza się z adresem na który wystawiony jest certyfikat.</translation>
    </message>
    <message>
        <source>General certificate validation error.</source>
        <translation>Błąd walidacji certyfikatu.</translation>
    </message>
    <message>
        <source>No certificate presented.</source>
        <translation>Nie otrzymano certyfikatu.</translation>
    </message>
    <message>
        <source>Hostname mismatch.</source>
        <translation>Niezgodność nazw hostów.</translation>
    </message>
    <message>
        <source>Invalid Certificate.</source>
        <translation>Nieprawidłowy Certyfikat.</translation>
    </message>
    <message>
        <source>General validation error.</source>
        <translation>Błąd walidacji.</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nazwa użytkownika</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Pseudonim</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>First Name</source>
        <translation>Imi</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Miasto</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Województwo</translation>
    </message>
    <message>
        <source>Zipcode</source>
        <translation>Kod pocztowy</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>Adres WWW</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <source>Misc</source>
        <translation>Różne</translation>
    </message>
</context>
<context>
    <name>ShowXmlConsoleActionDescription</name>
    <message>
        <source>Show XML Console</source>
        <translation>Otwórz konsolę XML</translation>
    </message>
</context>
<context>
    <name>TestDlg</name>
    <message>
        <source>XMPP Test</source>
        <translation>XMPP Test</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>&amp;Połącz</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>O %1</translation>
    </message>
    <message>
        <source>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Currently supports:
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</source>
        <translation>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Currently supports:
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <source>Please enter the Full JID to connect with.</source>
        <translation>Podaj pełny JabberID, z którym chcesz się połączyć.</translation>
    </message>
    <message>
        <source>You must specify a host:port for the proxy.</source>
        <translation>Musisz podać adres hosta oraz port dla proxy.</translation>
    </message>
    <message>
        <source>You must at least enter a URL to use http poll.</source>
        <translation>You must at least enter a URL to use http poll.</translation>
    </message>
    <message>
        <source>Please enter the proxy host in the form &apos;host:port&apos;.</source>
        <translation>Please enter the proxy host in the form &apos;host:port&apos;.</translation>
    </message>
    <message>
        <source>Please enter the host in the form &apos;host:port&apos;.</source>
        <translation>Please enter the host in the form &apos;host:port&apos;.</translation>
    </message>
    <message>
        <source>Error: SSF Min is greater than SSF Max.</source>
        <translation>Error: SSF Min is greater than SSF Max.</translation>
    </message>
    <message>
        <source>Error: TLS not available.  Disable any TLS options.</source>
        <translation>Error: TLS not available.  Disable any TLS options.</translation>
    </message>
    <message>
        <source>&amp;Disconnect</source>
        <translation>&amp;Rozłączenie</translation>
    </message>
    <message>
        <source>Bad XML input (%1,%2): %3
Please correct and try again.</source>
        <translation>Nieprawidłowy XML (%1,%2): %3
Proszę poprawić i spróbować ponownie.</translation>
    </message>
    <message>
        <source>Bad Stanza &apos;%1&apos;.  Must be &apos;message&apos;, &apos;presence&apos;, or &apos;iq&apos;</source>
        <translation>Bad Stanza &apos;%1&apos;.  Must be &apos;message&apos;, &apos;presence&apos;, or &apos;iq&apos;</translation>
    </message>
    <message>
        <source>You must enter at least one stanza!</source>
        <translation>You must enter at least one stanza!</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <source>Enter the password for %1</source>
        <translation>Podaj hasło dla %1</translation>
    </message>
</context>
<context>
    <name>XMPP::ClientStream</name>
    <message>
        <source>Offered mechanisms: </source>
        <translation>Dostępne mechanizmy:</translation>
    </message>
</context>
<context>
    <name>XMPP::Features::FeatureName</name>
    <message>
        <source>ERROR: Incorrect usage of Features class</source>
        <translation>ERROR: Incorrect usage of Features class</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Żaden</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>Rejestruj</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Szukaj</translation>
    </message>
    <message>
        <source>Groupchat</source>
        <translation>Konferencja</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation>Brama</translation>
    </message>
    <message>
        <source>Service Discovery</source>
        <translation>Przeglądanie usług</translation>
    </message>
    <message>
        <source>VCard</source>
        <translation>VCard</translation>
    </message>
    <message>
        <source>Execute command</source>
        <translation>Wykonaj polecenie</translation>
    </message>
    <message>
        <source>Add to roster</source>
        <translation>Dodaj do rostera</translation>
    </message>
</context>
<context>
    <name>XMPP::JT_VCard</name>
    <message>
        <source>No VCard available</source>
        <translation>Wizytówka VCard niedostępna</translation>
    </message>
</context>
<context>
    <name>XMPP::JabberConnectionService</name>
    <message>
        <source>security problem</source>
        <translation>Problem bezpieczeństwa</translation>
    </message>
    <message>
        <source>TLS certificate not accepted</source>
        <translation>Certyfikat TLS nie został zaakceptowany</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>Nieznany błąd</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>Serwer nie wspiera szyfrowania TLS.</translation>
    </message>
    <message>
        <source>Connection error.
Details: %1</source>
        <translation>Błąd połączenia.
Szczegóły: %1</translation>
    </message>
    <message>
        <source>Account disconnected.
Details: %1</source>
        <translation>Konto rozłączone.
Szczegóły: %1</translation>
    </message>
    <message>
        <source>SSL support could not be initialized for account %1. This is most likely because the QCA TLS plugin is not installed on your system.</source>
        <translation>Nie można zainicjalizować obsługi SSL dla konta %1. Sprawdź, czy wtyczka QCA TLS jest zainstalowana w Twoim systemie.</translation>
    </message>
</context>
<context>
    <name>XMPP::Stanza::Error::Private</name>
    <message>
        <source>Bad request</source>
        <translation>Błędne żądanie</translation>
    </message>
    <message>
        <source>The sender has sent XML that is malformed or that cannot be processed.</source>
        <translation>Otrzymany XML jest nieprawidłowy.</translation>
    </message>
    <message>
        <source>Conflict</source>
        <translation>Konflikt</translation>
    </message>
    <message>
        <source>Access cannot be granted because an existing resource or session exists with the same name or address.</source>
        <translation>Nie można uzyskać dostępu, ponieważ istnieje już zasób lub sesja o tej samej nazwie lub adresie.</translation>
    </message>
    <message>
        <source>Feature not implemented</source>
        <translation>Funkcjonalność niezaimplementowana</translation>
    </message>
    <message>
        <source>The feature requested is not implemented by the recipient or server and therefore cannot be processed.</source>
        <translation>Funkcjonalność niewspierana przez serwer lub odbiorcę.</translation>
    </message>
    <message>
        <source>Forbidden</source>
        <translation>Brak dostępu</translation>
    </message>
    <message>
        <source>The requesting entity does not possess the required permissions to perform the action.</source>
        <translation>Zgłaszający żadanie nie posiada uprawnień do jego wykonania.</translation>
    </message>
    <message>
        <source>Gone</source>
        <translation>Nieobecny</translation>
    </message>
    <message>
        <source>The recipient or server can no longer be contacted at this address.</source>
        <translation>Odbiorca lub serwer nie jest już dostępny pod tym adresem.</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation>Błąd wewnętrzny serwera</translation>
    </message>
    <message>
        <source>The server could not process the stanza because of a misconfiguration or an otherwise-undefined internal server error.</source>
        <translation>Niewyjaśniony błąd po stronie serwera.</translation>
    </message>
    <message>
        <source>Item not found</source>
        <translation>Nie odnaleziono obiektu</translation>
    </message>
    <message>
        <source>The addressed JID or item requested cannot be found.</source>
        <translation>Wskazany użytkownik nie został odnaleziony.</translation>
    </message>
    <message>
        <source>JID malformed</source>
        <translation>Nieprawidłowy JID</translation>
    </message>
    <message>
        <source>The sending entity has provided or communicated an XMPP address (e.g., a value of the &apos;to&apos; attribute) or aspect thereof (e.g., a resource identifier) that does not adhere to the syntax defined in Addressing Scheme.</source>
        <translation>Nieznany błąd</translation>
    </message>
    <message>
        <source>Not acceptable</source>
        <translation>Nieakceptowalny</translation>
    </message>
    <message>
        <source>The recipient or server understands the request but is refusing to process it because it does not meet criteria defined by the recipient or server (e.g., a local policy regarding acceptable words in messages).</source>
        <translation>Odbiorca odrzucił żądanie.</translation>
    </message>
    <message>
        <source>Not allowed</source>
        <translation>Niedozwolony</translation>
    </message>
    <message>
        <source>The recipient or server does not allow any entity to perform the action.</source>
        <translation>Odbiorca lub serwer nie pozwala jednostce na wykonanie akcji.</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation>Nieautoryzowany</translation>
    </message>
    <message>
        <source>The sender must provide proper credentials before being allowed to perform the action, or has provided improper credentials.</source>
        <translation>Nadawca musi zapewnić odpowiednie uprawnienia przed dopuszczeniem do wykonywania czynności, lub dostarczył niewłaściwych poświadczeń.</translation>
    </message>
    <message>
        <source>Recipient unavailable</source>
        <translation>Odbiorca nieosiągalny</translation>
    </message>
    <message>
        <source>The intended recipient is temporarily unavailable.</source>
        <translation>Odbiorca jest czasowo niedostępny.</translation>
    </message>
    <message>
        <source>Redirect</source>
        <translation>Przekierowanie</translation>
    </message>
    <message>
        <source>The recipient or server is redirecting requests for this information to another entity, usually temporarily.</source>
        <translation>Odbiorca lub serwer przekierowuje żądanie informacji innej usłudze, zazwyczaj tymczasowej.</translation>
    </message>
    <message>
        <source>Registration required</source>
        <translation>Wymagana rejestracja</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because registration is required.</source>
        <translation>Żądanie do usługi nie może być ukończone, ponieważ wymagana jest rejestracja.</translation>
    </message>
    <message>
        <source>Remote server not found</source>
        <translation>Nie odnaleziono serwera</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient does not exist.</source>
        <translation>Zdalny serwer lub usługa określona jako część lub całość JID nie istnieje.</translation>
    </message>
    <message>
        <source>Remote server timeout</source>
        <translation>Limit czasu serwera zdalnego przekroczony</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient (or required to fulfill a request) could not be contacted within a reasonable amount of time.</source>
        <translation>Zdalny serwer lub usługa określona jako część lub całość JID nie mógł nawiązać kontaktu, w danym czasie.</translation>
    </message>
    <message>
        <source>Resource constraint</source>
        <translation>Ograniczenie zasobów</translation>
    </message>
    <message>
        <source>The server or recipient lacks the system resources necessary to service the request.</source>
        <translation>Serwer lub odbiorca posiada za mało zasobów systemowych niezbędnych do obsługi żądania.</translation>
    </message>
    <message>
        <source>Service unavailable</source>
        <translation>Usługa niedostępna</translation>
    </message>
    <message>
        <source>The server or recipient does not currently provide the requested service.</source>
        <translation>Serwer lub odbiorca obecnie nie zapewnia żądanej usługi.</translation>
    </message>
    <message>
        <source>Subscription required</source>
        <translation>Wymagana subskrypcja</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because a subscription is required.</source>
        <translation>Żądana jednostka nie jest upoważniona do dostępu do wybranej usługi, ponieważ wymagane jest zalogowanie się do usługi.</translation>
    </message>
    <message>
        <source>Undefined condition</source>
        <translation>Nieokreślony warunek</translation>
    </message>
    <message>
        <source>The error condition is not one of those defined by the other conditions in this list.</source>
        <translation>Kod błędu nie znajduje się na liście kodów błędu programu.</translation>
    </message>
    <message>
        <source>Unexpected request</source>
        <translation>Niespodziewane żądanie</translation>
    </message>
    <message>
        <source>The recipient or server understood the request but was not expecting it at this time (e.g., the request was out of order).</source>
        <translation>Odbiorca lub serwer przyjął żądanie, ale nie mógł obsłużyć go w tej chwili (np. żądanie dotarło poza właściwą kolejnością).</translation>
    </message>
</context>
<context>
    <name>XMPP::Task</name>
    <message>
        <source>Disconnected</source>
        <translation>Rozłączony</translation>
    </message>
</context>
</TS>