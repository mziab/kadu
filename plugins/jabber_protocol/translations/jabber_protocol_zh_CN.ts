<?xml version="1.0" ?><!DOCTYPE TS><TS language="zh_CN" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>External address</source>
        <translation>外部地址</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>XML Parsing Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>XMPP Protocol Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Generic stream error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Conflict(remote login replacing this one)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Timed out from inactivity</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Internal server error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid XML</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Policy violation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server out of resources</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server is shutting down</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>XMPP Stream Error: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unable to connect to server</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Host not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Error connecting to proxy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Error during proxy negotiation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Proxy authentication failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Socket/stream error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Connection Error: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Host no longer hosted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Host unknown</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>A required remote connection failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>See other host: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server does not support proper XMPP version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Stream Negotiation Error: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server rejected STARTTLS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>TLS handshake error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Broken security layer (TLS)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unable to login</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No appropriate mechanism available for given security settings(e.g. SASL library too weak, or plaintext authentication not enabled)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Bad server response</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server failed mutual authentication</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Encryption required for chosen SASL mechanism</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid account information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid SASL mechanism</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid realm</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>SASL mechanism too weak for this account</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Not authorized</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Temporary auth failure</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Authentication error: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Broken security layer (SASL)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>None</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>XMPP/Jabber</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Port for data transfers</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CertificateDisplayDialog</name>
    <message>
        <source>Certificate Information</source>
        <translation>证书信息</translation>
    </message>
    <message>
        <source>Certificate Validation</source>
        <translation>证书验证</translation>
    </message>
    <message>
        <source>Valid From</source>
        <translation>之后有效</translation>
    </message>
    <message>
        <source>Valid Until</source>
        <translation>之前有效</translation>
    </message>
    <message>
        <source>Serial Number</source>
        <translation>序列号</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>The certificate is valid.</source>
        <translation>证书有效.</translation>
    </message>
    <message>
        <source>The certificate is NOT valid!</source>
        <translation>无效证书.</translation>
    </message>
    <message>
        <source>Reason: %1.</source>
        <translation>原因: %1.</translation>
    </message>
    <message>
        <source>Subject Details:</source>
        <translation>主题详情:</translation>
    </message>
    <message>
        <source>Issuer Details:</source>
        <translation>颁发者详情:</translation>
    </message>
    <message>
        <source>Organization:</source>
        <translation>组织:</translation>
    </message>
    <message>
        <source>Organizational unit:</source>
        <translation>组织单位:</translation>
    </message>
    <message>
        <source>Locality:</source>
        <translation>地区:</translation>
    </message>
    <message>
        <source>State:</source>
        <translation>省:</translation>
    </message>
    <message>
        <source>Country:</source>
        <translation>国家:</translation>
    </message>
    <message>
        <source>Common name:</source>
        <translation>公共名称:</translation>
    </message>
    <message>
        <source>Domain name:</source>
        <translation>域名:</translation>
    </message>
    <message>
        <source>XMPP name:</source>
        <translation>XMPP 名称:</translation>
    </message>
    <message>
        <source>Email:</source>
        <translation>电子邮件:</translation>
    </message>
</context>
<context>
    <name>CertificateErrorWindow</name>
    <message>
        <source>Cannot establish secure connection with server &lt;i&gt;%1&lt;/i&gt;.</source>
        <translation>不能与服务器 &lt;i&gt;%1&lt;/i&gt; 建立安全连接.</translation>
    </message>
    <message>
        <source>Show certificate...</source>
        <translation>显示证书...</translation>
    </message>
    <message>
        <source>If you do not trust &lt;i&gt;%1&lt;/i&gt;, cancel the connection.</source>
        <translation>如果你不信任 &lt;i&gt;%1&lt;/i&gt;,  取消本连接</translation>
    </message>
    <message>
        <source>Remember my choice for this certificate</source>
        <translation>记住我对此证书的选择</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>FacebookProtocolFactory</name>
    <message>
        <source>Facebook ID:</source>
        <translation>Facebook ID:</translation>
    </message>
    <message>
        <source>Your username is available at &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; under Username field. If this field is empty, you can choose your Username and enter it there.</source>
        <translation>Your username is available at &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; under Username field. If this field is empty, you can choose your Username and enter it there.</translation>
    </message>
</context>
<context>
    <name>GTalkProtocolFactory</name>
    <message>
        <source>Gmail/Google Talk ID:</source>
        <translation>Gmail/GTalk ID:</translation>
    </message>
</context>
<context>
    <name>HttpConnect</name>
    <message>
        <source>Authentication failed</source>
        <translation>认证失败</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>找不到主机</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>拒绝访问</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>连接被拒绝</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>无效回复</translation>
    </message>
</context>
<context>
    <name>HttpProxyGetStream</name>
    <message>
        <source>Authentication failed</source>
        <translation>认证失败</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>找不到主机</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>拒绝访问</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>连接被拒绝</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>无效回复</translation>
    </message>
</context>
<context>
    <name>HttpProxyPost</name>
    <message>
        <source>Authentication failed</source>
        <translation>认证失败</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>找不到主机</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>拒绝访问</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>连接被拒绝</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>无效回复</translation>
    </message>
</context>
<context>
    <name>JabberActions</name>
    <message>
        <source>Resend Subscription</source>
        <translation>重新发送订阅请求</translation>
    </message>
    <message>
        <source>Remove Subscription</source>
        <translation>移除订阅</translation>
    </message>
    <message>
        <source>Ask for Subscription</source>
        <translation>订阅请求</translation>
    </message>
</context>
<context>
    <name>JabberAddAccountWidget</name>
    <message>
        <source>&lt;a href=&apos;#&apos;&gt;What is my username?&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;#&apos;&gt;我的用户名是什么?&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <source>Remember Password</source>
        <translation>记住密码</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>帐号标识</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;选择或输入此帐号的标识.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>添加帐号</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>JabberChangePasswordWindow</name>
    <message>
        <source>Change Password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation>旧密码</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>
&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;输入你的 XMPP/Jabber 帐号的当前密码.&lt;/i&gt;&lt;/font&gt; </translation>
    </message>
    <message>
        <source>New password</source>
        <translation>新密码</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;输入你的 XMPP/Jabber 帐号新密码.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Retype new password</source>
        <translation>重复</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Changing password was successful.</source>
        <translation>密码修改成功.</translation>
    </message>
    <message>
        <source>An error has occurred. Please try again later.</source>
        <translation>出现错误. 请稍后重试.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;Password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>必填项输入了无效数据.

两项(密码和重复密码)中输入的内容必需完全一致!</translation>
    </message>
</context>
<context>
    <name>JabberContactPersonalInfoWidget</name>
    <message>
        <source>Full Name</source>
        <translation>全名</translation>
    </message>
    <message>
        <source>Family Name</source>
        <translation>姓</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>昵称</translation>
    </message>
    <message>
        <source>Birthdate</source>
        <translation>生日</translation>
    </message>
    <message>
        <source>City</source>
        <translation>城市</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>邮件</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>网站</translation>
    </message>
</context>
<context>
    <name>JabberCreateAccountWidget</name>
    <message>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <source>Retype Password</source>
        <translation>重复密码</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>记住密码</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>帐号标识</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;选择或输入准备分配给此帐号的标识.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>More options:</source>
        <translation>更多选项:</translation>
    </message>
    <message>
        <source>Connection settings</source>
        <translation>连接设置</translation>
    </message>
    <message>
        <source>Manually Specify Server Host/Port</source>
        <translation>手工指定服务器和端口</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>主机</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <source>Encrypt connection</source>
        <translation>加密连接</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>总是</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>可用时</translation>
    </message>
    <message>
        <source>Legacy SSL</source>
        <translation>传统 SSL</translation>
    </message>
    <message>
        <source>Probe legacy SSL port</source>
        <translation>检测传统SSL工作端口</translation>
    </message>
    <message>
        <source>Register Account</source>
        <translation>注册帐号</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>不能启用安全连接. SSL/TLS 插件未找到.</translation>
    </message>
    <message>
        <source>Legacy secure connection (SSL) is only available in combination with manual host/port.</source>
        <translation>传统安全连接(SSL)仅在手工指定服务器和端口的情况下可用</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;New password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>必填项输入了无效数据.

两项(密码和重复密码)中输入的内容必需完全一致!</translation>
    </message>
</context>
<context>
    <name>JabberEditAccountWidget</name>
    <message>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>删除帐号</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>记住密码</translation>
    </message>
    <message>
        <source>Change your password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>帐号标识</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;选择或输入将要分配给此帐号的标识.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Personal Information</source>
        <translation>个人信息</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>从不</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>总是</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>可用时</translation>
    </message>
    <message>
        <source>Allow plaintext authentication</source>
        <translation>总是明文认证</translation>
    </message>
    <message>
        <source>Over encrypted connection</source>
        <translation>基于加密的连接</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <source>Resource</source>
        <translation>资源</translation>
    </message>
    <message>
        <source>Priority</source>
        <translation>优先级</translation>
    </message>
    <message>
        <source>Data transfer proxy</source>
        <translation>数据传输代理</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>加密连接不可用. SSL/TLS 插件未找到.</translation>
    </message>
    <message>
        <source>Legacy SSL is only available in combination with manual host/port.</source>
        <translation>传统 SSL 仅在手工指定主机/端口时可用.</translation>
    </message>
    <message>
        <source>Remove account</source>
        <translation>删除帐号</translation>
    </message>
    <message>
        <source>Proxy configuration</source>
        <translation>代理服务器配置</translation>
    </message>
    <message>
        <source>Publish system information</source>
        <translation>公共系统信息</translation>
    </message>
    <message>
        <source>XMPP Server</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use custom server address/port</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use encrypted connection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Only in older version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Network</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use computer name as a resource</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enable composing events</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enable chat activity events</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Your interlocutor will be notified when you suspend or end conversation. And vice versa.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Others can see your system name/version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Confrim Account Removal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Are you sure do you want to remove account %1 (%2)?</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>JabberPersonalInfoWidget</name>
    <message>
        <source>Full name</source>
        <translation>全名</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>显示名</translation>
    </message>
    <message>
        <source>Family name</source>
        <translation>姓</translation>
    </message>
    <message>
        <source>Birth year</source>
        <translation>生日</translation>
    </message>
    <message>
        <source>City</source>
        <translation>城市</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>邮件</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>网站</translation>
    </message>
</context>
<context>
    <name>JabberProtocolFactory</name>
    <message>
        <source>User JID:</source>
        <translation>用户 JID:</translation>
    </message>
</context>
<context>
    <name>JabberServerRegisterAccount</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>This server does not support registration</source>
        <translation>服务器不支持用户注册</translation>
    </message>
    <message>
        <source>There was an error registering the account.
Reason: %1</source>
        <translation>注册帐号出现错误.
原因: %1</translation>
    </message>
</context>
<context>
    <name>JabberWaitForAccountRegisterWindow</name>
    <message>
        <source>Plase wait. New XMPP account is being registered.</source>
        <translation>请等待. 新的 XMPP 帐号正在创建中.</translation>
    </message>
    <message>
        <source>Registration was successful. Your new XMPP username is %1.
Store it in a safe place along with the password.
Now please add your friends to the buddy list.</source>
        <translation>注册成功. 你的新 XMPP 用户名是 %1.
你可以讲它和你的密码存放到一个安全的位置.
现在请添加你的朋友到好友列表.</translation>
    </message>
    <message>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation>注册时发生错误. 请稍后重试.</translation>
    </message>
    <message>
        <source>Registering new XMPP account</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MiniClient</name>
    <message>
        <source>Server Authentication</source>
        <translation>服务器认证</translation>
    </message>
    <message>
        <source>Server Error</source>
        <translation>服务器错误</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>服务器不支持 TLS 加密.</translation>
    </message>
    <message>
        <source>There was an error communicating with the Jabber server.
Details: %1</source>
        <translation>与 Jabber 服务器通讯时出现了一个错误.
详情: %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>The server did not present a certificate.</source>
        <translation>服务器不能提供证书.</translation>
    </message>
    <message>
        <source>Certificate is valid.</source>
        <translation>证书有效.</translation>
    </message>
    <message>
        <source>The hostname does not match the one the certificate was issued to.</source>
        <translation>颁发的证书与主机名不匹配.</translation>
    </message>
    <message>
        <source>General certificate validation error.</source>
        <translation>常规证书验证错误.</translation>
    </message>
    <message>
        <source>No certificate presented.</source>
        <translation>未提供证书.</translation>
    </message>
    <message>
        <source>Hostname mismatch.</source>
        <translation>主机名不匹配.</translation>
    </message>
    <message>
        <source>Invalid Certificate.</source>
        <translation>无效证书.</translation>
    </message>
    <message>
        <source>General validation error.</source>
        <translation>常规验证错误.</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>昵称</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <source>First Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>姓</translation>
    </message>
    <message>
        <source>E-mail</source>
        <translation>邮件</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <source>City</source>
        <translation>城市</translation>
    </message>
    <message>
        <source>State</source>
        <translation>省</translation>
    </message>
    <message>
        <source>Zipcode</source>
        <translation>邮编</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>电话</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <source>Misc</source>
        <translation>杂项</translation>
    </message>
</context>
<context>
    <name>ShowXmlConsoleActionDescription</name>
    <message>
        <source>Show XML Console</source>
        <translation>显示 XML 控制台</translation>
    </message>
</context>
<context>
    <name>TestDlg</name>
    <message>
        <source>XMPP Test</source>
        <translation>XMPP 测试</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>关于 %1</translation>
    </message>
    <message>
        <source>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Currently supports:
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</source>
        <translation>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Currently supports:
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <source>Please enter the Full JID to connect with.</source>
        <translation>请输入用于连接的完整 JID.</translation>
    </message>
    <message>
        <source>You must specify a host:port for the proxy.</source>
        <translation>你必需为代理服务器指定一个 &quot;主机名:端口&quot;对.</translation>
    </message>
    <message>
        <source>You must at least enter a URL to use http poll.</source>
        <translation>You must at least enter a URL to use http poll.</translation>
    </message>
    <message>
        <source>Please enter the proxy host in the form &apos;host:port&apos;.</source>
        <translation>请在&quot;主机名:端口&quot;项中输入代理服务器主机名.</translation>
    </message>
    <message>
        <source>Please enter the host in the form &apos;host:port&apos;.</source>
        <translation>请在&quot;主机名:端口&quot;项中输入主机名.</translation>
    </message>
    <message>
        <source>Error: SSF Min is greater than SSF Max.</source>
        <translation>错误: SSF 最小值比 SSF 最大值大.</translation>
    </message>
    <message>
        <source>Error: TLS not available.  Disable any TLS options.</source>
        <translation>错误: TLS 不可用.  禁用所有 TLS 选项.</translation>
    </message>
    <message>
        <source>&amp;Disconnect</source>
        <translation>断开</translation>
    </message>
    <message>
        <source>Bad XML input (%1,%2): %3
Please correct and try again.</source>
        <translation>错误的 XML 输入 (%1,%2): %3
请纠正并重试.</translation>
    </message>
    <message>
        <source>Bad Stanza &apos;%1&apos;.  Must be &apos;message&apos;, &apos;presence&apos;, or &apos;iq&apos;</source>
        <translation>Bad Stanza &apos;%1&apos;.  Must be &apos;message&apos;, &apos;presence&apos;, or &apos;iq&apos;</translation>
    </message>
    <message>
        <source>You must enter at least one stanza!</source>
        <translation>You must enter at least one stanza!</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <source>Enter the password for %1</source>
        <translation>输入 %1 的密码</translation>
    </message>
</context>
<context>
    <name>XMPP::ClientStream</name>
    <message>
        <source>Offered mechanisms: </source>
        <translation>Offered mechanisms: </translation>
    </message>
</context>
<context>
    <name>XMPP::Features::FeatureName</name>
    <message>
        <source>ERROR: Incorrect usage of Features class</source>
        <translation>ERROR: Incorrect usage of Features class</translation>
    </message>
    <message>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>注册</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <source>Groupchat</source>
        <translation>群聊</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation>网关</translation>
    </message>
    <message>
        <source>Service Discovery</source>
        <translation>服务发现</translation>
    </message>
    <message>
        <source>VCard</source>
        <translation>VCard</translation>
    </message>
    <message>
        <source>Execute command</source>
        <translation>执行命令</translation>
    </message>
    <message>
        <source>Add to roster</source>
        <translation>加入好友</translation>
    </message>
</context>
<context>
    <name>XMPP::JT_VCard</name>
    <message>
        <source>No VCard available</source>
        <translation>没有可用的 VCard</translation>
    </message>
</context>
<context>
    <name>XMPP::JabberConnectionService</name>
    <message>
        <source>security problem</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>TLS certificate not accepted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Connection error.
Details: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Account disconnected.
Details: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>SSL support could not be initialized for account %1. This is most likely because the QCA TLS plugin is not installed on your system.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>XMPP::Stanza::Error::Private</name>
    <message>
        <source>Bad request</source>
        <translation>错误请求</translation>
    </message>
    <message>
        <source>The sender has sent XML that is malformed or that cannot be processed.</source>
        <translation>发送者发送了的XML不完整或无法处理.</translation>
    </message>
    <message>
        <source>Conflict</source>
        <translation>冲突</translation>
    </message>
    <message>
        <source>Access cannot be granted because an existing resource or session exists with the same name or address.</source>
        <translation>由于一个现有资源或会话存在同样的名称或地址, 无法访问.</translation>
    </message>
    <message>
        <source>Feature not implemented</source>
        <translation>功能未实现</translation>
    </message>
    <message>
        <source>The feature requested is not implemented by the recipient or server and therefore cannot be processed.</source>
        <translation>The feature requested is not implemented by the recipient or server and therefore cannot be processed.</translation>
    </message>
    <message>
        <source>Forbidden</source>
        <translation>禁止</translation>
    </message>
    <message>
        <source>The requesting entity does not possess the required permissions to perform the action.</source>
        <translation>The requesting entity does not possess the required permissions to perform the action.</translation>
    </message>
    <message>
        <source>Gone</source>
        <translation>Gone</translation>
    </message>
    <message>
        <source>The recipient or server can no longer be contacted at this address.</source>
        <translation>The recipient or server can no longer be contacted at this address.</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation>内部服务器错误</translation>
    </message>
    <message>
        <source>The server could not process the stanza because of a misconfiguration or an otherwise-undefined internal server error.</source>
        <translation>The server could not process the stanza because of a misconfiguration or an otherwise-undefined internal server error.</translation>
    </message>
    <message>
        <source>Item not found</source>
        <translation>项目未找到</translation>
    </message>
    <message>
        <source>The addressed JID or item requested cannot be found.</source>
        <translation>The addressed JID or item requested cannot be found.</translation>
    </message>
    <message>
        <source>JID malformed</source>
        <translation>JID 不完整</translation>
    </message>
    <message>
        <source>The sending entity has provided or communicated an XMPP address (e.g., a value of the &apos;to&apos; attribute) or aspect thereof (e.g., a resource identifier) that does not adhere to the syntax defined in Addressing Scheme.</source>
        <translation>The sending entity has provided or communicated an XMPP address (e.g., a value of the &apos;to&apos; attribute) or aspect thereof (e.g., a resource identifier) that does not adhere to the syntax defined in Addressing Scheme.</translation>
    </message>
    <message>
        <source>Not acceptable</source>
        <translation>不接受</translation>
    </message>
    <message>
        <source>The recipient or server understands the request but is refusing to process it because it does not meet criteria defined by the recipient or server (e.g., a local policy regarding acceptable words in messages).</source>
        <translation>The recipient or server understands the request but is refusing to process it because it does not meet criteria defined by the recipient or server (e.g., a local policy regarding acceptable words in messages).</translation>
    </message>
    <message>
        <source>Not allowed</source>
        <translation>不允许</translation>
    </message>
    <message>
        <source>The recipient or server does not allow any entity to perform the action.</source>
        <translation>The recipient or server does not allow any entity to perform the action.</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation>未认证</translation>
    </message>
    <message>
        <source>The sender must provide proper credentials before being allowed to perform the action, or has provided improper credentials.</source>
        <translation>The sender must provide proper credentials before being allowed to perform the action, or has provided improper credentials.</translation>
    </message>
    <message>
        <source>Recipient unavailable</source>
        <translation>收件人不可用</translation>
    </message>
    <message>
        <source>The intended recipient is temporarily unavailable.</source>
        <translation>预期的收件人临时不可用.</translation>
    </message>
    <message>
        <source>Redirect</source>
        <translation>重定向</translation>
    </message>
    <message>
        <source>The recipient or server is redirecting requests for this information to another entity, usually temporarily.</source>
        <translation>The recipient or server is redirecting requests for this information to another entity, usually temporarily.</translation>
    </message>
    <message>
        <source>Registration required</source>
        <translation>需要注册</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because registration is required.</source>
        <translation>The requesting entity is not authorized to access the requested service because registration is required.</translation>
    </message>
    <message>
        <source>Remote server not found</source>
        <translation>远程服务器未找到</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient does not exist.</source>
        <translation>A remote server or service specified as part or all of the JID of the intended recipient does not exist.</translation>
    </message>
    <message>
        <source>Remote server timeout</source>
        <translation>远程服务器超时</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient (or required to fulfill a request) could not be contacted within a reasonable amount of time.</source>
        <translation>A remote server or service specified as part or all of the JID of the intended recipient (or required to fulfill a request) could not be contacted within a reasonable amount of time.</translation>
    </message>
    <message>
        <source>Resource constraint</source>
        <translation>资源限制</translation>
    </message>
    <message>
        <source>The server or recipient lacks the system resources necessary to service the request.</source>
        <translation>The server or recipient lacks the system resources necessary to service the request.</translation>
    </message>
    <message>
        <source>Service unavailable</source>
        <translation>服务不可用</translation>
    </message>
    <message>
        <source>The server or recipient does not currently provide the requested service.</source>
        <translation>The server or recipient does not currently provide the requested service.</translation>
    </message>
    <message>
        <source>Subscription required</source>
        <translation>必需订阅</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because a subscription is required.</source>
        <translation>The requesting entity is not authorized to access the requested service because a subscription is required.</translation>
    </message>
    <message>
        <source>Undefined condition</source>
        <translation>未定义的状态</translation>
    </message>
    <message>
        <source>The error condition is not one of those defined by the other conditions in this list.</source>
        <translation>The error condition is not one of those defined by the other conditions in this list.</translation>
    </message>
    <message>
        <source>Unexpected request</source>
        <translation>意外的请求</translation>
    </message>
    <message>
        <source>The recipient or server understood the request but was not expecting it at this time (e.g., the request was out of order).</source>
        <translation>The recipient or server understood the request but was not expecting it at this time (e.g., the request was out of order).</translation>
    </message>
</context>
<context>
    <name>XMPP::Task</name>
    <message>
        <source>Disconnected</source>
        <translation>断开</translation>
    </message>
</context>
</TS>