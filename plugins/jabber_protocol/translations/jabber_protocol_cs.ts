<?xml version="1.0" ?><!DOCTYPE TS><TS language="cs" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>External address</source>
        <translation>Vnější adresa</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Odpojeno</translation>
    </message>
    <message>
        <source>XML Parsing Error</source>
        <translation>Chyba při zpracování XML</translation>
    </message>
    <message>
        <source>XMPP Protocol Error</source>
        <translation>Chyba protokolu XMPP</translation>
    </message>
    <message>
        <source>Generic stream error</source>
        <translation>Obecná chyba proudu</translation>
    </message>
    <message>
        <source>Conflict(remote login replacing this one)</source>
        <translation>Rozpor (vzdálené přihlášení nahrazující toto)</translation>
    </message>
    <message>
        <source>Timed out from inactivity</source>
        <translation>Překročení času kvůli nečinnosti</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation>Vnitřní chyba serveru</translation>
    </message>
    <message>
        <source>Invalid XML</source>
        <translation>Neplatné XML</translation>
    </message>
    <message>
        <source>Policy violation</source>
        <translation>Nedodržení politiky</translation>
    </message>
    <message>
        <source>Server out of resources</source>
        <translation>Server nemá dostatek prostředků</translation>
    </message>
    <message>
        <source>Server is shutting down</source>
        <translation>Server se vypíná</translation>
    </message>
    <message>
        <source>XMPP Stream Error: %1</source>
        <translation>Chyba proudu XMPP: %1</translation>
    </message>
    <message>
        <source>Unable to connect to server</source>
        <translation>Nelze se připojit k serveru</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Nepodařilo se najít počítač</translation>
    </message>
    <message>
        <source>Error connecting to proxy</source>
        <translation>Chyba při připojování se k proxy</translation>
    </message>
    <message>
        <source>Error during proxy negotiation</source>
        <translation>Chyba při jednání s proxy</translation>
    </message>
    <message>
        <source>Proxy authentication failed</source>
        <translation>Autentizace u proxy serveru se nezdařila</translation>
    </message>
    <message>
        <source>Socket/stream error</source>
        <translation>Chyba zásuvky (socket)/proudu</translation>
    </message>
    <message>
        <source>Connection Error: %1</source>
        <translation>Chyba ve spojení: %1</translation>
    </message>
    <message>
        <source>Host no longer hosted</source>
        <translation>Počítač není nadále hostěn</translation>
    </message>
    <message>
        <source>Host unknown</source>
        <translation>Počítač neznámý</translation>
    </message>
    <message>
        <source>A required remote connection failed</source>
        <translation>Požadované vzdálené spojení se nezdařilo</translation>
    </message>
    <message>
        <source>See other host: %1</source>
        <translation>Podívejte se na jiného hostitele: %1</translation>
    </message>
    <message>
        <source>Server does not support proper XMPP version</source>
        <translation>Server nepodporuje správnou verzi XMPP</translation>
    </message>
    <message>
        <source>Stream Negotiation Error: %1</source>
        <translation>Chyba při vyjednání proudu: %1</translation>
    </message>
    <message>
        <source>Server rejected STARTTLS</source>
        <translation>Server odmítl STARTTLS</translation>
    </message>
    <message>
        <source>TLS handshake error</source>
        <translation>Chyba při potřesení rukou TLS</translation>
    </message>
    <message>
        <source>Broken security layer (TLS)</source>
        <translation>Poškozená bezpečnostní vrstva (TLS)</translation>
    </message>
    <message>
        <source>Unable to login</source>
        <translation>Nelze se přihlásit</translation>
    </message>
    <message>
        <source>No appropriate mechanism available for given security settings(e.g. SASL library too weak, or plaintext authentication not enabled)</source>
        <translation>Pro daná bezpečnostní nastavení není dostupný žádný vhodný mechanismus (např. knihovna SASL je příliš slabá, nebo ověření prostým textem není povoleno)</translation>
    </message>
    <message>
        <source>Bad server response</source>
        <translation>Špatná odpověď serveru</translation>
    </message>
    <message>
        <source>Server failed mutual authentication</source>
        <translation>Serveru se nepodařilo vzájemné ověření</translation>
    </message>
    <message>
        <source>Encryption required for chosen SASL mechanism</source>
        <translation>Pro vybraný mechanismus SASL je požadováno šifrování</translation>
    </message>
    <message>
        <source>Invalid account information</source>
        <translation>Neplatné informace o účtu</translation>
    </message>
    <message>
        <source>Invalid SASL mechanism</source>
        <translation>Neplatný mechanismus SASL</translation>
    </message>
    <message>
        <source>Invalid realm</source>
        <translation>Neplatný prostor</translation>
    </message>
    <message>
        <source>SASL mechanism too weak for this account</source>
        <translation>Mechanismus SASL je pro tento účet příliš slabý</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation>Neoprávněno</translation>
    </message>
    <message>
        <source>Temporary auth failure</source>
        <translation>Dočasné selhání ověřování pravosti</translation>
    </message>
    <message>
        <source>Authentication error: %1</source>
        <translation>Chyba při ověřování pravosti: %1</translation>
    </message>
    <message>
        <source>Broken security layer (SASL)</source>
        <translation>Poškozená bezpečnostní vrstva (SASL)</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Žádný</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Pokročilé</translation>
    </message>
    <message>
        <source>XMPP/Jabber</source>
        <translation>XMPP/Jabber</translation>
    </message>
    <message>
        <source>Port for data transfers</source>
        <translation>Přípojka (port) pro přenosy dat</translation>
    </message>
</context>
<context>
    <name>CertificateDisplayDialog</name>
    <message>
        <source>Certificate Information</source>
        <translation>Informace o osvědčení</translation>
    </message>
    <message>
        <source>Certificate Validation</source>
        <translation>Schválení osvědčení</translation>
    </message>
    <message>
        <source>Valid From</source>
        <translation>Platné od</translation>
    </message>
    <message>
        <source>Valid Until</source>
        <translation>Platné do</translation>
    </message>
    <message>
        <source>Serial Number</source>
        <translation>Sériové číslo</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <source>The certificate is valid.</source>
        <translation>Osvědčení je platné.</translation>
    </message>
    <message>
        <source>The certificate is NOT valid!</source>
        <translation>Osvědčení NENÍ platné!</translation>
    </message>
    <message>
        <source>Reason: %1.</source>
        <translation>Důvod: %1.</translation>
    </message>
    <message>
        <source>Subject Details:</source>
        <translation>Podrobnosti subjektu:</translation>
    </message>
    <message>
        <source>Issuer Details:</source>
        <translation>Podrobnosti k vydavateli:</translation>
    </message>
    <message>
        <source>Organization:</source>
        <translation>Organizace:</translation>
    </message>
    <message>
        <source>Organizational unit:</source>
        <translation>Organizační jednotka:</translation>
    </message>
    <message>
        <source>Locality:</source>
        <translation>Místo:</translation>
    </message>
    <message>
        <source>State:</source>
        <translation>Stát:</translation>
    </message>
    <message>
        <source>Country:</source>
        <translation>Země:</translation>
    </message>
    <message>
        <source>Common name:</source>
        <translation>Obecný název:</translation>
    </message>
    <message>
        <source>Domain name:</source>
        <translation>Název domény:</translation>
    </message>
    <message>
        <source>XMPP name:</source>
        <translation>Název XMPP:</translation>
    </message>
    <message>
        <source>Email:</source>
        <translation>E-mail:</translation>
    </message>
</context>
<context>
    <name>CertificateErrorWindow</name>
    <message>
        <source>Cannot establish secure connection with server &lt;i&gt;%1&lt;/i&gt;.</source>
        <translation>Nelze zřídit bezpečné spojení se serverem &lt;i&gt;%1&lt;/i&gt;</translation>
    </message>
    <message>
        <source>Show certificate...</source>
        <translation>Ukázat osvědčení...</translation>
    </message>
    <message>
        <source>If you do not trust &lt;i&gt;%1&lt;/i&gt;, cancel the connection.</source>
        <translation>Pokud nedůvěřujete &lt;i&gt;%1&lt;/i&gt;, zrušte spojení.</translation>
    </message>
    <message>
        <source>Remember my choice for this certificate</source>
        <translation>Zapamatovat si moji volbu pro toto osvědčení</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Spojit</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>FacebookProtocolFactory</name>
    <message>
        <source>Facebook ID:</source>
        <translation>Facebook ID:</translation>
    </message>
    <message>
        <source>Your username is available at &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; under Username field. If this field is empty, you can choose your Username and enter it there.</source>
        <translation>Vaše uživatelské jméno je dostupné na &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; v poli s uživatelským jménem. Pokud je toto pole prázdné, můžete si své uživatelské jméno vybrat, anebo jej tam zadat.</translation>
    </message>
</context>
<context>
    <name>GTalkProtocolFactory</name>
    <message>
        <source>Gmail/Google Talk ID:</source>
        <translation>Gmail/Google Talk ID:</translation>
    </message>
</context>
<context>
    <name>HttpConnect</name>
    <message>
        <source>Authentication failed</source>
        <translation>Ověření selhalo</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Nepodařilo se najít server</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Přístup odepřen</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Spojení bylo odmítnuto</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Neplatná odpověď</translation>
    </message>
</context>
<context>
    <name>HttpProxyGetStream</name>
    <message>
        <source>Authentication failed</source>
        <translation>Ověření selhalo</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Nepodařilo se najít server</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Přístup odepřen</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Spojení bylo odmítnuto</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Neplatná odpověď</translation>
    </message>
</context>
<context>
    <name>HttpProxyPost</name>
    <message>
        <source>Authentication failed</source>
        <translation>Ověření selhalo</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Nepodařilo se najít server</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Přístup odepřen</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Spojení bylo odmítnuto</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Neplatná odpověď</translation>
    </message>
</context>
<context>
    <name>JabberActions</name>
    <message>
        <source>Resend Subscription</source>
        <translation>Poslat přihlášení znovu</translation>
    </message>
    <message>
        <source>Remove Subscription</source>
        <translation>Odstranit přihlášení</translation>
    </message>
    <message>
        <source>Ask for Subscription</source>
        <translation>Požádat o přihlášení</translation>
    </message>
</context>
<context>
    <name>JabberAddAccountWidget</name>
    <message>
        <source>&lt;a href=&apos;#&apos;&gt;What is my username?&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;#&apos;&gt;Co je to to moje uživatelské jméno?&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>Remember Password</source>
        <translation>Zapamatovat si heslo</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Totožnost účtu</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Vyberte nebo zadejte totožnost, která bude spojena s tímto účtem.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Přidat účet</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>JabberChangePasswordWindow</name>
    <message>
        <source>Change Password</source>
        <translation>Změnit heslo</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation>Staré heslo</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Zadejte současné heslo pro svůj účet XMPP/Jabber.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>New password</source>
        <translation>Nové heslo</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Zadejte nové heslo pro svůj účet XMPP/Jabber.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Retype new password</source>
        <translation>Napište nové heslo znovu</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Changing password was successful.</source>
        <translation>Změna hesla byla úspěšná.</translation>
    </message>
    <message>
        <source>An error has occurred. Please try again later.</source>
        <translation>Vyskytla se chyba. Zkuste to, prosím, znovu později.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;Password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>Do požadovaných polí byla zadána neplatná data.

Heslo zadané do obou polí (heslo a nové zadání hesla) se musí shodovat!</translation>
    </message>
</context>
<context>
    <name>JabberContactPersonalInfoWidget</name>
    <message>
        <source>Full Name</source>
        <translation>Celé jméno</translation>
    </message>
    <message>
        <source>Family Name</source>
        <translation>Rodné jméno</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <source>Birthdate</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Stránky</translation>
    </message>
</context>
<context>
    <name>JabberCreateAccountWidget</name>
    <message>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>Retype Password</source>
        <translation>Napište heslo znovu</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Zapamatovat si heslo</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Totožnost účtu</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Vyberte nebo zadejte totožnost, která bude spojena s tímto účtem.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>More options:</source>
        <translation>Více voleb:</translation>
    </message>
    <message>
        <source>Connection settings</source>
        <translation>Nastavení spojení</translation>
    </message>
    <message>
        <source>Manually Specify Server Host/Port</source>
        <translation>Stanovit hostitele/přípojku serveru ručně</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hostitel</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Přípojka</translation>
    </message>
    <message>
        <source>Encrypt connection</source>
        <translation>Zašifrovat spojení</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Vždy</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>Když je tato možnost dostupná</translation>
    </message>
    <message>
        <source>Legacy SSL</source>
        <translation>Legacy SSL</translation>
    </message>
    <message>
        <source>Probe legacy SSL port</source>
        <translation>Vyzkoušet přípojku legacy SSL</translation>
    </message>
    <message>
        <source>Register Account</source>
        <translation>Přihlásit účet</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Nelze umožnit bezpečné spojení. Přídavný modul SSL/TLS nebyl nalezen.</translation>
    </message>
    <message>
        <source>Legacy secure connection (SSL) is only available in combination with manual host/port.</source>
        <translation>Legacy bezpečné spojení (SSL) je dostupné jen ve spojení s ručním hostitelem/přípojkou.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;New password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>Do požadovaných polí byla zadána neplatná data.

Heslo zadané do obou polí (nové heslo a nové zadání hesla) se musí shodovat!</translation>
    </message>
</context>
<context>
    <name>JabberEditAccountWidget</name>
    <message>
        <source>Apply</source>
        <translation>Použít</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Smazat účet</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Zapamatovat si heslo</translation>
    </message>
    <message>
        <source>Change your password</source>
        <translation>Změnit své heslo</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Totožnost účtu</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Vyberte nebo zadejte totožnost, která bude spojena s tímto účtem.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <source>Personal Information</source>
        <translation>Osobní informace</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Připojení</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Přípojka</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>Nikdy</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Vždy</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>Když je tato možnost dostupná</translation>
    </message>
    <message>
        <source>Allow plaintext authentication</source>
        <translation>Povolit ověření pravosti prostým textem</translation>
    </message>
    <message>
        <source>Over encrypted connection</source>
        <translation>Přes zašifrované spojení</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Volby</translation>
    </message>
    <message>
        <source>Resource</source>
        <translation>Zdroj</translation>
    </message>
    <message>
        <source>Priority</source>
        <translation>Přednost</translation>
    </message>
    <message>
        <source>Data transfer proxy</source>
        <translation>Proxy přenosu dat</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Nelze umožnit bezpečné spojení. Přídavný modul SSL/TLS nebyl nalezen.</translation>
    </message>
    <message>
        <source>Legacy SSL is only available in combination with manual host/port.</source>
        <translation>Legacy SSL je dostupné jen ve spojení s ručním hostitelem/přípojkou.</translation>
    </message>
    <message>
        <source>Remove account</source>
        <translation>Odstranit účet</translation>
    </message>
    <message>
        <source>Proxy configuration</source>
        <translation>Nastavení proxy</translation>
    </message>
    <message>
        <source>Publish system information</source>
        <translation>veřejnit informace o systému</translation>
    </message>
    <message>
        <source>XMPP Server</source>
        <translation>Server XMPP</translation>
    </message>
    <message>
        <source>Use custom server address/port</source>
        <translation>Použít vlastní adresu/přípojku serveru</translation>
    </message>
    <message>
        <source>Server address</source>
        <translation>Adresa serveru</translation>
    </message>
    <message>
        <source>Use encrypted connection</source>
        <translation>Použít šifrované připojení</translation>
    </message>
    <message>
        <source>Only in older version</source>
        <translation>Jen ve starší verzi</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Síť</translation>
    </message>
    <message>
        <source>Use computer name as a resource</source>
        <translation>Použít jméno počítače jako zdroj</translation>
    </message>
    <message>
        <source>Enable composing events</source>
        <translation>Povolit složené události</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</source>
        <translation>Účastník vašeho hovoru bude upozorněn, když budete psát zprávu, předtím než bude poslána. A naopak.</translation>
    </message>
    <message>
        <source>Enable chat activity events</source>
        <translation>Povolit události činnosti rozhovoru</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you suspend or end conversation. And vice versa.</source>
        <translation>Účastník vašeho hovoru bude upozorněn, když rozhovor pozastavíte nebo ukončíte. A naopak.</translation>
    </message>
    <message>
        <source>Others can see your system name/version</source>
        <translation>Ostatní mohou vidět název/verzi vašeho systému</translation>
    </message>
    <message>
        <source>Confrim Account Removal</source>
        <translation>Potvrdit odstranění účtu</translation>
    </message>
    <message>
        <source>Are you sure do you want to remove account %1 (%2)?</source>
        <translation>Opravdu chcete odstranit účet %1 (%2)?</translation>
    </message>
</context>
<context>
    <name>JabberPersonalInfoWidget</name>
    <message>
        <source>Full name</source>
        <translation>Celé jméno</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <source>Family name</source>
        <translation>Rodné jméno</translation>
    </message>
    <message>
        <source>Birth year</source>
        <translation>Rok narození</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Stránky</translation>
    </message>
</context>
<context>
    <name>JabberProtocolFactory</name>
    <message>
        <source>User JID:</source>
        <translation>Uživatelovo JID:</translation>
    </message>
</context>
<context>
    <name>JabberServerRegisterAccount</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>This server does not support registration</source>
        <translation>Tento server nepodporuje přihlášení účtu</translation>
    </message>
    <message>
        <source>There was an error registering the account.
Reason: %1</source>
        <translation>Při přihlašování účtu se objevila chyba.
Důvod: %1</translation>
    </message>
</context>
<context>
    <name>JabberWaitForAccountRegisterWindow</name>
    <message>
        <source>Plase wait. New XMPP account is being registered.</source>
        <translation>Počkejte, prosím. Je přihlašován nový účet XMPP.</translation>
    </message>
    <message>
        <source>Registration was successful. Your new XMPP username is %1.
Store it in a safe place along with the password.
Now please add your friends to the buddy list.</source>
        <translation>Přihlášení účtu bylo úspěšné. Vaše nové uživatelské jméno pro XMPP je %1.
Uložte je na bezpečném místě společně s heslem.
Nyní, prosím, přidejte své přátele do seznamu kamarádů.</translation>
    </message>
    <message>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation>Při přihlašování se vyskytla chyba. Zkuste to, prosím, znovu později.</translation>
    </message>
    <message>
        <source>Registering new XMPP account</source>
        <translation>Zaregistrování nového účtu XMPP</translation>
    </message>
</context>
<context>
    <name>MiniClient</name>
    <message>
        <source>Server Authentication</source>
        <translation>Ověření pravosti serveru</translation>
    </message>
    <message>
        <source>Server Error</source>
        <translation>Chyba serveru</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>Server nepodporuje šifrování TLS.</translation>
    </message>
    <message>
        <source>There was an error communicating with the Jabber server.
Details: %1</source>
        <translation>Během spojení se serverem Jabber se vyskytla chyba.
Podrobnosti: %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>The server did not present a certificate.</source>
        <translation>Server nepředstavil osvědčení.</translation>
    </message>
    <message>
        <source>Certificate is valid.</source>
        <translation>Osvědčení je platné.</translation>
    </message>
    <message>
        <source>The hostname does not match the one the certificate was issued to.</source>
        <translation>Název serveru neodpovídá tomu, pro které bylo osvědčení vydáno.</translation>
    </message>
    <message>
        <source>General certificate validation error.</source>
        <translation>Obecná chyba při schvalování osvědčení.</translation>
    </message>
    <message>
        <source>No certificate presented.</source>
        <translation>Nepředstaveno žádné osvědčení.</translation>
    </message>
    <message>
        <source>Hostname mismatch.</source>
        <translation>Neodpovídá název serveru.</translation>
    </message>
    <message>
        <source>Invalid Certificate.</source>
        <translation>Neplatné osvědčení.</translation>
    </message>
    <message>
        <source>General validation error.</source>
        <translation>Obecná chyba při schvalování.</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <source>First Name</source>
        <translation>Křestní jméno</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Stav</translation>
    </message>
    <message>
        <source>Zipcode</source>
        <translation>Směrovací číslo</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>Adresa (URL)</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <source>Misc</source>
        <translation>Různé</translation>
    </message>
</context>
<context>
    <name>ShowXmlConsoleActionDescription</name>
    <message>
        <source>Show XML Console</source>
        <translation>Ukázat konzoli XML</translation>
    </message>
</context>
<context>
    <name>TestDlg</name>
    <message>
        <source>XMPP Test</source>
        <translation>Zkouška XMPP</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>&amp;Připojit</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>O %1</translation>
    </message>
    <message>
        <source>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Currently supports:
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</source>
        <translation>%1 v1.0

Pomůcka pro ukázání knihovny XMPP.

V současnosti podporuje:
  draft-ietf-xmpp-core-21
  JEP-0025

Autorské právo (C) 2003 Justin Karneges</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <source>Please enter the Full JID to connect with.</source>
        <translation>Zadejte, prosím, celé JID, se kterým se má spojit.</translation>
    </message>
    <message>
        <source>You must specify a host:port for the proxy.</source>
        <translation>Musíte určit host:port pro proxy.</translation>
    </message>
    <message>
        <source>You must at least enter a URL to use http poll.</source>
        <translation>Musíte zadat alespoň adresu (URL) pro použití http poll.</translation>
    </message>
    <message>
        <source>Please enter the proxy host in the form &apos;host:port&apos;.</source>
        <translation>Zadejte, prosím, hostitele proxy v podobě &apos;host:port&apos;.</translation>
    </message>
    <message>
        <source>Please enter the host in the form &apos;host:port&apos;.</source>
        <translation>Zadejte, prosím, hostitele v podobě &apos;host:port&apos;.</translation>
    </message>
    <message>
        <source>Error: SSF Min is greater than SSF Max.</source>
        <translation>Chyba: SSF Min je větší než SSF Max.</translation>
    </message>
    <message>
        <source>Error: TLS not available.  Disable any TLS options.</source>
        <translation>Chyba: TLS není dostupné.  Zakázat jakoukoli volbu TLS.</translation>
    </message>
    <message>
        <source>&amp;Disconnect</source>
        <translation>&amp;Odpojit</translation>
    </message>
    <message>
        <source>Bad XML input (%1,%2): %3
Please correct and try again.</source>
        <translation>Špatný vstup XML (%1,%2): %3
Opravte, prosím, a zkuste to znovu..</translation>
    </message>
    <message>
        <source>Bad Stanza &apos;%1&apos;.  Must be &apos;message&apos;, &apos;presence&apos;, or &apos;iq&apos;</source>
        <translation>Špatná sloka &apos;%1&apos;.  Musí to být &apos;message&apos;, &apos;presence&apos;, nebo &apos;iq&apos;</translation>
    </message>
    <message>
        <source>You must enter at least one stanza!</source>
        <translation>Musíte zadat alespoň jednu sloku!</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>Enter the password for %1</source>
        <translation>Zadejte heslo pro %1</translation>
    </message>
</context>
<context>
    <name>XMPP::ClientStream</name>
    <message>
        <source>Offered mechanisms: </source>
        <translation>Nabídnuté mechanismy: </translation>
    </message>
</context>
<context>
    <name>XMPP::Features::FeatureName</name>
    <message>
        <source>ERROR: Incorrect usage of Features class</source>
        <translation>CHYBA: Nesprávné použití třídy Features</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Žádný</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>Přihlásit</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Hledat</translation>
    </message>
    <message>
        <source>Groupchat</source>
        <translation>Skupinový rozhovor</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation>Server</translation>
    </message>
    <message>
        <source>Service Discovery</source>
        <translation>Objevování služby</translation>
    </message>
    <message>
        <source>VCard</source>
        <translation>VCard</translation>
    </message>
    <message>
        <source>Execute command</source>
        <translation>Spustit příkaz</translation>
    </message>
    <message>
        <source>Add to roster</source>
        <translation>Přidat do seznamu služeb</translation>
    </message>
</context>
<context>
    <name>XMPP::JT_VCard</name>
    <message>
        <source>No VCard available</source>
        <translation>Není dostupná žádná VCard</translation>
    </message>
</context>
<context>
    <name>XMPP::JabberConnectionService</name>
    <message>
        <source>security problem</source>
        <translation>Bezpečnostní problém</translation>
    </message>
    <message>
        <source>TLS certificate not accepted</source>
        <translation>Certifikát TLS nepřijat</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>Server nepodporuje šifrování TLS.</translation>
    </message>
    <message>
        <source>Connection error.
Details: %1</source>
        <translation>Chyba spojení.
Podrobnosti: %1</translation>
    </message>
    <message>
        <source>Account disconnected.
Details: %1</source>
        <translation>Účet odpojen.
Podrobnosti: %1</translation>
    </message>
    <message>
        <source>SSL support could not be initialized for account %1. This is most likely because the QCA TLS plugin is not installed on your system.</source>
        <translation>Podporu SSL se nepodařilo spustit pro účet %1. Nejpravděpodobněji je to proto, že přídavný modul QCA TLS není nainstalován ve vašem systému.</translation>
    </message>
</context>
<context>
    <name>XMPP::Stanza::Error::Private</name>
    <message>
        <source>Bad request</source>
        <translation>Špatný požadavek</translation>
    </message>
    <message>
        <source>The sender has sent XML that is malformed or that cannot be processed.</source>
        <translation>Odesílatel poslal XML, které je poškozeno nebo nemůže být zpracováno.</translation>
    </message>
    <message>
        <source>Conflict</source>
        <translation>Střet</translation>
    </message>
    <message>
        <source>Access cannot be granted because an existing resource or session exists with the same name or address.</source>
        <translation>Přístup nemůže být potvrzen, protože se stejným názvem nebo adresou již existuje nějaký zdroj nebo sezení.</translation>
    </message>
    <message>
        <source>Feature not implemented</source>
        <translation>Vlastnost neprovedena</translation>
    </message>
    <message>
        <source>The feature requested is not implemented by the recipient or server and therefore cannot be processed.</source>
        <translation>Požadovaná vlastnost není příjemcem nebo serverem provedena, a proto nemůže být zpracována.</translation>
    </message>
    <message>
        <source>Forbidden</source>
        <translation>Zakázáno</translation>
    </message>
    <message>
        <source>The requesting entity does not possess the required permissions to perform the action.</source>
        <translation>Požadující bytí nemá požadovaná oprávnění pro provedení činnosti.</translation>
    </message>
    <message>
        <source>Gone</source>
        <translation>Hotovo</translation>
    </message>
    <message>
        <source>The recipient or server can no longer be contacted at this address.</source>
        <translation>Příjemce nebo server na této adrese nemůže být nadále zastižen.</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation>Vnitřní chyba serveru</translation>
    </message>
    <message>
        <source>The server could not process the stanza because of a misconfiguration or an otherwise-undefined internal server error.</source>
        <translation>Server nemohl zpracovat sloku z důvodu špatného nastavení nebo jinak neurčené vnitřní chyby serveru.</translation>
    </message>
    <message>
        <source>Item not found</source>
        <translation>Položka nenalezena</translation>
    </message>
    <message>
        <source>The addressed JID or item requested cannot be found.</source>
        <translation>Adresované JID nebo požadovanou položka nelze najít.</translation>
    </message>
    <message>
        <source>JID malformed</source>
        <translation>JID poškozeno</translation>
    </message>
    <message>
        <source>The sending entity has provided or communicated an XMPP address (e.g., a value of the &apos;to&apos; attribute) or aspect thereof (e.g., a resource identifier) that does not adhere to the syntax defined in Addressing Scheme.</source>
        <translation>Zasílající bytí poskytlo nebo sdělilo adresu XMPP (např., hodnota vlastnosti &apos;to&apos;) nebo aspekt (např., identifikátor zdroje), který nesouhlasí se skladbou stanovenou v adresním schématu.</translation>
    </message>
    <message>
        <source>Not acceptable</source>
        <translation>Nepřijatelné</translation>
    </message>
    <message>
        <source>The recipient or server understands the request but is refusing to process it because it does not meet criteria defined by the recipient or server (e.g., a local policy regarding acceptable words in messages).</source>
        <translation>Příjemce nebo server rozumí požadavku, ale odmítá jej zpracovat, protože tento nenaplňuje hlediska stanovená příjemcem nebo serverem (např., místní politika s ohledem na přijatelná slova ve zprávách).</translation>
    </message>
    <message>
        <source>Not allowed</source>
        <translation>Nedovoleno</translation>
    </message>
    <message>
        <source>The recipient or server does not allow any entity to perform the action.</source>
        <translation>Příjemce nebo server neumožňuje žádnému bytí provést činnost.</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation>Neoprávněno</translation>
    </message>
    <message>
        <source>The sender must provide proper credentials before being allowed to perform the action, or has provided improper credentials.</source>
        <translation>Zasílatel musí poskytnout správné osobní údaje, dříve než mu bude umožněno provést činnost, nebo poskytl nesprávné osobní údaje.</translation>
    </message>
    <message>
        <source>Recipient unavailable</source>
        <translation>Příjemce nedostupný</translation>
    </message>
    <message>
        <source>The intended recipient is temporarily unavailable.</source>
        <translation>Zamýšlený příjemce je dočasně nedostupný.</translation>
    </message>
    <message>
        <source>Redirect</source>
        <translation>Přesměrování</translation>
    </message>
    <message>
        <source>The recipient or server is redirecting requests for this information to another entity, usually temporarily.</source>
        <translation>Příjemce nebo server přesměrovává žádosti o tuto informaci na jiné bytí, obvykle dočasně.</translation>
    </message>
    <message>
        <source>Registration required</source>
        <translation>Přihlášení požadováno</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because registration is required.</source>
        <translation>Žádající bytí není oprávněno přistupovat k požadované službě, protože je požadováno přihlášení.</translation>
    </message>
    <message>
        <source>Remote server not found</source>
        <translation>Vzdálený server nenalezen</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient does not exist.</source>
        <translation>Vzdálený server nebo služba určená jako část nebo celé JID zamýšleného příjemce neexistuje.</translation>
    </message>
    <message>
        <source>Remote server timeout</source>
        <translation>Časové omezení vzdáleného serveru</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient (or required to fulfill a request) could not be contacted within a reasonable amount of time.</source>
        <translation>Se vzdáleným serverem nebo službou určenou jako část nebo celé JID zamýšleného příjemce (nebo požadovaný k vyplnění žádosti) se nepodařilo spojit během rozumného množství času.</translation>
    </message>
    <message>
        <source>Resource constraint</source>
        <translation>Omezení zdroje</translation>
    </message>
    <message>
        <source>The server or recipient lacks the system resources necessary to service the request.</source>
        <translation>Server nebo příjemce postrádá systemové zdroje nutné pro obsloužení požadavku..</translation>
    </message>
    <message>
        <source>Service unavailable</source>
        <translation>Služba nedostupná</translation>
    </message>
    <message>
        <source>The server or recipient does not currently provide the requested service.</source>
        <translation>Server nebo příjemce v současnosti neposkytuje požadovanou službu.</translation>
    </message>
    <message>
        <source>Subscription required</source>
        <translation>Přihlášení požadováno</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because a subscription is required.</source>
        <translation>Žádající bytí není oprávněno přistupovat k požadované službě, protože je požadováno přihlášení.</translation>
    </message>
    <message>
        <source>Undefined condition</source>
        <translation>Nestanovená podmínka</translation>
    </message>
    <message>
        <source>The error condition is not one of those defined by the other conditions in this list.</source>
        <translation>Podmínka chyby není jednou z těch, jež jsou stanoveny jinými podmínkami v tomto seznamu.</translation>
    </message>
    <message>
        <source>Unexpected request</source>
        <translation>Neočekávaný požadavek</translation>
    </message>
    <message>
        <source>The recipient or server understood the request but was not expecting it at this time (e.g., the request was out of order).</source>
        <translation>Příjemce nebo server rozumí požadavku, ale neočekával jej v tuto dobu (např. požadavek byl mimo pořadí).</translation>
    </message>
</context>
<context>
    <name>XMPP::Task</name>
    <message>
        <source>Disconnected</source>
        <translation>Odpojeno</translation>
    </message>
</context>
</TS>