<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>External address</source>
        <translation>Adresse externe</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Déconnecté</translation>
    </message>
    <message>
        <source>XML Parsing Error</source>
        <translation>Erreur Analyse XML</translation>
    </message>
    <message>
        <source>XMPP Protocol Error</source>
        <translation>Erreur Protocole XMPP</translation>
    </message>
    <message>
        <source>Generic stream error</source>
        <translation>Erreur de flux générique</translation>
    </message>
    <message>
        <source>Conflict(remote login replacing this one)</source>
        <translation>Conflit(l&apos;authentification distante remplace l&apos;actuelle)</translation>
    </message>
    <message>
        <source>Timed out from inactivity</source>
        <translation>Expiration du délai d&apos;inactivité</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation>Erreur interne serveur</translation>
    </message>
    <message>
        <source>Invalid XML</source>
        <translation>XML non valide</translation>
    </message>
    <message>
        <source>Policy violation</source>
        <translation>Violation vie privé</translation>
    </message>
    <message>
        <source>Server out of resources</source>
        <translation>Serveur saturé</translation>
    </message>
    <message>
        <source>Server is shutting down</source>
        <translation>Serveur éteint</translation>
    </message>
    <message>
        <source>XMPP Stream Error: %1</source>
        <translation>Erreur de flux XMPP: %1</translation>
    </message>
    <message>
        <source>Unable to connect to server</source>
        <translation>Impossible de se connecter au serveur</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Hôte non trouvé</translation>
    </message>
    <message>
        <source>Error connecting to proxy</source>
        <translation>Erreur de connexion au proxy</translation>
    </message>
    <message>
        <source>Error during proxy negotiation</source>
        <translation>Erreur pendant la négociation avec le proxy</translation>
    </message>
    <message>
        <source>Proxy authentication failed</source>
        <translation>Échec d&apos;authentification au proxy</translation>
    </message>
    <message>
        <source>Socket/stream error</source>
        <translation>Socket/flux erreur</translation>
    </message>
    <message>
        <source>Connection Error: %1</source>
        <translation>Erreur connexion: %1</translation>
    </message>
    <message>
        <source>Host no longer hosted</source>
        <translation>l&apos;Hôte n&apos;est plus hébergé</translation>
    </message>
    <message>
        <source>Host unknown</source>
        <translation>Hôte inconnu</translation>
    </message>
    <message>
        <source>A required remote connection failed</source>
        <translation>Une connexion requise à échoué</translation>
    </message>
    <message>
        <source>See other host: %1</source>
        <translation>Voir autre Hôte: %1</translation>
    </message>
    <message>
        <source>Server does not support proper XMPP version</source>
        <translation>Le Serveur ne supporte pas la version XMPP utilisé</translation>
    </message>
    <message>
        <source>Stream Negotiation Error: %1</source>
        <translation>Erreur flux Négociation: %1</translation>
    </message>
    <message>
        <source>Server rejected STARTTLS</source>
        <translation>Le serveur a rejeté STARTTLS</translation>
    </message>
    <message>
        <source>TLS handshake error</source>
        <translation>Erreur handshake TLS</translation>
    </message>
    <message>
        <source>Broken security layer (TLS)</source>
        <translation>Couche de sécurité brisée (TLS)</translation>
    </message>
    <message>
        <source>Unable to login</source>
        <translation>Impossible de s&apos;identifier</translation>
    </message>
    <message>
        <source>No appropriate mechanism available for given security settings(e.g. SASL library too weak, or plaintext authentication not enabled)</source>
        <translation>Pas de mécanisme approprié disponible pour la configuration de sécurité(e.g SASL librairie trop faible ou authentification en texte plain non disponible)</translation>
    </message>
    <message>
        <source>Bad server response</source>
        <translation>Mauvaise réponse du serveur</translation>
    </message>
    <message>
        <source>Server failed mutual authentication</source>
        <translation>Le serveur a échoué lors de l&apos;authentification mutuelle</translation>
    </message>
    <message>
        <source>Encryption required for chosen SASL mechanism</source>
        <translation>Le cryptage est requis pour le mécanisme SASL choisi</translation>
    </message>
    <message>
        <source>Invalid account information</source>
        <translation>Informations du compte non valides</translation>
    </message>
    <message>
        <source>Invalid SASL mechanism</source>
        <translation>Mécanisme SASL invalide</translation>
    </message>
    <message>
        <source>Invalid realm</source>
        <translation>Domaine non valide</translation>
    </message>
    <message>
        <source>SASL mechanism too weak for this account</source>
        <translation>Mécanisme SASL trop faible pour ce compte</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation>Non authorisé</translation>
    </message>
    <message>
        <source>Temporary auth failure</source>
        <translation>Échec temporaire d&apos;authentifiation</translation>
    </message>
    <message>
        <source>Authentication error: %1</source>
        <translation>Erreur d&apos;authentification: %1</translation>
    </message>
    <message>
        <source>Broken security layer (SASL)</source>
        <translation>Couche de sécurité brisée (SASL)</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <source>XMPP/Jabber</source>
        <translation>XMPP/Jabber</translation>
    </message>
    <message>
        <source>Port for data transfers</source>
        <translation>Port de communication pour le transfert de données</translation>
    </message>
</context>
<context>
    <name>CertificateDisplayDialog</name>
    <message>
        <source>Certificate Information</source>
        <translation>Informations du certificat</translation>
    </message>
    <message>
        <source>Certificate Validation</source>
        <translation>Validation du certificat</translation>
    </message>
    <message>
        <source>Valid From</source>
        <translation>Formulaire de validation</translation>
    </message>
    <message>
        <source>Valid Until</source>
        <translation>Valide jusqu&apos;au</translation>
    </message>
    <message>
        <source>Serial Number</source>
        <translation>Numéro de série</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>The certificate is valid.</source>
        <translation>Ce certificat est valide.</translation>
    </message>
    <message>
        <source>The certificate is NOT valid!</source>
        <translation>Ce certificat n&apos;est pas valide.</translation>
    </message>
    <message>
        <source>Reason: %1.</source>
        <translation>Raison : %1.</translation>
    </message>
    <message>
        <source>Subject Details:</source>
        <translation>Sujet Précisions:</translation>
    </message>
    <message>
        <source>Issuer Details:</source>
        <translation>Émetteur Précisions:</translation>
    </message>
    <message>
        <source>Organization:</source>
        <translation>Organisation :</translation>
    </message>
    <message>
        <source>Organizational unit:</source>
        <translation>Unité organisationnelle</translation>
    </message>
    <message>
        <source>Locality:</source>
        <translation>Lieu :</translation>
    </message>
    <message>
        <source>State:</source>
        <translation>État / Département :</translation>
    </message>
    <message>
        <source>Country:</source>
        <translation>Pays :</translation>
    </message>
    <message>
        <source>Common name:</source>
        <translation>Nom commun :</translation>
    </message>
    <message>
        <source>Domain name:</source>
        <translation>Nom de domaine :</translation>
    </message>
    <message>
        <source>XMPP name:</source>
        <translation>Nom XMPP :</translation>
    </message>
    <message>
        <source>Email:</source>
        <translation>Email :</translation>
    </message>
</context>
<context>
    <name>CertificateErrorWindow</name>
    <message>
        <source>Cannot establish secure connection with server &lt;i&gt;%1&lt;/i&gt;.</source>
        <translation>Impossible d&apos;établir une connexion sécurisé avec le serveur &lt;i&gt;%1&lt;/i&gt;.</translation>
    </message>
    <message>
        <source>Show certificate...</source>
        <translation>Afficher le certificat...</translation>
    </message>
    <message>
        <source>If you do not trust &lt;i&gt;%1&lt;/i&gt;, cancel the connection.</source>
        <translation>Si vous n&apos;avez pas confiance en &lt;i&gt;%1&lt;/i&gt;, coupez la connexion.</translation>
    </message>
    <message>
        <source>Remember my choice for this certificate</source>
        <translation>Mémoriser mon choix pour ce certificat</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Connecter</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>FacebookProtocolFactory</name>
    <message>
        <source>Facebook ID:</source>
        <translation>Identifient Facebook :</translation>
    </message>
    <message>
        <source>Your username is available at &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; under Username field. If this field is empty, you can choose your Username and enter it there.</source>
        <translation>Votre nom d&apos;utilisateur est disponible sur &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; dans le champ Nom d&apos;utilisateur. Si ce champ est vide, vous pouvez choisir votre nom d&apos;utilisateur et le saisir ici.</translation>
    </message>
</context>
<context>
    <name>GTalkProtocolFactory</name>
    <message>
        <source>Gmail/Google Talk ID:</source>
        <translation>Identifient Gmail / Google Talk :</translation>
    </message>
</context>
<context>
    <name>HttpConnect</name>
    <message>
        <source>Authentication failed</source>
        <translation>Authentification échouée</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Hôte introuvable</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Accès refusé</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Connexion refusée</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Réponse invalide</translation>
    </message>
</context>
<context>
    <name>HttpProxyGetStream</name>
    <message>
        <source>Authentication failed</source>
        <translation>Authentification échouée</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Hôte introuvable</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Accès refusé</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Connexion refusée</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Réponse invalide</translation>
    </message>
</context>
<context>
    <name>HttpProxyPost</name>
    <message>
        <source>Authentication failed</source>
        <translation>Authentification échouée</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Hôte introuvable</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Accès refusé</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Connexion refusée</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Réponse invalide</translation>
    </message>
</context>
<context>
    <name>JabberActions</name>
    <message>
        <source>Resend Subscription</source>
        <translation>Renvoyer l&apos;inscription</translation>
    </message>
    <message>
        <source>Remove Subscription</source>
        <translation>Supprimer l&apos;inscription</translation>
    </message>
    <message>
        <source>Ask for Subscription</source>
        <translation>Demander l&apos;inscription</translation>
    </message>
</context>
<context>
    <name>JabberAddAccountWidget</name>
    <message>
        <source>&lt;a href=&apos;#&apos;&gt;What is my username?&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;#&apos;&gt;Quel est mon nom d&apos;utilisateur ?&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Remember Password</source>
        <translation>Rappeler le mot de passe</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Identité du compte</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Sélectionner ou saisir l&apos;identifiant qui sera associé à votre compte.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Ajouter un compte</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>JabberChangePasswordWindow</name>
    <message>
        <source>Change Password</source>
        <translation>Changer le mot de passe</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation>Ancien mot de passe</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Saisir le mot de passe actuel de votre compte XMPP / Jabber.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>New password</source>
        <translation>Nouveau mot de passe</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Saisir un nouveau mot de passe pour votre compte XMPP / Jabber.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Retype new password</source>
        <translation>Répéter un nouveau mot de passe</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Changing password was successful.</source>
        <translation>Le mot de passe a été changé avec succès.</translation>
    </message>
    <message>
        <source>An error has occurred. Please try again later.</source>
        <translation>Une erreur est survenue. Veuillez réessayer plus tard.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;Password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>Données saisies dans les champs requis non valides.

Mot de passes entrés dans les 2 champs(&quot;mot de passe&quot; et &quot;saisissez de nouveau votre mot de passe&quot;) doivent être les mêmes!</translation>
    </message>
</context>
<context>
    <name>JabberContactPersonalInfoWidget</name>
    <message>
        <source>Full Name</source>
        <translation>Nom complet</translation>
    </message>
    <message>
        <source>Family Name</source>
        <translation>Nom de famille</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Pseudonyme</translation>
    </message>
    <message>
        <source>Birthdate</source>
        <translation>Date de naissance</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
</context>
<context>
    <name>JabberCreateAccountWidget</name>
    <message>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Retype Password</source>
        <translation>Répéter le mot de passe</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Rappeler le mot de passe</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Identité du compte</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Sélectionner ou saisir l&apos;identifiant qui sera associé à ce compte.&lt;/i&gt;&lt;/font&gt;
</translation>
    </message>
    <message>
        <source>More options:</source>
        <translation>Plus d&apos;options :</translation>
    </message>
    <message>
        <source>Connection settings</source>
        <translation>Paramètres de connexion</translation>
    </message>
    <message>
        <source>Manually Specify Server Host/Port</source>
        <translation>Saisir manuellement l&apos;adresse du serveur (hôte/port)</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Encrypt connection</source>
        <translation>Crypter la connexion </translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Toujours</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>Lorsque disponible</translation>
    </message>
    <message>
        <source>Legacy SSL</source>
        <translation>SSL hérité</translation>
    </message>
    <message>
        <source>Probe legacy SSL port</source>
        <translation>Port SSL probablement hérité</translation>
    </message>
    <message>
        <source>Register Account</source>
        <translation>Inscription de compte</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Impossible d&apos;activer la connexion. Le module SSL/TLS n&apos;a pas été trouvé.</translation>
    </message>
    <message>
        <source>Legacy secure connection (SSL) is only available in combination with manual host/port.</source>
        <translation>La connexion sécurisé hérité (SSL) n&apos;est disponible qu&apos;en combinaison avec un hôte/port manuel.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;New password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>Données invalides saisies dans les champs requis. ⏎ ⏎ Mot de passe entré dans les deux champs (&quot;Nouveau mot de passe&quot; et &quot;Confirmer mot de passe&quot;) doit être le même.</translation>
    </message>
</context>
<context>
    <name>JabberEditAccountWidget</name>
    <message>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Supprimer le compte</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Rappeler le mot de passe</translation>
    </message>
    <message>
        <source>Change your password</source>
        <translation>Changer votre mot de passe</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Identité du compte</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Sélectionner ou saisisser l&apos;identifiant qui sera associé à votre compte.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Personal Information</source>
        <translation>Informations personnelles</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>Jamais</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Toujours</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>Lorsque disponible</translation>
    </message>
    <message>
        <source>Allow plaintext authentication</source>
        <translation>Autoriser l&apos;authentification en clair</translation>
    </message>
    <message>
        <source>Over encrypted connection</source>
        <translation>À travers une connexion cryptée</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Resource</source>
        <translation>Resource</translation>
    </message>
    <message>
        <source>Priority</source>
        <translation>Priorité</translation>
    </message>
    <message>
        <source>Data transfer proxy</source>
        <translation>Proxy de transfert de données</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Ne peut pas établir de connexion sécurisé. Le module SSL/TLS n&apos;a pas été trouvé.</translation>
    </message>
    <message>
        <source>Legacy SSL is only available in combination with manual host/port.</source>
        <translation>Le SSL hérité n&apos;est disponible qu&apos;en combinaison avec un hôte/port manuel.</translation>
    </message>
    <message>
        <source>Remove account</source>
        <translation>Supprimer un compte</translation>
    </message>
    <message>
        <source>Proxy configuration</source>
        <translation>Configuration du proxy</translation>
    </message>
    <message>
        <source>Publish system information</source>
        <translation>Publier les informations système</translation>
    </message>
    <message>
        <source>XMPP Server</source>
        <translation>Serveur XMPP</translation>
    </message>
    <message>
        <source>Use custom server address/port</source>
        <translation>Utiliser une adresse/port personnalisé pour le serveur</translation>
    </message>
    <message>
        <source>Server address</source>
        <translation>Adresse Serveur</translation>
    </message>
    <message>
        <source>Use encrypted connection</source>
        <translation>Utiliser une connexion cryptée</translation>
    </message>
    <message>
        <source>Only in older version</source>
        <translation>Seulement dans les anciennes versions</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Réseau IM</translation>
    </message>
    <message>
        <source>Use computer name as a resource</source>
        <translation>Utiliser le nom de l&apos;hôte comme une ressource</translation>
    </message>
    <message>
        <source>Enable composing events</source>
        <translation>Activer la génération d&apos;évènements</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</source>
        <translation>Votre interlocuteur va être notifié quand vous composez un message, avant qu&apos;il soit envoyé et vice versa.</translation>
    </message>
    <message>
        <source>Enable chat activity events</source>
        <translation>Activer les évènements de conversation</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you suspend or end conversation. And vice versa.</source>
        <translation>Votre interlocuteur va être notifié lorsque vous suspendez ou terminez la conversation.</translation>
    </message>
    <message>
        <source>Others can see your system name/version</source>
        <translation>Les autres peuvent voir le nom/version de votre système</translation>
    </message>
    <message>
        <source>Confrim Account Removal</source>
        <translation>Confirmer la suppression du compte</translation>
    </message>
    <message>
        <source>Are you sure do you want to remove account %1 (%2)?</source>
        <translation>Êtes vous certain de vouloir supprimer ce compte %1 (%2)</translation>
    </message>
</context>
<context>
    <name>JabberPersonalInfoWidget</name>
    <message>
        <source>Full name</source>
        <translation>Nom complet</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Pseudo</translation>
    </message>
    <message>
        <source>Family name</source>
        <translation>Nom de famille</translation>
    </message>
    <message>
        <source>Birth year</source>
        <translation>Date de naissance</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
</context>
<context>
    <name>JabberProtocolFactory</name>
    <message>
        <source>User JID:</source>
        <translation>Utilisateur JID :</translation>
    </message>
</context>
<context>
    <name>JabberServerRegisterAccount</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>This server does not support registration</source>
        <translation>Ce serveur n&apos;accepte pas d&apos;inscription</translation>
    </message>
    <message>
        <source>There was an error registering the account.
Reason: %1</source>
        <translation>Il s&apos;est passé une erreur pendant l&apos;enregistrement du compte.
Raison: %1</translation>
    </message>
</context>
<context>
    <name>JabberWaitForAccountRegisterWindow</name>
    <message>
        <source>Plase wait. New XMPP account is being registered.</source>
        <translation>Veuillez patienter. le nouveau compte XMPP est en cours d&apos;inscription.</translation>
    </message>
    <message>
        <source>Registration was successful. Your new XMPP username is %1.
Store it in a safe place along with the password.
Now please add your friends to the buddy list.</source>
        <translation>L&apos;inscription a réussie. Votre nouveau nom d&apos;utilisateur XMPP est %1.
Conservez le dans un endroit sûr avec son mot de passe.
Maintenant veuillez ajouter vos amis à la liste d&apos;amis.</translation>
    </message>
    <message>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation>Une erreur est survenue pendant l&apos;enregistrement. Merci de réessayer plus tard. </translation>
    </message>
    <message>
        <source>Registering new XMPP account</source>
        <translation>Créer un compte XMPP sur le serveur</translation>
    </message>
</context>
<context>
    <name>MiniClient</name>
    <message>
        <source>Server Authentication</source>
        <translation>Serveur d&apos;authentification</translation>
    </message>
    <message>
        <source>Server Error</source>
        <translation>Erreur du serveur</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>Ce serveur ne pend pas en charge le cryptage TLS.</translation>
    </message>
    <message>
        <source>There was an error communicating with the Jabber server.
Details: %1</source>
        <translation>Il y avait une erreur de communication avec le serveur Jabber.
Détails : %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>The server did not present a certificate.</source>
        <translation>Ce serveur n&apos;expose pas de certificat.</translation>
    </message>
    <message>
        <source>Certificate is valid.</source>
        <translation>Le certificat est invalide.</translation>
    </message>
    <message>
        <source>The hostname does not match the one the certificate was issued to.</source>
        <translation>Le nom d&apos;hôte ne correspond pas à celui pour lequel le certificat a été délivré.</translation>
    </message>
    <message>
        <source>General certificate validation error.</source>
        <translation>Erreur générale de validation de certificat.</translation>
    </message>
    <message>
        <source>No certificate presented.</source>
        <translation>Aucun certificat présent.</translation>
    </message>
    <message>
        <source>Hostname mismatch.</source>
        <translation>Le nom d&apos;hôte ne correspond pas.</translation>
    </message>
    <message>
        <source>Invalid Certificate.</source>
        <translation>Certificat invalide.</translation>
    </message>
    <message>
        <source>General validation error.</source>
        <translation>Erreur générale de validation.</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Pseudonyme</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Rom</translation>
    </message>
    <message>
        <source>First Name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>E-mail</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <source>State</source>
        <translation>État / Département</translation>
    </message>
    <message>
        <source>Zipcode</source>
        <translation>Code postal</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <source>Misc</source>
        <translation>Divers</translation>
    </message>
</context>
<context>
    <name>ShowXmlConsoleActionDescription</name>
    <message>
        <source>Show XML Console</source>
        <translation>Afficher la console XML</translation>
    </message>
</context>
<context>
    <name>TestDlg</name>
    <message>
        <source>XMPP Test</source>
        <translation>Test XMPP</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>&amp;Connecter</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>À propos de %1</translation>
    </message>
    <message>
        <source>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Currently supports:
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</source>
        <translation>%1 v1.0

Utilitaire de démonstration de la bibliothèque Iris XMPP.

Prend en charge :
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Please enter the Full JID to connect with.</source>
        <translation>Veuillez saisir le JID complet pour vous connecter avec.</translation>
    </message>
    <message>
        <source>You must specify a host:port for the proxy.</source>
        <translation>Vous devez indiquer un adresse (host:port) pour le proxy.</translation>
    </message>
    <message>
        <source>You must at least enter a URL to use http poll.</source>
        <translation>Vous devez au moins saisir une URL pour utiliser le poll http.</translation>
    </message>
    <message>
        <source>Please enter the proxy host in the form &apos;host:port&apos;.</source>
        <translation>Veuillez saisir l&apos;hôte proxy sous la forme &apos;hôte:port&apos;.</translation>
    </message>
    <message>
        <source>Please enter the host in the form &apos;host:port&apos;.</source>
        <translation>Veuillez saisir l&apos;hôte sous la forme &apos;hôte:port&apos;.</translation>
    </message>
    <message>
        <source>Error: SSF Min is greater than SSF Max.</source>
        <translation>Erreur: SSF Min est plus important que SSF Max.</translation>
    </message>
    <message>
        <source>Error: TLS not available.  Disable any TLS options.</source>
        <translation>Error : TLS est indisponible.  Désactivez toutes les options liées à TLS</translation>
    </message>
    <message>
        <source>&amp;Disconnect</source>
        <translation>&amp;Déconnecter</translation>
    </message>
    <message>
        <source>Bad XML input (%1,%2): %3
Please correct and try again.</source>
        <translation>Mauvaise entrée XML (%1,%2) : %3
Veuillez corriger et essayer encore.</translation>
    </message>
    <message>
        <source>Bad Stanza &apos;%1&apos;.  Must be &apos;message&apos;, &apos;presence&apos;, or &apos;iq&apos;</source>
        <translation>mauvaise strophe %1. Doit être &apos;message&apos;,&apos;présence&apos;, ou &apos;iq&apos;</translation>
    </message>
    <message>
        <source>You must enter at least one stanza!</source>
        <translation>Vous devez entrer au moins une strophe!</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Enter the password for %1</source>
        <translation>Saisir le mot de passe de %1</translation>
    </message>
</context>
<context>
    <name>XMPP::ClientStream</name>
    <message>
        <source>Offered mechanisms: </source>
        <translation>Mécanismes offerts:</translation>
    </message>
</context>
<context>
    <name>XMPP::Features::FeatureName</name>
    <message>
        <source>ERROR: Incorrect usage of Features class</source>
        <translation>Erreur: usage incorrecte de la caractéristique de la classe</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Rien</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>Inscription</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>Groupchat</source>
        <translation>Groupe de conversation</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation>Passerelle</translation>
    </message>
    <message>
        <source>Service Discovery</source>
        <translation>Découverte de service</translation>
    </message>
    <message>
        <source>VCard</source>
        <translation>VCard</translation>
    </message>
    <message>
        <source>Execute command</source>
        <translation>Exécuter une commande</translation>
    </message>
    <message>
        <source>Add to roster</source>
        <translation>Ajouter à roster</translation>
    </message>
</context>
<context>
    <name>XMPP::JT_VCard</name>
    <message>
        <source>No VCard available</source>
        <translation>Aucun VCard disponible</translation>
    </message>
</context>
<context>
    <name>XMPP::JabberConnectionService</name>
    <message>
        <source>security problem</source>
        <translation>Problème de sécurité</translation>
    </message>
    <message>
        <source>TLS certificate not accepted</source>
        <translation>Certificat TLS non accepté</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>Erreur inconnu</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>LE serveur ne pend pas en charge le cryptage TLS.</translation>
    </message>
    <message>
        <source>Connection error.
Details: %1</source>
        <translation>Erreur de connexion.
Détail de l&apos;erreur: %1</translation>
    </message>
    <message>
        <source>Account disconnected.
Details: %1</source>
        <translation>Compte désactivé.
Détails: %1</translation>
    </message>
    <message>
        <source>SSL support could not be initialized for account %1. This is most likely because the QCA TLS plugin is not installed on your system.</source>
        <translation>Le support du SSL ne peut pas être initialisé pour le compte %1. C&apos;est généralement parce que le module QCA TLS n&apos;est pas installé sur votre système.</translation>
    </message>
</context>
<context>
    <name>XMPP::Stanza::Error::Private</name>
    <message>
        <source>Bad request</source>
        <translation>Mauvaise demande</translation>
    </message>
    <message>
        <source>The sender has sent XML that is malformed or that cannot be processed.</source>
        <translation>L&apos;émetteur a envoyé un fichier XML mal formé ou qui ne peut être analysé.</translation>
    </message>
    <message>
        <source>Conflict</source>
        <translation>Conflit</translation>
    </message>
    <message>
        <source>Access cannot be granted because an existing resource or session exists with the same name or address.</source>
        <translation>L&apos;accès ne peut être accordé parce qu&apos;une ressource ou une session de même nom ou même adresse existe déjà.</translation>
    </message>
    <message>
        <source>Feature not implemented</source>
        <translation>Fonctionnalité non implémentée</translation>
    </message>
    <message>
        <source>The feature requested is not implemented by the recipient or server and therefore cannot be processed.</source>
        <translation>La fonctionnalité demandée n&apos;est pas implémenté par le destinataire ou le serveur et donc ne peut être traité.</translation>
    </message>
    <message>
        <source>Forbidden</source>
        <translation>Interdit</translation>
    </message>
    <message>
        <source>The requesting entity does not possess the required permissions to perform the action.</source>
        <translation>L&apos;entité demandé n&apos;a pas l&apos;autorisation d&apos;exécuter l&apos;action.</translation>
    </message>
    <message>
        <source>Gone</source>
        <translation>Disparu</translation>
    </message>
    <message>
        <source>The recipient or server can no longer be contacted at this address.</source>
        <translation>Le destinataire ou le serveur ne peut plus être contacté à cette adresse.</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation>Erreur interne du serveur</translation>
    </message>
    <message>
        <source>The server could not process the stanza because of a misconfiguration or an otherwise-undefined internal server error.</source>
        <translation>Le serveur ne peut traiter la strophe à cause de mauvaise configuration ou une erreur non défini internet dans le serveur.</translation>
    </message>
    <message>
        <source>Item not found</source>
        <translation>Élément introuvable</translation>
    </message>
    <message>
        <source>The addressed JID or item requested cannot be found.</source>
        <translation>Le JID destinataire ou l&apos;élément demandé est introuvable.</translation>
    </message>
    <message>
        <source>JID malformed</source>
        <translation>JID mal formé</translation>
    </message>
    <message>
        <source>The sending entity has provided or communicated an XMPP address (e.g., a value of the &apos;to&apos; attribute) or aspect thereof (e.g., a resource identifier) that does not adhere to the syntax defined in Addressing Scheme.</source>
        <translation>L&apos;entité émettrice a fourni ou communiqué une adresse XMPP (par exemple, une valeur de l&apos;attribut &apos;to&apos;) ou un aspect de celle-ci (par exemple, un identifiant de ressource) qui ne adhère pas à la syntaxe définie dans schéma d&apos;adressage.</translation>
    </message>
    <message>
        <source>Not acceptable</source>
        <translation>Inacceptable</translation>
    </message>
    <message>
        <source>The recipient or server understands the request but is refusing to process it because it does not meet criteria defined by the recipient or server (e.g., a local policy regarding acceptable words in messages).</source>
        <translation>Le destinataire ou le serveur comprends la requête mais refuse de la traiter car elle ne correspond pas aux critères définis par ce dernier (ex, une politique locale en matière de mots acceptables dans les messages).</translation>
    </message>
    <message>
        <source>Not allowed</source>
        <translation>Non autorisé</translation>
    </message>
    <message>
        <source>The recipient or server does not allow any entity to perform the action.</source>
        <translation>Le destinataire ou le serveur n&apos;autorise aucune entité à exécuter cette action.</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation>Non autorisé</translation>
    </message>
    <message>
        <source>The sender must provide proper credentials before being allowed to perform the action, or has provided improper credentials.</source>
        <translation>L&apos;émetteur doit fournir un certificat approprié avant de pouvoir traiter l&apos;action ou a fourni des certificats incorrects.</translation>
    </message>
    <message>
        <source>Recipient unavailable</source>
        <translation>Destinataire indisponible</translation>
    </message>
    <message>
        <source>The intended recipient is temporarily unavailable.</source>
        <translation>Le destinataire est temporairement indisponible.</translation>
    </message>
    <message>
        <source>Redirect</source>
        <translation>Rediriger</translation>
    </message>
    <message>
        <source>The recipient or server is redirecting requests for this information to another entity, usually temporarily.</source>
        <translation>Le destinataire ou le serveur sont en train de rediriger les demandes pour cette information vers une autre entité, habituellement temporaire.</translation>
    </message>
    <message>
        <source>Registration required</source>
        <translation>Inscription nécessaire</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because registration is required.</source>
        <translation>L&apos;entité demandé n&apos;est pas autorisé à accéder au service demandé car l&apos;enregistrement est requis.</translation>
    </message>
    <message>
        <source>Remote server not found</source>
        <translation>Serveur distant introuvable</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient does not exist.</source>
        <translation>Le serveur distant ou le service spécifié comme élément de tous les JID destinataires attendu n&apos;existe pas.</translation>
    </message>
    <message>
        <source>Remote server timeout</source>
        <translation>Délais de connexion dépassé au serveur distant</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient (or required to fulfill a request) could not be contacted within a reasonable amount of time.</source>
        <translation>Un serveur distant ou service spécifié parmi les JID du destinataire attendu( ou attendu à remplir la requête) n&apos;a pas été contacté dans le temps imparti.</translation>
    </message>
    <message>
        <source>Resource constraint</source>
        <translation>Ressource restreinte</translation>
    </message>
    <message>
        <source>The server or recipient lacks the system resources necessary to service the request.</source>
        <translation>Le serveur ou le destinataire manque de ressources systèmes nécessaire pour traiter la requête.</translation>
    </message>
    <message>
        <source>Service unavailable</source>
        <translation>Service indisponible</translation>
    </message>
    <message>
        <source>The server or recipient does not currently provide the requested service.</source>
        <translation>Le serveur ou le receveur ne fournit pas le service demandé.</translation>
    </message>
    <message>
        <source>Subscription required</source>
        <translation>Inscription nécessaire</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because a subscription is required.</source>
        <translation>La entité demandé n&apos;est pas autorisé à accéder au service demandé car un enregistrement est requis.</translation>
    </message>
    <message>
        <source>Undefined condition</source>
        <translation>Condition indéfinie</translation>
    </message>
    <message>
        <source>The error condition is not one of those defined by the other conditions in this list.</source>
        <translation>La condition d&apos;erreur n&apos;est pas une de celles définies par les autres conditions de cette liste.</translation>
    </message>
    <message>
        <source>Unexpected request</source>
        <translation>Demande inattendue</translation>
    </message>
    <message>
        <source>The recipient or server understood the request but was not expecting it at this time (e.g., the request was out of order).</source>
        <translation>Le destinataire ou serveur a compris la requête mais ne l&apos;attendait pas à ce moment la (ex, la requêtes n&apos;était pas ordonné).</translation>
    </message>
</context>
<context>
    <name>XMPP::Task</name>
    <message>
        <source>Disconnected</source>
        <translation>Déconnecté</translation>
    </message>
</context>
</TS>