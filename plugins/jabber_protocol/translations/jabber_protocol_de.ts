<?xml version="1.0" ?><!DOCTYPE TS><TS language="de" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>External address</source>
        <translation>Externe Adresse</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>XML Parsing Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>XMPP Protocol Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Generic stream error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Conflict(remote login replacing this one)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Timed out from inactivity</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Internal server error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid XML</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Policy violation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server out of resources</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server is shutting down</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>XMPP Stream Error: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unable to connect to server</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Host not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Error connecting to proxy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Error during proxy negotiation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Proxy authentication failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Socket/stream error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Connection Error: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Host no longer hosted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Host unknown</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>A required remote connection failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>See other host: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server does not support proper XMPP version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Stream Negotiation Error: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server rejected STARTTLS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>TLS handshake error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Broken security layer (TLS)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unable to login</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No appropriate mechanism available for given security settings(e.g. SASL library too weak, or plaintext authentication not enabled)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Bad server response</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server failed mutual authentication</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Encryption required for chosen SASL mechanism</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid account information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid SASL mechanism</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid realm</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>SASL mechanism too weak for this account</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Not authorized</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Temporary auth failure</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Authentication error: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Broken security layer (SASL)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>None</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>XMPP/Jabber</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Port for data transfers</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CertificateDisplayDialog</name>
    <message>
        <source>Certificate Information</source>
        <translation>Zertifikat-Informationen</translation>
    </message>
    <message>
        <source>Certificate Validation</source>
        <translation>Zertifikat-Prüfung</translation>
    </message>
    <message>
        <source>Valid From</source>
        <translation>Gültige seit</translation>
    </message>
    <message>
        <source>Valid Until</source>
        <translation>Gültig bis</translation>
    </message>
    <message>
        <source>Serial Number</source>
        <translation>Seriennummer</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <source>The certificate is valid.</source>
        <translation>Dieses Zertifikat ist gültig.</translation>
    </message>
    <message>
        <source>The certificate is NOT valid!</source>
        <translation>Dieses Zertifikat NICHT ist gültig!</translation>
    </message>
    <message>
        <source>Reason: %1.</source>
        <translation>Antwort: %1.</translation>
    </message>
    <message>
        <source>Subject Details:</source>
        <translation>Details:</translation>
    </message>
    <message>
        <source>Issuer Details:</source>
        <translation>Details zum Zertifikatnehmer:</translation>
    </message>
    <message>
        <source>Organization:</source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <source>Organizational unit:</source>
        <translation>Abteilung:</translation>
    </message>
    <message>
        <source>Locality:</source>
        <translation>Ortschaft:</translation>
    </message>
    <message>
        <source>State:</source>
        <translation>Staat:</translation>
    </message>
    <message>
        <source>Country:</source>
        <translation>Land:</translation>
    </message>
    <message>
        <source>Common name:</source>
        <translation>Allgemeiner Name (CN):</translation>
    </message>
    <message>
        <source>Domain name:</source>
        <translation>Domainname:</translation>
    </message>
    <message>
        <source>XMPP name:</source>
        <translation>XMPP-Name:</translation>
    </message>
    <message>
        <source>Email:</source>
        <translation>E-Mail:</translation>
    </message>
</context>
<context>
    <name>CertificateErrorWindow</name>
    <message>
        <source>Cannot establish secure connection with server &lt;i&gt;%1&lt;/i&gt;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show certificate...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>If you do not trust &lt;i&gt;%1&lt;/i&gt;, cancel the connection.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Remember my choice for this certificate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Connect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FacebookProtocolFactory</name>
    <message>
        <source>Facebook ID:</source>
        <translation>Facebook-Id:</translation>
    </message>
    <message>
        <source>Your username is available at &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; under Username field. If this field is empty, you can choose your Username and enter it there.</source>
        <translation>Ihren Benutzernamen finden Sie unter &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; im Feld Name heraus. Sollte dieses Feld leer sein, können Sie einen neuen Namen dort und hier eintragen.</translation>
    </message>
</context>
<context>
    <name>GTalkProtocolFactory</name>
    <message>
        <source>Gmail/Google Talk ID:</source>
        <translation>Gmail/Google-Talk-ID:</translation>
    </message>
</context>
<context>
    <name>HttpConnect</name>
    <message>
        <source>Authentication failed</source>
        <translation>Authentifizierung ist fehlgeschlagen</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Rechner nicht gefunden</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Zugriff verweigert</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Verbindung wurde abgelehnt</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Ungültige Antwort</translation>
    </message>
</context>
<context>
    <name>HttpProxyGetStream</name>
    <message>
        <source>Authentication failed</source>
        <translation>Authentifizierung ist fehlgeschlagen</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Rechner nicht gefunden</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Zugriff verweigert</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Verbindung wurde abgelehnt</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Ungültige Antwort</translation>
    </message>
</context>
<context>
    <name>HttpProxyPost</name>
    <message>
        <source>Authentication failed</source>
        <translation>Authentifizierung ist fehlgeschlagen</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Rechner nicht gefunden</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Zugriff verweigert</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Verbindung wurde abgelehnt</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Ungültige Antwort</translation>
    </message>
</context>
<context>
    <name>JabberActions</name>
    <message>
        <source>Resend Subscription</source>
        <translation>Abonnement erneut senden</translation>
    </message>
    <message>
        <source>Remove Subscription</source>
        <translation>Abonnement entfernen</translation>
    </message>
    <message>
        <source>Ask for Subscription</source>
        <translation>Abonnement anfordern</translation>
    </message>
</context>
<context>
    <name>JabberAddAccountWidget</name>
    <message>
        <source>&lt;a href=&apos;#&apos;&gt;What is my username?&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;#&apos;&gt;Wie lautet mein Benutzername?&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <source>Remember Password</source>
        <translation>Passwort speichern</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Konto-Identität</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wählen Sie aus oder geben Sie eine Identität für dieses Konto ein.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Konto hinzufügen</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>JabberChangePasswordWindow</name>
    <message>
        <source>Change Password</source>
        <translation>Passwort ändern</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation>Altes Passwort</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Geben Sie Ihr aktuelles Passwort für Ihr XMPP/Jabber-Konto ein.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>New password</source>
        <translation>Neues Passwort</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Geben Sie ein neues Passwort für Ihr XMPP/Jabber-Konto ein.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Retype new password</source>
        <translation>Passwort erneut eingeben</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Changing password was successful.</source>
        <translation>Passwort erfolgreich geändert.</translation>
    </message>
    <message>
        <source>An error has occurred. Please try again later.</source>
        <translation>Ein Fehler ist eingetreten. Versuchen Sie später noch einmal.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;Password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>JabberContactPersonalInfoWidget</name>
    <message>
        <source>Full Name</source>
        <translation>Voller Name</translation>
    </message>
    <message>
        <source>Family Name</source>
        <translation>Familienname</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Nick</translation>
    </message>
    <message>
        <source>Birthdate</source>
        <translation>Geburtsdatum</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Stadt</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
</context>
<context>
    <name>JabberCreateAccountWidget</name>
    <message>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <source>Retype Password</source>
        <translation>Passwort erneut eingeben</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Passwort speichern</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Konto-Identität</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wählen Sie aus oder geben Sie eine Identität für dieses Konto ein.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>More options:</source>
        <translation>Mehr Optionen:</translation>
    </message>
    <message>
        <source>Connection settings</source>
        <translation>Verbindungseinstellungen</translation>
    </message>
    <message>
        <source>Manually Specify Server Host/Port</source>
        <translation>Den Rechnernamen und Port selber angeben</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Rechnername</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Encrypt connection</source>
        <translation>Verbindung verschlüsseln</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Immer</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>Wenn möglich</translation>
    </message>
    <message>
        <source>Legacy SSL</source>
        <translation>Veraltetes SSL</translation>
    </message>
    <message>
        <source>Probe legacy SSL port</source>
        <translation>Versuche Port des veralteten SSL</translation>
    </message>
    <message>
        <source>Register Account</source>
        <translation>Konto erstellen</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Kann sichere Verbindung nicht ermöglichen. SSL/TLS-Plugin fehlt.</translation>
    </message>
    <message>
        <source>Legacy secure connection (SSL) is only available in combination with manual host/port.</source>
        <translation>Die veraltete sichere Verbindung (SSL) ist nur unter Angabe von Rechnername und Port möglich.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;New password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>JabberEditAccountWidget</name>
    <message>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Konto löschen</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Passwort speichern</translation>
    </message>
    <message>
        <source>Change your password</source>
        <translation>Passwort ändern</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Konto-Identität</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wählen Sie aus oder geben Sie eine Identität für dieses Konto ein.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Personal Information</source>
        <translation>Persönliche Daten</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Verbindung</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>Nie</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Immer</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>Wenn möglich</translation>
    </message>
    <message>
        <source>Allow plaintext authentication</source>
        <translation>Erlaube Anmeldung mit Klartext</translation>
    </message>
    <message>
        <source>Over encrypted connection</source>
        <translation>Über verschlüsselte Verbindung</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <source>Resource</source>
        <translation>Ressource</translation>
    </message>
    <message>
        <source>Priority</source>
        <translation>Priorität</translation>
    </message>
    <message>
        <source>Data transfer proxy</source>
        <translation>Proxy für den Datenaustausch</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Kann sichere Verbindung nicht ermöglichen. SSL/TLS-Plugin fehlt.</translation>
    </message>
    <message>
        <source>Legacy SSL is only available in combination with manual host/port.</source>
        <translation>Die veraltete sichere Verbindung (SSL) ist nur unter Angabe von Rechnername und Port möglich.</translation>
    </message>
    <message>
        <source>Remove account</source>
        <translation>Konto entfernen</translation>
    </message>
    <message>
        <source>Proxy configuration</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Publish system information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>XMPP Server</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use custom server address/port</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use encrypted connection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Only in older version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Network</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use computer name as a resource</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enable composing events</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enable chat activity events</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Your interlocutor will be notified when you suspend or end conversation. And vice versa.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Others can see your system name/version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Confrim Account Removal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Are you sure do you want to remove account %1 (%2)?</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>JabberPersonalInfoWidget</name>
    <message>
        <source>Full name</source>
        <translation>Voller Name</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Nick</translation>
    </message>
    <message>
        <source>Family name</source>
        <translation>Familienname</translation>
    </message>
    <message>
        <source>Birth year</source>
        <translation>Geburtsjahr</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Stadt</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
</context>
<context>
    <name>JabberProtocolFactory</name>
    <message>
        <source>User JID:</source>
        <translation>Benutzer JID:</translation>
    </message>
</context>
<context>
    <name>JabberServerRegisterAccount</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>This server does not support registration</source>
        <translation>Dieser Server lässt keine Registrierung zu</translation>
    </message>
    <message>
        <source>There was an error registering the account.
Reason: %1</source>
        <translation>Während der Kontenregistrierung ist ein Fehler eingetreren: %1</translation>
    </message>
</context>
<context>
    <name>JabberWaitForAccountRegisterWindow</name>
    <message>
        <source>Plase wait. New XMPP account is being registered.</source>
        <translation>Bitte warten. Neues Konto XMPP wird erstellt.</translation>
    </message>
    <message>
        <source>Registration was successful. Your new XMPP username is %1.
Store it in a safe place along with the password.
Now please add your friends to the buddy list.</source>
        <translation>Registrierung war erfolgreich. Ihr neuer Bentzername im XMPP ist %1.
Speichern Sie es mit Ihrem Passwort nur an einem sicheren Ort.
Jetzt können Sie Ihre Freunde in die Kontaktliste aufnehmen.</translation>
    </message>
    <message>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation>Ein Fehler ist während der Registrierung eingetreten. Versuchen Sie später noch einmal.</translation>
    </message>
    <message>
        <source>Registering new XMPP account</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MiniClient</name>
    <message>
        <source>Server Authentication</source>
        <translation>Server Authentifizierung</translation>
    </message>
    <message>
        <source>Server Error</source>
        <translation>Serverfehler</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>Dieser Server unterstützt keine TLS-Verschlüsselung.</translation>
    </message>
    <message>
        <source>There was an error communicating with the Jabber server.
Details: %1</source>
        <translation>Es entstand ein Fehler während der Kommunikation mit dem Server Jabber.
Genauer gesagt: %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>The server did not present a certificate.</source>
        <translation>Der Server hat kein Zertifikat vorgezeigt.</translation>
    </message>
    <message>
        <source>Certificate is valid.</source>
        <translation>Zertifikat ist gültig.</translation>
    </message>
    <message>
        <source>The hostname does not match the one the certificate was issued to.</source>
        <translation>Der Rechnername passt nicht zu dem im Zertifikat angegebenen.</translation>
    </message>
    <message>
        <source>General certificate validation error.</source>
        <translation>Allgemeiner Fehler der Zertifikat-Validierung.</translation>
    </message>
    <message>
        <source>No certificate presented.</source>
        <translation>Kein Zertifikat ausgeliefert.</translation>
    </message>
    <message>
        <source>Hostname mismatch.</source>
        <translation>Rechnername passt nicht.</translation>
    </message>
    <message>
        <source>Invalid Certificate.</source>
        <translation>Ungültiges Zertifikat.</translation>
    </message>
    <message>
        <source>General validation error.</source>
        <translation>Allgemeiner Fehler der Validierung.</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Nick</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <source>E-mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Stadt</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Staat</translation>
    </message>
    <message>
        <source>Zipcode</source>
        <translation>PLZ</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <source>Misc</source>
        <translation>Andere</translation>
    </message>
</context>
<context>
    <name>ShowXmlConsoleActionDescription</name>
    <message>
        <source>Show XML Console</source>
        <translation>Zeige XML-Konsole</translation>
    </message>
</context>
<context>
    <name>TestDlg</name>
    <message>
        <source>XMPP Test</source>
        <translation>XMPP Test</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>&amp;Verbinden</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>Über %1</translation>
    </message>
    <message>
        <source>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Currently supports:
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</source>
        <translation>%1 v1.0

Werkzeug zum demonstrieren der Iris-XMPP-Bibliothek.

Unterstützt gegenwärtig:
  draft-ietf-xmpp-core-21
  JEP-0025

Urheberrecht (c) 2003 Justin Karneges</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Please enter the Full JID to connect with.</source>
        <translation>Geben Sie bitte die vollständige JID, die ich verbinden soll.</translation>
    </message>
    <message>
        <source>You must specify a host:port for the proxy.</source>
        <translation>Sie müssen den Rechnernamen und den Port des Proxy angeben.</translation>
    </message>
    <message>
        <source>You must at least enter a URL to use http poll.</source>
        <translation>Sie müssen zumindest die URL angeben, um http-Abfrage zu nutzen.</translation>
    </message>
    <message>
        <source>Please enter the proxy host in the form &apos;host:port&apos;.</source>
        <translation>Geben Sie bitte den Rechnernamen und Port des Proxy in der folgenden Form an: &apos;Rechnername:Port&apos;.</translation>
    </message>
    <message>
        <source>Please enter the host in the form &apos;host:port&apos;.</source>
        <translation>Geben Sie den Rechnernamen in der folgenden Form an: &apos;Rechnername:Port&apos;.</translation>
    </message>
    <message>
        <source>Error: SSF Min is greater than SSF Max.</source>
        <translation>Fehler SSF Min ist größer als SSF Max.</translation>
    </message>
    <message>
        <source>Error: TLS not available.  Disable any TLS options.</source>
        <translation>Fehler: TLS ist nicht verfügbar. Deaktiviere alle TSL-Optionen.</translation>
    </message>
    <message>
        <source>&amp;Disconnect</source>
        <translation>&amp;Trennen</translation>
    </message>
    <message>
        <source>Bad XML input (%1,%2): %3
Please correct and try again.</source>
        <translation>Falsche XML-Eingabe (%1, %2): %3
Bitte verbinden Sie sich erneut und versuchen noch einmal.</translation>
    </message>
    <message>
        <source>Bad Stanza &apos;%1&apos;.  Must be &apos;message&apos;, &apos;presence&apos;, or &apos;iq&apos;</source>
        <translation>Falsche Strophe &apos;%1&apos;. Solle &apos;message&apos;, &apos;presence&apos;, oder &apos;iq&apos; sein</translation>
    </message>
    <message>
        <source>You must enter at least one stanza!</source>
        <translation>Sie müssen zumindest eine Strophe angeben!</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <source>Enter the password for %1</source>
        <translation>Geben Sie das Passwort für %1 ein</translation>
    </message>
</context>
<context>
    <name>XMPP::ClientStream</name>
    <message>
        <source>Offered mechanisms: </source>
        <translation>Angebotene Mechanismen:</translation>
    </message>
</context>
<context>
    <name>XMPP::Features::FeatureName</name>
    <message>
        <source>ERROR: Incorrect usage of Features class</source>
        <translation>FEHLER: Falsche Nutzng der Klasse Features</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Nichts</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>Registrieren</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <source>Groupchat</source>
        <translation>Gruppenchat</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation>Gateway</translation>
    </message>
    <message>
        <source>Service Discovery</source>
        <translation>Service-Fund</translation>
    </message>
    <message>
        <source>VCard</source>
        <translation>VCard</translation>
    </message>
    <message>
        <source>Execute command</source>
        <translation>Kommando ausführen</translation>
    </message>
    <message>
        <source>Add to roster</source>
        <translation>Zum Dienstplan hinzufügen</translation>
    </message>
</context>
<context>
    <name>XMPP::JT_VCard</name>
    <message>
        <source>No VCard available</source>
        <translation>Kein VCard verfügbar</translation>
    </message>
</context>
<context>
    <name>XMPP::JabberConnectionService</name>
    <message>
        <source>security problem</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>TLS certificate not accepted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Connection error.
Details: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Account disconnected.
Details: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>SSL support could not be initialized for account %1. This is most likely because the QCA TLS plugin is not installed on your system.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>XMPP::Stanza::Error::Private</name>
    <message>
        <source>Bad request</source>
        <translation>Ja Gustaw und Du?</translation>
    </message>
    <message>
        <source>The sender has sent XML that is malformed or that cannot be processed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Conflict</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Access cannot be granted because an existing resource or session exists with the same name or address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Feature not implemented</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The feature requested is not implemented by the recipient or server and therefore cannot be processed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Forbidden</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The requesting entity does not possess the required permissions to perform the action.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Gone</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The recipient or server can no longer be contacted at this address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Internal server error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The server could not process the stanza because of a misconfiguration or an otherwise-undefined internal server error.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Item not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The addressed JID or item requested cannot be found.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>JID malformed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The sending entity has provided or communicated an XMPP address (e.g., a value of the &apos;to&apos; attribute) or aspect thereof (e.g., a resource identifier) that does not adhere to the syntax defined in Addressing Scheme.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Not acceptable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The recipient or server understands the request but is refusing to process it because it does not meet criteria defined by the recipient or server (e.g., a local policy regarding acceptable words in messages).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Not allowed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The recipient or server does not allow any entity to perform the action.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Not authorized</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The sender must provide proper credentials before being allowed to perform the action, or has provided improper credentials.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Recipient unavailable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The intended recipient is temporarily unavailable.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Redirect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The recipient or server is redirecting requests for this information to another entity, usually temporarily.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Registration required</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because registration is required.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Remote server not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient does not exist.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Remote server timeout</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient (or required to fulfill a request) could not be contacted within a reasonable amount of time.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Resource constraint</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The server or recipient lacks the system resources necessary to service the request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Service unavailable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The server or recipient does not currently provide the requested service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subscription required</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because a subscription is required.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Undefined condition</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The error condition is not one of those defined by the other conditions in this list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unexpected request</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The recipient or server understood the request but was not expecting it at this time (e.g., the request was out of order).</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>XMPP::Task</name>
    <message>
        <source>Disconnected</source>
        <translation>Getrennt</translation>
    </message>
</context>
</TS>