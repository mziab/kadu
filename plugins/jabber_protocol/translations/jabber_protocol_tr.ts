<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>External address</source>
        <translation>Harici adres</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Bağlantı kesildi</translation>
    </message>
    <message>
        <source>XML Parsing Error</source>
        <translation>XML Ayrıştırma Hata</translation>
    </message>
    <message>
        <source>XMPP Protocol Error</source>
        <translation>XMPP Protokol Hatası</translation>
    </message>
    <message>
        <source>Generic stream error</source>
        <translation>Genel akış hatası</translation>
    </message>
    <message>
        <source>Conflict(remote login replacing this one)</source>
        <translation>Çakışma(uzaktan oturumda bir bunu değiştirin)</translation>
    </message>
    <message>
        <source>Timed out from inactivity</source>
        <translation>Aktifsizlikten zaman aşımı</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation>İç Sunucu Hatası</translation>
    </message>
    <message>
        <source>Invalid XML</source>
        <translation>Hatalı XML</translation>
    </message>
    <message>
        <source>Policy violation</source>
        <translation>Politika ihlali</translation>
    </message>
    <message>
        <source>Server out of resources</source>
        <translation>Sunucu kaynakları dışında</translation>
    </message>
    <message>
        <source>Server is shutting down</source>
        <translation>Sunucu kapatılıyor</translation>
    </message>
    <message>
        <source>XMPP Stream Error: %1</source>
        <translation>XMPP Akış Hata: %1</translation>
    </message>
    <message>
        <source>Unable to connect to server</source>
        <translation>Sunucuya bağlanılamıyor</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Host bulunamadı</translation>
    </message>
    <message>
        <source>Error connecting to proxy</source>
        <translation>Proxy bağlantı hatası</translation>
    </message>
    <message>
        <source>Error during proxy negotiation</source>
        <translation>Proxy anlaşması sırasında hata</translation>
    </message>
    <message>
        <source>Proxy authentication failed</source>
        <translation>Proxy doğrulama hatası</translation>
    </message>
    <message>
        <source>Socket/stream error</source>
        <translation>Soket/akış hatası</translation>
    </message>
    <message>
        <source>Connection Error: %1</source>
        <translation>Bağlantı Hatası: %1</translation>
    </message>
    <message>
        <source>Host no longer hosted</source>
        <translation>Host artık sağlıyor</translation>
    </message>
    <message>
        <source>Host unknown</source>
        <translation>Host bilinmiyor</translation>
    </message>
    <message>
        <source>A required remote connection failed</source>
        <translation>Gerekli uzak bağlantı başarısız oldu</translation>
    </message>
    <message>
        <source>See other host: %1</source>
        <translation>Diğer hosta bakın: %1</translation>
    </message>
    <message>
        <source>Server does not support proper XMPP version</source>
        <translation>Sunucu uygun XMPP verisyonunu desteklemiyor</translation>
    </message>
    <message>
        <source>Stream Negotiation Error: %1</source>
        <translation>Akış Anlaşma Hatası: %1</translation>
    </message>
    <message>
        <source>Server rejected STARTTLS</source>
        <translation>Sunucu STARTTLS reddedildi</translation>
    </message>
    <message>
        <source>TLS handshake error</source>
        <translation>TLS el sıkışma hatası</translation>
    </message>
    <message>
        <source>Broken security layer (TLS)</source>
        <translation>Kırılmış güvenlik katmanı (TLS)</translation>
    </message>
    <message>
        <source>Unable to login</source>
        <translation>Giriş yapılamıyor</translation>
    </message>
    <message>
        <source>No appropriate mechanism available for given security settings(e.g. SASL library too weak, or plaintext authentication not enabled)</source>
        <translation>Verilen güvenlik ayarları için yok uygun bir mekanizma(örn. SASL kütüphanesi çok zayıf, ya da düz metin kimlik doğrulaması etkin değil)</translation>
    </message>
    <message>
        <source>Bad server response</source>
        <translation>Kötü sunucu yanıtı</translation>
    </message>
    <message>
        <source>Server failed mutual authentication</source>
        <translation>Sunucu karşılıklı kimlik doğrulaması başarısız oldu</translation>
    </message>
    <message>
        <source>Encryption required for chosen SASL mechanism</source>
        <translation>Şifreleme için gerekli olan seçilmiş SASL mekanizması</translation>
    </message>
    <message>
        <source>Invalid account information</source>
        <translation>Hatalı hesap bilgisi</translation>
    </message>
    <message>
        <source>Invalid SASL mechanism</source>
        <translation>Geçersiz SASL mekanizması</translation>
    </message>
    <message>
        <source>Invalid realm</source>
        <translation>Geçersiz bölge</translation>
    </message>
    <message>
        <source>SASL mechanism too weak for this account</source>
        <translation>SASL mekanizması bu hesap için çok zayıf</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation>Yetkili değil</translation>
    </message>
    <message>
        <source>Temporary auth failure</source>
        <translation>Geçici yetkilendirme hatası</translation>
    </message>
    <message>
        <source>Authentication error: %1</source>
        <translation>Doğrulama hatası: %1</translation>
    </message>
    <message>
        <source>Broken security layer (SASL)</source>
        <translation>Kırılmış güvenlik katmanı (SASL)</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Yok</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Gelişmiş</translation>
    </message>
    <message>
        <source>XMPP/Jabber</source>
        <translation>XMPP/Jabber</translation>
    </message>
    <message>
        <source>Port for data transfers</source>
        <translation>Veri aktarımları için port:</translation>
    </message>
</context>
<context>
    <name>CertificateDisplayDialog</name>
    <message>
        <source>Certificate Information</source>
        <translation>Sertifika Bilgisi</translation>
    </message>
    <message>
        <source>Certificate Validation</source>
        <translation>Sertifika Doğrulama</translation>
    </message>
    <message>
        <source>Valid From</source>
        <translation>Geçerli Gönderen</translation>
    </message>
    <message>
        <source>Valid Until</source>
        <translation>Geçerli Zaman</translation>
    </message>
    <message>
        <source>Serial Number</source>
        <translation>Seri Numarası</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <source>The certificate is valid.</source>
        <translation>Sertifika geçerli</translation>
    </message>
    <message>
        <source>The certificate is NOT valid!</source>
        <translation>Sertifika geçersiz!</translation>
    </message>
    <message>
        <source>Reason: %1.</source>
        <translation>Sebep: %1.</translation>
    </message>
    <message>
        <source>Subject Details:</source>
        <translation>Konu Detayları:</translation>
    </message>
    <message>
        <source>Issuer Details:</source>
        <translation>İhraççı Detayları:</translation>
    </message>
    <message>
        <source>Organization:</source>
        <translation>Kuruluş:</translation>
    </message>
    <message>
        <source>Organizational unit:</source>
        <translation>Kuruluş birimi:</translation>
    </message>
    <message>
        <source>Locality:</source>
        <translation>Yer:</translation>
    </message>
    <message>
        <source>State:</source>
        <translation>Devlet:</translation>
    </message>
    <message>
        <source>Country:</source>
        <translation>Şehir:</translation>
    </message>
    <message>
        <source>Common name:</source>
        <translation>Ortak ad:</translation>
    </message>
    <message>
        <source>Domain name:</source>
        <translation>Alan adı:</translation>
    </message>
    <message>
        <source>XMPP name:</source>
        <translation>XMPP adı</translation>
    </message>
    <message>
        <source>Email:</source>
        <translation>Email:</translation>
    </message>
</context>
<context>
    <name>CertificateErrorWindow</name>
    <message>
        <source>Cannot establish secure connection with server &lt;i&gt;%1&lt;/i&gt;.</source>
        <translation>&lt;i&gt;%1&lt;/i&gt; sunucusuyla güvenli bağlantı kurulamıyor.</translation>
    </message>
    <message>
        <source>Show certificate...</source>
        <translation>Sertifikayı göster...</translation>
    </message>
    <message>
        <source>If you do not trust &lt;i&gt;%1&lt;/i&gt;, cancel the connection.</source>
        <translation>&lt;i&gt;%1&lt;/i&gt; güvenmiyorsanız, bağlantıyı iptal edin.</translation>
    </message>
    <message>
        <source>Remember my choice for this certificate</source>
        <translation>Bu sertifika için seçimimi hatırla</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Bağlan</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
</context>
<context>
    <name>FacebookProtocolFactory</name>
    <message>
        <source>Facebook ID:</source>
        <translation>Facebook ID:</translation>
    </message>
    <message>
        <source>Your username is available at &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; under Username field. If this field is empty, you can choose your Username and enter it there.</source>
        <translation>Kullanıcı adınız &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; altında Kullanıcı adı alanında mevcut. Eğer bu alan boş ise, Kullanıcı adınızı seçip buraya giriniz.</translation>
    </message>
</context>
<context>
    <name>GTalkProtocolFactory</name>
    <message>
        <source>Gmail/Google Talk ID:</source>
        <translation>Gmail/Google Talk ID:</translation>
    </message>
</context>
<context>
    <name>HttpConnect</name>
    <message>
        <source>Authentication failed</source>
        <translation>Doğrulama başarısız</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Host bulunamadı</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Erişim engellendi</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Bağlantı reddedildi</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Geçersiz cevap</translation>
    </message>
</context>
<context>
    <name>HttpProxyGetStream</name>
    <message>
        <source>Authentication failed</source>
        <translation>Doğrulama başarısız</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Host bulunamadı</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Erişim engellendi</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Bağlantı reddedildi</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Geçersiz cevap</translation>
    </message>
</context>
<context>
    <name>HttpProxyPost</name>
    <message>
        <source>Authentication failed</source>
        <translation>Doğrulama başarısız</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Host bulunamadı</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Erişim engellendi</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Geçersiz cevap</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Geçersiz cevap</translation>
    </message>
</context>
<context>
    <name>JabberActions</name>
    <message>
        <source>Resend Subscription</source>
        <translation>Yeniden Abonelik Gönder</translation>
    </message>
    <message>
        <source>Remove Subscription</source>
        <translation>Abonelik Kaldır</translation>
    </message>
    <message>
        <source>Ask for Subscription</source>
        <translation>Abonelik için Sor</translation>
    </message>
</context>
<context>
    <name>JabberAddAccountWidget</name>
    <message>
        <source>&lt;a href=&apos;#&apos;&gt;What is my username?&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;#&apos;&gt;Benim kullanıcı adım ne?&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Kullanıcı adı</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Şifre</translation>
    </message>
    <message>
        <source>Remember Password</source>
        <translation>Şifremi Hatırla</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Hesap Kimliği</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Bu hesapla ilişkili kimliği girin veya seçin.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Hesap Ekle</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
</context>
<context>
    <name>JabberChangePasswordWindow</name>
    <message>
        <source>Change Password</source>
        <translation>Şifre Değiştir</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation>Eski Şifre</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;XMPP/Jabber hesabınız için şifrenizi girin.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>New password</source>
        <translation>Yeni şifre</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;XMPP/Jabber hesabınız için yeni şifrenizi girin.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Retype new password</source>
        <translation>Yeni şifreyi tekrar yazın</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Changing password was successful.</source>
        <translation>Şifre değiştirme başarılı.</translation>
    </message>
    <message>
        <source>An error has occurred. Please try again later.</source>
        <translation>Bir hata meydana geldi. Lütfen sonra tekrar deneyin.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;Password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>Gerekli alanlara hatalı veri girildi.

Girilen şifre alanları (&quot;Şifre&quot; ve &quot;Şifreyi tekrar yazın&quot;) aynı olmalı!</translation>
    </message>
</context>
<context>
    <name>JabberContactPersonalInfoWidget</name>
    <message>
        <source>Full Name</source>
        <translation>Tam İsim</translation>
    </message>
    <message>
        <source>Family Name</source>
        <translation>Aile İsmi</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Takma Ad</translation>
    </message>
    <message>
        <source>Birthdate</source>
        <translation>Doğum tarihi</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Şehir</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Website</translation>
    </message>
</context>
<context>
    <name>JabberCreateAccountWidget</name>
    <message>
        <source>Username</source>
        <translation>Kullanıcı adı</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Şifre</translation>
    </message>
    <message>
        <source>Retype Password</source>
        <translation>Şifreyi tekrar yazın</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Şifremi hatırla</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Hesap Kimliği</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Bu hesapla ilişkili kimliği girin veya seçin.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>More options:</source>
        <translation>Daha fazla seçenek:</translation>
    </message>
    <message>
        <source>Connection settings</source>
        <translation>Bağlantı ayarları</translation>
    </message>
    <message>
        <source>Manually Specify Server Host/Port</source>
        <translation>Özel El ile Sunucu Host/Port</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Encrypt connection</source>
        <translation>Şifreli bağlantı</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Her zaman</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>Mümkün olduğunda</translation>
    </message>
    <message>
        <source>Legacy SSL</source>
        <translation>Yerleşik SSL</translation>
    </message>
    <message>
        <source>Probe legacy SSL port</source>
        <translation>Sonda yerleşik SSL portu</translation>
    </message>
    <message>
        <source>Register Account</source>
        <translation>Hesap Kayıt Et</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Güvenli bağlantı yapılamaz. SSL/TLS eklentisi bulunamadı.</translation>
    </message>
    <message>
        <source>Legacy secure connection (SSL) is only available in combination with manual host/port.</source>
        <translation>Yerleşik güvenli bağlantı (SSL) sadece el ile girinlen host/port combinasyonununda mevcut</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;New password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>Gerekli alanlara hatalı veri girildi.

Girilen şifre alanları (&quot;Şifre&quot; ve &quot;Şifreyi tekrar yazın&quot;) aynı olmalı!</translation>
    </message>
</context>
<context>
    <name>JabberEditAccountWidget</name>
    <message>
        <source>Apply</source>
        <translation>Uygula</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Hesabı sil</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Kullanıcı adı</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Şifre</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Şifremi hatırla</translation>
    </message>
    <message>
        <source>Change your password</source>
        <translation>Şifrenizi değiştirin</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Hesap Kimliği</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Bu hesapla ilişkili kimliği girin veya seçin.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Personal Information</source>
        <translation>Kişisel Bilgiler</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Bağlantı</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>Hiç bir zaman</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Her zaman</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>Mümkün olduğunda</translation>
    </message>
    <message>
        <source>Allow plaintext authentication</source>
        <translation>Düz metin kimlik doğrulamasına izin ver</translation>
    </message>
    <message>
        <source>Over encrypted connection</source>
        <translation>Şifreli bağlantı üzerinden</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Ayarlar</translation>
    </message>
    <message>
        <source>Resource</source>
        <translation>Kaynak</translation>
    </message>
    <message>
        <source>Priority</source>
        <translation>Öncelik</translation>
    </message>
    <message>
        <source>Data transfer proxy</source>
        <translation>Veri aktarımı proxy</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Güvenli bağlantı yapılamaz. SSL/TLS eklentisi bulunamadı.</translation>
    </message>
    <message>
        <source>Legacy SSL is only available in combination with manual host/port.</source>
        <translation>Yerleşik SSL sadece el ile girinlen host/port combinasyonununda mevcut</translation>
    </message>
    <message>
        <source>Remove account</source>
        <translation>Hesabı kaldır</translation>
    </message>
    <message>
        <source>Proxy configuration</source>
        <translation>Proxy ayarı</translation>
    </message>
    <message>
        <source>Publish system information</source>
        <translation>Sistem bilgisi yayımla</translation>
    </message>
    <message>
        <source>XMPP Server</source>
        <translation>XMPP Sunucu</translation>
    </message>
    <message>
        <source>Use custom server address/port</source>
        <translation>Özel sunucu adresi/portu kullan</translation>
    </message>
    <message>
        <source>Server address</source>
        <translation>Sunucu adresi</translation>
    </message>
    <message>
        <source>Use encrypted connection</source>
        <translation>Şifreli bağlantı kullan</translation>
    </message>
    <message>
        <source>Only in older version</source>
        <translation>Sadece eski versiyonda</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Ağ</translation>
    </message>
    <message>
        <source>Use computer name as a resource</source>
        <translation>Bilgisayar adını bir kaynak olarak kullan</translation>
    </message>
    <message>
        <source>Enable composing events</source>
        <translation>Yazma olaylarını etkinleştirin</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</source>
        <translation>Konuştuğunuz kişi siz mesajı yazarken, göndermeden önce ve tam tersi durumda uyarılacaktır.</translation>
    </message>
    <message>
        <source>Enable chat activity events</source>
        <translation>Konuşma etkinliği olaylarını etkinleştirin</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you suspend or end conversation. And vice versa.</source>
        <translation>Konuştuğunuz kişi askıya aldığınızda, konuşmayı bitirdiğinizde veya tam tersi durumda uyarılacaktır.</translation>
    </message>
    <message>
        <source>Others can see your system name/version</source>
        <translation>Diğerleri sizin sistem adınızı/versiyonunu görebilir</translation>
    </message>
    <message>
        <source>Confrim Account Removal</source>
        <translation>Hesap Kaldırma Onayı</translation>
    </message>
    <message>
        <source>Are you sure do you want to remove account %1 (%2)?</source>
        <translation>%1 (%2) hesabını kaldırmak istiyor musunuz?</translation>
    </message>
</context>
<context>
    <name>JabberPersonalInfoWidget</name>
    <message>
        <source>Full name</source>
        <translation>İsim</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Takma isim</translation>
    </message>
    <message>
        <source>Family name</source>
        <translation>Aile adı</translation>
    </message>
    <message>
        <source>Birth year</source>
        <translation>Doğum yılı</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Şehir</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Website</translation>
    </message>
</context>
<context>
    <name>JabberProtocolFactory</name>
    <message>
        <source>User JID:</source>
        <translation>Kullanıcı JID:</translation>
    </message>
</context>
<context>
    <name>JabberServerRegisterAccount</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>This server does not support registration</source>
        <translation>Bu sunucu kayıt desteklemiyor</translation>
    </message>
    <message>
        <source>There was an error registering the account.
Reason: %1</source>
        <translation>Hesap kayıt edilirken hata meydana geldi.
Nedeni: %1</translation>
    </message>
</context>
<context>
    <name>JabberWaitForAccountRegisterWindow</name>
    <message>
        <source>Plase wait. New XMPP account is being registered.</source>
        <translation>Lütfen bekleyiniz. Yeni XMPP hesabı kayıt ediliyor.</translation>
    </message>
    <message>
        <source>Registration was successful. Your new XMPP username is %1.
Store it in a safe place along with the password.
Now please add your friends to the buddy list.</source>
        <translation>Kayınıdınız başarıyla oluşturuldu. Yeni XMPP kullanıcı adınız %1.
Şifrenizi güvenli bir yerde saklayınız.
Lütfen şimdi arkadaş listenize arkadaşlarınızı ekleyin.</translation>
    </message>
    <message>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation>Kayıt edilirken hata meydana geldi. Lütfen sonra tekrar deneyin.</translation>
    </message>
    <message>
        <source>Registering new XMPP account</source>
        <translation>Yeni XMPP hesabı kayıt ediliyor</translation>
    </message>
</context>
<context>
    <name>MiniClient</name>
    <message>
        <source>Server Authentication</source>
        <translation>Sunucu Doğrulaması</translation>
    </message>
    <message>
        <source>Server Error</source>
        <translation>Sunucu Hatası</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>Sunucu TLS şifrelemesini desteklemiyor</translation>
    </message>
    <message>
        <source>There was an error communicating with the Jabber server.
Details: %1</source>
        <translation>Jabber sunucusuyla iletişim hatası.
Detaylar: %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>The server did not present a certificate.</source>
        <translation>Sunucu sertifikası mevcut değildi.</translation>
    </message>
    <message>
        <source>Certificate is valid.</source>
        <translation>Sertifika geçerli.</translation>
    </message>
    <message>
        <source>The hostname does not match the one the certificate was issued to.</source>
        <translation>Host adı belgesi verildiği biriyle uyuşmuyor.</translation>
    </message>
    <message>
        <source>General certificate validation error.</source>
        <translation>Genel sertifika doğrulama hatası.</translation>
    </message>
    <message>
        <source>No certificate presented.</source>
        <translation>Sertifika sunulmadı.</translation>
    </message>
    <message>
        <source>Hostname mismatch.</source>
        <translation>Host adı eşleşmiyor.</translation>
    </message>
    <message>
        <source>Invalid Certificate.</source>
        <translation>Geçersiz sertifika.</translation>
    </message>
    <message>
        <source>General validation error.</source>
        <translation>Genel doğrulama hatası.</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Kullanıcı adı</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Takma isim</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Şifre</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>İsim</translation>
    </message>
    <message>
        <source>First Name</source>
        <translation>İsim</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Soyisim</translation>
    </message>
    <message>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Şehir</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Devlet</translation>
    </message>
    <message>
        <source>Zipcode</source>
        <translation>Posta Kodu</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Tarih</translation>
    </message>
    <message>
        <source>Misc</source>
        <translation>Çeşitli</translation>
    </message>
</context>
<context>
    <name>ShowXmlConsoleActionDescription</name>
    <message>
        <source>Show XML Console</source>
        <translation>XML Konsolunu Göster</translation>
    </message>
</context>
<context>
    <name>TestDlg</name>
    <message>
        <source>XMPP Test</source>
        <translation>XMPP Test</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>&amp;Bağlan</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>Hakkında %1</translation>
    </message>
    <message>
        <source>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Currently supports:
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</source>
        <translation>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Şuanda desteklenenler:
  draft-ietf-xmpp-core-21
  JEP-0025

Telif Hakkı (C) 2003 Justin Karneges</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <source>Please enter the Full JID to connect with.</source>
        <translation>Tam JID ile bağlanmak için lütfen girin.</translation>
    </message>
    <message>
        <source>You must specify a host:port for the proxy.</source>
        <translation>Proxy için bir host:port belirtmelisiniz.</translation>
    </message>
    <message>
        <source>You must at least enter a URL to use http poll.</source>
        <translation>Http sorgulama kullanmak için en azından bir URL girmelisiniz.</translation>
    </message>
    <message>
        <source>Please enter the proxy host in the form &apos;host:port&apos;.</source>
        <translation>Lütfen proxy host formuna &apos;host:port&apos; girin.</translation>
    </message>
    <message>
        <source>Please enter the host in the form &apos;host:port&apos;.</source>
        <translation>Lütfen host formuna &apos;host:port&apos; girin.</translation>
    </message>
    <message>
        <source>Error: SSF Min is greater than SSF Max.</source>
        <translation>Hata: SSF Min değeri SSF Max dan büyük.</translation>
    </message>
    <message>
        <source>Error: TLS not available.  Disable any TLS options.</source>
        <translation>Hata: TLS mevcut değil. TLS ayarları devre dışı bırakın.</translation>
    </message>
    <message>
        <source>&amp;Disconnect</source>
        <translation>&amp;Bağlantıyı Kes</translation>
    </message>
    <message>
        <source>Bad XML input (%1,%2): %3
Please correct and try again.</source>
        <translation>Kötü XML girişi (%1,%2): %3
Lütfen doğrulayın ve tekrar deneyin.</translation>
    </message>
    <message>
        <source>Bad Stanza &apos;%1&apos;.  Must be &apos;message&apos;, &apos;presence&apos;, or &apos;iq&apos;</source>
        <translation>Kötü Dörtlük &apos;%1&apos; &apos;mesaj&apos;.&apos;varlık&apos; veya &apos;iq&apos; olmalı</translation>
    </message>
    <message>
        <source>You must enter at least one stanza!</source>
        <translation>En az bir dörtlük girmelisiniz!</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Şifre</translation>
    </message>
    <message>
        <source>Enter the password for %1</source>
        <translation>%1 için şifre girin</translation>
    </message>
</context>
<context>
    <name>XMPP::ClientStream</name>
    <message>
        <source>Offered mechanisms: </source>
        <translation>Teklif edilen mekanizmalar:</translation>
    </message>
</context>
<context>
    <name>XMPP::Features::FeatureName</name>
    <message>
        <source>ERROR: Incorrect usage of Features class</source>
        <translation>HATA: Özellik sınıfı yanlış kullanımı</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Yok</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>Kayıt Et</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Ara</translation>
    </message>
    <message>
        <source>Groupchat</source>
        <translation>Grup konuşması</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation>Ağ Geçidi</translation>
    </message>
    <message>
        <source>Service Discovery</source>
        <translation>Servis Keşfet</translation>
    </message>
    <message>
        <source>VCard</source>
        <translation>VCard</translation>
    </message>
    <message>
        <source>Execute command</source>
        <translation>Uygulama komutu</translation>
    </message>
    <message>
        <source>Add to roster</source>
        <translation>Nöbet listesine ekle</translation>
    </message>
</context>
<context>
    <name>XMPP::JT_VCard</name>
    <message>
        <source>No VCard available</source>
        <translation>VCard mevcut değil</translation>
    </message>
</context>
<context>
    <name>XMPP::JabberConnectionService</name>
    <message>
        <source>security problem</source>
        <translation>güvenlik problemi</translation>
    </message>
    <message>
        <source>TLS certificate not accepted</source>
        <translation>TLS sertifikası kabul edilmedi</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>Bilinmeyen hata</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>Sunucu TLS şifrelemesini desteklemiyor</translation>
    </message>
    <message>
        <source>Connection error.
Details: %1</source>
        <translation>Bağlantı hatası.
Detaylar: %1</translation>
    </message>
    <message>
        <source>Account disconnected.
Details: %1</source>
        <translation>Hesap bağlantısı kesildi.
Detaylar: %1</translation>
    </message>
    <message>
        <source>SSL support could not be initialized for account %1. This is most likely because the QCA TLS plugin is not installed on your system.</source>
        <translation>%1 hesabı için SSL desteği başlatılamadı. Bu büyük olasılıkla QCA TLS eklentisi sisteminizde yüklü değil.</translation>
    </message>
</context>
<context>
    <name>XMPP::Stanza::Error::Private</name>
    <message>
        <source>Bad request</source>
        <translation>Kötü istek</translation>
    </message>
    <message>
        <source>The sender has sent XML that is malformed or that cannot be processed.</source>
        <translation>Gönderici XML gönderdi bu bozulmuş veya işlenemez.</translation>
    </message>
    <message>
        <source>Conflict</source>
        <translation>Çakışma</translation>
    </message>
    <message>
        <source>Access cannot be granted because an existing resource or session exists with the same name or address.</source>
        <translation>Varolan bir kaynağa erişim izni olamaz veya var olan oturuma aynı isim veya adrese</translation>
    </message>
    <message>
        <source>Feature not implemented</source>
        <translation>Özellik uygulanmadı</translation>
    </message>
    <message>
        <source>The feature requested is not implemented by the recipient or server and therefore cannot be processed.</source>
        <translation>Alıcı veya sunucu özellik isteği uygulanamadı ve bu nedenle işlenemez.</translation>
    </message>
    <message>
        <source>Forbidden</source>
        <translation>Yasak</translation>
    </message>
    <message>
        <source>The requesting entity does not possess the required permissions to perform the action.</source>
        <translation>Taraf isteği eylemi gerçekleştirmek için gerekli izinlere sahip değil</translation>
    </message>
    <message>
        <source>Gone</source>
        <translation>Gitmiş</translation>
    </message>
    <message>
        <source>The recipient or server can no longer be contacted at this address.</source>
        <translation>Alıcı veya sunucu artık bu adresle irtibata geçmiyor</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation>Sunucu iç hatası</translation>
    </message>
    <message>
        <source>The server could not process the stanza because of a misconfiguration or an otherwise-undefined internal server error.</source>
        <translation>Sunucu dörtlüğü işleyemiyor çünkü bir yanlış yapılandırma veya bir başka-tanımlanmamış iç sunucu hatası.</translation>
    </message>
    <message>
        <source>Item not found</source>
        <translation>Nesne bulunamadı</translation>
    </message>
    <message>
        <source>The addressed JID or item requested cannot be found.</source>
        <translation>JID adresi veya nesne isteği bulunamaz.</translation>
    </message>
    <message>
        <source>JID malformed</source>
        <translation>JID hatalı</translation>
    </message>
    <message>
        <source>The sending entity has provided or communicated an XMPP address (e.g., a value of the &apos;to&apos; attribute) or aspect thereof (e.g., a resource identifier) that does not adhere to the syntax defined in Addressing Scheme.</source>
        <translation>Gönderen tarafından sağlanan veya iletişimdeki bir XMPP adresiyle (örn, &apos;için&apos; özniteliği değeri) veya bunların en boy (örn, bir kaynak kimliği) Adresleme Şemasında sözdizimi bağlı değildir.</translation>
    </message>
    <message>
        <source>Not acceptable</source>
        <translation>Kabul edilemez</translation>
    </message>
    <message>
        <source>The recipient or server understands the request but is refusing to process it because it does not meet criteria defined by the recipient or server (e.g., a local policy regarding acceptable words in messages).</source>
        <translation>Alıcı veya sunucu isteği anlıyor fakat işlemi reddediyor çünkü alıcı veya sunucu tanımlanmış kriteri eşleşmiyor (örn., mesajlardaki kabul edilebilir kelimeleri ile ilgili yerel politika).</translation>
    </message>
    <message>
        <source>Not allowed</source>
        <translation>İzin verilmedi</translation>
    </message>
    <message>
        <source>The recipient or server does not allow any entity to perform the action.</source>
        <translation>Alıcıya veya sunucu herhangi bir tarafın eylem gerçekleştirmesine izin vermiyor.</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation>Yetkili değil</translation>
    </message>
    <message>
        <source>The sender must provide proper credentials before being allowed to perform the action, or has provided improper credentials.</source>
        <translation>Eylemi gerçekleştirmek için izin verilmeden önce gönderen uygun kimlik bilgilerini sağlamanız gerekir veya yanlış kimlik sağlanmıştır.</translation>
    </message>
    <message>
        <source>Recipient unavailable</source>
        <translation>Alıcı mevcut değil</translation>
    </message>
    <message>
        <source>The intended recipient is temporarily unavailable.</source>
        <translation>Alıcı geçici olarak mevcut değil</translation>
    </message>
    <message>
        <source>Redirect</source>
        <translation>Yönlendiriliyor</translation>
    </message>
    <message>
        <source>The recipient or server is redirecting requests for this information to another entity, usually temporarily.</source>
        <translation>Alıcı veya sunucu bu bilgiler için istekleri farklı tarafa yönlendiriyor, genellikle geçici.</translation>
    </message>
    <message>
        <source>Registration required</source>
        <translation>Kayıt işlemi gerekli</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because registration is required.</source>
        <translation>Talep eden taraf istenen servise erişmek için yetkili değil çünkü kayıt gerekli.</translation>
    </message>
    <message>
        <source>Remote server not found</source>
        <translation>Uzak sunucu bulunamadı</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient does not exist.</source>
        <translation>Bir uzak sunucu veya belirli hizmet parçası olarak veya hepsinin amaçlanan alıcının JID yok.</translation>
    </message>
    <message>
        <source>Remote server timeout</source>
        <translation>Uzak sunucu zaman aşımı</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient (or required to fulfill a request) could not be contacted within a reasonable amount of time.</source>
        <translation>Bir uzak sunucu veya belirli hizmet parçası olarak veya hepsinin amaçlanan alıcının (veya gerekli isteği yerine getirmek) makul bir süre içinde bağlantı kurulamadı.</translation>
    </message>
    <message>
        <source>Resource constraint</source>
        <translation>Kaynak kısıtı</translation>
    </message>
    <message>
        <source>The server or recipient lacks the system resources necessary to service the request.</source>
        <translation>Sunucu veya alıcı isteğine hizmet için gerekli sistem kaynaklarını yoksun.</translation>
    </message>
    <message>
        <source>Service unavailable</source>
        <translation>Servis kullanım dışı</translation>
    </message>
    <message>
        <source>The server or recipient does not currently provide the requested service.</source>
        <translation>Sunucu ya da alıcı şu anda istenen hizmeti sunmamaktadır.</translation>
    </message>
    <message>
        <source>Subscription required</source>
        <translation>Abonelik gerekli</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because a subscription is required.</source>
        <translation>Talep eden taraf istenen servise erişmek için yetkili değil çünkü bir abonelik gerekli.</translation>
    </message>
    <message>
        <source>Undefined condition</source>
        <translation>Tanımsız durum</translation>
    </message>
    <message>
        <source>The error condition is not one of those defined by the other conditions in this list.</source>
        <translation>Hata durumu bu listedeki diğer koşullara göre belirlenenlerden biri değil.</translation>
    </message>
    <message>
        <source>Unexpected request</source>
        <translation>Beklenmeyen istek</translation>
    </message>
    <message>
        <source>The recipient or server understood the request but was not expecting it at this time (e.g., the request was out of order).</source>
        <translation>Alıcı veya sunucu isteği anladı ama şu anda bunu beklemiyordu (örn, istek bozuk).</translation>
    </message>
</context>
<context>
    <name>XMPP::Task</name>
    <message>
        <source>Disconnected</source>
        <translation>Bağlantı kesildi</translation>
    </message>
</context>
</TS>