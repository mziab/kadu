<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>External address</source>
        <translation>External address</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Disconnected</translation>
    </message>
    <message>
        <source>XML Parsing Error</source>
        <translation>XML Parsing Error</translation>
    </message>
    <message>
        <source>XMPP Protocol Error</source>
        <translation>XMPP Protocol Error</translation>
    </message>
    <message>
        <source>Generic stream error</source>
        <translation>Generic stream error</translation>
    </message>
    <message>
        <source>Conflict(remote login replacing this one)</source>
        <translation>Conflict(remote login replacing this one)</translation>
    </message>
    <message>
        <source>Timed out from inactivity</source>
        <translation>Timed out from inactivity</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation>Internal server error</translation>
    </message>
    <message>
        <source>Invalid XML</source>
        <translation>Invalid XML</translation>
    </message>
    <message>
        <source>Policy violation</source>
        <translation>Policy violation</translation>
    </message>
    <message>
        <source>Server out of resources</source>
        <translation>Server out of resources</translation>
    </message>
    <message>
        <source>Server is shutting down</source>
        <translation>Server is shutting down</translation>
    </message>
    <message>
        <source>XMPP Stream Error: %1</source>
        <translation>XMPP Stream Error: %1</translation>
    </message>
    <message>
        <source>Unable to connect to server</source>
        <translation>Unable to connect to server</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Host not found</translation>
    </message>
    <message>
        <source>Error connecting to proxy</source>
        <translation>Error connecting to proxy</translation>
    </message>
    <message>
        <source>Error during proxy negotiation</source>
        <translation>Error during proxy negotiation</translation>
    </message>
    <message>
        <source>Proxy authentication failed</source>
        <translation>Proxy authentication failed</translation>
    </message>
    <message>
        <source>Socket/stream error</source>
        <translation>Socket/stream error</translation>
    </message>
    <message>
        <source>Connection Error: %1</source>
        <translation>Connection Error: %1</translation>
    </message>
    <message>
        <source>Host no longer hosted</source>
        <translation>Host no longer hosted</translation>
    </message>
    <message>
        <source>Host unknown</source>
        <translation>Host unknown</translation>
    </message>
    <message>
        <source>A required remote connection failed</source>
        <translation>A required remote connection failed</translation>
    </message>
    <message>
        <source>See other host: %1</source>
        <translation>See other host: %1</translation>
    </message>
    <message>
        <source>Server does not support proper XMPP version</source>
        <translation>Server does not support proper XMPP version</translation>
    </message>
    <message>
        <source>Stream Negotiation Error: %1</source>
        <translation>Stream Negotiation Error: %1</translation>
    </message>
    <message>
        <source>Server rejected STARTTLS</source>
        <translation>Server rejected STARTTLS</translation>
    </message>
    <message>
        <source>TLS handshake error</source>
        <translation>TLS handshake error</translation>
    </message>
    <message>
        <source>Broken security layer (TLS)</source>
        <translation>Broken security layer (TLS)</translation>
    </message>
    <message>
        <source>Unable to login</source>
        <translation>Unable to login</translation>
    </message>
    <message>
        <source>No appropriate mechanism available for given security settings(e.g. SASL library too weak, or plaintext authentication not enabled)</source>
        <translation>No appropriate mechanism available for given security settings(e.g. SASL library too weak, or plaintext authentication not enabled)</translation>
    </message>
    <message>
        <source>Bad server response</source>
        <translation>Bad server response</translation>
    </message>
    <message>
        <source>Server failed mutual authentication</source>
        <translation>Server failed mutual authentication</translation>
    </message>
    <message>
        <source>Encryption required for chosen SASL mechanism</source>
        <translation>Encryption required for chosen SASL mechanism</translation>
    </message>
    <message>
        <source>Invalid account information</source>
        <translation>Invalid account information</translation>
    </message>
    <message>
        <source>Invalid SASL mechanism</source>
        <translation>Invalid SASL mechanism</translation>
    </message>
    <message>
        <source>Invalid realm</source>
        <translation>Invalid realm</translation>
    </message>
    <message>
        <source>SASL mechanism too weak for this account</source>
        <translation>SASL mechanism too weak for this account</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation>Not authorized</translation>
    </message>
    <message>
        <source>Temporary auth failure</source>
        <translation>Temporary auth failure</translation>
    </message>
    <message>
        <source>Authentication error: %1</source>
        <translation>Authentication error: %1</translation>
    </message>
    <message>
        <source>Broken security layer (SASL)</source>
        <translation>Broken security layer (SASL)</translation>
    </message>
    <message>
        <source>None</source>
        <translation>None</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <source>XMPP/Jabber</source>
        <translation>XMPP/Jabber</translation>
    </message>
    <message>
        <source>Port for data transfers</source>
        <translation>Port for data transfers</translation>
    </message>
</context>
<context>
    <name>CertificateDisplayDialog</name>
    <message>
        <source>Certificate Information</source>
        <translation>Certificate Information</translation>
    </message>
    <message>
        <source>Certificate Validation</source>
        <translation>Certificate Validation</translation>
    </message>
    <message>
        <source>Valid From</source>
        <translation>Valid From</translation>
    </message>
    <message>
        <source>Valid Until</source>
        <translation>Valid Until</translation>
    </message>
    <message>
        <source>Serial Number</source>
        <translation>Serial Number</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <source>The certificate is valid.</source>
        <translation>The certificate is valid.</translation>
    </message>
    <message>
        <source>The certificate is NOT valid!</source>
        <translation>The certificate is NOT valid!</translation>
    </message>
    <message>
        <source>Reason: %1.</source>
        <translation>Reason: %1.</translation>
    </message>
    <message>
        <source>Subject Details:</source>
        <translation>Subject Details:</translation>
    </message>
    <message>
        <source>Issuer Details:</source>
        <translation>Issuer Details:</translation>
    </message>
    <message>
        <source>Organization:</source>
        <translation>Organization:</translation>
    </message>
    <message>
        <source>Organizational unit:</source>
        <translation>Organizational unit:</translation>
    </message>
    <message>
        <source>Locality:</source>
        <translation>Locality:</translation>
    </message>
    <message>
        <source>State:</source>
        <translation>State:</translation>
    </message>
    <message>
        <source>Country:</source>
        <translation>Country:</translation>
    </message>
    <message>
        <source>Common name:</source>
        <translation>Common name:</translation>
    </message>
    <message>
        <source>Domain name:</source>
        <translation>Domain name:</translation>
    </message>
    <message>
        <source>XMPP name:</source>
        <translation>XMPP name:</translation>
    </message>
    <message>
        <source>Email:</source>
        <translation>Email:</translation>
    </message>
</context>
<context>
    <name>CertificateErrorWindow</name>
    <message>
        <source>Cannot establish secure connection with server &lt;i&gt;%1&lt;/i&gt;.</source>
        <translation>Cannot establish secure connection with server &lt;i&gt;%1&lt;/i&gt;.</translation>
    </message>
    <message>
        <source>Show certificate...</source>
        <translation>Show certificate...</translation>
    </message>
    <message>
        <source>If you do not trust &lt;i&gt;%1&lt;/i&gt;, cancel the connection.</source>
        <translation>If you do not trust &lt;i&gt;%1&lt;/i&gt;, cancel the connection.</translation>
    </message>
    <message>
        <source>Remember my choice for this certificate</source>
        <translation>Remember my choice for this certificate</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Connect</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>FacebookProtocolFactory</name>
    <message>
        <source>Facebook ID:</source>
        <translation>Facebook ID:</translation>
    </message>
    <message>
        <source>Your username is available at &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; under Username field. If this field is empty, you can choose your Username and enter it there.</source>
        <translation>Your username is available at &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; under Username field. If this field is empty, you can choose your Username and enter it there.</translation>
    </message>
</context>
<context>
    <name>GTalkProtocolFactory</name>
    <message>
        <source>Gmail/Google Talk ID:</source>
        <translation>Gmail/Google Talk ID:</translation>
    </message>
</context>
<context>
    <name>HttpConnect</name>
    <message>
        <source>Authentication failed</source>
        <translation>Authentication failed</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Host not found</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Access denied</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Connection refused</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Invalid reply</translation>
    </message>
</context>
<context>
    <name>HttpProxyGetStream</name>
    <message>
        <source>Authentication failed</source>
        <translation>Authentication failed</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Host not found</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Access denied</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Connection refused</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Invalid reply</translation>
    </message>
</context>
<context>
    <name>HttpProxyPost</name>
    <message>
        <source>Authentication failed</source>
        <translation>Authentication failed</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Host not found</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation>Access denied</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>Connection refused</translation>
    </message>
    <message>
        <source>Invalid reply</source>
        <translation>Invalid reply</translation>
    </message>
</context>
<context>
    <name>JabberActions</name>
    <message>
        <source>Resend Subscription</source>
        <translation>Resend Subscription</translation>
    </message>
    <message>
        <source>Remove Subscription</source>
        <translation>Remove Subscription</translation>
    </message>
    <message>
        <source>Ask for Subscription</source>
        <translation>Ask for Subscription</translation>
    </message>
</context>
<context>
    <name>JabberAddAccountWidget</name>
    <message>
        <source>&lt;a href=&apos;#&apos;&gt;What is my username?&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;#&apos;&gt;What is my username?&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Remember Password</source>
        <translation>Remember Password</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Account Identity</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Add Account</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>JabberChangePasswordWindow</name>
    <message>
        <source>Change Password</source>
        <translation>Change Password</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation>Old Password</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>New password</source>
        <translation>New password</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Retype new password</source>
        <translation>Retype new password</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Changing password was successful.</source>
        <translation>Changing password was successful.</translation>
    </message>
    <message>
        <source>An error has occurred. Please try again later.</source>
        <translation>An error has occurred. Please try again later.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;Password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>Invalid data entered in required fields.

Password entered in both fields (&quot;Password&quot; and &quot;Retype password&quot;) must be the same!</translation>
    </message>
</context>
<context>
    <name>JabberContactPersonalInfoWidget</name>
    <message>
        <source>Full Name</source>
        <translation>Full Name</translation>
    </message>
    <message>
        <source>Family Name</source>
        <translation>Family Name</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Nickname</translation>
    </message>
    <message>
        <source>Birthdate</source>
        <translation>Birthdate</translation>
    </message>
    <message>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Website</translation>
    </message>
</context>
<context>
    <name>JabberCreateAccountWidget</name>
    <message>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Retype Password</source>
        <translation>Retype Password</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Remember password</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Account Identity</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>More options:</source>
        <translation>More options:</translation>
    </message>
    <message>
        <source>Connection settings</source>
        <translation>Connection settings</translation>
    </message>
    <message>
        <source>Manually Specify Server Host/Port</source>
        <translation>Manually Specify Server Host/Port</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Encrypt connection</source>
        <translation>Encrypt connection</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Always</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>When available</translation>
    </message>
    <message>
        <source>Legacy SSL</source>
        <translation>Legacy SSL</translation>
    </message>
    <message>
        <source>Probe legacy SSL port</source>
        <translation>Probe legacy SSL port</translation>
    </message>
    <message>
        <source>Register Account</source>
        <translation>Register Account</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Cannot enable secure connection. SSL/TLS plugin not found.</translation>
    </message>
    <message>
        <source>Legacy secure connection (SSL) is only available in combination with manual host/port.</source>
        <translation>Legacy secure connection (SSL) is only available in combination with manual host/port.</translation>
    </message>
    <message>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;New password&quot; and &quot;Retype password&quot;) must be the same!</source>
        <translation>Invalid data entered in required fields.

Password entered in both fields (&quot;New password&quot; and &quot;Retype password&quot;) must be the same!</translation>
    </message>
</context>
<context>
    <name>JabberEditAccountWidget</name>
    <message>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Delete account</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Remember password</translation>
    </message>
    <message>
        <source>Change your password</source>
        <translation>Change your password</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Account Identity</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Personal Information</source>
        <translation>Personal Information</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Connection</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>Never</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Always</translation>
    </message>
    <message>
        <source>When available</source>
        <translation>When available</translation>
    </message>
    <message>
        <source>Allow plaintext authentication</source>
        <translation>Allow plaintext authentication</translation>
    </message>
    <message>
        <source>Over encrypted connection</source>
        <translation>Over encrypted connection</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Resource</source>
        <translation>Resource</translation>
    </message>
    <message>
        <source>Priority</source>
        <translation>Priority</translation>
    </message>
    <message>
        <source>Data transfer proxy</source>
        <translation>Data transfer proxy</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation>Cannot enable secure connection. SSL/TLS plugin not found.</translation>
    </message>
    <message>
        <source>Legacy SSL is only available in combination with manual host/port.</source>
        <translation>Legacy SSL is only available in combination with manual host/port.</translation>
    </message>
    <message>
        <source>Remove account</source>
        <translation>Remove account</translation>
    </message>
    <message>
        <source>Proxy configuration</source>
        <translation>Proxy configuration</translation>
    </message>
    <message>
        <source>Publish system information</source>
        <translation>Publish system information</translation>
    </message>
    <message>
        <source>XMPP Server</source>
        <translation>XMPP Server</translation>
    </message>
    <message>
        <source>Use custom server address/port</source>
        <translation>Use custom server address/port</translation>
    </message>
    <message>
        <source>Server address</source>
        <translation>Server address</translation>
    </message>
    <message>
        <source>Use encrypted connection</source>
        <translation>Use encrypted connection</translation>
    </message>
    <message>
        <source>Only in older version</source>
        <translation>Only in older version</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Network</translation>
    </message>
    <message>
        <source>Use computer name as a resource</source>
        <translation>Use computer name as a resource</translation>
    </message>
    <message>
        <source>Enable composing events</source>
        <translation>Enable composing events</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</source>
        <translation>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</translation>
    </message>
    <message>
        <source>Enable chat activity events</source>
        <translation>Enable chat activity events</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you suspend or end conversation. And vice versa.</source>
        <translation>Your interlocutor will be notified when you suspend or end conversation. And vice versa.</translation>
    </message>
    <message>
        <source>Others can see your system name/version</source>
        <translation>Others can see your system name/version</translation>
    </message>
    <message>
        <source>Confrim Account Removal</source>
        <translation>Confrim Account Removal</translation>
    </message>
    <message>
        <source>Are you sure do you want to remove account %1 (%2)?</source>
        <translation>Are you sure do you want to remove account %1 (%2)?</translation>
    </message>
</context>
<context>
    <name>JabberPersonalInfoWidget</name>
    <message>
        <source>Full name</source>
        <translation>Full name</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Nick</translation>
    </message>
    <message>
        <source>Family name</source>
        <translation>Family name</translation>
    </message>
    <message>
        <source>Birth year</source>
        <translation>Birth year</translation>
    </message>
    <message>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Website</translation>
    </message>
</context>
<context>
    <name>JabberProtocolFactory</name>
    <message>
        <source>User JID:</source>
        <translation>User JID:</translation>
    </message>
</context>
<context>
    <name>JabberServerRegisterAccount</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>This server does not support registration</source>
        <translation>This server does not support registration</translation>
    </message>
    <message>
        <source>There was an error registering the account.
Reason: %1</source>
        <translation>There was an error registering the account.
Reason: %1</translation>
    </message>
</context>
<context>
    <name>JabberWaitForAccountRegisterWindow</name>
    <message>
        <source>Plase wait. New XMPP account is being registered.</source>
        <translation>Plase wait. New XMPP account is being registered.</translation>
    </message>
    <message>
        <source>Registration was successful. Your new XMPP username is %1.
Store it in a safe place along with the password.
Now please add your friends to the buddy list.</source>
        <translation>Registration was successful. Your new XMPP username is %1.
Store it in a safe place along with the password.
Now please add your friends to the buddy list.</translation>
    </message>
    <message>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation>An error has occurred during registration. Please try again later.</translation>
    </message>
    <message>
        <source>Registering new XMPP account</source>
        <translation>Registering new XMPP account</translation>
    </message>
</context>
<context>
    <name>MiniClient</name>
    <message>
        <source>Server Authentication</source>
        <translation>Server Authentication</translation>
    </message>
    <message>
        <source>Server Error</source>
        <translation>Server Error</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>The server does not support TLS encryption.</translation>
    </message>
    <message>
        <source>There was an error communicating with the Jabber server.
Details: %1</source>
        <translation>There was an error communicating with the Jabber server.
Details: %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>The server did not present a certificate.</source>
        <translation>The server did not present a certificate.</translation>
    </message>
    <message>
        <source>Certificate is valid.</source>
        <translation>Certificate is valid.</translation>
    </message>
    <message>
        <source>The hostname does not match the one the certificate was issued to.</source>
        <translation>The hostname does not match the one the certificate was issued to.</translation>
    </message>
    <message>
        <source>General certificate validation error.</source>
        <translation>General certificate validation error.</translation>
    </message>
    <message>
        <source>No certificate presented.</source>
        <translation>No certificate presented.</translation>
    </message>
    <message>
        <source>Hostname mismatch.</source>
        <translation>Hostname mismatch.</translation>
    </message>
    <message>
        <source>Invalid Certificate.</source>
        <translation>Invalid Certificate.</translation>
    </message>
    <message>
        <source>General validation error.</source>
        <translation>General validation error.</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Nickname</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>First Name</source>
        <translation>First Name</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Last Name</translation>
    </message>
    <message>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Address</translation>
    </message>
    <message>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <source>State</source>
        <translation>State</translation>
    </message>
    <message>
        <source>Zipcode</source>
        <translation>Zipcode</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <source>Misc</source>
        <translation>Misc</translation>
    </message>
</context>
<context>
    <name>ShowXmlConsoleActionDescription</name>
    <message>
        <source>Show XML Console</source>
        <translation>Show XML Console</translation>
    </message>
</context>
<context>
    <name>TestDlg</name>
    <message>
        <source>XMPP Test</source>
        <translation>XMPP Test</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>&amp;Connect</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>About %1</translation>
    </message>
    <message>
        <source>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Currently supports:
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</source>
        <translation>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Currently supports:
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Please enter the Full JID to connect with.</source>
        <translation>Please enter the Full JID to connect with.</translation>
    </message>
    <message>
        <source>You must specify a host:port for the proxy.</source>
        <translation>You must specify a host:port for the proxy.</translation>
    </message>
    <message>
        <source>You must at least enter a URL to use http poll.</source>
        <translation>You must at least enter a URL to use http poll.</translation>
    </message>
    <message>
        <source>Please enter the proxy host in the form &apos;host:port&apos;.</source>
        <translation>Please enter the proxy host in the form &apos;host:port&apos;.</translation>
    </message>
    <message>
        <source>Please enter the host in the form &apos;host:port&apos;.</source>
        <translation>Please enter the host in the form &apos;host:port&apos;.</translation>
    </message>
    <message>
        <source>Error: SSF Min is greater than SSF Max.</source>
        <translation>Error: SSF Min is greater than SSF Max.</translation>
    </message>
    <message>
        <source>Error: TLS not available.  Disable any TLS options.</source>
        <translation>Error: TLS not available.  Disable any TLS options.</translation>
    </message>
    <message>
        <source>&amp;Disconnect</source>
        <translation>&amp;Disconnect</translation>
    </message>
    <message>
        <source>Bad XML input (%1,%2): %3
Please correct and try again.</source>
        <translation>Bad XML input (%1,%2): %3
Please correct and try again.</translation>
    </message>
    <message>
        <source>Bad Stanza &apos;%1&apos;.  Must be &apos;message&apos;, &apos;presence&apos;, or &apos;iq&apos;</source>
        <translation>Bad Stanza &apos;%1&apos;.  Must be &apos;message&apos;, &apos;presence&apos;, or &apos;iq&apos;</translation>
    </message>
    <message>
        <source>You must enter at least one stanza!</source>
        <translation>You must enter at least one stanza!</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Enter the password for %1</source>
        <translation>Enter the password for %1</translation>
    </message>
</context>
<context>
    <name>XMPP::ClientStream</name>
    <message>
        <source>Offered mechanisms: </source>
        <translation>Offered mechanisms: </translation>
    </message>
</context>
<context>
    <name>XMPP::Features::FeatureName</name>
    <message>
        <source>ERROR: Incorrect usage of Features class</source>
        <translation>ERROR: Incorrect usage of Features class</translation>
    </message>
    <message>
        <source>None</source>
        <translation>None</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>Register</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Search</translation>
    </message>
    <message>
        <source>Groupchat</source>
        <translation>Groupchat</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation>Gateway</translation>
    </message>
    <message>
        <source>Service Discovery</source>
        <translation>Service Discovery</translation>
    </message>
    <message>
        <source>VCard</source>
        <translation>VCard</translation>
    </message>
    <message>
        <source>Execute command</source>
        <translation>Execute command</translation>
    </message>
    <message>
        <source>Add to roster</source>
        <translation>Add to roster</translation>
    </message>
</context>
<context>
    <name>XMPP::JT_VCard</name>
    <message>
        <source>No VCard available</source>
        <translation>No VCard available</translation>
    </message>
</context>
<context>
    <name>XMPP::JabberConnectionService</name>
    <message>
        <source>security problem</source>
        <translation>security problem</translation>
    </message>
    <message>
        <source>TLS certificate not accepted</source>
        <translation>TLS certificate not accepted</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>Unknown error</translation>
    </message>
    <message>
        <source>The server does not support TLS encryption.</source>
        <translation>The server does not support TLS encryption.</translation>
    </message>
    <message>
        <source>Connection error.
Details: %1</source>
        <translation>Connection error.
Details: %1</translation>
    </message>
    <message>
        <source>Account disconnected.
Details: %1</source>
        <translation>Account disconnected.
Details: %1</translation>
    </message>
    <message>
        <source>SSL support could not be initialized for account %1. This is most likely because the QCA TLS plugin is not installed on your system.</source>
        <translation>SSL support could not be initialized for account %1. This is most likely because the QCA TLS plugin is not installed on your system.</translation>
    </message>
</context>
<context>
    <name>XMPP::Stanza::Error::Private</name>
    <message>
        <source>Bad request</source>
        <translation>Bad request</translation>
    </message>
    <message>
        <source>The sender has sent XML that is malformed or that cannot be processed.</source>
        <translation>The sender has sent XML that is malformed or that cannot be processed.</translation>
    </message>
    <message>
        <source>Conflict</source>
        <translation>Conflict</translation>
    </message>
    <message>
        <source>Access cannot be granted because an existing resource or session exists with the same name or address.</source>
        <translation>Access cannot be granted because an existing resource or session exists with the same name or address.</translation>
    </message>
    <message>
        <source>Feature not implemented</source>
        <translation>Feature not implemented</translation>
    </message>
    <message>
        <source>The feature requested is not implemented by the recipient or server and therefore cannot be processed.</source>
        <translation>The feature requested is not implemented by the recipient or server and therefore cannot be processed.</translation>
    </message>
    <message>
        <source>Forbidden</source>
        <translation>Forbidden</translation>
    </message>
    <message>
        <source>The requesting entity does not possess the required permissions to perform the action.</source>
        <translation>The requesting entity does not possess the required permissions to perform the action.</translation>
    </message>
    <message>
        <source>Gone</source>
        <translation>Gone</translation>
    </message>
    <message>
        <source>The recipient or server can no longer be contacted at this address.</source>
        <translation>The recipient or server can no longer be contacted at this address.</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation>Internal server error</translation>
    </message>
    <message>
        <source>The server could not process the stanza because of a misconfiguration or an otherwise-undefined internal server error.</source>
        <translation>The server could not process the stanza because of a misconfiguration or an otherwise-undefined internal server error.</translation>
    </message>
    <message>
        <source>Item not found</source>
        <translation>Item not found</translation>
    </message>
    <message>
        <source>The addressed JID or item requested cannot be found.</source>
        <translation>The addressed JID or item requested cannot be found.</translation>
    </message>
    <message>
        <source>JID malformed</source>
        <translation>JID malformed</translation>
    </message>
    <message>
        <source>The sending entity has provided or communicated an XMPP address (e.g., a value of the &apos;to&apos; attribute) or aspect thereof (e.g., a resource identifier) that does not adhere to the syntax defined in Addressing Scheme.</source>
        <translation>The sending entity has provided or communicated an XMPP address (e.g., a value of the &apos;to&apos; attribute) or aspect thereof (e.g., a resource identifier) that does not adhere to the syntax defined in Addressing Scheme.</translation>
    </message>
    <message>
        <source>Not acceptable</source>
        <translation>Not acceptable</translation>
    </message>
    <message>
        <source>The recipient or server understands the request but is refusing to process it because it does not meet criteria defined by the recipient or server (e.g., a local policy regarding acceptable words in messages).</source>
        <translation>The recipient or server understands the request but is refusing to process it because it does not meet criteria defined by the recipient or server (e.g., a local policy regarding acceptable words in messages).</translation>
    </message>
    <message>
        <source>Not allowed</source>
        <translation>Not allowed</translation>
    </message>
    <message>
        <source>The recipient or server does not allow any entity to perform the action.</source>
        <translation>The recipient or server does not allow any entity to perform the action.</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation>Not authorized</translation>
    </message>
    <message>
        <source>The sender must provide proper credentials before being allowed to perform the action, or has provided improper credentials.</source>
        <translation>The sender must provide proper credentials before being allowed to perform the action, or has provided improper credentials.</translation>
    </message>
    <message>
        <source>Recipient unavailable</source>
        <translation>Recipient unavailable</translation>
    </message>
    <message>
        <source>The intended recipient is temporarily unavailable.</source>
        <translation>The intended recipient is temporarily unavailable.</translation>
    </message>
    <message>
        <source>Redirect</source>
        <translation>Redirect</translation>
    </message>
    <message>
        <source>The recipient or server is redirecting requests for this information to another entity, usually temporarily.</source>
        <translation>The recipient or server is redirecting requests for this information to another entity, usually temporarily.</translation>
    </message>
    <message>
        <source>Registration required</source>
        <translation>Registration required</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because registration is required.</source>
        <translation>The requesting entity is not authorized to access the requested service because registration is required.</translation>
    </message>
    <message>
        <source>Remote server not found</source>
        <translation>Remote server not found</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient does not exist.</source>
        <translation>A remote server or service specified as part or all of the JID of the intended recipient does not exist.</translation>
    </message>
    <message>
        <source>Remote server timeout</source>
        <translation>Remote server timeout</translation>
    </message>
    <message>
        <source>A remote server or service specified as part or all of the JID of the intended recipient (or required to fulfill a request) could not be contacted within a reasonable amount of time.</source>
        <translation>A remote server or service specified as part or all of the JID of the intended recipient (or required to fulfill a request) could not be contacted within a reasonable amount of time.</translation>
    </message>
    <message>
        <source>Resource constraint</source>
        <translation>Resource constraint</translation>
    </message>
    <message>
        <source>The server or recipient lacks the system resources necessary to service the request.</source>
        <translation>The server or recipient lacks the system resources necessary to service the request.</translation>
    </message>
    <message>
        <source>Service unavailable</source>
        <translation>Service unavailable</translation>
    </message>
    <message>
        <source>The server or recipient does not currently provide the requested service.</source>
        <translation>The server or recipient does not currently provide the requested service.</translation>
    </message>
    <message>
        <source>Subscription required</source>
        <translation>Subscription required</translation>
    </message>
    <message>
        <source>The requesting entity is not authorized to access the requested service because a subscription is required.</source>
        <translation>The requesting entity is not authorized to access the requested service because a subscription is required.</translation>
    </message>
    <message>
        <source>Undefined condition</source>
        <translation>Undefined condition</translation>
    </message>
    <message>
        <source>The error condition is not one of those defined by the other conditions in this list.</source>
        <translation>The error condition is not one of those defined by the other conditions in this list.</translation>
    </message>
    <message>
        <source>Unexpected request</source>
        <translation>Unexpected request</translation>
    </message>
    <message>
        <source>The recipient or server understood the request but was not expecting it at this time (e.g., the request was out of order).</source>
        <translation>The recipient or server understood the request but was not expecting it at this time (e.g., the request was out of order).</translation>
    </message>
</context>
<context>
    <name>XMPP::Task</name>
    <message>
        <source>Disconnected</source>
        <translation>Disconnected</translation>
    </message>
</context>
</TS>