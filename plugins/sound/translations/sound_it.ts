<?xml version="1.0" ?><!DOCTYPE TS><TS language="it" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Sound</source>
        <translation>Suono</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notifiche</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Sound theme</source>
        <translation>Thema di suoni</translation>
    </message>
    <message>
        <source>Sound paths</source>
        <translation>Percorso per suoni</translation>
    </message>
    <message>
        <source>Test sound playing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Play a sound</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enable sound notifications</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SoundActions</name>
    <message>
        <source>Play Sounds</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SoundConfigurationUiHandler</name>
    <message>
        <source>Custom</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>