<?xml version="1.0" ?><!DOCTYPE TS><TS language="de" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Sound</source>
        <translation>Klang</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Benachrichtigungen</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Sound theme</source>
        <translation>Klang-Thema</translation>
    </message>
    <message>
        <source>Sound paths</source>
        <translation>Pfad zu den Klängen</translation>
    </message>
    <message>
        <source>Test sound playing</source>
        <translation>Testklang wird abgespielt</translation>
    </message>
    <message>
        <source>Play a sound</source>
        <translation>Spiele einen Klang ab</translation>
    </message>
    <message>
        <source>Enable sound notifications</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SoundActions</name>
    <message>
        <source>Play Sounds</source>
        <translation>Spiele Klänge ab</translation>
    </message>
</context>
<context>
    <name>SoundConfigurationUiHandler</name>
    <message>
        <source>Custom</source>
        <translation>Eigene</translation>
    </message>
</context>
</TS>