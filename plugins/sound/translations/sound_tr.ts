<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Sound</source>
        <translation>Ses</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Uyarılar</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Sound theme</source>
        <translation>Ses teması</translation>
    </message>
    <message>
        <source>Sound paths</source>
        <translation>Ses yolları</translation>
    </message>
    <message>
        <source>Test sound playing</source>
        <translation>Ses çalarak test edin</translation>
    </message>
    <message>
        <source>Play a sound</source>
        <translation>Bir ses çal</translation>
    </message>
    <message>
        <source>Enable sound notifications</source>
        <translation>Sesli uyarılar olsun</translation>
    </message>
</context>
<context>
    <name>SoundActions</name>
    <message>
        <source>Play Sounds</source>
        <translation>Sesleri Çal</translation>
    </message>
</context>
<context>
    <name>SoundConfigurationUiHandler</name>
    <message>
        <source>Custom</source>
        <translation>Özel</translation>
    </message>
</context>
</TS>