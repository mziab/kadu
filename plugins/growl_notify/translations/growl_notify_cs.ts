<?xml version="1.0" ?><!DOCTYPE TS><TS language="cs" version="2.0">
<context>
    <name>GrowlNotify</name>
    <message>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <source>Growl is not installed in your system</source>
        <translation>Growl není nainstalován ve vašem systému</translation>
    </message>
</context>
<context>
    <name>GrowlNotifyConfigurationWidget</name>
    <message>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>%&amp;t - Název (např. nová zpráva) %&amp;m - Obsah oznámení (např. zpráva od Jana), %&amp;d - Podrobnosti (např. obsah zprávy),%&amp;i - obličejík</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Název</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Skladba</translation>
    </message>
</context>
</TS>