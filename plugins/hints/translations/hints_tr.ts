<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Hints</source>
        <translation>İpucular</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Görünüm</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Uyarılar</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation>Yerleşim</translation>
    </message>
    <message>
        <source>New hints go</source>
        <translation>Yeni ipuculara git</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>Otomatik</translation>
    </message>
    <message>
        <source>Own hints position</source>
        <translation>İpucu özel konumu</translation>
    </message>
    <message>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <source>Minimum width</source>
        <translation>Minimum genişlik</translation>
    </message>
    <message>
        <source>Maximum width</source>
        <translation>Maksimum genişlik</translation>
    </message>
    <message>
        <source>Hints position preview</source>
        <translation>İpucu konumu ön izleme</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Zaman aşımı</translation>
    </message>
    <message>
        <source>Font color</source>
        <translation>Yazı tipi rengi</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Arka plan rengi</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Yazı tipi</translation>
    </message>
    <message>
        <source>Left button</source>
        <translation>Sol buton</translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation>Hiçbir şey</translation>
    </message>
    <message>
        <source>Middle button</source>
        <translation>Orta buton</translation>
    </message>
    <message>
        <source>Right button</source>
        <translation>Sağ buton</translation>
    </message>
    <message>
        <source>&apos;Open chat&apos; works on all events</source>
        <translation>Tüm olaylarda &apos;Konuşmayı Aç&apos;</translation>
    </message>
    <message>
        <source>Show message content in hint</source>
        <translation>Mesaj içeriğinde ipucu göster</translation>
    </message>
    <message>
        <source>Number of quoted characters</source>
        <translation>Alıntı karakter sayısı</translation>
    </message>
    <message>
        <source>Delete pending message when user deletes hint</source>
        <translation>Kullanıcı ipuçlarını silerken bekleyen mesajı sil</translation>
    </message>
    <message>
        <source>Buddy List</source>
        <translation>Arkadaş Listesi</translation>
    </message>
    <message>
        <source>Hint Over Buddy</source>
        <translation>Arkadaşın Üstündeyken İpucu</translation>
    </message>
    <message>
        <source>Border color</source>
        <translation>Kenar rengi</translation>
    </message>
    <message>
        <source>Border width</source>
        <translation>Kenar genişliği</translation>
    </message>
    <message>
        <source>Transparency</source>
        <translation>Şeffaflık</translation>
    </message>
    <message>
        <source>Mouse Buttons</source>
        <translation>Fare Butonları</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Sözdizimi</translation>
    </message>
    <message>
        <source>Icon size</source>
        <translation>Simge boyutu</translation>
    </message>
    <message>
        <source>16px</source>
        <translation>16px</translation>
    </message>
    <message>
        <source>22px</source>
        <translation>22px</translation>
    </message>
    <message>
        <source>32px</source>
        <translation>32px</translation>
    </message>
    <message>
        <source>48px</source>
        <translation>48px</translation>
    </message>
    <message>
        <source>64px</source>
        <translation>64px</translation>
    </message>
    <message>
        <source>New Chat/Message</source>
        <translation>Yeni Konuşma/Mesaj</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Gelişmiş</translation>
    </message>
    <message>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation><numerusform>%n saniye</numerusform><numerusform>%n saniye</numerusform></translation>
    </message>
    <message>
        <source>Margin size</source>
        <translation>Kenar boyutu</translation>
    </message>
    <message>
        <source>Hint&apos;s corner</source>
        <translation>İpucu&apos;nun köşesi</translation>
    </message>
    <message>
        <source>On Top</source>
        <translation>En Üstte</translation>
    </message>
    <message>
        <source>On Bottom</source>
        <translation>En Altta</translation>
    </message>
    <message>
        <source>Top Left</source>
        <translation>Sol Üst</translation>
    </message>
    <message>
        <source>Top Right</source>
        <translation>Sağ Üst</translation>
    </message>
    <message>
        <source>Bottom Left</source>
        <translation>Sol Alt</translation>
    </message>
    <message>
        <source>Bottom Right</source>
        <translation>Sağ Alt</translation>
    </message>
    <message>
        <source>Open Chat</source>
        <translation>Konuşma Aç</translation>
    </message>
    <message>
        <source>Delete Hint</source>
        <translation>İpucunu Sil</translation>
    </message>
    <message>
        <source>Delete All Hints</source>
        <translation>Tüm İpuçlarını Sil</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Buradan&lt;/b&gt; ön izleme yapabilirsiniz</translation>
    </message>
    <message>
        <source>Show buttons only if notification requires user&apos;s action</source>
        <translation>Uyarıda kullanıcı etkileşimi gerekliyse sadece butonları göster</translation>
    </message>
    <message>
        <source>Hints size and position...</source>
        <translation>İpucu boyut ve konumu...</translation>
    </message>
</context>
<context>
    <name>HintManager</name>
    <message>
        <source>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</source>
        <translation>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</translation>
    </message>
</context>
<context>
    <name>HintOverUserConfigurationWindow</name>
    <message>
        <source>Hint Over Buddy Configuration</source>
        <translation>Arkadaşın Üstündeyken İpucu Ayarı</translation>
    </message>
    <message>
        <source>Update preview</source>
        <translation>Ön izlemeyi güncelle</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Sözdizimi</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationUiHandler</name>
    <message>
        <source>Configure</source>
        <translation>Ayarla</translation>
    </message>
    <message>
        <source>Hint over buddy list: </source>
        <translation>Arkadaş listesinin üzerinde ipucu:</translation>
    </message>
    <message>
        <source>Advanced hints&apos; configuration</source>
        <translation>Gelişmiş ipucu ayarları</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWidget</name>
    <message>
        <source>Configure</source>
        <translation>Ayarla</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Buradan&lt;/b&gt; ön izleme yapabilirsiniz</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWindow</name>
    <message>
        <source>Hints configuration</source>
        <translation>İpuçları ayarı</translation>
    </message>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Gizleme</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Buradan&lt;/b&gt; ön izleme yapabilirsiniz</translation>
    </message>
</context>
</TS>