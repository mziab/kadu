<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Hints</source>
        <translation>Fumée</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation>Interface</translation>
    </message>
    <message>
        <source>New hints go</source>
        <translation>La nouvelle notification vient de</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <source>Own hints position</source>
        <translation>Position de l&apos;indication</translation>
    </message>
    <message>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <source>Minimum width</source>
        <translation>Largeur minimum</translation>
    </message>
    <message>
        <source>Maximum width</source>
        <translation>Largeur maximum</translation>
    </message>
    <message>
        <source>Hints position preview</source>
        <translation>Aperçu de la position de l&apos;indice</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Compte à rebours</translation>
    </message>
    <message>
        <source>Font color</source>
        <translation>Couleur de la police</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Couleur de fond</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Police</translation>
    </message>
    <message>
        <source>Left button</source>
        <translation>Bouton gauche</translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation>Rien</translation>
    </message>
    <message>
        <source>Middle button</source>
        <translation>Bouton du milieu</translation>
    </message>
    <message>
        <source>Right button</source>
        <translation>Bouton droit</translation>
    </message>
    <message>
        <source>&apos;Open chat&apos; works on all events</source>
        <translation>&apos;ouvrir la conversation&apos; actif sur tous les évènements</translation>
    </message>
    <message>
        <source>Show message content in hint</source>
        <translation>Afficher le contenu du message dans la notification</translation>
    </message>
    <message>
        <source>Number of quoted characters</source>
        <translation>Nombre de caractères cités</translation>
    </message>
    <message>
        <source>Delete pending message when user deletes hint</source>
        <translation>Supprimer les messages en attentes lorsque l&apos;utilisateur ferme la notification</translation>
    </message>
    <message>
        <source>Buddy List</source>
        <translation>Liste de Contacts</translation>
    </message>
    <message>
        <source>Hint Over Buddy</source>
        <translation>Notification par dessus le contact</translation>
    </message>
    <message>
        <source>Border color</source>
        <translation>Couler de bordure</translation>
    </message>
    <message>
        <source>Border width</source>
        <translation>Taille de bordure</translation>
    </message>
    <message>
        <source>Transparency</source>
        <translation>Transparence</translation>
    </message>
    <message>
        <source>Mouse Buttons</source>
        <translation>Boutons de la souris</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Syntaxe</translation>
    </message>
    <message>
        <source>Icon size</source>
        <translation>Dimension des icônes</translation>
    </message>
    <message>
        <source>16px</source>
        <translation>16px</translation>
    </message>
    <message>
        <source>22px</source>
        <translation>22px</translation>
    </message>
    <message>
        <source>32px</source>
        <translation>32px</translation>
    </message>
    <message>
        <source>48px</source>
        <translation>48px</translation>
    </message>
    <message>
        <source>64px</source>
        <translation>64px</translation>
    </message>
    <message>
        <source>New Chat/Message</source>
        <translation>Nouvelle conversation / Nouveau message</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation><numerusform>%n seconde</numerusform><numerusform>%n secondes</numerusform></translation>
    </message>
    <message>
        <source>Margin size</source>
        <translation>Taille de marge</translation>
    </message>
    <message>
        <source>Hint&apos;s corner</source>
        <translation>Coin de la notification</translation>
    </message>
    <message>
        <source>On Top</source>
        <translation>En haut</translation>
    </message>
    <message>
        <source>On Bottom</source>
        <translation>En bas</translation>
    </message>
    <message>
        <source>Top Left</source>
        <translation>À gauche</translation>
    </message>
    <message>
        <source>Top Right</source>
        <translation>À droite</translation>
    </message>
    <message>
        <source>Bottom Left</source>
        <translation>En bas à gauche</translation>
    </message>
    <message>
        <source>Bottom Right</source>
        <translation>En bas à droite</translation>
    </message>
    <message>
        <source>Open Chat</source>
        <translation>Ouvrir une conversation</translation>
    </message>
    <message>
        <source>Delete Hint</source>
        <translation>Supprimer Notification</translation>
    </message>
    <message>
        <source>Delete All Hints</source>
        <translation>Supprimer Toutes les Notification</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>Vous pouvez voir l&apos;aperçu &lt;b&gt;ici&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Show buttons only if notification requires user&apos;s action</source>
        <translation>Afficher les boutons que si la notification requiert l&apos;action de l&apos;utilisateur</translation>
    </message>
    <message>
        <source>Hints size and position...</source>
        <translation>Taille et positionnement de la zone de notification</translation>
    </message>
</context>
<context>
    <name>HintManager</name>
    <message>
        <source>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</source>
        <translation>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</translation>
    </message>
</context>
<context>
    <name>HintOverUserConfigurationWindow</name>
    <message>
        <source>Hint Over Buddy Configuration</source>
        <translation>Notifier par dessus la configuration des contacts</translation>
    </message>
    <message>
        <source>Update preview</source>
        <translation>Actualisé l&apos;aperçu</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Syntaxe</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationUiHandler</name>
    <message>
        <source>Configure</source>
        <translation>Configurer</translation>
    </message>
    <message>
        <source>Hint over buddy list: </source>
        <translation>Notifier par dessus la liste des contacts</translation>
    </message>
    <message>
        <source>Advanced hints&apos; configuration</source>
        <translation>Configuration avancée des notifications</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWidget</name>
    <message>
        <source>Configure</source>
        <translation>Configurer</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>Vous pouvez voir l&apos;aperçu &lt;b&gt;ici&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWindow</name>
    <message>
        <source>Hints configuration</source>
        <translation>Configuration Notification</translation>
    </message>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Ne pas masquer</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>Vous pouvez voir l&apos;aperçu &lt;b&gt;ici&lt;/b&gt;</translation>
    </message>
</context>
</TS>