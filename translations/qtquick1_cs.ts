<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>Debugger::JSAgentWatchData</name>
    <message>
        <location filename="../../qtquick1/src/declarative/debugger/qjsdebuggeragent.cpp" line="+111"/>
        <source>[Array of length %1]</source>
        <translation>[Pole délky %1]</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&lt;undefined&gt;</source>
        <translation>&lt;nevymezeno&gt;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAbstractAnimation</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeanimation.cpp" line="+161"/>
        <source>Cannot animate non-existent property &quot;%1&quot;</source>
        <translation>Vlastnost &apos;%1&quot; neexistuje a nelze ji proto animovat</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot animate read-only property &quot;%1&quot;</source>
        <translation>Vlastnost &apos;%1&quot; je pouze pro čtení a nelze ji proto animovat</translation>
    </message>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeutilmodule.cpp" line="+101"/>
        <location line="+50"/>
        <source>Animation is an abstract class</source>
        <translation>Animation je abstraktní třída</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAnchorAnimation</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeanimation.cpp" line="+2704"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Nelze nastavit dobu trvání &lt; 0</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAnchors</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeanchors.cpp" line="+190"/>
        <source>Possible anchor loop detected on fill.</source>
        <translation>Při operaci naplnění byla zjištěna případná nekonečná smyčka ukotvení.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Possible anchor loop detected on centerIn.</source>
        <translation>Při operaci &apos;centerIn&apos; byla zjištěna případná nekonečná smyčka ukotvení.</translation>
    </message>
    <message>
        <location line="+208"/>
        <location line="+34"/>
        <location line="+646"/>
        <location line="+37"/>
        <source>Cannot anchor to an item that isn&apos;t a parent or sibling.</source>
        <translation>Cílem ukotvení musí být rodičovský prvek nebo prvek na stejné úrovni.</translation>
    </message>
    <message>
        <location line="-570"/>
        <source>Possible anchor loop detected on vertical anchor.</source>
        <translation>Při svislém ukotvení byla zjištěna případná nekonečná smyčka ukotvení.</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Possible anchor loop detected on horizontal anchor.</source>
        <translation>Při vodorovném ukotvení byla zjištěna případná nekonečná smyčka ukotvení.</translation>
    </message>
    <message>
        <location line="+422"/>
        <source>Cannot specify left, right, and hcenter anchors.</source>
        <translation>Nelze zadat ukotvení vlevo, vpravo a vodorovně vystředěné. Nesmí se vyskytovat společně.</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+37"/>
        <source>Cannot anchor to a null item.</source>
        <translation>Nelze dát ukotvení k nulovému prvku.</translation>
    </message>
    <message>
        <location line="-34"/>
        <source>Cannot anchor a horizontal edge to a vertical edge.</source>
        <translation>Nelze ukotvit vodorovný okraj ke svislému.</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+37"/>
        <source>Cannot anchor item to self.</source>
        <translation>Prvek nemůže mít ukotvení k sobě samému.</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>Cannot specify top, bottom, and vcenter anchors.</source>
        <translation>Nelze zadat ukotvení nahoře, dole a svisle vystředěné. Nesmí se vyskytovat společně.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Baseline anchor cannot be used in conjunction with top, bottom, or vcenter anchors.</source>
        <translation>Ukotvení na účaří se nesmí používat společně s dalšími ukotveními nahoře, dole a svisle vystředěnými.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot anchor a vertical edge to a horizontal edge.</source>
        <translation>Nelze ukotvit svislý okraj k vodorovnému.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAnimatedImage</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeitemsmodule.cpp" line="+98"/>
        <location line="+108"/>
        <source>Qt was built without support for QMovie</source>
        <translation>Qt bylo sestaveno bez podpory pro QMovie</translation>
    </message>
</context>
<context>
    <name>QDeclarativeApplication</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeutilmodule.cpp" line="-78"/>
        <source>Application is an abstract class</source>
        <translation>Application je abstraktní třída</translation>
    </message>
</context>
<context>
    <name>QDeclarativeBehavior</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativebehavior.cpp" line="+120"/>
        <source>Cannot change the animation assigned to a Behavior.</source>
        <translation>Animaci patřící k prvku Behavior nelze změnit.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeBinding</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativebinding.cpp" line="+448"/>
        <source>Binding loop detected for property &quot;%1&quot;</source>
        <translation>U vlastnosti &quot;%1&quot; byla zjištěna nekonečná vazebná smyčka</translation>
    </message>
</context>
<context>
    <name>QDeclarativeCompiledBindings</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativecompiledbindings.cpp" line="+388"/>
        <source>Binding loop detected for property &quot;%1&quot;</source>
        <translation>U pro vlastnost &quot;%1&quot; zadané vazby byla zjištěna nekonečná smyčka</translation>
    </message>
</context>
<context>
    <name>QDeclarativeCompiler</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativecompiler.cpp" line="+179"/>
        <location line="+1674"/>
        <location line="+205"/>
        <location line="+81"/>
        <location line="+81"/>
        <location line="+594"/>
        <source>Invalid property assignment: &quot;%1&quot; is a read-only property</source>
        <translation>Neplatné přiřazení vlastnosti: &quot;%1&quot; je vlastnost pouze pro čtení</translation>
    </message>
    <message>
        <location line="-2626"/>
        <source>Invalid property assignment: unknown enumeration</source>
        <translation>Neplatné přiřazení vlastnosti: neplatná výčtová hodnota</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: string expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekáván řetězec</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid property assignment: url expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekávána adresa (URL)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Invalid property assignment: unsigned int expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekávána celočíselná hodnota bez znaménka</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid property assignment: int expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekávána celočíselná hodnota</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+3"/>
        <source>Invalid property assignment: number expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekáváno číslo</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: color expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekáváno vymezení barvy</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: date expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekáván údaj s datem</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: time expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekáván časový údaj</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: datetime expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekáván údaj s datem</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: point expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekáván souřadnicový údaj pro jeden bod</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: size expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekáván údaj s velikostí</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: rect expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekáván parametr pro pravoúhelník</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid property assignment: boolean expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekávána booleánská hodnota</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: 3D vector expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekáván třírozměrný vektor</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: unsupported type &quot;%1&quot;</source>
        <translation>Neplatné přiřazení vlastnosti: Typ &quot;%1&quot; není podporován</translation>
    </message>
    <message>
        <location line="+288"/>
        <source>Element is not creatable.</source>
        <translation>Příkaz nelze vytvořit.</translation>
    </message>
    <message>
        <location line="+650"/>
        <source>Component elements may not contain properties other than id</source>
        <translation>Prvky součástek nesmí kromě ID obsahovat další vlastnosti</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid component id specification</source>
        <translation>Neplatné vymezení součástky</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+515"/>
        <source>id is not unique</source>
        <translation>Hodnota ID není jednoznačná</translation>
    </message>
    <message>
        <location line="-505"/>
        <source>Invalid component body specification</source>
        <translation>Neplatné vymezení obsahu součástky</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Component objects cannot declare new properties.</source>
        <translation>Objekty součástek nemohou prohlásit nové vlastnosti.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component objects cannot declare new signals.</source>
        <translation>Objekty součástek nemohou prohlásit nové signály.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component objects cannot declare new functions.</source>
        <translation>Objekty součástek nemohou prohlásit nové funkce.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot create empty component specification</source>
        <translation>Nelze vytvořit prázdné vymezení součástky</translation>
    </message>
    <message>
        <location line="+88"/>
        <location line="+121"/>
        <source>&quot;%1.%2&quot; is not available in %3 %4.%5.</source>
        <translation>&quot;%1.%2&quot; není dostupný v %3 %4.%5.</translation>
    </message>
    <message>
        <location line="-119"/>
        <location line="+121"/>
        <source>&quot;%1.%2&quot; is not available due to component versioning.</source>
        <translation>&quot;%1.%2&quot; není dostupný kvůli verzování součástky.</translation>
    </message>
    <message>
        <location line="-110"/>
        <source>Incorrectly specified signal assignment</source>
        <translation>Nesprávně vymezené přiřazení signálu</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Cannot assign a value to a signal (expecting a script to be run)</source>
        <translation>K signálu nelze přiřadit hodnotu (očekává se spuštění skriptu)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Empty signal assignment</source>
        <translation>Prázdné přiřazení signálu</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Empty property assignment</source>
        <translation>Prázdné přiřazení vlastnosti</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Attached properties cannot be used here</source>
        <translation>Na tomto místě nemohou být připojené vlastnosti (typu &apos;attached&apos;) používány</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+128"/>
        <source>Non-existent attached object</source>
        <translation>Pro vlastnost neexistuje žádný připojený objekt</translation>
    </message>
    <message>
        <location line="-124"/>
        <location line="+127"/>
        <source>Invalid attached object assignment</source>
        <translation>Neplatné přiřazení připojeného objektu</translation>
    </message>
    <message>
        <location line="-48"/>
        <source>Cannot assign to non-existent default property</source>
        <translation>Nelze provést žádné přiřazení, neboť neexistuje výchozí vlastnost</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+356"/>
        <location line="+3"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Nelze provést žádné přiřazení, neboť neexistuje výchozí vlastnost pojmenovaná jako &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="-329"/>
        <source>Invalid use of namespace</source>
        <translation>Neplatné použití jmenného prostoru</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Not an attached property name</source>
        <translation>Neplatný název pro připojenou vlastnost (typu &apos;attached&apos;)</translation>
    </message>
    <message>
        <location line="+182"/>
        <source>Invalid use of id property</source>
        <translation>Neplatné použití vlastnosti typu &apos;id&apos;</translation>
    </message>
    <message>
        <location line="+87"/>
        <location line="+2"/>
        <source>Property has already been assigned a value</source>
        <translation>Vlastnosti již byla přiřazena hodnota</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+7"/>
        <source>Invalid grouped property access</source>
        <translation>Nesprávné seskupení při přístupu k vlastnosti</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot assign a value directly to a grouped property</source>
        <translation>U vlastnosti, která je částí seskupení, není žádné přímé přiřazení hodnoty přípustné</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Invalid property use</source>
        <translation>Neplatné použití vlastnosti</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Property assignment expected</source>
        <translation>Očekáváno přiřazení vlastnosti</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single property assignment expected</source>
        <translation>Očekáváno jednotlivé přiřazení vlastnosti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unexpected object assignment</source>
        <translation>Nepřípustné přiřazení objektu</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Cannot assign object to list</source>
        <translation>Přiřazení objektu k seznamům není přípustné</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Can only assign one binding to lists</source>
        <translation>K seznamům lze přiřadit pouze jednu jedinou vazbu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot assign primitives to lists</source>
        <translation>Přiřazení jednoduché hodnoty (primitivy) k seznamům není přípustné</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot assign multiple values to a script property</source>
        <translation>Přiřazení více hodnot k vlastnosti skriptu není přípustné</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid property assignment: script expected</source>
        <translation>Neplatné přiřazení vlastnosti: očekáván skript</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Cannot assign multiple values to a singular property</source>
        <translation>Přiřazení více hodnot k jedné vlastnosti není přípustné</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Cannot assign object to property</source>
        <translation>Přiřazení objektu k vlastnosti není přípustné</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>&quot;%1&quot; cannot operate on &quot;%2&quot;</source>
        <translation>&quot;%1&quot; nelze použít na &quot;%2&quot;</translation>
    </message>
    <message>
        <location line="+182"/>
        <source>Duplicate default property</source>
        <translation>Zdvojená výchozí vlastnost</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Duplicate property name</source>
        <translation>Zdvojený název vlastnosti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Property names cannot begin with an upper case letter</source>
        <translation>Názvy vlastností nesmí začínat velkým písmenem</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Illegal property name</source>
        <translation>Neplatný název vlastnosti</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Duplicate signal name</source>
        <translation>Zdvojený název signálu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Signal names cannot begin with an upper case letter</source>
        <translation>Názvy signálů nesmí začínat velkým písmenem</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Illegal signal name</source>
        <translation>Neplatný název signálu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Duplicate method name</source>
        <translation>Zdvojený název metody</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Method names cannot begin with an upper case letter</source>
        <translation>Názvy metod nesmí začínat velkým písmenem</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Illegal method name</source>
        <translation>Neplatný název metody</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Property value set multiple times</source>
        <translation>Vícenásobné přiřazení hodnoty k vlastnosti</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid property nesting</source>
        <translation>Neplatné vkládání vlastností</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Cannot override FINAL property</source>
        <translation>Nelze přepsat vlastnost prohlašovanou jako &apos;FINAL&apos;</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Invalid property type</source>
        <translation>Neplatný typ vlastnosti</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>Invalid empty ID</source>
        <translation>Neplatná, protože prázdná, hodnota ID</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>IDs cannot start with an uppercase letter</source>
        <translation>Hodnoty ID nesmí začínat velkým písmenem</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>IDs must start with a letter or underscore</source>
        <translation>Hodnoty ID musí začínat písmenem nebo znakem _podtržítka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>IDs must contain only letters, numbers, and underscores</source>
        <translation>Hodnoty ID musí obsahovat pouze písmena, číslice nebo znaky _podtržítka</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>ID illegally masks global JavaScript property</source>
        <translation>Hodnota ID nedovoleně zakrývá celkovou vlastnost z JavaScriptu</translation>
    </message>
    <message>
        <location line="+31"/>
        <location line="+9"/>
        <source>No property alias location</source>
        <translation>Vlastnost alias bez umístění</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+25"/>
        <location line="+7"/>
        <location line="+7"/>
        <location line="+6"/>
        <source>Invalid alias location</source>
        <translation>Neplatné umístění alias</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Invalid alias reference. An alias reference must be specified as &lt;id&gt;, &lt;id&gt;.&lt;property&gt; or &lt;id&gt;.&lt;value property&gt;.&lt;property&gt;</source>
        <translation>Neplatné odkazování na vlastnost alias. Odkazování na vlastnost alias musí být určeno jako &lt;id&gt;, &lt;id&gt;.&lt;vlastnost&gt; nebo &lt;id&gt;.&lt;vlastnost hodnoty&gt;.&lt;vlastnost&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid alias reference. Unable to find id &quot;%1&quot;</source>
        <translation>Neplatné odkazování na vlastnost alias. Nelze najít ID &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Alias property exceeds alias bounds</source>
        <translation>Vlastnost alias překračuje vazby alias</translation>
    </message>
</context>
<context>
    <name>QDeclarativeComponent</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativecomponent.cpp" line="+524"/>
        <source>Invalid empty URL</source>
        <translation>Neplátná prázdná adresa (URL)</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>createObject: value is not an object</source>
        <translation>createObject: Hodnota není objektem</translation>
    </message>
</context>
<context>
    <name>QDeclarativeConnections</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeconnections.cpp" line="+201"/>
        <location line="+64"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Nelze provést žádné přiřazení, neboť neexistuje výchozí vlastnost pojmenovaná jako &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="-54"/>
        <source>Connections: nested objects not allowed</source>
        <translation>Connections: vkládané objekty nejsou povoleny</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connections: syntax error</source>
        <translation>Connections: chyba ve skladbě</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Connections: script expected</source>
        <translation>Connections: očekáván skript</translation>
    </message>
</context>
<context>
    <name>QDeclarativeEngine</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativesqldatabase.cpp" line="+203"/>
        <source>executeSql called outside transaction()</source>
        <translation>&apos;executeSql&apos; byl vyvolán mimo &apos;transaction()&apos;</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Read-only Transaction</source>
        <translation>Transakce pouze pro čtení</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Version mismatch: expected %1, found %2</source>
        <translation>Verzi %2 nelze používat; je třeba %1</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>SQL transaction failed</source>
        <translation>Transakce SQL se nezdařila</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>transaction: missing callback</source>
        <translation>Transakce: chybí callback (zavolat znovu)</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+14"/>
        <source>SQL: database version mismatch</source>
        <translation>SQL: Verze databáze neodpovídá očekávané verzi</translation>
    </message>
</context>
<context>
    <name>QDeclarativeFlipable</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeflipable.cpp" line="+131"/>
        <source>front is a write-once property</source>
        <translation>&apos;front&apos; lze přidělit pouze jednou</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>back is a write-once property</source>
        <translation>&apos;back&apos; lze přidělit pouze jednou</translation>
    </message>
</context>
<context>
    <name>QDeclarativeGestureArea</name>
    <message>
        <location filename="../../qtquick1/src/imports/gestures/qdeclarativegesturearea.cpp" line="+176"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Nelze provést žádné přiřazení, neboť neexistuje výchozí vlastnost pojmenovaná jako &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>GestureArea: nested objects not allowed</source>
        <translation>GestureArea: vkládané objekty nejsou povoleny</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>GestureArea: syntax error</source>
        <translation>GestureArea: chyba ve skladbě</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>GestureArea: script expected</source>
        <translation>GestureArea: očekáván skript</translation>
    </message>
</context>
<context>
    <name>QDeclarativeImportDatabase</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativeimport.cpp" line="+373"/>
        <source>plugin cannot be loaded for module &quot;%1&quot;: %2</source>
        <translation>Nelze nahrát přídavný modul pro modul &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>module &quot;%1&quot; plugin &quot;%2&quot; not found</source>
        <translation>Modul &quot;%1&quot; přídavný modul &quot;%2&quot; nenalezen</translation>
    </message>
    <message>
        <location line="+144"/>
        <location line="+68"/>
        <source>module &quot;%1&quot; version %2.%3 is not installed</source>
        <translation>Modul &quot;%1&quot; verze %2.%3 není nainstalován</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>module &quot;%1&quot; is not installed</source>
        <translation>Modul &quot;%1&quot; není nainstalován</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+20"/>
        <source>&quot;%1&quot;: no such directory</source>
        <translation>&quot;%1&quot;: žádný takový adresář není</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>import &quot;%1&quot; has no qmldir and no namespace</source>
        <translation>Zavedení &quot;%1&quot; nemá žádný qmldir a nemá žádný jmenný prostor</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>- %1 is not a namespace</source>
        <translation>- %1 jmenným prostorem</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>- nested namespaces not allowed</source>
        <translation>- vkládané jmenné prostory nejsou povoleny</translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+4"/>
        <source>local directory</source>
        <translation>Místní adresář</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>is ambiguous. Found in %1 and in %2</source>
        <translation>je dvojznačný. Nalezen v %1 a v %2</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>is ambiguous. Found in %1 in version %2.%3 and %4.%5</source>
        <translation>je dvojznačný. Nalezen v %1 ve verzi %2.%3 a %4.%5</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>is instantiated recursively</source>
        <translation>je doložen příkladem rekurzivně</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>is not a type</source>
        <translation>není typ</translation>
    </message>
    <message>
        <location line="+297"/>
        <source>File name case mismatch for &quot;%1&quot;</source>
        <translation>Velikost písmen v názvu souboru neodpovídá pro &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeKeyNavigationAttached</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeitemsmodule.cpp" line="-36"/>
        <location line="+88"/>
        <source>KeyNavigation is only available via attached properties</source>
        <translation>Klávesové navádění (KeyNavigation) je dostupné pouze pomocí připojených vlastností</translation>
    </message>
</context>
<context>
    <name>QDeclarativeKeysAttached</name>
    <message>
        <location line="-87"/>
        <location line="+88"/>
        <source>Keys is only available via attached properties</source>
        <translation>Keys je dostupné pouze pomocí připojených vlastností</translation>
    </message>
</context>
<context>
    <name>QDeclarativeLayoutMirroringAttached</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeitem.cpp" line="+812"/>
        <source>LayoutDirection attached property only works with Items</source>
        <translation>Připojená vlastnost LayoutDirection pracuje jen s položkami</translation>
    </message>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeitemsmodule.cpp" line="-63"/>
        <source>LayoutMirroring is only available via attached properties</source>
        <translation>LayoutMirroring je dostupné pouze prostřednictvím připojených vlastností</translation>
    </message>
</context>
<context>
    <name>QDeclarativeListModel</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativelistmodel.cpp" line="+385"/>
        <source>remove: index %1 out of range</source>
        <translation>odstranit (remove): Index %1 je mimo platnou oblast</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>insert: value is not an object</source>
        <translation>vložit (insert): Hodnota není objektem</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>insert: index %1 out of range</source>
        <translation>vložit (insert): Index %1 je mimo platnou oblast</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>move: out of range</source>
        <translation>přesunout (move): je mimo platnou oblast</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>append: value is not an object</source>
        <translation>připojit (append): Hodnota není objektem</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>set: value is not an object</source>
        <translation>nastavit (set): Hodnota není objektem</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+38"/>
        <source>set: index %1 out of range</source>
        <translation>nastavit (set): Index %1 je mimo platnou oblast</translation>
    </message>
    <message>
        <location line="+37"/>
        <location line="+17"/>
        <source>ListElement: cannot contain nested elements</source>
        <translation>ListElement: nesmí obsahovat vnořené prvky</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>ListElement: cannot use reserved &quot;id&quot; property</source>
        <translation>ListElement: nelze používat vlastnost &quot;ID&quot;</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>ListElement: cannot use script for property value</source>
        <translation>ListElement: nelze používat skript pro hodnotu vlastnosti</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>ListModel: undefined property &apos;%1&apos;</source>
        <translation>ListModel: Vlastnost &apos;%1&apos; není vymezena</translation>
    </message>
</context>
<context>
    <name>QDeclarativeLoader</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeloader.cpp" line="+400"/>
        <source>Loader does not support loading non-visual elements.</source>
        <translation>Nahrávání neviditelných prvků není podporováno.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeParentAnimation</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeanimation.cpp" line="-169"/>
        <source>Unable to preserve appearance under complex transform</source>
        <translation>Při složité proměně nelze zachovat vzhled</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+7"/>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation>Při nejednotné změně velikosti nelze zachovat vzhled</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation>Při změně velikosti s 0 nelze zachovat vzhled</translation>
    </message>
</context>
<context>
    <name>QDeclarativeParentChange</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativestateoperations.cpp" line="+95"/>
        <source>Unable to preserve appearance under complex transform</source>
        <translation>Při složité proměně nelze zachovat vzhled</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+7"/>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation>Při nejednotné změně velikosti nelze zachovat vzhled</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation>Při změně velikosti s 0 nelze zachovat vzhled</translation>
    </message>
</context>
<context>
    <name>QDeclarativeParser</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/parser/qdeclarativejslexer.cpp" line="+537"/>
        <location line="+123"/>
        <location line="+54"/>
        <source>Illegal unicode escape sequence</source>
        <translation>Neplatná úniková posloupnost unicode</translation>
    </message>
    <message>
        <location line="-140"/>
        <source>Illegal character</source>
        <translation>Neplatný znak</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unclosed string at end of line</source>
        <translation>Neuzavřený řetězec na konci řádku</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Illegal escape sequence</source>
        <translation>Neplatná úniková posloupnost</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Unclosed comment at end of file</source>
        <translation>Neuzavřená poznámka na konci souboru</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Illegal syntax for exponential number</source>
        <translation>Neplatná skladba exponenciálního čísla</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Identifier cannot start with numeric literal</source>
        <translation>Identifikátor nemůže začínat s číselným překlepem</translation>
    </message>
    <message>
        <location line="+338"/>
        <source>Unterminated regular expression literal</source>
        <translation>Neuzavřený regulární výraz</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Invalid regular expression flag &apos;%0&apos;</source>
        <translation>Neplatný příznak &apos;%0&apos; u regulárního výrazu</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+22"/>
        <source>Unterminated regular expression backslash sequence</source>
        <translation>Neuzavřená posloupnost se zpětným lomítkem u regulárního výrazu</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unterminated regular expression class</source>
        <translation>Neuzavřená třída u neuzavřeného regulárního výrazu</translation>
    </message>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/parser/qdeclarativejsparser.cpp" line="+1830"/>
        <location line="+67"/>
        <source>Syntax error</source>
        <translation>Chyba ve skladbě</translation>
    </message>
    <message>
        <location line="-65"/>
        <source>Unexpected token `%1&apos;</source>
        <translation>Neočekávaný symbol `%1&apos;</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+24"/>
        <source>Expected token `%1&apos;</source>
        <translation>Očekávaný symbol `%1&apos;</translation>
    </message>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativescriptparser.cpp" line="+246"/>
        <location line="+429"/>
        <location line="+59"/>
        <source>Property value set multiple times</source>
        <translation>Vícenásobné přiřazení hodnoty k vlastnosti</translation>
    </message>
    <message>
        <location line="-477"/>
        <source>Expected type name</source>
        <translation>Očekávaný název typu</translation>
    </message>
    <message>
        <location line="+134"/>
        <source>Invalid import qualifier ID</source>
        <translation>Neplatný údaj o ID při zavádění</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Reserved name &quot;Qt&quot; cannot be used as an qualifier</source>
        <translation>Zamluvený název &quot;Qt&quot; nemůže být použit jako kvalifikant</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Script import qualifiers must be unique.</source>
        <translation>Kvalifikanty zadané pro zavedení skriptu musí být jednoznačné.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Script import requires a qualifier</source>
        <translation>Zavedení skriptu vyžaduje údaj o kvalifikantu</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Library import requires a version</source>
        <translation>Zavedení knihovny vyžaduje údaj o verzi</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Expected parameter type</source>
        <translation>Očekáván typ parametru</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Invalid property type modifier</source>
        <translation>Neplatný modifikátor pro typ vlastnosti</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Unexpected property type modifier</source>
        <translation>Neočekávaný modifikátor pro typ vlastnosti</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Expected property type</source>
        <translation>Očekáván typ vlastnosti</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Readonly not yet supported</source>
        <translation>&quot;Pouze pro čtení&quot; není na tomto místě ještě podporováno</translation>
    </message>
    <message>
        <location line="+218"/>
        <source>JavaScript declaration outside Script element</source>
        <translation>Deklarace JavaScriptu není mimo prvek skriptu přípustná</translation>
    </message>
</context>
<context>
    <name>QDeclarativePauseAnimation</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeanimation.cpp" line="-2125"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Nelze nastavit dobu trvání &lt; 0</translation>
    </message>
</context>
<context>
    <name>QDeclarativePixmap</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativepixmapcache.cpp" line="+303"/>
        <source>Error decoding: %1: %2</source>
        <translation>Chyba při dekódování: %1: %2</translation>
    </message>
    <message>
        <location line="+167"/>
        <location line="+360"/>
        <source>Failed to get image from provider: %1</source>
        <translation>Obrazová data se od poskytovatele nepodařilo získat: %1</translation>
    </message>
    <message>
        <location line="-342"/>
        <location line="+360"/>
        <source>Cannot open: %1</source>
        <translation>Nelze otevřít: %1</translation>
    </message>
</context>
<context>
    <name>QDeclarativePropertyAnimation</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeanimation.cpp" line="+1247"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Nelze nastavit dobu trvání &lt; 0</translation>
    </message>
</context>
<context>
    <name>QDeclarativePropertyChanges</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativepropertychanges.cpp" line="+249"/>
        <source>PropertyChanges does not support creating state-specific objects.</source>
        <translation>PropertyChanges nepodporuje vytváření objektů, které jsou přiřazeny jednomu stavu.</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Nelze provést žádné přiřazení, neboť neexistuje výchozí vlastnost pojmenovaná jako &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot assign to read-only property &quot;%1&quot;</source>
        <translation>Vlastnost &apos;%1&quot; je pouze pro čtení a nelze ji proto přiřadit</translation>
    </message>
</context>
<context>
    <name>QDeclarativeTextInput</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativetextinput.cpp" line="+962"/>
        <location line="+8"/>
        <source>Could not load cursor delegate</source>
        <translation>Nepodařilo se nahrát zástupce kurzoru</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Could not instantiate cursor delegate</source>
        <translation>Nepodařilo se vytvořit instanci zástupce kurzoru</translation>
    </message>
</context>
<context>
    <name>QDeclarativeTypeLoader</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativetypeloader.cpp" line="+929"/>
        <source>Script %1 unavailable</source>
        <translation>Skript %1 nedostupný</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Type %1 unavailable</source>
        <translation>Typ %1 nedostupný</translation>
    </message>
    <message>
        <location line="+189"/>
        <source>Namespace %1 cannot be used as a type</source>
        <translation>Jmenný prostor %1 nelze použít jako typ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
</context>
<context>
    <name>QDeclarativeVME</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativevme.cpp" line="+223"/>
        <source>Unable to create object of type %1</source>
        <translation>Nepodařilo se vytvořit žádný objekt typu %1</translation>
    </message>
    <message>
        <location line="+443"/>
        <source>Cannot assign value %1 to property %2</source>
        <translation>Hodnotu &apos;%1&apos; nelze přiřadit vlastnosti %2</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cannot assign object type %1 with no default method</source>
        <translation>Typ objektu %1 nelze přiřadit, protože neexistuje žádná výchozí metoda</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot connect mismatched signal/slot %1 %vs. %2</source>
        <translation>Nelze vytvořit žádné spojení mezi signálem %1 a zdířkou %2, protože se k sobě nehodí</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot assign an object to signal property %1</source>
        <translation>Vlastnosti signálu %1 nelze přiřadit žádný objekt</translation>
    </message>
    <message>
        <location line="+154"/>
        <source>Cannot assign object to list</source>
        <translation>Přiřazení objektu k seznamům není přípustné</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Cannot assign object to interface property</source>
        <translation>Vlastnosti rozhraní nelze přiřadit žádný objekt</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Unable to create attached object</source>
        <translation>Nepodařilo se vytvořit žádný připojený objekt (typu &apos;attached&apos;)</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Cannot set properties on %1 as it is null</source>
        <translation>Vlastnosti nelze nastavit na %1, protože jsou &apos;null&apos;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeVisualDataModel</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativevisualitemmodel.cpp" line="+1097"/>
        <source>Delegate component must be Item type.</source>
        <translation>Součástka zástupce musí být typu &apos;item&apos;.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeXmlListModel</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeutilmodule.cpp" line="+40"/>
        <location line="+2"/>
        <location line="+49"/>
        <location line="+2"/>
        <source>Qt was built without support for xmlpatterns</source>
        <translation>Qt bylo sestaveno bez podpory pro xmlpatterns</translation>
    </message>
</context>
<context>
    <name>QDeclarativeXmlListModelRole</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativexmllistmodel_p.h" line="+164"/>
        <source>An XmlRole query must not start with &apos;/&apos;</source>
        <translation>Vyhledávání XmlRole nesmí začínat s &apos;/&apos;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeXmlRoleList</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativexmllistmodel.cpp" line="+820"/>
        <source>An XmlListModel query must start with &apos;/&apos; or &quot;//&quot;</source>
        <translation>Vyhledávání XmlListModel nesmí začínat s &apos;/&apos; nebo &quot;//&quot;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location line="-246"/>
        <source>&quot;%1&quot; duplicates a previous role name and will be disabled.</source>
        <translation>&quot;%1&quot; je již zadán jako název předchozí úlohy, a je proto vypnut.</translation>
    </message>
    <message>
        <location line="+525"/>
        <location line="+4"/>
        <source>invalid query: &quot;%1&quot;</source>
        <translation>Neplatný dotaz: &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QmlJSDebugger::QtQuick1::LiveSelectionTool</name>
    <message>
        <location filename="../../qtquick1/src/plugins/qmltooling/qmldbg_inspector/editor/liveselectiontool.cpp" line="+139"/>
        <source>Items</source>
        <translation>Položky</translation>
    </message>
</context>
<context>
    <name>QmlJSDebugger::QtQuick1::ZoomTool</name>
    <message>
        <location filename="../../qtquick1/src/plugins/qmltooling/qmldbg_inspector/editor/zoomtool.cpp" line="+57"/>
        <source>Zoom to &amp;100%</source>
        <translation>Zvětšit na &amp;100%</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom In</source>
        <translation>Přiblížit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom Out</source>
        <translation>Oddálit</translation>
    </message>
</context>
</TS>
