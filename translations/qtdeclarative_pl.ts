<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>CloseButton</name>
    <message>
        <source>Close Tab</source>
        <translation type="vanished">Zamknij kartę</translation>
    </message>
</context>
<context>
    <name>Debugger::JSAgentWatchData</name>
    <message>
        <source>[Array of length %1]</source>
        <translation type="vanished">[Tablica o długości %1]</translation>
    </message>
    <message>
        <source>&lt;undefined&gt;</source>
        <translation type="vanished">&lt;niezdefiniowanej&gt;</translation>
    </message>
</context>
<context>
    <name>FakeReply</name>
    <message>
        <source>Fake error!</source>
        <translation type="vanished">Fałszywy błąd!</translation>
    </message>
    <message>
        <source>Invalid URL</source>
        <translation type="vanished">Niepoprawny URL</translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <source>Services</source>
        <translation type="vanished">Usługi</translation>
    </message>
    <message>
        <source>Hide %1</source>
        <translation type="vanished">Ukryj %1</translation>
    </message>
    <message>
        <source>Hide Others</source>
        <translation type="vanished">Ukryj pozostałe</translation>
    </message>
    <message>
        <source>Show All</source>
        <translation type="vanished">Pokaż wszystko</translation>
    </message>
    <message>
        <source>Preferences...</source>
        <translation type="vanished">Preferencje…</translation>
    </message>
    <message>
        <source>Quit %1</source>
        <translation type="vanished">Zakończ %1</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation type="vanished">%1…</translation>
    </message>
</context>
<context>
    <name>Object</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmlirbuilder.cpp" line="+95"/>
        <location line="+5"/>
        <source>Duplicate method name</source>
        <translation type="unfinished">Powielona nazwa metody</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Method names cannot begin with an upper case letter</source>
        <translation type="unfinished">Nazwy metod nie mogą rozpoczynać się wielką literą</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Illegal method name</source>
        <translation type="unfinished">Niepoprawna nazwa metody</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Duplicate signal name</source>
        <translation type="unfinished">Powielona nazwa sygnału</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Duplicate property name</source>
        <translation type="unfinished">Powielona nazwa właściwości</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Property names cannot begin with an upper case letter</source>
        <translation type="unfinished">Nazwy właściwości nie mogą rozpoczynać się wielką literą</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Duplicate default property</source>
        <translation type="unfinished">Powielona domyślna właściwość</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Property value set multiple times</source>
        <translation type="unfinished">Wartość właściwości ustawiona wielokrotnie</translation>
    </message>
</context>
<context>
    <name>Phonon::</name>
    <message>
        <source>Notifications</source>
        <translation type="vanished">Powiadomienia</translation>
    </message>
    <message>
        <source>Music</source>
        <translation type="vanished">Muzyka</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">Wideo</translation>
    </message>
    <message>
        <source>Communication</source>
        <translation type="vanished">Komunikacja</translation>
    </message>
    <message>
        <source>Games</source>
        <translation type="vanished">Gry</translation>
    </message>
    <message>
        <source>Accessibility</source>
        <translation type="vanished">Dostępność</translation>
    </message>
</context>
<context>
    <name>Phonon::AudioOutput</name>
    <message>
        <source>&lt;html&gt;The audio playback device &lt;b&gt;%1&lt;/b&gt; does not work.&lt;br/&gt;Falling back to &lt;b&gt;%2&lt;/b&gt;.&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;Urządzenie dźwiękowe &lt;b&gt;%1&lt;/b&gt; nie działa.&lt;br/&gt;Przywracanie do &lt;b&gt;%2&lt;/b&gt;.&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;Switching to the audio playback device &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;which just became available and has higher preference.&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;Przełączanie na urządzenie dźwiękowe &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;które właśnie stało się dostępne i ma wyższy priorytet.&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Revert back to device &apos;%1&apos;</source>
        <translation type="vanished">Przywróć do urządzenia &apos;%1&apos;</translation>
    </message>
    <message>
        <source>&lt;html&gt;Switching to the audio playback device &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;which has higher preference or is specifically configured for this stream.&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;Przełączanie na urządzenie dźwiękowe &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;które ma wyższy priorytet lub jest specjalnie skonfigurowane dla tego strumienia.&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::Backend</name>
    <message>
        <source>Warning: You do not seem to have the package gstreamer0.10-plugins-good installed.
          Some video features have been disabled.</source>
        <translation type="vanished">Ostrzeżenie: Wygląda na to, że pakiet gstreamer0.10-plugins-good nie jest zainstalowany w tym systemie.
Niektóre możliwości wideo zostały wyłączone.</translation>
    </message>
    <message>
        <source>Warning: You do not seem to have the base GStreamer plugins installed.
          All audio and video support has been disabled</source>
        <translation type="vanished">Ostrzeżenie: Wygląda na to, że podstawowe wtyczki GStreamer nie są zainstalowane w tym systemie.
Obsługa dźwięku i wideo została wyłączona</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::MediaObject</name>
    <message>
        <source>Cannot start playback. 

Check your GStreamer installation and make sure you 
have libgstreamer-plugins-base installed.</source>
        <translation type="vanished">Nie można rozpocząć odtwarzania.

Sprawdź instalację Gstreamer i upewnij się że
zainstalowałeś libgstreamer-plugins-base.</translation>
    </message>
    <message>
        <source>Plugin codec installation failed for codec: %0</source>
        <translation type="vanished">Błąd podczas instalacji wtyczki dla kodeka: %0</translation>
    </message>
    <message>
        <source>A required codec is missing. You need to install the following codec(s) to play this content: %0</source>
        <translation type="vanished">Brak wymaganego kodeka. Aby odtworzyć zawartość musisz zainstalować poniższy kodek: %0</translation>
    </message>
    <message>
        <source>Could not open media source.</source>
        <translation type="vanished">Nie można otworzyć źródła mediów.</translation>
    </message>
    <message>
        <source>Invalid source type.</source>
        <translation type="vanished">Niepoprawny typ źródła.</translation>
    </message>
    <message>
        <source>Could not locate media source.</source>
        <translation type="vanished">Nie można znaleźć źródła mediów.</translation>
    </message>
    <message>
        <source>Could not open audio device. The device is already in use.</source>
        <translation type="vanished">Nie można otworzyć urządzenia dźwiękowego. Urządzenie jest już używane.</translation>
    </message>
    <message>
        <source>Could not decode media source.</source>
        <translation type="vanished">Nie można zdekodować źródła mediów.</translation>
    </message>
</context>
<context>
    <name>Phonon::MMF</name>
    <message>
        <source>Audio Output</source>
        <translation type="vanished">Wyjście dźwięku</translation>
    </message>
    <message>
        <source>The audio output device</source>
        <translation type="vanished">Wyjściowe urządzenie dźwiękowe</translation>
    </message>
    <message>
        <source>No error</source>
        <translation type="vanished">Brak błędu</translation>
    </message>
    <message>
        <source>Not found</source>
        <translation type="vanished">Nie znaleziono</translation>
    </message>
    <message>
        <source>Out of memory</source>
        <translation type="vanished">Brak pamięci</translation>
    </message>
    <message>
        <source>Not supported</source>
        <translation type="vanished">Nieobsługiwane</translation>
    </message>
    <message>
        <source>Overflow</source>
        <translation type="vanished">Przepełnienie</translation>
    </message>
    <message>
        <source>Underflow</source>
        <translation type="vanished">Niedopełnienie</translation>
    </message>
    <message>
        <source>Already exists</source>
        <translation type="vanished">Już istnieje</translation>
    </message>
    <message>
        <source>Path not found</source>
        <translation type="vanished">Nie znaleziono ścieżki</translation>
    </message>
    <message>
        <source>In use</source>
        <translation type="vanished">W użyciu</translation>
    </message>
    <message>
        <source>Not ready</source>
        <translation type="vanished">Brak gotowości</translation>
    </message>
    <message>
        <source>Access denied</source>
        <translation type="vanished">Odmowa dostępu</translation>
    </message>
    <message>
        <source>Could not connect</source>
        <translation type="vanished">Nie można połączyć</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">Rozłączono</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Brak uprawnień</translation>
    </message>
    <message>
        <source>Insufficient bandwidth</source>
        <translation type="vanished">Niewystarczająca szerokość pasma</translation>
    </message>
    <message>
        <source>Network unavailable</source>
        <translation type="vanished">Sieć niedostępna</translation>
    </message>
    <message>
        <source>Network communication error</source>
        <translation type="vanished">Błąd komunikacji sieciowej</translation>
    </message>
    <message>
        <source>Streaming not supported</source>
        <translation type="vanished">Transmisje strumieniowe nie są obsługiwane</translation>
    </message>
    <message>
        <source>Server alert</source>
        <translation type="vanished">Ostrzeżenie serwera</translation>
    </message>
    <message>
        <source>Invalid protocol</source>
        <translation type="vanished">Nieprawidłowy protokół</translation>
    </message>
    <message>
        <source>Invalid URL</source>
        <translation type="vanished">Nieprawidłowy adres URL</translation>
    </message>
    <message>
        <source>Multicast error</source>
        <translation type="vanished">Błąd multiemisji</translation>
    </message>
    <message>
        <source>Proxy server error</source>
        <translation type="vanished">Błąd serwera pośredniczącego</translation>
    </message>
    <message>
        <source>Proxy server not supported</source>
        <translation type="vanished">Nieobsługiwany serwer pośredniczący</translation>
    </message>
    <message>
        <source>Audio output error</source>
        <translation type="vanished">Błąd wyjściowego sygnału dźwiękowego</translation>
    </message>
    <message>
        <source>Video output error</source>
        <translation type="vanished">Błąd wyjściowego sygnału wideo</translation>
    </message>
    <message>
        <source>Decoder error</source>
        <translation type="vanished">Błąd dekodera</translation>
    </message>
    <message>
        <source>Audio or video components could not be played</source>
        <translation type="vanished">Nie można odtworzyć dźwięku lub wideo</translation>
    </message>
    <message>
        <source>DRM error</source>
        <translation type="vanished">Błąd DRM</translation>
    </message>
    <message>
        <source>Unknown error (%1)</source>
        <translation type="vanished">Nieznany błąd (%1)</translation>
    </message>
</context>
<context>
    <name>Phonon::MMF::AbstractMediaPlayer</name>
    <message>
        <source>Not ready to play</source>
        <translation type="vanished">Brak gotowości odtwarzania</translation>
    </message>
    <message>
        <source>Error opening file</source>
        <translation type="vanished">Błąd otwierania pliku</translation>
    </message>
    <message>
        <source>Error opening URL</source>
        <translation type="vanished">Błąd otwierania adresu URL</translation>
    </message>
    <message>
        <source>Error opening resource</source>
        <translation type="vanished">Błąd otwierania zasobu</translation>
    </message>
    <message>
        <source>Error opening source: resource not opened</source>
        <translation type="vanished">Błąd otwierania źródła: zasób nie został otwarty</translation>
    </message>
    <message>
        <source>Setting volume failed</source>
        <translation type="vanished">Ustawienie głośności zakończone błędem</translation>
    </message>
    <message>
        <source>Loading clip failed</source>
        <translation type="vanished">Załadowanie klipu zakończone błędem</translation>
    </message>
    <message>
        <source>Playback complete</source>
        <translation type="vanished">Zakończono odtwarzanie</translation>
    </message>
    <message>
        <source>Download error</source>
        <translation type="vanished">Błąd pobierania</translation>
    </message>
</context>
<context>
    <name>Phonon::MMF::AbstractVideoPlayer</name>
    <message>
        <source>Pause failed</source>
        <translation type="vanished">Zatrzymanie zakończone błędem</translation>
    </message>
    <message>
        <source>Seek failed</source>
        <translation type="vanished">Wyszukiwanie zakończone błędem</translation>
    </message>
    <message>
        <source>Getting position failed</source>
        <translation type="vanished">Ustalanie pozycji zakończone błędem</translation>
    </message>
    <message>
        <source>Opening clip failed</source>
        <translation type="vanished">Otwieranie klipu zakończone błędem</translation>
    </message>
</context>
<context>
    <name>Phonon::MMF::AudioEqualizer</name>
    <message>
        <source>%1 Hz</source>
        <translation type="vanished">%1 Hz</translation>
    </message>
</context>
<context>
    <name>Phonon::MMF::AudioPlayer</name>
    <message>
        <source>Getting position failed</source>
        <translation type="vanished">Ustalanie pozycji zakończone błędem</translation>
    </message>
</context>
<context>
    <name>Phonon::MMF::DsaVideoPlayer</name>
    <message>
        <source>Video display error</source>
        <translation type="vanished">Błąd wyświetlacza wideo</translation>
    </message>
</context>
<context>
    <name>Phonon::MMF::EffectFactory</name>
    <message>
        <source>Enabled</source>
        <translation type="vanished">Włączono</translation>
    </message>
</context>
<context>
    <name>Phonon::MMF::EnvironmentalReverb</name>
    <message>
        <source>Decay HF ratio (%)</source>
        <extracomment>DecayHFRatio: Ratio of high-frequency decay time to the value specified by DecayTime.</extracomment>
        <translation type="vanished">Współczynnik zanikania HF (%)</translation>
    </message>
    <message>
        <source>Decay time (ms)</source>
        <extracomment>DecayTime: Time over which reverberation is diminished.</extracomment>
        <translation type="vanished">Czas zanikania (ms)</translation>
    </message>
    <message>
        <source>Density (%)</source>
        <extracomment>Density Delay between first and subsequent reflections. Note that the S60 platform documentation does not make clear the distinction between this value and the Diffusion value.</extracomment>
        <translation type="vanished">Gęstość (%)</translation>
    </message>
    <message>
        <source>Diffusion (%)</source>
        <extracomment>Diffusion: Delay between first and subsequent reflections. Note that the S60 platform documentation does not make clear the distinction between this value and the Density value.</extracomment>
        <translation type="vanished">Rozpraszanie (%)</translation>
    </message>
    <message>
        <source>Reflections delay (ms)</source>
        <extracomment>ReflectionsDelay: Amount of delay between the arrival of the direct path from the source and the arrival of the first reflection.</extracomment>
        <translation type="vanished">Opóźnienie odbić (ms)</translation>
    </message>
    <message>
        <source>Reflections level (mB)</source>
        <extracomment>ReflectionsLevel: Amplitude of reflections. This value is corrected by the RoomLevel to give the final reflection amplitude.</extracomment>
        <translation type="vanished">Poziom odbić (mB)</translation>
    </message>
    <message>
        <source>Reverb delay (ms)</source>
        <extracomment>ReverbDelay: Amount of time between arrival of the first reflection and start of the late reverberation.</extracomment>
        <translation type="vanished">Opóźnienie pogłosu (ms)</translation>
    </message>
    <message>
        <source>Reverb level (mB)</source>
        <extracomment>ReverbLevel: Amplitude of reverberations. This value is corrected by the RoomLevel to give the final reverberation amplitude.</extracomment>
        <translation type="vanished">Poziom pogłosu (mB)</translation>
    </message>
    <message>
        <source>Room HF level</source>
        <extracomment>RoomHFLevel: Amplitude of low-pass filter used to attenuate the high frequency component of reflected sound.</extracomment>
        <translation type="vanished">Poziom HF pomieszczenia</translation>
    </message>
    <message>
        <source>Room level (mB)</source>
        <extracomment>RoomLevel: Master volume control for all reflected sound.</extracomment>
        <translation type="vanished">Poziom pomieszczenia (mB)</translation>
    </message>
</context>
<context>
    <name>Phonon::MMF::MediaObject</name>
    <message>
        <source>Error opening source: type not supported</source>
        <translation type="vanished">Błąd otwierania źródła: typ źródła nie jest obsługiwany</translation>
    </message>
    <message>
        <source>Error opening source: resource is compressed</source>
        <translation type="vanished">Błąd otwierania źródła: zasób jest skompresowany</translation>
    </message>
    <message>
        <source>Error opening source: resource not valid</source>
        <translation type="vanished">Błąd otwierania źródła: niepoprawny zasób</translation>
    </message>
    <message>
        <source>Error opening source: media type could not be determined</source>
        <translation type="vanished">Błąd otwierania źródła: nie można określić typu multimediów</translation>
    </message>
    <message>
        <source>Failed to set requested IAP</source>
        <translation type="vanished">Nie można ustawić żądanego IAP</translation>
    </message>
</context>
<context>
    <name>Phonon::MMF::StereoWidening</name>
    <message>
        <source>Level (%)</source>
        <translation type="vanished">Poziom (%)</translation>
    </message>
</context>
<context>
    <name>Phonon::MMF::SurfaceVideoPlayer</name>
    <message>
        <source>Video display error</source>
        <translation type="vanished">Błąd wyświetlacza wideo</translation>
    </message>
</context>
<context>
    <name>Phonon::VolumeSlider</name>
    <message>
        <source>Volume: %1%</source>
        <translation type="vanished">Głośność: %1%</translation>
    </message>
    <message>
        <source>Use this slider to adjust the volume. The leftmost position is 0%. The rightmost is %1%</source>
        <translation type="vanished">Użyj tego suwaka aby zmienić głośność. Skrajnie lewa pozycja to 0%. Skrajnie prawa to %1%</translation>
    </message>
    <message>
        <source>Muted</source>
        <translation type="vanished">Wyciszony</translation>
    </message>
</context>
<context>
    <name>Q3Accel</name>
    <message>
        <source>%1, %2 not defined</source>
        <translation type="vanished">%1, %2 nie określone</translation>
    </message>
    <message>
        <source>Ambiguous %1 not handled</source>
        <translation type="vanished">Niejednoznaczne %1, nie obsłużone</translation>
    </message>
</context>
<context>
    <name>Q3DataTable</name>
    <message>
        <source>Delete</source>
        <translation type="vanished">Usuń</translation>
    </message>
    <message>
        <source>False</source>
        <translation type="vanished">Fałsz</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="vanished">Wstaw</translation>
    </message>
    <message>
        <source>True</source>
        <translation type="vanished">Prawda</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Uaktualnij</translation>
    </message>
</context>
<context>
    <name>Q3FileDialog</name>
    <message>
        <source>%1
File not found.
Check path and filename.</source>
        <translation type="vanished">%1
Plik nie znaleziony.
Sprawdź ścieżkę i nazwę pliku.</translation>
    </message>
    <message>
        <source>&lt;qt&gt;Are you sure you wish to delete %1 &quot;%2&quot;?&lt;/qt&gt;</source>
        <translation type="vanished">&lt;qt&gt;Na pewno chcesz skasować %1 &quot;%2&quot;?&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation type="vanished">Wszystkie pliki (*)</translation>
    </message>
    <message>
        <source>Attributes</source>
        <translation type="vanished">Atrybuty</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Powrót</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Anuluj</translation>
    </message>
    <message>
        <source>Copy or Move a File</source>
        <translation type="vanished">Kopiuj lub przenieś plik</translation>
    </message>
    <message>
        <source>Create New Folder</source>
        <translation type="vanished">Utwórz nowy katalog</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Data</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Usuń</translation>
    </message>
    <message>
        <source>Delete %1</source>
        <translation type="vanished">Usuń %1</translation>
    </message>
    <message>
        <source>Detail View</source>
        <translation type="vanished">Szczegóły</translation>
    </message>
    <message>
        <source>Dir</source>
        <translation type="vanished">Katalog</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation type="vanished">Katalogi</translation>
    </message>
    <message>
        <source>Directory:</source>
        <translation type="vanished">Katalog:</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Błąd</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Plik</translation>
    </message>
    <message>
        <source>File &amp;name:</source>
        <translation type="vanished">Nazwa &amp;pliku:</translation>
    </message>
    <message>
        <source>File &amp;type:</source>
        <translation type="vanished">&amp;Rodzaj pliku:</translation>
    </message>
    <message>
        <source>Find Directory</source>
        <translation type="vanished">Znajdź katalog</translation>
    </message>
    <message>
        <source>Inaccessible</source>
        <translation type="vanished">Niedostępny</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">Lista</translation>
    </message>
    <message>
        <source>Look &amp;in:</source>
        <translation type="vanished">Sprawdź &amp;w:</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nazwa</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="vanished">Nowy katalog</translation>
    </message>
    <message>
        <source>New Folder %1</source>
        <translation type="vanished">Nowy katalog %1</translation>
    </message>
    <message>
        <source>New Folder 1</source>
        <translation type="vanished">Nowy katalog 1</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="vanished">&amp;Nie</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="vanished">&amp;OK</translation>
    </message>
    <message>
        <source>One directory up</source>
        <translation type="vanished">Katalog wyżej</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">&amp;Otwórz</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Otwórz</translation>
    </message>
    <message>
        <source>Preview File Contents</source>
        <translation type="vanished">Podgląd zawartości pliku</translation>
    </message>
    <message>
        <source>Preview File Info</source>
        <translation type="vanished">Podgląd informacji o pliku</translation>
    </message>
    <message>
        <source>Read: %1</source>
        <translation type="vanished">Czytaj: %1</translation>
    </message>
    <message>
        <source>Read-only</source>
        <translation type="vanished">Tylko do odczytu</translation>
    </message>
    <message>
        <source>Read-write</source>
        <translation type="vanished">Do zapisu i odczytu</translation>
    </message>
    <message>
        <source>R&amp;eload</source>
        <translation type="vanished">&amp;Odśwież</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">&amp;Zmień nazwę</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="vanished">&amp;Zachowaj</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="vanished">Zachowaj jako</translation>
    </message>
    <message>
        <source>Show &amp;hidden files</source>
        <translation type="vanished">Pokaż &amp;ukryte pliki</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Rozmiar</translation>
    </message>
    <message>
        <source>Sort</source>
        <translation type="vanished">Sortuj</translation>
    </message>
    <message>
        <source>Sort by &amp;Date</source>
        <translation type="vanished">Sortuj po &amp;dacie</translation>
    </message>
    <message>
        <source>Sort by &amp;Name</source>
        <translation type="vanished">Sortuj &amp;po nazwie</translation>
    </message>
    <message>
        <source>Sort by &amp;Size</source>
        <translation type="vanished">Sortuj po &amp;rozmiarze</translation>
    </message>
    <message>
        <source>Special</source>
        <translation type="vanished">Specjalny </translation>
    </message>
    <message>
        <source>Symlink to Directory</source>
        <translation type="vanished">Dowiązanie symboliczne do katalogu</translation>
    </message>
    <message>
        <source>Symlink to File</source>
        <translation type="vanished">Dowiązanie symboliczne do pliku</translation>
    </message>
    <message>
        <source>Symlink to Special</source>
        <translation type="vanished">Dowiązanie symboliczne do pliku specjalnego</translation>
    </message>
    <message>
        <source>the directory</source>
        <translation type="vanished">katalog</translation>
    </message>
    <message>
        <source>the file</source>
        <translation type="vanished">plik</translation>
    </message>
    <message>
        <source>the symlink</source>
        <translation type="vanished">dowiązanie symboliczne</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Rodzaj</translation>
    </message>
    <message>
        <source>&amp;Unsorted</source>
        <translation type="vanished">&amp;Bez sortowania</translation>
    </message>
    <message>
        <source>Write: %1</source>
        <translation type="vanished">Pisz: %1</translation>
    </message>
    <message>
        <source>Write-only</source>
        <translation type="vanished">Tylko do zapisu</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation type="vanished">&amp;Tak</translation>
    </message>
    <message>
        <source>All Files (*.*)</source>
        <translation type="vanished">Wszystkie pliki (*.*)</translation>
    </message>
    <message>
        <source>Open </source>
        <translation type="vanished">Otwórz </translation>
    </message>
    <message>
        <source>Select a Directory</source>
        <translation type="vanished">Wybierz katalog</translation>
    </message>
</context>
<context>
    <name>Q3LocalFs</name>
    <message>
        <source>Could not create directory
%1</source>
        <translation type="vanished">Nie można utworzyć katalogu
%1</translation>
    </message>
    <message>
        <source>Could not open
%1</source>
        <translation type="vanished">Nie można otworzyć
%1</translation>
    </message>
    <message>
        <source>Could not read directory
%1</source>
        <translation type="vanished">Nie można czytać katalogu
%1</translation>
    </message>
    <message>
        <source>Could not remove file or directory
%1</source>
        <translation type="vanished">Nie można usunąć pliku lub katalogu
%1</translation>
    </message>
    <message>
        <source>Could not rename
%1
to
%2</source>
        <translation type="vanished">Nie można zmienić nazwy
%1
na
%2</translation>
    </message>
    <message>
        <source>Could not write
%1</source>
        <translation type="vanished">Nie można zapisać
%1</translation>
    </message>
</context>
<context>
    <name>Q3MainWindow</name>
    <message>
        <source>Customize...</source>
        <translation type="vanished">Ustawienia użytkownika...</translation>
    </message>
    <message>
        <source>Line up</source>
        <translation type="vanished">Wyrównaj położenie</translation>
    </message>
</context>
<context>
    <name>Q3NetworkProtocol</name>
    <message>
        <source>Operation stopped by the user</source>
        <translation type="vanished">Operacja zatrzymana przez użytkownika</translation>
    </message>
</context>
<context>
    <name>Q3ProgressDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Anuluj</translation>
    </message>
</context>
<context>
    <name>Q3TabDialog</name>
    <message>
        <source>Apply</source>
        <translation type="vanished">Zatwierdź</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Anuluj</translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation type="vanished">Domyślne</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Pomoc</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
</context>
<context>
    <name>Q3TextEdit</name>
    <message>
        <source>Clear</source>
        <translation type="vanished">Wyczyść</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">&amp;Kopiuj</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">W&amp;ytnij</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">&amp;Wklej</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation type="vanished">&amp;Przywróć</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Zaznacz wszystko</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation type="vanished">&amp;Cofnij</translation>
    </message>
</context>
<context>
    <name>Q3TitleBar</name>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij okno</translation>
    </message>
    <message>
        <source>Closes the window</source>
        <translation type="vanished">Zamyka okno</translation>
    </message>
    <message>
        <source>Contains commands to manipulate the window</source>
        <translation type="vanished">Zawiera polecenia zarządzające oknem</translation>
    </message>
    <message>
        <source>Displays the name of the window and contains controls to manipulate it</source>
        <translation type="vanished">Wyświetla nazwę okna i zawiera elementy do zarządzania nim</translation>
    </message>
    <message>
        <source>Makes the window full screen</source>
        <translation type="vanished">Powiększa maksymalnie okno</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">Zmaksymalizuj</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Zminimalizuj</translation>
    </message>
    <message>
        <source>Puts a minimized window back to normal</source>
        <translation type="vanished">Przywraca normalny rozmiar uprzednio zminimalizowanego okna</translation>
    </message>
    <message>
        <source>Moves the window out of the way</source>
        <translation type="vanished">Przenosi okno w inne położenie</translation>
    </message>
    <message>
        <source>Puts a maximized window back to normal</source>
        <translation type="vanished">Przywraca normalny rozmiar uprzednio zmaksymalizowanego okna</translation>
    </message>
    <message>
        <source>Restore down</source>
        <translation type="vanished">Przywróć pod spód</translation>
    </message>
    <message>
        <source>Restore up</source>
        <translation type="vanished">Przywróć na wierzch</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="vanished">System</translation>
    </message>
</context>
<context>
    <name>Q3ToolBar</name>
    <message>
        <source>More...</source>
        <translation type="vanished">Więcej...</translation>
    </message>
</context>
<context>
    <name>Q3UrlOperator</name>
    <message>
        <source>(unknown)</source>
        <translation type="vanished">(nieznany)</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support copying or moving files or directories</source>
        <translation type="vanished">Protokół &apos;%1&apos; nie obsługuje kopiowania lub przenoszenia plików lub katalogów</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support creating new directories</source>
        <translation type="vanished">Protokół &apos;%1&apos; nie obsługuje tworzenia nowych katalogów</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support getting files</source>
        <translation type="vanished">Protokół &apos;%1&apos; nie obsługuje pobierania plików</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support listing directories</source>
        <translation type="vanished">Protokół &apos;%1&apos; nie obsługuje pokazywania katalogów</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support putting files</source>
        <translation type="vanished">Protokół &apos;%1&apos; nie obsługuje wysyłania plików</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support removing files or directories</source>
        <translation type="vanished">Protokół &apos;%1&apos; nie obsługuje usuwania plików lub katalogów</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support renaming files or directories</source>
        <translation type="vanished">Protokół &apos;%1&apos; nie obsługuje zmiany nazwy plików lub katalogów</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; is not supported</source>
        <translation type="vanished">Protokół &apos;%1&apos; nie jest obsługiwany</translation>
    </message>
</context>
<context>
    <name>Q3Wizard</name>
    <message>
        <source>&lt; &amp;Back</source>
        <translation type="vanished">&lt; &amp;Wstecz</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;Anuluj</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation type="vanished">&amp;Zakończ</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="vanished">&amp;Pomoc</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation type="vanished">&amp;Dalej &gt;</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <source>Connection refused</source>
        <translation type="vanished">Połączenie odrzucone</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation type="vanished">Host nie znaleziony</translation>
    </message>
    <message>
        <source>Connection timed out</source>
        <translation type="vanished">Przekroczony czas połączenia</translation>
    </message>
    <message>
        <source>Operation on socket is not supported</source>
        <translation type="vanished">Operacja na gnieździe nie jest obsługiwana</translation>
    </message>
    <message>
        <source>Socket is not connected</source>
        <translation type="vanished">Gniazdo nie jest podłączone</translation>
    </message>
    <message>
        <source>Socket operation timed out</source>
        <translation type="vanished">Przekroczony czas operacji gniazda</translation>
    </message>
    <message>
        <source>Network unreachable</source>
        <translation type="vanished">Sieć niedostępna</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <source>Step &amp;down</source>
        <translation type="vanished">Krok w &amp;dół</translation>
    </message>
    <message>
        <source>&amp;Step up</source>
        <translation type="vanished">Krok do &amp;góry</translation>
    </message>
    <message>
        <source>&amp;Select All</source>
        <translation type="vanished">&amp;Zaznacz wszystko</translation>
    </message>
</context>
<context>
    <name>QAccessibleButton</name>
    <message>
        <source>Uncheck</source>
        <translation type="vanished">Odznacz</translation>
    </message>
    <message>
        <source>Check</source>
        <translation type="vanished">Zaznacz</translation>
    </message>
    <message>
        <source>Press</source>
        <translation type="vanished">Wciśnij</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Activate</source>
        <translation type="vanished">Uaktywnij</translation>
    </message>
    <message>
        <source>Activates the program&apos;s main window</source>
        <translation type="vanished">Uaktywnia główne okno programu</translation>
    </message>
    <message>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation type="vanished">Program &apos;%1&apos; wymaga do uruchomienia Qt %2, znaleziono Qt %3.</translation>
    </message>
    <message>
        <source>Incompatible Qt Library Error</source>
        <translation type="vanished">Niekompatybilność biblioteki Qt</translation>
    </message>
    <message>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation type="vanished">LTR</translation>
    </message>
</context>
<context>
    <name>QAxSelect</name>
    <message>
        <source>Select ActiveX Control</source>
        <translation type="vanished">Wybierz kontrolkę ActiveX</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;Anuluj</translation>
    </message>
    <message>
        <source>COM &amp;Object:</source>
        <translation type="vanished">&amp;Obiekt COM:</translation>
    </message>
</context>
<context>
    <name>QCheckBox</name>
    <message>
        <source>Check</source>
        <translation type="vanished">Zaznacz</translation>
    </message>
    <message>
        <source>Toggle</source>
        <translation type="vanished">Przełącz</translation>
    </message>
    <message>
        <source>Uncheck</source>
        <translation type="vanished">Odznacz</translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <source>&amp;Add to Custom Colors</source>
        <translation type="vanished">&amp;Dodaj do własnych kolorów</translation>
    </message>
    <message>
        <source>A&amp;lpha channel:</source>
        <translation type="vanished">Kanał &amp;alfa:</translation>
    </message>
    <message>
        <source>Select Color</source>
        <translation type="vanished">Wybierz kolor</translation>
    </message>
    <message>
        <source>&amp;Basic colors</source>
        <translation type="vanished">&amp;Kolory podstawowe</translation>
    </message>
    <message>
        <source>Bl&amp;ue:</source>
        <translation type="vanished">Błęki&amp;t:</translation>
    </message>
    <message>
        <source>&amp;Custom colors</source>
        <translation type="vanished">Wła&amp;sne kolory</translation>
    </message>
    <message>
        <source>&amp;Green:</source>
        <translation type="vanished">&amp;Zieleń:</translation>
    </message>
    <message>
        <source>Hu&amp;e:</source>
        <translation type="vanished">&amp;Barwa:</translation>
    </message>
    <message>
        <source>&amp;Red:</source>
        <translation type="vanished">&amp;Czerwień:</translation>
    </message>
    <message>
        <source>&amp;Sat:</source>
        <translation type="vanished">&amp;Nasycenie:</translation>
    </message>
    <message>
        <source>&amp;Val:</source>
        <translation type="vanished">&amp;Wartość:</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <source>Open</source>
        <translation type="vanished">Otwórz</translation>
    </message>
    <message>
        <source>False</source>
        <translation type="vanished">Fałsz</translation>
    </message>
    <message>
        <source>True</source>
        <translation type="vanished">Prawda</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>%1: already exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="vanished">%1: już istnieje</translation>
    </message>
    <message>
        <source>%1: does not exist</source>
        <comment>QSystemSemaphore</comment>
        <translation type="vanished">%1: nie istnieje</translation>
    </message>
    <message>
        <source>%1: out of resources</source>
        <comment>QSystemSemaphore</comment>
        <translation type="vanished">%1: zasoby wyczerpane</translation>
    </message>
    <message>
        <source>%1: permission denied</source>
        <comment>QSystemSemaphore</comment>
        <translation type="vanished">%1: brak uprawnień</translation>
    </message>
    <message>
        <source>%1: unknown error %2</source>
        <comment>QSystemSemaphore</comment>
        <translation type="vanished">%1: nieznany błąd %2</translation>
    </message>
    <message>
        <source>%1: key is empty</source>
        <comment>QSystemSemaphore</comment>
        <translation type="vanished">%1: klucz jest pusty</translation>
    </message>
    <message>
        <source>%1: unable to make key</source>
        <comment>QSystemSemaphore</comment>
        <translation type="vanished">%1: nie można utworzyć klucza</translation>
    </message>
    <message>
        <source>%1: ftok failed</source>
        <comment>QSystemSemaphore</comment>
        <translation type="vanished">%1: wystąpił błąd w funkcji ftok()</translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Nie można nawiązać połączenia</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Nie można dokonać transakcji</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Nie można wycofać transakcji</translation>
    </message>
    <message>
        <source>Unable to set autocommit</source>
        <translation type="vanished">Nie można ustawić trybu automatycznego dokonywania transakcji</translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Nie można wykonać polecenia</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Nie można przygotować polecenia</translation>
    </message>
    <message>
        <source>Unable to bind variable</source>
        <translation type="vanished">Nie można powiązać zmiennej</translation>
    </message>
    <message>
        <source>Unable to fetch record %1</source>
        <translation type="vanished">Nie można pobrać rekordu %1</translation>
    </message>
    <message>
        <source>Unable to fetch next</source>
        <translation type="vanished">Nie można pobrać kolejnego wiersza danych</translation>
    </message>
    <message>
        <source>Unable to fetch first</source>
        <translation type="vanished">Nie można pobrać pierwszego wiersza danych</translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <source>am</source>
        <translation type="vanished">am</translation>
    </message>
    <message>
        <source>AM</source>
        <translation type="vanished">AM</translation>
    </message>
    <message>
        <source>pm</source>
        <translation type="vanished">pm</translation>
    </message>
    <message>
        <source>PM</source>
        <translation type="vanished">PM</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAbstractAnimation</name>
    <message>
        <source>Cannot animate non-existent property &quot;%1&quot;</source>
        <translation type="vanished">Nie można animować nieistniejącej właściwości &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Cannot animate read-only property &quot;%1&quot;</source>
        <translation type="vanished">Nie można animować właściwości (tylko do odczytu): &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Animation is an abstract class</source>
        <translation type="vanished">&quot;Animation&quot; jest klasą abstrakcyjną</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAnchorAnimation</name>
    <message>
        <source>Cannot set a duration of &lt; 0</source>
        <translation type="vanished">Nie można ustawić ujemnego czasu trwania</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAnchors</name>
    <message>
        <source>Possible anchor loop detected on fill.</source>
        <translation type="vanished">Wykryto możliwe zapętlenie dla kotwicy &quot;fill&quot;.</translation>
    </message>
    <message>
        <source>Possible anchor loop detected on centerIn.</source>
        <translation type="vanished">Wykryto możliwe zapętlenie dla kotwicy &quot;centerIn&quot;.</translation>
    </message>
    <message>
        <source>Cannot anchor to an item that isn&apos;t a parent or sibling.</source>
        <translation type="vanished">Nie można doczepić kotwicy do elementu który nie jest rodzicem ani rodzeństwem.</translation>
    </message>
    <message>
        <source>Possible anchor loop detected on vertical anchor.</source>
        <translation type="vanished">Wykryto możliwe zapętlenie dla pionowej kotwicy.</translation>
    </message>
    <message>
        <source>Possible anchor loop detected on horizontal anchor.</source>
        <translation type="vanished">Wykryto możliwe zapętlenie dla poziomej kotwicy.</translation>
    </message>
    <message>
        <source>Cannot specify left, right, and hcenter anchors.</source>
        <translation type="vanished">Nie można jednocześnie podać lewej, prawej i centralnej poziomej kotwicy.</translation>
    </message>
    <message>
        <source>Cannot anchor to a null item.</source>
        <translation type="vanished">Nie można doczepić kotwicy do zerowego elementu.</translation>
    </message>
    <message>
        <source>Cannot anchor a horizontal edge to a vertical edge.</source>
        <translation type="vanished">Nie można doczepić poziomej krawędzi do pionowej.</translation>
    </message>
    <message>
        <source>Cannot anchor item to self.</source>
        <translation type="vanished">Nie można doczepić kotwicy do tego samego elementu.</translation>
    </message>
    <message>
        <source>Cannot specify top, bottom, and vcenter anchors.</source>
        <translation type="vanished">Nie można jednocześnie podać górnej, dolnej i centralnej pionowej kotwicy.</translation>
    </message>
    <message>
        <source>Baseline anchor cannot be used in conjunction with top, bottom, or vcenter anchors.</source>
        <translation type="vanished">Bazowa kotwica nie może być użyta w połączeniu z górną, dolną lub centralną pionową kotwicą.</translation>
    </message>
    <message>
        <source>Cannot anchor a vertical edge to a horizontal edge.</source>
        <translation type="vanished">Nie można doczepić pionowej krawędzi do poziomej.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAnimatedImage</name>
    <message>
        <source>Qt was built without support for QMovie</source>
        <translation type="vanished">Qt zostało zbudowane bez obsługi QMovie</translation>
    </message>
</context>
<context>
    <name>QDeclarativeApplication</name>
    <message>
        <source>Application is an abstract class</source>
        <translation type="vanished">&quot;Application&quot; jest klasą abstrakcyjną</translation>
    </message>
</context>
<context>
    <name>QDeclarativeBehavior</name>
    <message>
        <source>Cannot change the animation assigned to a Behavior.</source>
        <translation type="vanished">Nie można zmienić animacji przypisanej do &quot;Zachowania&quot;.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeBinding</name>
    <message>
        <source>Binding loop detected for property &quot;%1&quot;</source>
        <translation type="vanished">Zapętlenie powiązania dla właściwości &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeCompiledBindings</name>
    <message>
        <source>Binding loop detected for property &quot;%1&quot;</source>
        <translation type="vanished">Zapętlenie powiązania dla właściwości &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeCompiler</name>
    <message>
        <source>Invalid property assignment: &quot;%1&quot; is a read-only property</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: &quot;%1&quot; jest właściwością tylko do odczytu</translation>
    </message>
    <message>
        <source>Invalid property assignment: unknown enumeration</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: nieznana wartość wyliczeniowa</translation>
    </message>
    <message>
        <source>Invalid property assignment: string expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano ciągu</translation>
    </message>
    <message>
        <source>Invalid property assignment: url expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano url</translation>
    </message>
    <message>
        <source>Invalid property assignment: unsigned int expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano liczby naturalnej</translation>
    </message>
    <message>
        <source>Invalid property assignment: int expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano liczby całkowitej</translation>
    </message>
    <message>
        <source>Invalid property assignment: number expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano liczby</translation>
    </message>
    <message>
        <source>Invalid property assignment: color expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano koloru</translation>
    </message>
    <message>
        <source>Invalid property assignment: date expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano daty</translation>
    </message>
    <message>
        <source>Invalid property assignment: time expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano czasu</translation>
    </message>
    <message>
        <source>Invalid property assignment: datetime expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano daty i czasu</translation>
    </message>
    <message>
        <source>Invalid property assignment: point expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano punktu</translation>
    </message>
    <message>
        <source>Invalid property assignment: size expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano rozmiaru</translation>
    </message>
    <message>
        <source>Invalid property assignment: rect expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano prostokąta</translation>
    </message>
    <message>
        <source>Invalid property assignment: boolean expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano wartości boolowskiej</translation>
    </message>
    <message>
        <source>Invalid property assignment: 3D vector expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano wektora 3D</translation>
    </message>
    <message>
        <source>Invalid property assignment: unsupported type &quot;%1&quot;</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: nieobsługiwany typ &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Element is not creatable.</source>
        <translation type="vanished">Nie można utworzyć elementu (&quot;creatable&quot; wyłączone).</translation>
    </message>
    <message>
        <source>Component elements may not contain properties other than id</source>
        <translation type="vanished">Elementy komponentu nie mogą posiadać właściwości innych niż &quot;id&quot;</translation>
    </message>
    <message>
        <source>Invalid component id specification</source>
        <translation type="vanished">Niepoprawna specyfikacja &quot;id&quot; komponentu</translation>
    </message>
    <message>
        <source>id is not unique</source>
        <translation type="vanished">Wartość &quot;id&quot; nie jest unikatowa</translation>
    </message>
    <message>
        <source>Invalid component body specification</source>
        <translation type="vanished">Niepoprawna specyfikacja &quot;body&quot; komponentu</translation>
    </message>
    <message>
        <source>Component objects cannot declare new properties.</source>
        <translation type="vanished">Instancje komponentu nie mogą deklarować nowych właściwości.</translation>
    </message>
    <message>
        <source>Component objects cannot declare new signals.</source>
        <translation type="vanished">Instancje komponentu nie mogą deklarować nowych sygnałów.</translation>
    </message>
    <message>
        <source>Component objects cannot declare new functions.</source>
        <translation type="vanished">Instancje komponentu nie mogą deklarować nowych funkcji.</translation>
    </message>
    <message>
        <source>Cannot create empty component specification</source>
        <translation type="vanished">Nie można utworzyć pustej specyfikacji komponentu</translation>
    </message>
    <message>
        <source>&quot;%1.%2&quot; is not available in %3 %4.%5.</source>
        <translation type="vanished">&quot;%1.%2&quot; nie jest dostępne w %3 %4.%5.</translation>
    </message>
    <message>
        <source>&quot;%1.%2&quot; is not available due to component versioning.</source>
        <translation type="vanished">&quot;%1.%2&quot; nie jest dostępny z powodu niekompatybilności wersji komponentu.</translation>
    </message>
    <message>
        <source>Incorrectly specified signal assignment</source>
        <translation type="vanished">Przypisanie sygnału błędnie podane</translation>
    </message>
    <message>
        <source>Cannot assign a value to a signal (expecting a script to be run)</source>
        <translation type="vanished">Nie można przypisać wartości do sygnału (oczekiwano uruchomienia skryptu)</translation>
    </message>
    <message>
        <source>Empty signal assignment</source>
        <translation type="vanished">Przypisanie pustego sygnału</translation>
    </message>
    <message>
        <source>Empty property assignment</source>
        <translation type="vanished">Przypisanie pustej właściwości</translation>
    </message>
    <message>
        <source>Attached properties cannot be used here</source>
        <translation type="vanished">Dołączone właściwości nie mogą być tutaj użyte</translation>
    </message>
    <message>
        <source>Non-existent attached object</source>
        <translation type="vanished">Nieistniejący dołączony obiekt</translation>
    </message>
    <message>
        <source>Invalid attached object assignment</source>
        <translation type="vanished">Niepoprawne przypisanie dołączonego obiektu</translation>
    </message>
    <message>
        <source>Cannot assign to non-existent default property</source>
        <translation type="vanished">Nie można przypisać wartości do nieistniejącej domyślnej właściwości</translation>
    </message>
    <message>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation type="vanished">Nie można przypisać wartości do nieistniejącej właściwości &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Invalid use of namespace</source>
        <translation type="vanished">Niepoprawne użycie przestrzeni nazw</translation>
    </message>
    <message>
        <source>Not an attached property name</source>
        <translation type="vanished">Nie jest to nazwa dołączonej właściwości</translation>
    </message>
    <message>
        <source>Invalid use of id property</source>
        <translation type="vanished">Niepoprawne użycie właściwości &quot;id&quot;</translation>
    </message>
    <message>
        <source>Property has already been assigned a value</source>
        <translation type="vanished">Wartość została już przypisana do właściwości</translation>
    </message>
    <message>
        <source>Invalid grouped property access</source>
        <translation type="vanished">Błędny dostęp do zgrupowanej właściwości</translation>
    </message>
    <message>
        <source>Cannot assign a value directly to a grouped property</source>
        <translation type="vanished">Nie można bezpośrednio przypisać wartości do zgrupowanej właściwości</translation>
    </message>
    <message>
        <source>Invalid property use</source>
        <translation type="vanished">Niepoprawne użycie właściwości</translation>
    </message>
    <message>
        <source>Property assignment expected</source>
        <translation type="vanished">Oczekiwano przypisania wartości</translation>
    </message>
    <message>
        <source>Single property assignment expected</source>
        <translation type="vanished">Oczekiwano przypisania pojedynczej wartości</translation>
    </message>
    <message>
        <source>Unexpected object assignment</source>
        <translation type="vanished">Nieoczekiwane przypisanie obiektu</translation>
    </message>
    <message>
        <source>Cannot assign object to list</source>
        <translation type="vanished">Nie można przypisać obiektu do listy</translation>
    </message>
    <message>
        <source>Can only assign one binding to lists</source>
        <translation type="vanished">Tylko jedno powiązanie może być przypisane do listy</translation>
    </message>
    <message>
        <source>Cannot assign primitives to lists</source>
        <translation type="vanished">Nie można przypisać elementu do listy</translation>
    </message>
    <message>
        <source>Cannot assign multiple values to a script property</source>
        <translation type="vanished">Nie można przypisać wielu wartości do skryptowej właściwości</translation>
    </message>
    <message>
        <source>Invalid property assignment: script expected</source>
        <translation type="vanished">Niepoprawne przypisanie wartości: oczekiwano skryptu</translation>
    </message>
    <message>
        <source>Cannot assign multiple values to a singular property</source>
        <translation type="vanished">Nie można przypisać wielu wartości do pojedynczej właściwości</translation>
    </message>
    <message>
        <source>Cannot assign object to property</source>
        <translation type="vanished">Nie można przypisać obiektu dla właściwości</translation>
    </message>
    <message>
        <source>&quot;%1&quot; cannot operate on &quot;%2&quot;</source>
        <translation type="vanished">&quot;%1&quot; nie może operować na &quot;%2&quot;</translation>
    </message>
    <message>
        <source>Duplicate default property</source>
        <translation type="vanished">Powielona domyślna właściwość</translation>
    </message>
    <message>
        <source>Duplicate property name</source>
        <translation type="vanished">Powielona nazwa właściwości</translation>
    </message>
    <message>
        <source>Property names cannot begin with an upper case letter</source>
        <translation type="vanished">Nazwy właściwości nie mogą rozpoczynać się wielką literą</translation>
    </message>
    <message>
        <source>Illegal property name</source>
        <translation type="vanished">Niepoprawna nazwa właściwości</translation>
    </message>
    <message>
        <source>Duplicate signal name</source>
        <translation type="vanished">Powielona nazwa sygnału</translation>
    </message>
    <message>
        <source>Signal names cannot begin with an upper case letter</source>
        <translation type="vanished">Nazwy sygnałów nie mogą rozpoczynać się wielką literą</translation>
    </message>
    <message>
        <source>Illegal signal name</source>
        <translation type="vanished">Niepoprawna nazwa sygnału</translation>
    </message>
    <message>
        <source>Duplicate method name</source>
        <translation type="vanished">Powielona nazwa metody</translation>
    </message>
    <message>
        <source>Method names cannot begin with an upper case letter</source>
        <translation type="vanished">Nazwy metod nie mogą rozpoczynać się wielką literą</translation>
    </message>
    <message>
        <source>Illegal method name</source>
        <translation type="vanished">Niepoprawna nazwa metody</translation>
    </message>
    <message>
        <source>Property value set multiple times</source>
        <translation type="vanished">Wartość właściwości ustawiona wielokrotnie</translation>
    </message>
    <message>
        <source>Invalid property nesting</source>
        <translation type="vanished">Niepoprawne zagnieżdżenie właściwości</translation>
    </message>
    <message>
        <source>Cannot override FINAL property</source>
        <translation type="vanished">Nie można nadpisać właściwości &quot;FINAL&quot;</translation>
    </message>
    <message>
        <source>Invalid property type</source>
        <translation type="vanished">Niepoprawny typ właściwości</translation>
    </message>
    <message>
        <source>Invalid empty ID</source>
        <translation type="vanished">Niepoprawny pusty identyfikator</translation>
    </message>
    <message>
        <source>IDs cannot start with an uppercase letter</source>
        <translation type="vanished">Identyfikatory nie mogą rozpoczynać się wielką literą</translation>
    </message>
    <message>
        <source>IDs must start with a letter or underscore</source>
        <translation type="vanished">Identyfikatory muszą rozpoczynać się literą lub znakiem podkreślenia</translation>
    </message>
    <message>
        <source>IDs must contain only letters, numbers, and underscores</source>
        <translation type="vanished">Identyfikatory mogą zawierać jedynie litery, cyfry i znaki podkreślenia</translation>
    </message>
    <message>
        <source>ID illegally masks global JavaScript property</source>
        <translation type="vanished">Identyfikator nielegalnie maskuje globalną właściwość JavaScript</translation>
    </message>
    <message>
        <source>Invalid alias location</source>
        <translation type="vanished">Niepoprawne położenie aliasu</translation>
    </message>
    <message>
        <source>Invalid alias reference. An alias reference must be specified as &lt;id&gt;, &lt;id&gt;.&lt;property&gt; or &lt;id&gt;.&lt;value property&gt;.&lt;property&gt;</source>
        <translation type="vanished">Niepoprawna referencja aliasu. Referencja aliasu musi być podana jako &lt;id&gt;, &lt;id&gt;.&lt;property&gt; lub &lt;id&gt;.&lt;value property&gt;.&lt;property&gt;</translation>
    </message>
    <message>
        <source>Invalid alias reference. Unable to find id &quot;%1&quot;</source>
        <translation type="vanished">Niepoprawna referencja aliasu. Nie można odnaleźć identyfikatora &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeComponent</name>
    <message>
        <source>Invalid empty URL</source>
        <translation type="vanished">Niepoprawny pusty URL</translation>
    </message>
    <message>
        <source>createObject: value is not an object</source>
        <translation type="vanished">&quot;createObject&quot;: wartość nie jest obiektem</translation>
    </message>
</context>
<context>
    <name>QDeclarativeConnections</name>
    <message>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation type="vanished">Nie można przypisać wartości do nieistniejącej właściwości &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Connections: nested objects not allowed</source>
        <translation type="vanished">Połączenia: zagnieżdżone obiekty nie są dozwolone</translation>
    </message>
    <message>
        <source>Connections: syntax error</source>
        <translation type="vanished">Połączenia: błąd składni</translation>
    </message>
    <message>
        <source>Connections: script expected</source>
        <translation type="vanished">Połączenia: oczekiwano skryptu</translation>
    </message>
</context>
<context>
    <name>QDeclarativeEngine</name>
    <message>
        <source>executeSql called outside transaction()</source>
        <translation type="vanished">&quot;executeSql&quot; zawołane na zewnątrz &quot;transation()&quot;</translation>
    </message>
    <message>
        <source>Read-only Transaction</source>
        <translation type="vanished">Transakcja tylko do odczytu</translation>
    </message>
    <message>
        <source>Version mismatch: expected %1, found %2</source>
        <translation type="vanished">Niezgodność wersji: oczekiwano %1, znaleziono %2</translation>
    </message>
    <message>
        <source>SQL transaction failed</source>
        <translation type="vanished">Transakcja SQL zakończona błędem</translation>
    </message>
    <message>
        <source>transaction: missing callback</source>
        <translation type="vanished">transakcja: brak wywołania zwrotnego</translation>
    </message>
    <message>
        <source>SQL: database version mismatch</source>
        <translation type="vanished">SQL: niezgodność wersji bazy danych</translation>
    </message>
</context>
<context>
    <name>QDeclarativeFlipable</name>
    <message>
        <source>front is a write-once property</source>
        <translation type="vanished">&quot;front&quot; jest właściwością tylko do odczytu</translation>
    </message>
    <message>
        <source>back is a write-once property</source>
        <translation type="vanished">&quot;back&quot; jest właściwością tylko do odczytu</translation>
    </message>
</context>
<context>
    <name>QDeclarativeImportDatabase</name>
    <message>
        <source>plugin cannot be loaded for module &quot;%1&quot;: %2</source>
        <translation type="vanished">wtyczka nie może zostać załadowana dla modułu &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <source>module &quot;%1&quot; plugin &quot;%2&quot; not found</source>
        <translation type="vanished">wtyczka &quot;%2&quot; modułu &quot;%1&quot; nie została odnaleziona</translation>
    </message>
    <message>
        <source>module &quot;%1&quot; version %2.%3 is not installed</source>
        <translation type="vanished">wersja %2.%3 modułu %1 nie jest zainstalowana</translation>
    </message>
    <message>
        <source>module &quot;%1&quot; is not installed</source>
        <translation type="vanished">moduł &quot;%1&quot; nie jest zainstalowany</translation>
    </message>
    <message>
        <source>&quot;%1&quot;: no such directory</source>
        <translation type="vanished">&quot;%1&quot;: brak katalogu</translation>
    </message>
    <message>
        <source>- %1 is not a namespace</source>
        <translation type="vanished">- %1 nie jest przestrzenią nazw</translation>
    </message>
    <message>
        <source>- nested namespaces not allowed</source>
        <translation type="vanished">- zagnieżdżone przestrzenie nazw nie są dozwolone</translation>
    </message>
    <message>
        <source>local directory</source>
        <translation type="vanished">lokalny katalog</translation>
    </message>
    <message>
        <source>is ambiguous. Found in %1 and in %2</source>
        <translation type="vanished">jest niejednoznaczny. Znaleziono w %1 i w %2</translation>
    </message>
    <message>
        <source>is ambiguous. Found in %1 in version %2.%3 and %4.%5</source>
        <translation type="vanished">jest niejednoznaczny. Znaleziono w %1 w wersji %2.%3 i %4.%5</translation>
    </message>
    <message>
        <source>is instantiated recursively</source>
        <translation type="vanished">jest zinstancjonowany rekurencyjnie</translation>
    </message>
    <message>
        <source>is not a type</source>
        <translation type="vanished">nie jest typem</translation>
    </message>
    <message>
        <source>File name case mismatch for &quot;%1&quot;</source>
        <translation type="vanished">Niezgodność wielkości liter w &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeKeyNavigationAttached</name>
    <message>
        <source>KeyNavigation is only available via attached properties</source>
        <translation type="vanished">&quot;KeyNavigation&quot; jest dostępny jedynie poprzez dołączone właściwości</translation>
    </message>
</context>
<context>
    <name>QDeclarativeKeysAttached</name>
    <message>
        <source>Keys is only available via attached properties</source>
        <translation type="vanished">&quot;Keys&quot; jest dostępny jedynie poprzez dołączone właściwości</translation>
    </message>
</context>
<context>
    <name>QDeclarativeLayoutMirroringAttached</name>
    <message>
        <source>LayoutDirection attached property only works with Items</source>
        <translation type="vanished">Dołączona właściwość &quot;LayoutDirection&quot; działa tylko z &quot;Item&quot;</translation>
    </message>
    <message>
        <source>LayoutMirroring is only available via attached properties</source>
        <translation type="vanished">&quot;LayoutMirroring&quot; dostępny jest tylko poprzez dołączone właściwości</translation>
    </message>
</context>
<context>
    <name>QDeclarativeListModel</name>
    <message>
        <source>remove: index %1 out of range</source>
        <translation type="vanished">remove: indeks %1 poza zakresem</translation>
    </message>
    <message>
        <source>insert: value is not an object</source>
        <translation type="vanished">insert: wartość nie jest obiektem</translation>
    </message>
    <message>
        <source>insert: index %1 out of range</source>
        <translation type="vanished">insert: indeks %1 poza zakresem</translation>
    </message>
    <message>
        <source>move: out of range</source>
        <translation type="vanished">move: poza zakresem</translation>
    </message>
    <message>
        <source>append: value is not an object</source>
        <translation type="vanished">append: wartość nie jest obiektem</translation>
    </message>
    <message>
        <source>set: value is not an object</source>
        <translation type="vanished">set: wartość nie jest obiektem</translation>
    </message>
    <message>
        <source>set: index %1 out of range</source>
        <translation type="vanished">set: indeks %1 poza zakresem</translation>
    </message>
    <message>
        <source>ListElement: cannot contain nested elements</source>
        <translation type="vanished">ListElement: nie może zawierać zagnieżdżonych elementów</translation>
    </message>
    <message>
        <source>ListElement: cannot use reserved &quot;id&quot; property</source>
        <translation type="vanished">ListElement: nie można używać zarezerwowanej właściwości &quot;id&quot;</translation>
    </message>
    <message>
        <source>ListElement: cannot use script for property value</source>
        <translation type="vanished">ListElement: nie można używać skryptu jako wartości właściwości</translation>
    </message>
    <message>
        <source>ListModel: undefined property &apos;%1&apos;</source>
        <translation type="vanished">ListModel: niezdefiniowana właściwość &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeLoader</name>
    <message>
        <source>Loader does not support loading non-visual elements.</source>
        <translation type="vanished">Ładowanie elementów niewizualnych nie jest obsługiwane.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeParentAnimation</name>
    <message>
        <source>Unable to preserve appearance under complex transform</source>
        <translation type="vanished">Nie można utrzymać wyglądu przy złożonej transformacji</translation>
    </message>
    <message>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation type="vanished">Nie można utrzymać wyglądu przy niejednolitej skali</translation>
    </message>
    <message>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation type="vanished">Nie można utrzymać wyglądu przy zerowej skali</translation>
    </message>
</context>
<context>
    <name>QDeclarativeParentChange</name>
    <message>
        <source>Unable to preserve appearance under complex transform</source>
        <translation type="vanished">Nie można utrzymać wyglądu przy złożonej transformacji</translation>
    </message>
    <message>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation type="vanished">Nie można utrzymać wyglądu przy niejednolitej skali</translation>
    </message>
    <message>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation type="vanished">Nie można utrzymać wyglądu przy zerowej skali</translation>
    </message>
</context>
<context>
    <name>QDeclarativeParser</name>
    <message>
        <source>Illegal unicode escape sequence</source>
        <translation type="vanished">Niepoprawny znak w sekwencji escape</translation>
    </message>
    <message>
        <source>Illegal character</source>
        <translation type="vanished">Niepoprawny znak</translation>
    </message>
    <message>
        <source>Unclosed string at end of line</source>
        <translation type="vanished">Niedomknięty ciąg na końcu linii</translation>
    </message>
    <message>
        <source>Illegal escape sequence</source>
        <translation type="vanished">Niepoprawna sekwencja escape</translation>
    </message>
    <message>
        <source>Unclosed comment at end of file</source>
        <translation type="vanished">Niedomknięty komentarz na końcu linii</translation>
    </message>
    <message>
        <source>Illegal syntax for exponential number</source>
        <translation type="vanished">Niepoprawna składnia liczby o postaci wykładniczej</translation>
    </message>
    <message>
        <source>Identifier cannot start with numeric literal</source>
        <translation type="vanished">Identyfikator nie może rozpoczynać się literałem liczbowym</translation>
    </message>
    <message>
        <source>Unterminated regular expression literal</source>
        <translation type="vanished">Niedokończone wyrażenie regularne</translation>
    </message>
    <message>
        <source>Invalid regular expression flag &apos;%0&apos;</source>
        <translation type="vanished">Niepoprawna flaga &quot;%0&quot; w wyrażeniu regularnym</translation>
    </message>
    <message>
        <source>Unterminated regular expression backslash sequence</source>
        <translation type="vanished">Niedokończone sekwencja backslash wyrażenia regularnego</translation>
    </message>
    <message>
        <source>Unterminated regular expression class</source>
        <translation type="vanished">Niedokończona klasa wyrażenia regularnego</translation>
    </message>
    <message>
        <source>Syntax error</source>
        <translation type="vanished">Błąd składni</translation>
    </message>
    <message>
        <source>Unexpected token `%1&apos;</source>
        <translation type="vanished">Nieoczekiwany znak &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Expected token `%1&apos;</source>
        <translation type="vanished">Oczekiwany znak &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Property value set multiple times</source>
        <translation type="vanished">Wartość właściwości ustawiona wielokrotnie</translation>
    </message>
    <message>
        <source>Expected type name</source>
        <translation type="vanished">Oczekiwana nazwa typu</translation>
    </message>
    <message>
        <source>Invalid import qualifier ID</source>
        <translation type="vanished">Niepoprawny kwalifikator ID importu</translation>
    </message>
    <message>
        <source>Reserved name &quot;Qt&quot; cannot be used as an qualifier</source>
        <translation type="vanished">&quot;Qt&quot; jest nazwą zarezerwowaną i nie może być użyta jako kwalifikator</translation>
    </message>
    <message>
        <source>Script import qualifiers must be unique.</source>
        <translation type="vanished">Kwalifikator importu skryptu musi być unikatowy.</translation>
    </message>
    <message>
        <source>Script import requires a qualifier</source>
        <translation type="vanished">Import skryptu wymaga użycia kwalifikatora</translation>
    </message>
    <message>
        <source>Library import requires a version</source>
        <translation type="vanished">Import biblioteki wymaga podania wersji</translation>
    </message>
    <message>
        <source>Expected parameter type</source>
        <translation type="vanished">Oczekiwany typ parametru</translation>
    </message>
    <message>
        <source>Invalid property type modifier</source>
        <translation type="vanished">Niepoprawny modyfikator typu właściwości</translation>
    </message>
    <message>
        <source>Unexpected property type modifier</source>
        <translation type="vanished">Nieoczekiwany modyfikator typu właściwości</translation>
    </message>
    <message>
        <source>Expected property type</source>
        <translation type="vanished">Oczekiwany typ właściwości</translation>
    </message>
    <message>
        <source>Readonly not yet supported</source>
        <translation type="vanished">&quot;Tylko do odczytu&quot; nie jest jeszcze obsługiwane</translation>
    </message>
    <message>
        <source>JavaScript declaration outside Script element</source>
        <translation type="vanished">Deklaracja &quot;JavaScript&quot; na zewnątrz elementu &quot;Script&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativePauseAnimation</name>
    <message>
        <source>Cannot set a duration of &lt; 0</source>
        <translation type="vanished">Nie można ustawić ujemnego czasu trwania</translation>
    </message>
</context>
<context>
    <name>QDeclarativePixmap</name>
    <message>
        <source>Error decoding: %1: %2</source>
        <translation type="vanished">Błąd dekodowania: %1: %2</translation>
    </message>
    <message>
        <source>Failed to get image from provider: %1</source>
        <translation type="vanished">Pobieranie obrazka od dostawcy zakończone błędem: %1</translation>
    </message>
    <message>
        <source>Cannot open: %1</source>
        <translation type="vanished">Nie można otworzyć: %1</translation>
    </message>
</context>
<context>
    <name>QDeclarativePropertyAnimation</name>
    <message>
        <source>Cannot set a duration of &lt; 0</source>
        <translation type="vanished">Nie można ustawić ujemnego czasu trwania</translation>
    </message>
</context>
<context>
    <name>QDeclarativePropertyChanges</name>
    <message>
        <source>PropertyChanges does not support creating state-specific objects.</source>
        <translation type="vanished">&quot;PropertyChanges&quot; nie obsługuje tworzenia obiektów charakterystycznych dla stanów.</translation>
    </message>
    <message>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation type="vanished">Nie można przypisać wartości do nieistniejącej właściwości &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Cannot assign to read-only property &quot;%1&quot;</source>
        <translation type="vanished">Nie można przypisać wartości do właściwości (tylko do odczytu): &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeTextInput</name>
    <message>
        <source>Could not load cursor delegate</source>
        <translation type="vanished">Nie można załadować delegata kursora</translation>
    </message>
    <message>
        <source>Could not instantiate cursor delegate</source>
        <translation type="vanished">Nie można zinstancjonować delegata kursora</translation>
    </message>
</context>
<context>
    <name>QDeclarativeTypeLoader</name>
    <message>
        <source>Script %1 unavailable</source>
        <translation type="vanished">Skrypt &quot;%1&quot; nie jest dostępny</translation>
    </message>
    <message>
        <source>Type %1 unavailable</source>
        <translation type="vanished">Typ %1 nie jest dostępny</translation>
    </message>
    <message>
        <source>Namespace %1 cannot be used as a type</source>
        <translation type="vanished">Przestrzeń nazw %1 nie może być użyta jako typ</translation>
    </message>
    <message>
        <source>%1 %2</source>
        <translation type="vanished">%1 %2</translation>
    </message>
</context>
<context>
    <name>QDeclarativeVME</name>
    <message>
        <source>Unable to create object of type %1</source>
        <translation type="vanished">Nie można utworzyć obiektu typu %1</translation>
    </message>
    <message>
        <source>Cannot assign value %1 to property %2</source>
        <translation type="vanished">Nie można przypisać wartości %1 do właściwości %2</translation>
    </message>
    <message>
        <source>Cannot assign object type %1 with no default method</source>
        <translation type="vanished">Nie można przypisać obiektu typu %1 który nie posiada domyślnej metody</translation>
    </message>
    <message>
        <source>Cannot connect mismatched signal/slot %1 %vs. %2</source>
        <translation type="vanished">Nie można podłączyć niezgodnych sygnałów / slotów (%1 i %2)</translation>
    </message>
    <message>
        <source>Cannot assign an object to signal property %1</source>
        <translation type="vanished">Nie można przypisać obiektu do właściwości sygnału %1</translation>
    </message>
    <message>
        <source>Cannot assign object to list</source>
        <translation type="vanished">Nie można przypisać obiektu do listy</translation>
    </message>
    <message>
        <source>Cannot assign object to interface property</source>
        <translation type="vanished">Nie można przypisać obiektu do właściwości interfejsu</translation>
    </message>
    <message>
        <source>Unable to create attached object</source>
        <translation type="vanished">Nie można utworzyć dołączonego obiektu</translation>
    </message>
    <message>
        <source>Cannot set properties on %1 as it is null</source>
        <translation type="vanished">Nie można ustawić właściwości dla %1 ponieważ jest on zerowy</translation>
    </message>
</context>
<context>
    <name>QDeclarativeVisualDataModel</name>
    <message>
        <source>Delegate component must be Item type.</source>
        <translation type="vanished">Delegat musi być typu &quot;Item&quot;.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeXmlListModel</name>
    <message>
        <source>Qt was built without support for xmlpatterns</source>
        <translation type="vanished">Qt zostało zbudowane bez obsługi xmlpatterns</translation>
    </message>
</context>
<context>
    <name>QDeclarativeXmlListModelRole</name>
    <message>
        <source>An XmlRole query must not start with &apos;/&apos;</source>
        <translation type="vanished">Zapytanie XmlRole nie może rozpoczynać się od &quot;/&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeXmlRoleList</name>
    <message>
        <source>An XmlListModel query must start with &apos;/&apos; or &quot;//&quot;</source>
        <translation type="vanished">Zapytanie XmlListModel nie może rozpoczynać się od &quot;/&quot; ani od &quot;//&quot;</translation>
    </message>
</context>
<context>
    <name>QDial</name>
    <message>
        <source>QDial</source>
        <translation type="vanished">QDial</translation>
    </message>
    <message>
        <source>SpeedoMeter</source>
        <translation type="vanished">Miernik prędkości</translation>
    </message>
    <message>
        <source>SliderHandle</source>
        <translation type="vanished">Uchwyt suwaka</translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <source>What&apos;s This?</source>
        <translation type="vanished">Co to jest?</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="vanished">Zrobione</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <source>Abort</source>
        <translation type="vanished">Przerwij</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">Zastosuj</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="vanished">&amp;Zachowaj</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Anuluj</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;Anuluj</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="vanished">&amp;Zamknij</translation>
    </message>
    <message>
        <source>Close without Saving</source>
        <translation type="vanished">Zamknij bez zapisywania</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="vanished">Odrzuć</translation>
    </message>
    <message>
        <source>Don&apos;t Save</source>
        <translation type="vanished">Nie zachowuj</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Pomoc</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">Zignoruj</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="vanished">&amp;Nie</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation type="vanished">Ni&amp;e dla wszystkich</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Otwórz</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Zresetuj</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation type="vanished">Przywróć ustawienia</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Ponów</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Zachowaj</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation type="vanished">Zachowaj wszystko</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation type="vanished">&amp;Tak</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation type="vanished">Ta&amp;k dla wszystkich</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="vanished">&amp;OK</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <source>Date Modified</source>
        <translation type="vanished">Data modyfikacji</translation>
    </message>
    <message>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation type="vanished">Typ</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nazwa</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Rozmiar</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation type="vanished">Rodzaj</translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
    <message>
        <source>Dock</source>
        <translation type="vanished">Zadokuj</translation>
    </message>
    <message>
        <source>Float</source>
        <translation type="vanished">Uwolnij</translation>
    </message>
</context>
<context>
    <name>QDoubleSpinBox</name>
    <message>
        <source>More</source>
        <translation type="vanished">Więcej</translation>
    </message>
    <message>
        <source>Less</source>
        <translation type="vanished">Mniej</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <source>Debug Message:</source>
        <translation type="vanished">Komunikat dla programisty:</translation>
    </message>
    <message>
        <source>Fatal Error:</source>
        <translation type="vanished">Błąd krytyczny:</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="vanished">&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Show this message again</source>
        <translation type="vanished">&amp;Pokaż ten komunikat ponownie</translation>
    </message>
    <message>
        <source>Warning:</source>
        <translation type="vanished">Ostrzeżenie:</translation>
    </message>
</context>
<context>
    <name>QFile</name>
    <message>
        <source>Destination file exists</source>
        <translation type="vanished">Plik wyjściowy już istnieje</translation>
    </message>
    <message>
        <source>Will not rename sequential file using block copy</source>
        <translation type="vanished">Nie można zmienić nazwy pliku sekwencyjnego używając kopiowania blokowego</translation>
    </message>
    <message>
        <source>Cannot remove source file</source>
        <translation type="vanished">Nie można usunąć oryginalnego pliku</translation>
    </message>
    <message>
        <source>Cannot open %1 for input</source>
        <translation type="vanished">Nie można otworzyć pliku wejściowego %1</translation>
    </message>
    <message>
        <source>Cannot open for output</source>
        <translation type="vanished">Nie można otworzyć pliku wyjściowego</translation>
    </message>
    <message>
        <source>Failure to write block</source>
        <translation type="vanished">Nie można zapisać bloku</translation>
    </message>
    <message>
        <source>Cannot create %1 for output</source>
        <translation type="vanished">Nie można utworzyć pliku wyjściowego %1</translation>
    </message>
    <message>
        <source>No file engine available or engine does not support UnMapExtension</source>
        <translation type="vanished">Brak dostępnego silnika lub silnik nie obsługuje UnMapExtension</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation type="vanished">%1 już istnieje.
Czy chcesz zamienić?</translation>
    </message>
    <message>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation type="vanished">&apos;%1&apos; jest zabezpieczony przed zapisem.
Czy na pewno chcesz go skasować?</translation>
    </message>
    <message>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation type="vanished">%1
Katalog nie znaleziony.
Sprawdź podaną nazwę katalogu.</translation>
    </message>
    <message>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation type="vanished">%1
Plik nie znaleziony.
Proszę o sprawdzenie podanej nazwy pliku.</translation>
    </message>
    <message>
        <source>Are sure you want to delete &apos;%1&apos;?</source>
        <translation type="vanished">Czy na pewno chcesz skasować &apos;%1&apos;?</translation>
    </message>
    <message>
        <source>Recent Places</source>
        <translation type="vanished">Ostatnie miejsca</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Powrót</translation>
    </message>
    <message>
        <source>Could not delete directory.</source>
        <translation type="vanished">Nie można skasować katalogu.</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Usuń</translation>
    </message>
    <message>
        <source>Detail View</source>
        <translation type="vanished">Szczegóły</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation type="vanished">Katalogi</translation>
    </message>
    <message>
        <source>Directory:</source>
        <translation type="vanished">Katalog:</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation type="vanished">Urządzenie</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Plik</translation>
    </message>
    <message>
        <source>File Folder</source>
        <comment>Match Windows Explorer</comment>
        <translation type="vanished">Katalog</translation>
    </message>
    <message>
        <source>Folder</source>
        <comment>All other platforms</comment>
        <translation type="vanished">Katalog</translation>
    </message>
    <message>
        <source>Alias</source>
        <comment>Mac OS X Finder</comment>
        <translation type="vanished">Alias</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <comment>All other platforms</comment>
        <translation type="vanished">Skrót</translation>
    </message>
    <message>
        <source>Files of type:</source>
        <translation type="vanished">Pliki rodzaju:</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">Lista</translation>
    </message>
    <message>
        <source>My Computer</source>
        <translation type="vanished">Mój komputer</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">&amp;Otwórz</translation>
    </message>
    <message>
        <source>Parent Directory</source>
        <translation type="vanished">Katalog wyżej</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">&amp;Zmień nazwę</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="vanished">&amp;Zachowaj</translation>
    </message>
    <message>
        <source>Show &amp;hidden files</source>
        <translation type="vanished">Pokaż &amp;ukryte pliki</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Nieznany</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Otwórz</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="vanished">Zachowaj jako</translation>
    </message>
    <message>
        <source>Find Directory</source>
        <translation type="vanished">Znajdź katalog</translation>
    </message>
    <message>
        <source>All Files (*.*)</source>
        <translation type="vanished">Wszystkie pliki (*.*)</translation>
    </message>
    <message>
        <source>Show </source>
        <translation type="vanished">Pokaż </translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">Do przodu</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="vanished">Nowy katalog</translation>
    </message>
    <message>
        <source>&amp;New Folder</source>
        <translation type="vanished">&amp;Nowy katalog</translation>
    </message>
    <message>
        <source>&amp;Choose</source>
        <translation type="vanished">&amp;Wybierz</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">Usuń</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation type="vanished">Wszystkie pliki (*)</translation>
    </message>
    <message>
        <source>File &amp;name:</source>
        <translation type="vanished">Nazwa &amp;pliku:</translation>
    </message>
    <message>
        <source>Look in:</source>
        <translation type="vanished">Szukaj w:</translation>
    </message>
    <message>
        <source>Create New Folder</source>
        <translation type="vanished">Utwórz nowy katalog</translation>
    </message>
    <message>
        <source>Go back</source>
        <translation type="vanished">Wróć</translation>
    </message>
    <message>
        <source>Go forward</source>
        <translation type="vanished">Przejdź dalej</translation>
    </message>
    <message>
        <source>Go to the parent directory</source>
        <translation type="vanished">Przejdź do katalogu wyżej</translation>
    </message>
    <message>
        <source>Create a New Folder</source>
        <translation type="vanished">Utwórz nowy katalog</translation>
    </message>
    <message>
        <source>Change to list view mode</source>
        <translation type="vanished">Pokaż listę</translation>
    </message>
    <message>
        <source>Change to detail view mode</source>
        <translation type="vanished">Pokaż szczegóły</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <source>%1 TB</source>
        <translation type="vanished">%1 TB</translation>
    </message>
    <message>
        <source>%1 GB</source>
        <translation type="vanished">%1 GB</translation>
    </message>
    <message>
        <source>%1 MB</source>
        <translation type="vanished">%1 MB</translation>
    </message>
    <message>
        <source>%1 KB</source>
        <translation type="vanished">%1 KB</translation>
    </message>
    <message>
        <source>%1 bytes</source>
        <translation type="vanished">%1 bajtów</translation>
    </message>
    <message>
        <source>Invalid filename</source>
        <translation type="vanished">Niepoprawna nazwa pliku</translation>
    </message>
    <message>
        <source>&lt;b&gt;The name &quot;%1&quot; can not be used.&lt;/b&gt;&lt;p&gt;Try using another name, with fewer characters or no punctuations marks.</source>
        <translation type="vanished">&lt;b&gt;Nazwa &quot;%1&quot; nie może zostać użyta.&lt;/b&gt;&lt;p&gt;Spróbuj użyć nowej nazwy z mniejszą liczbą znaków lub bez znaków przystankowych.</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nazwa</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Rozmiar</translation>
    </message>
    <message>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation type="vanished">Typ</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation type="vanished">Rodzaj</translation>
    </message>
    <message>
        <source>Date Modified</source>
        <translation type="vanished">Data modyfikacji</translation>
    </message>
    <message>
        <source>My Computer</source>
        <translation type="vanished">Mój komputer</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation type="vanished">Komputer</translation>
    </message>
    <message>
        <source>%1 byte(s)</source>
        <translation type="vanished">%1 bajt(ów)</translation>
    </message>
</context>
<context>
    <name>QFontDatabase</name>
    <message>
        <source>Normal</source>
        <translation type="vanished">Normalny</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="vanished">Pogrubiony</translation>
    </message>
    <message>
        <source>Demi Bold</source>
        <translation type="vanished">Na wpół pogrubiony</translation>
    </message>
    <message>
        <source>Black</source>
        <translatorcomment>it&apos;s about font weight</translatorcomment>
        <translation type="vanished">Bardzo gruby</translation>
    </message>
    <message>
        <source>Demi</source>
        <translation type="vanished">Na wpół</translation>
    </message>
    <message>
        <source>Light</source>
        <translatorcomment>it&apos;s about font weight</translatorcomment>
        <translation type="vanished">Cienki</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="vanished">Kursywa</translation>
    </message>
    <message>
        <source>Oblique</source>
        <translation type="vanished">Pochyły</translation>
    </message>
    <message>
        <source>Any</source>
        <translation type="vanished">Każdy</translation>
    </message>
    <message>
        <source>Latin</source>
        <translation type="vanished">Łaciński</translation>
    </message>
    <message>
        <source>Greek</source>
        <translation type="vanished">Grecki</translation>
    </message>
    <message>
        <source>Cyrillic</source>
        <translation type="vanished">Cyrylica</translation>
    </message>
    <message>
        <source>Armenian</source>
        <translation type="vanished">Ormiański</translation>
    </message>
    <message>
        <source>Hebrew</source>
        <translation type="vanished">Hebrajski</translation>
    </message>
    <message>
        <source>Arabic</source>
        <translation type="vanished">Arabski</translation>
    </message>
    <message>
        <source>Syriac</source>
        <translation type="vanished">Syryjski</translation>
    </message>
    <message>
        <source>Thaana</source>
        <translation type="vanished">Thaana</translation>
    </message>
    <message>
        <source>Devanagari</source>
        <translation type="vanished">Devanagari</translation>
    </message>
    <message>
        <source>Bengali</source>
        <translation type="vanished">Bengalski</translation>
    </message>
    <message>
        <source>Gurmukhi</source>
        <translation type="vanished">Gurmukhi</translation>
    </message>
    <message>
        <source>Gujarati</source>
        <translation type="vanished">Gudżaracki</translation>
    </message>
    <message>
        <source>Oriya</source>
        <translation type="vanished">Orija</translation>
    </message>
    <message>
        <source>Tamil</source>
        <translation type="vanished">Tamilski</translation>
    </message>
    <message>
        <source>Telugu</source>
        <translation type="vanished">Telugu</translation>
    </message>
    <message>
        <source>Kannada</source>
        <translation type="vanished">Kannada</translation>
    </message>
    <message>
        <source>Malayalam</source>
        <translation type="vanished">Malajalam</translation>
    </message>
    <message>
        <source>Sinhala</source>
        <translation type="vanished">Syngaleski</translation>
    </message>
    <message>
        <source>Thai</source>
        <translation type="vanished">Tajski</translation>
    </message>
    <message>
        <source>Lao</source>
        <translation type="vanished">Laotański</translation>
    </message>
    <message>
        <source>Tibetan</source>
        <translation type="vanished">Tybetański</translation>
    </message>
    <message>
        <source>Myanmar</source>
        <translation type="vanished">Birmański</translation>
    </message>
    <message>
        <source>Georgian</source>
        <translation type="vanished">Gruziński</translation>
    </message>
    <message>
        <source>Khmer</source>
        <translation type="vanished">Khmerski</translation>
    </message>
    <message>
        <source>Simplified Chinese</source>
        <translation type="vanished">Uproszczony chiński</translation>
    </message>
    <message>
        <source>Traditional Chinese</source>
        <translation type="vanished">Tradycyjny chiński</translation>
    </message>
    <message>
        <source>Japanese</source>
        <translation type="vanished">Japoński</translation>
    </message>
    <message>
        <source>Korean</source>
        <translation type="vanished">Koreański</translation>
    </message>
    <message>
        <source>Vietnamese</source>
        <translation type="vanished">Wietnamski</translation>
    </message>
    <message>
        <source>Symbol</source>
        <translation type="vanished">Symboliczny</translation>
    </message>
    <message>
        <source>Ogham</source>
        <translation type="vanished">Ogamiczny</translation>
    </message>
    <message>
        <source>Runic</source>
        <translation type="vanished">Runiczny</translation>
    </message>
    <message>
        <source>N&apos;Ko</source>
        <translation type="vanished">N&apos;Ko</translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <source>Effects</source>
        <translation type="vanished">Efekty</translation>
    </message>
    <message>
        <source>&amp;Font</source>
        <translation type="vanished">&amp;Czcionka</translation>
    </message>
    <message>
        <source>Font st&amp;yle</source>
        <translation type="vanished">St&amp;yl czcionki</translation>
    </message>
    <message>
        <source>Sample</source>
        <translation type="vanished">Przykład</translation>
    </message>
    <message>
        <source>Select Font</source>
        <translation type="vanished">Wybierz czcionkę</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation type="vanished">&amp;Rozmiar</translation>
    </message>
    <message>
        <source>Stri&amp;keout</source>
        <translation type="vanished">Pr&amp;zekreślenie</translation>
    </message>
    <message>
        <source>&amp;Underline</source>
        <translation type="vanished">&amp;Podkreślenie</translation>
    </message>
    <message>
        <source>Wr&amp;iting System</source>
        <translation type="vanished">Sys&amp;tem pisania</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <source>Changing directory failed:
%1</source>
        <translation type="vanished">Zmiana katalogu zakończona błędem:
%1</translation>
    </message>
    <message>
        <source>Connected to host</source>
        <translation type="vanished">Podłączony do hosta</translation>
    </message>
    <message>
        <source>Connected to host %1</source>
        <translation type="vanished">Podłączony do hosta %1</translation>
    </message>
    <message>
        <source>Connecting to host failed:
%1</source>
        <translation type="vanished">Podłączanie do hosta zakończone błędem:
%1</translation>
    </message>
    <message>
        <source>Connection closed</source>
        <translation type="vanished">Połączenie zamknięte</translation>
    </message>
    <message>
        <source>Connection refused for data connection</source>
        <translation type="vanished">Połączenie do przesyłu danych odrzucone</translation>
    </message>
    <message>
        <source>Connection refused to host %1</source>
        <translation type="vanished">Połączenie do hosta %1 odrzucone</translation>
    </message>
    <message>
        <source>Connection to %1 closed</source>
        <translation type="vanished">Połączenie do %1 zakończone</translation>
    </message>
    <message>
        <source>Creating directory failed:
%1</source>
        <translation type="vanished">Tworzenie katalogu zakończone błędem:
%1</translation>
    </message>
    <message>
        <source>Downloading file failed:
%1</source>
        <translation type="vanished">Pobieranie pliku zakończone błędem:
%1</translation>
    </message>
    <message>
        <source>Host %1 found</source>
        <translation type="vanished">Host %1 znaleziony</translation>
    </message>
    <message>
        <source>Host %1 not found</source>
        <translation type="vanished">Host %1 nie znaleziony</translation>
    </message>
    <message>
        <source>Host found</source>
        <translation type="vanished">Host znaleziony</translation>
    </message>
    <message>
        <source>Listing directory failed:
%1</source>
        <translation type="vanished">Listowanie katalogu zakończone błędem:
%1</translation>
    </message>
    <message>
        <source>Login failed:
%1</source>
        <translation type="vanished">Logowanie zakończone błędem:
%1</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="vanished">Nie podłączony</translation>
    </message>
    <message>
        <source>Connection timed out to host %1</source>
        <translation type="vanished">Przekroczony czas połączenia do hosta %1</translation>
    </message>
    <message>
        <source>Removing directory failed:
%1</source>
        <translation type="vanished">Usuwanie katalogu zakończone błędem:
%1</translation>
    </message>
    <message>
        <source>Removing file failed:
%1</source>
        <translation type="vanished">Usuwanie pliku zakończone błędem:
%1</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Nieznany błąd</translation>
    </message>
    <message>
        <source>Uploading file failed:
%1</source>
        <translation type="vanished">Wysyłanie pliku zakończone błędem:
%1</translation>
    </message>
</context>
<context>
    <name>QGroupBox</name>
    <message>
        <source>Toggle</source>
        <translation type="vanished">Przełącz</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Nieznany błąd</translation>
    </message>
    <message>
        <source>No host name given</source>
        <translation type="vanished">Nie podano nazwy hosta</translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <source>Host not found</source>
        <translation type="vanished">Host nie znaleziony</translation>
    </message>
    <message>
        <source>Unknown address type</source>
        <translation type="vanished">Nieznany typ adresu</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Nieznany błąd</translation>
    </message>
    <message>
        <source>No host name given</source>
        <translation type="vanished">Nie podano nazwy hosta</translation>
    </message>
    <message>
        <source>Invalid hostname</source>
        <translation type="vanished">Niepoprawna nazwa hosta</translation>
    </message>
    <message>
        <source>Unknown error (%1)</source>
        <translation type="vanished">Nieznany błąd (%1)</translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <source>Connected to host</source>
        <translation type="vanished">Podłączony do hosta</translation>
    </message>
    <message>
        <source>Connected to host %1</source>
        <translation type="vanished">Podłączony do hosta %1</translation>
    </message>
    <message>
        <source>Connection closed</source>
        <translation type="vanished">Połączenie zakończone</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation type="vanished">Połączenie odrzucone</translation>
    </message>
    <message>
        <source>Connection to %1 closed</source>
        <translation type="vanished">Połączenie do %1 zamknięte</translation>
    </message>
    <message>
        <source>Host %1 found</source>
        <translation type="vanished">Host %1 znaleziony</translation>
    </message>
    <message>
        <source>Host %1 not found</source>
        <translation type="vanished">Host %1 nie znaleziony</translation>
    </message>
    <message>
        <source>Host found</source>
        <translation type="vanished">Host znaleziony</translation>
    </message>
    <message>
        <source>HTTP request failed</source>
        <translation type="vanished">Komenda HTTP zakończona błędem</translation>
    </message>
    <message>
        <source>Invalid HTTP chunked body</source>
        <translation type="vanished">Niepoprawne ciało HTTP</translation>
    </message>
    <message>
        <source>Invalid HTTP response header</source>
        <translation type="vanished">Niepoprawny nagłówek odpowiedzi HTTP</translation>
    </message>
    <message>
        <source>No server set to connect to</source>
        <translation type="vanished">Brak serwera do podłączenia</translation>
    </message>
    <message>
        <source>Request aborted</source>
        <translation type="vanished">Komenda przerwana</translation>
    </message>
    <message>
        <source>Server closed connection unexpectedly</source>
        <translation type="vanished">Serwer nieoczekiwanie zakończył połączenie</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Nieznany błąd</translation>
    </message>
    <message>
        <source>HTTPS connection requested but SSL support not compiled in</source>
        <translation type="vanished">Zażądano połączenia HTTPS lecz obsługa SSL nie jest wkompilowana</translation>
    </message>
    <message>
        <source>Wrong content length</source>
        <translation type="vanished">Błędna długość zawartości</translation>
    </message>
    <message>
        <source>Unknown authentication method</source>
        <translation type="vanished">Nieznana metoda autoryzacji</translation>
    </message>
    <message>
        <source>Proxy authentication required</source>
        <translation type="vanished">Wymagana autoryzacja pośrednika</translation>
    </message>
    <message>
        <source>Authentication required</source>
        <translation type="vanished">Wymagana autoryzacja</translation>
    </message>
    <message>
        <source>Error writing response to device</source>
        <translation type="vanished">Błąd zapisywania odpowiedzi do urządzenia</translation>
    </message>
    <message>
        <source>Proxy requires authentication</source>
        <translation type="vanished">Pośrednik wymaga autoryzacji</translation>
    </message>
    <message>
        <source>Host requires authentication</source>
        <translation type="vanished">Host wymaga autoryzacji</translation>
    </message>
    <message>
        <source>Data corrupted</source>
        <translation type="vanished">Dane uszkodzone</translation>
    </message>
    <message>
        <source>Unknown protocol specified</source>
        <translation type="vanished">Podano nieznany protokół</translation>
    </message>
    <message>
        <source>SSL handshake failed</source>
        <translation type="vanished">Nawiązanie sesji SSL zakończone błędem</translation>
    </message>
    <message>
        <source>Connection refused (or timed out)</source>
        <translation type="vanished">Połączenie odrzucone (przekroczony czas połączenia)</translation>
    </message>
</context>
<context>
    <name>QHttpSocketEngine</name>
    <message>
        <source>Did not receive HTTP response from proxy</source>
        <translation type="vanished">Nie odebrano odpowiedzi HTTP od pośrednika</translation>
    </message>
    <message>
        <source>Error parsing authentication request from proxy</source>
        <translation type="vanished">Błąd parsowania żądania autoryzacji od pośrednika</translation>
    </message>
    <message>
        <source>Authentication required</source>
        <translation type="vanished">Wymagana autoryzacja</translation>
    </message>
    <message>
        <source>Proxy denied connection</source>
        <translation type="vanished">Pośrednik odmówił połączenia</translation>
    </message>
    <message>
        <source>Error communicating with HTTP proxy</source>
        <translation type="vanished">Błąd podczas komunikacji z pośrednikiem HTTP</translation>
    </message>
    <message>
        <source>Proxy server not found</source>
        <translation type="vanished">Nie znaleziono serwera pośredniczącego</translation>
    </message>
    <message>
        <source>Proxy connection refused</source>
        <translation type="vanished">Odmowa połączenia z pośrednikiem</translation>
    </message>
    <message>
        <source>Proxy server connection timed out</source>
        <translation type="vanished">Przekroczony czas połączenia do serwera pośredniczącego</translation>
    </message>
    <message>
        <source>Proxy connection closed prematurely</source>
        <translation type="vanished">Przedwczesne zakończenie połączenia z pośrednikiem</translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <source>Could not start transaction</source>
        <translation type="vanished">Nie można rozpocząć transakcji</translation>
    </message>
    <message>
        <source>Error opening database</source>
        <translation type="vanished">Błąd otwierania bazy danych</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Nie można dokonać transakcji</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Nie można wycofać transakcji</translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <source>Could not allocate statement</source>
        <translation type="vanished">Nie można zaallokować polecenia</translation>
    </message>
    <message>
        <source>Could not describe input statement</source>
        <translation type="vanished">Nie można opisać polecenia wejściowego</translation>
    </message>
    <message>
        <source>Could not describe statement</source>
        <translation type="vanished">Nie można opisać polecenia</translation>
    </message>
    <message>
        <source>Could not fetch next item</source>
        <translation type="vanished">Nie można pobrać kolejnego elementu</translation>
    </message>
    <message>
        <source>Could not find array</source>
        <translation type="vanished">Nie można odnaleźć tablicy</translation>
    </message>
    <message>
        <source>Could not get array data</source>
        <translation type="vanished">Nie można pobrać danych z tablicy</translation>
    </message>
    <message>
        <source>Could not get query info</source>
        <translation type="vanished">Nie można pobrać informacji o zapytaniu</translation>
    </message>
    <message>
        <source>Could not get statement info</source>
        <translation type="vanished">Nie można pobrać informacji o poleceniu</translation>
    </message>
    <message>
        <source>Could not prepare statement</source>
        <translation type="vanished">Nie można przygotować polecenia</translation>
    </message>
    <message>
        <source>Could not start transaction</source>
        <translation type="vanished">Nie można rozpocząć transakcji</translation>
    </message>
    <message>
        <source>Unable to close statement</source>
        <translation type="vanished">Nie można zamknąć polecenia</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Nie można dokonać transakcji</translation>
    </message>
    <message>
        <source>Unable to create BLOB</source>
        <translation type="vanished">Nie można utworzyć obiektu typu BLOB</translation>
    </message>
    <message>
        <source>Unable to execute query</source>
        <translation type="vanished">Nie można wykonać zapytania</translation>
    </message>
    <message>
        <source>Unable to open BLOB</source>
        <translation type="vanished">Nie można otworzyć obiektu typu BLOB</translation>
    </message>
    <message>
        <source>Unable to read BLOB</source>
        <translation type="vanished">Nie można odczytać obiektu typu BLOB</translation>
    </message>
    <message>
        <source>Unable to write BLOB</source>
        <translation type="vanished">Nie można zapisać obiektu typu BLOB</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <source>No space left on device</source>
        <translation type="vanished">Brak wolnego miejsca na urządzeniu</translation>
    </message>
    <message>
        <source>No such file or directory</source>
        <translation type="vanished">Brak pliku lub katalogu</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Brak uprawnień</translation>
    </message>
    <message>
        <source>Too many open files</source>
        <translation type="vanished">Zbyt wiele otwartych plików</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Nieznany błąd</translation>
    </message>
</context>
<context>
    <name>QInputContext</name>
    <message>
        <source>Mac OS X input method</source>
        <translation type="vanished">Metoda wprowadzania Mac OS X</translation>
    </message>
    <message>
        <source>Windows input method</source>
        <translation type="vanished">Metoda wprowadzania Windows</translation>
    </message>
    <message>
        <source>XIM</source>
        <translation type="vanished">XIM</translation>
    </message>
    <message>
        <source>FEP</source>
        <translation type="vanished">FEP</translation>
    </message>
    <message>
        <source>XIM input method</source>
        <translation type="vanished">Metoda wprowadzania XIM</translation>
    </message>
    <message>
        <source>S60 FEP input method</source>
        <translation type="vanished">Metoda wprowadzania S60 FEP</translation>
    </message>
</context>
<context>
    <name>QInputDialog</name>
    <message>
        <source>Enter a value:</source>
        <translation type="vanished">Podaj wartość:</translation>
    </message>
</context>
<context>
    <name>QInputMethod</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickutilmodule.cpp" line="+64"/>
        <source>InputMethod is an abstract class</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <source>Plugin verification data mismatch in &apos;%1&apos;</source>
        <translation type="vanished">Niezgodność podczas weryfikacji danych we wtyczce &quot;%1&quot;</translation>
    </message>
    <message>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (%2.%3.%4) [%5]</source>
        <translation type="vanished">Wtyczka &apos;%1&apos; używa niepoprawnej wersji biblioteki QT. (%2.%3.%4) [%5]</translation>
    </message>
    <message>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. Expected build key &quot;%2&quot;, got &quot;%3&quot;</source>
        <translation type="vanished">Wtyczka &apos;%1&apos; używa niepoprawnej wersji biblioteki QT. Oczekiwano klucza &quot;%2&quot;, uzyskano &quot;%3&quot;</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Nieznany błąd</translation>
    </message>
    <message>
        <source>The shared library was not found.</source>
        <translation type="vanished">Biblioteka współdzielona niedostępna.</translation>
    </message>
    <message>
        <source>The file &apos;%1&apos; is not a valid Qt plugin.</source>
        <translation type="vanished">Plik &quot;%1&quot; nie jest poprawną wtyczką Qt.</translation>
    </message>
    <message>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (Cannot mix debug and release libraries.)</source>
        <translation type="vanished">Wtyczka &quot;%1&quot; używa innej wersji biblioteki Qt. (Nie można łączyć bibliotek zwykłych i debugowych.)</translation>
    </message>
    <message>
        <source>Cannot load library %1: %2</source>
        <translation type="vanished">Nie można załadować biblioteki %1: %2</translation>
    </message>
    <message>
        <source>Cannot unload library %1: %2</source>
        <translation type="vanished">Nie można zwolnić biblioteki %1: %2</translation>
    </message>
    <message>
        <source>Cannot resolve symbol &quot;%1&quot; in %2: %3</source>
        <translation type="vanished">Nie można zidentyfikować symbolu &quot;%1&quot; w %2: %3</translation>
    </message>
    <message>
        <source>&apos;%1&apos; is not an ELF object (%2)</source>
        <translation type="vanished">&quot;%1&quot; nie jest obiektem ELF (%2)</translation>
    </message>
    <message>
        <source>&apos;%1&apos; is not an ELF object</source>
        <translation type="vanished">&quot;%1&quot; nie jest obiektem ELF</translation>
    </message>
    <message>
        <source>&apos;%1&apos; is an invalid ELF object (%2)</source>
        <translation type="vanished">&quot;%1&quot; jest niepoprawnym obiektem ELF (%2)</translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">&amp;Kopiuj</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">W&amp;ytnij</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Usuń</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">&amp;Wklej</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation type="vanished">&amp;Przywróć</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Zaznacz wszystko</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation type="vanished">&amp;Cofnij</translation>
    </message>
</context>
<context>
    <name>QLocalServer</name>
    <message>
        <source>%1: Name error</source>
        <translation type="vanished">%1: Błąd nazwy</translation>
    </message>
    <message>
        <source>%1: Permission denied</source>
        <translation type="vanished">%1: Brak uprawnień</translation>
    </message>
    <message>
        <source>%1: Address in use</source>
        <translation type="vanished">%1: Adres użyty</translation>
    </message>
    <message>
        <source>%1: Unknown error %2</source>
        <translation type="vanished">%1: Nieznany błąd %2</translation>
    </message>
</context>
<context>
    <name>QLocalSocket</name>
    <message>
        <source>%1: Connection refused</source>
        <translation type="vanished">%1: Odmowa połączenia</translation>
    </message>
    <message>
        <source>%1: Remote closed</source>
        <translation type="vanished">%1: Drugi koniec odłączony</translation>
    </message>
    <message>
        <source>%1: Invalid name</source>
        <translation type="vanished">%1: Niepoprawna nazwa</translation>
    </message>
    <message>
        <source>%1: Socket access error</source>
        <translation type="vanished">%1: Błąd dostępu do gniazda</translation>
    </message>
    <message>
        <source>%1: Socket resource error</source>
        <translation type="vanished">%1: Błąd zasobów gniazda</translation>
    </message>
    <message>
        <source>%1: Socket operation timed out</source>
        <translation type="vanished">%1: Przekroczony czas operacji gniazda</translation>
    </message>
    <message>
        <source>%1: Datagram too large</source>
        <translation type="vanished">%1: Za duży datagram</translation>
    </message>
    <message>
        <source>%1: Connection error</source>
        <translation type="vanished">%1: Błąd połączenia</translation>
    </message>
    <message>
        <source>%1: The socket operation is not supported</source>
        <translation type="vanished">%1: Operacja nie jest obsługiwana przez gniazdo</translation>
    </message>
    <message>
        <source>%1: Unknown error</source>
        <translation type="vanished">%1: Nieznany błąd</translation>
    </message>
    <message>
        <source>%1: Unknown error %2</source>
        <translation type="vanished">%1: Nieznany błąd %2</translation>
    </message>
    <message>
        <source>%1: Access denied</source>
        <translation type="vanished">%1: Odmowa dostępu</translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="vanished">Nie można rozpocząć transakcji</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Nie można dokonać transakcji</translation>
    </message>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Nie można nawiązać połączenia</translation>
    </message>
    <message>
        <source>Unable to open database &apos;</source>
        <translation type="vanished">Nie można otworzyć bazy danych &apos;</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Nie można wycofać transakcji</translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <source>Unable to bind outvalues</source>
        <translation type="vanished">Nie można powiązać wartości zewnętrznych</translation>
    </message>
    <message>
        <source>Unable to bind value</source>
        <translation type="vanished">Nie można powiązać wartości</translation>
    </message>
    <message>
        <source>Unable to execute query</source>
        <translation type="vanished">Nie można wykonać zapytania</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Nie można wykonać polecenia</translation>
    </message>
    <message>
        <source>Unable to fetch data</source>
        <translation type="vanished">Nie można pobrać danych</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Nie można przygotować polecenia</translation>
    </message>
    <message>
        <source>Unable to reset statement</source>
        <translation type="vanished">Nie można zresetować polecenia</translation>
    </message>
    <message>
        <source>Unable to store result</source>
        <translation type="vanished">Nie można zachować wyników</translation>
    </message>
    <message>
        <source>Unable to store statement results</source>
        <translation type="vanished">Nie można zachować wyników polecenia</translation>
    </message>
    <message>
        <source>Unable to execute next query</source>
        <translation type="vanished">Nie można wykonać następnego zapytania</translation>
    </message>
    <message>
        <source>Unable to store next result</source>
        <translation type="vanished">Nie można zachować następnego wyniku</translation>
    </message>
</context>
<context>
    <name>QMdiArea</name>
    <message>
        <source>(Untitled)</source>
        <translation type="vanished">(Nienazwany)</translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <source>%1 - [%2]</source>
        <translation type="vanished">%1 - [%2]</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Zminimalizuj</translation>
    </message>
    <message>
        <source>Restore Down</source>
        <translation type="vanished">Przywróć pod spód</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">&amp;Przywróć</translation>
    </message>
    <message>
        <source>&amp;Move</source>
        <translation type="vanished">Prze&amp;nieś</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation type="vanished">&amp;Rozmiar</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation type="vanished">Zmi&amp;nimalizuj</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation type="vanished">Zma&amp;ksymalizuj</translation>
    </message>
    <message>
        <source>Stay on &amp;Top</source>
        <translation type="vanished">Pozostaw na &amp;wierzchu</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="vanished">&amp;Zamknij</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">Zmaksymalizuj</translation>
    </message>
    <message>
        <source>Unshade</source>
        <translation type="vanished">Rozwiń</translation>
    </message>
    <message>
        <source>Shade</source>
        <translation type="vanished">Zwiń</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">Przywróć</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Pomoc</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">Menu</translation>
    </message>
    <message>
        <source>- [%1]</source>
        <translation type="vanished">- [%1]</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
    <message>
        <source>Execute</source>
        <translation type="vanished">Wykonaj</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Otwórz</translation>
    </message>
</context>
<context>
    <name>QMenuBar</name>
    <message>
        <source>Actions</source>
        <translation type="vanished">Akcje</translation>
    </message>
    <message>
        <source>Corner Toolbar</source>
        <translation type="vanished">Narożny pasek narzędzi</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <source>About Qt</source>
        <translation type="vanished">Informacje o Qt</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Pomoc</translation>
    </message>
    <message>
        <source>Hide Details...</source>
        <translation type="vanished">Ukryj szczegóły...</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation type="vanished">&lt;h3&gt;Informacje o Qt&lt;/h3&gt;&lt;p&gt; Ten program używa Qt w wersji %1.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is available under three different licensing options designed to accommodate the needs of our various users.&lt;/p&gt;&lt;p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of the GNU LGPL version 2.1 or GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 2.1 is appropriate for the development of Qt applications (proprietary or open source) provided you can comply with the terms and conditions of the GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU General Public License version 3.0 is appropriate for the development of Qt applications where you wish to use such applications in combination with software subject to the terms of the GNU GPL version 3.0 or where you are otherwise willing to comply with the terms of the GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Qt jest zestawem narzędzi programistycznych dedykowanym dla języka C++. Służy on do opracowywania aplikacji międzyplatformowych.&lt;/p&gt;&lt;p&gt;Qt umożliwia jednoźródłowe przenoszenie między systemami MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux i wszystkimi głównymi wersjami komercyjnymi systemu Unix. Środowisko Qt jest dostępne dla urządzeń wbudowanych opartych na systemie Linux ( Qt dla wbudowanego systemu Linux) oraz Windows CE.&lt;/p&gt;&lt;p&gt;Zestaw Qt jest dostępny w trzech różnych opcjach licencjonowania stworzonych w celu zadowolenia naszych różnych użytkowników.&lt;/p&gt;&lt;p&gt;Qt podlegający licencji zgodnie z naszą komercyjną umową licencyjną jest odpowiedni do opracowywania oprogramowań własnościowych/komercyjnych, dzięki czemu kod źródłowy nie jest udostępniany osobom trzecim. W przeciwnym razie zestaw Qt jest niezgodny z warunkami licencji GNU LGPL w wersji 2.1 lub GNU GPL w wersji 3.0.&lt;/p&gt;&lt;p&gt;Środowisko Qt objęte licencją GNU LGPL w wersji 2.1 nadaje się do tworzenia aplikacji Qt (własnościowych lub oprogramowań otwartych) tylko wtedy, gdy przestrzegane są warunki licencji GNU LGPL w wersji 2.1.&lt;/p&gt;&lt;p&gt;Qt objęty Powszechną Licencją Publiczną GNU w wersji 3.0 jest odpowiedni do opracowywania aplikacji QT, aby móc korzystać z aplikacji w połączeniu z oprogramowaniem podlegającym warunkom licencji GNU GPL w wersji 3.0 lub aby przestrzegać warunków licencji GNU GPL w wersji 3.0.&lt;/p&gt;&lt;p&gt;Więcej informacji na temat licencji Qt można znaleźć na stronie &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;Copyright (C) 2012 Nokia Corporation i/lub oddziały firmy.&lt;/p&gt;&lt;p&gt;Qt jest produktem firmy Nokia. Dodatkowe informacje znajdują się na stronie &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; &lt;/p&gt;</translation>
    </message>
    <message>
        <source>Show Details...</source>
        <translation type="vanished">Pokaż szczegóły...</translation>
    </message>
</context>
<context>
    <name>QMultiInputContext</name>
    <message>
        <source>Select IM</source>
        <translation type="vanished">Wybierz metodę wprowadzania</translation>
    </message>
</context>
<context>
    <name>QMultiInputContextPlugin</name>
    <message>
        <source>Multiple input method switcher</source>
        <translation type="vanished">Przełącznik metody wprowadzania</translation>
    </message>
    <message>
        <source>Multiple input method switcher that uses the context menu of the text widgets</source>
        <translation type="vanished">Przełącznik metody wprowadzania, który w widżetach tekstowych używa podręcznego menu</translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <source>Another socket is already listening on the same port</source>
        <translation type="vanished">Inne gniazdo nasłuchuje już na tym porcie</translation>
    </message>
    <message>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation type="vanished">Próba użycia IPv6 na platformie bez obsługi IPv6</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation type="vanished">Połączenie odrzucone</translation>
    </message>
    <message>
        <source>Connection timed out</source>
        <translation type="vanished">Przekroczony czas połączenia</translation>
    </message>
    <message>
        <source>Datagram was too large to send</source>
        <translation type="vanished">Datagram za długi do wysłania</translation>
    </message>
    <message>
        <source>Host unreachable</source>
        <translation type="vanished">Komputer niedostępny</translation>
    </message>
    <message>
        <source>Invalid socket descriptor</source>
        <translation type="vanished">Niepoprawny opis gniazda</translation>
    </message>
    <message>
        <source>Network error</source>
        <translation type="vanished">Błąd sieci</translation>
    </message>
    <message>
        <source>Network operation timed out</source>
        <translation type="vanished">Przekroczony czas operacji sieciowej</translation>
    </message>
    <message>
        <source>Network unreachable</source>
        <translation type="vanished">Sieć niedostępna</translation>
    </message>
    <message>
        <source>Operation on non-socket</source>
        <translation type="vanished">Nieprawidłowa operacja na gnieździe</translation>
    </message>
    <message>
        <source>Out of resources</source>
        <translation type="vanished">Zasoby wyczerpane</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Brak uprawnień</translation>
    </message>
    <message>
        <source>Protocol type not supported</source>
        <translation type="vanished">Nieobsługiwany typ protokołu</translation>
    </message>
    <message>
        <source>The address is not available</source>
        <translation type="vanished">Adres nie jest dostępny</translation>
    </message>
    <message>
        <source>The address is protected</source>
        <translation type="vanished">Adres jest zabezpieczony</translation>
    </message>
    <message>
        <source>The bound address is already in use</source>
        <translation type="vanished">Adres jest aktualnie w użyciu</translation>
    </message>
    <message>
        <source>The remote host closed the connection</source>
        <translation type="vanished">Zdalny host zakończył połączenie</translation>
    </message>
    <message>
        <source>Unable to initialize broadcast socket</source>
        <translation type="vanished">Nie można uruchomić gniazda rozsyłającego</translation>
    </message>
    <message>
        <source>Unable to initialize non-blocking socket</source>
        <translation type="vanished">Nie można uruchomić gniazda w nieblokującym trybie</translation>
    </message>
    <message>
        <source>Unable to receive a message</source>
        <translation type="vanished">Nie można odebrać wiadomości</translation>
    </message>
    <message>
        <source>Unable to send a message</source>
        <translation type="vanished">Nie można wysłać wiadomości</translation>
    </message>
    <message>
        <source>Unable to write</source>
        <translation type="vanished">Nie można zapisać</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Nieznany błąd</translation>
    </message>
    <message>
        <source>Unsupported socket operation</source>
        <translation type="vanished">Nieobsługiwana operacja gniazda</translation>
    </message>
    <message>
        <source>The proxy type is invalid for this operation</source>
        <translation type="vanished">Typ pośrednika nie jest poprawny dla tej operacji</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessCacheBackend</name>
    <message>
        <source>Error opening %1</source>
        <translation type="vanished">Błąd otwierania %1</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessDataBackend</name>
    <message>
        <source>Invalid URI: %1</source>
        <translation type="vanished">Niepoprawny URI: %1</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessDebugPipeBackend</name>
    <message>
        <source>Write error writing to %1: %2</source>
        <translation type="vanished">Błąd w trakcie zapisywania do %1: %2</translation>
    </message>
    <message>
        <source>Socket error on %1: %2</source>
        <translation type="vanished">Błąd gniazda na %1: %2</translation>
    </message>
    <message>
        <source>Remote host closed the connection prematurely on %1</source>
        <translation type="vanished">Zdalny host przedwcześnie zakończył połączenie na %1</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFileBackend</name>
    <message>
        <source>Request for opening non-local file %1</source>
        <translation type="vanished">Żądanie otwarcia zdalnego pliku %1</translation>
    </message>
    <message>
        <source>Error opening %1: %2</source>
        <translation type="vanished">Błąd otwierania %1: %2</translation>
    </message>
    <message>
        <source>Write error writing to %1: %2</source>
        <translation type="vanished">Błąd w trakcie zapisywania do %1: %2</translation>
    </message>
    <message>
        <source>Cannot open %1: Path is a directory</source>
        <translation type="vanished">Nie można otworzyć %1: Ścieżka jest katalogiem</translation>
    </message>
    <message>
        <source>Read error reading from %1: %2</source>
        <translation type="vanished">Błąd w trakcie czytania z %1: %2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFtpBackend</name>
    <message>
        <source>No suitable proxy found</source>
        <translation type="vanished">Nie odnaleziono odpowiedniego pośrednika</translation>
    </message>
    <message>
        <source>Cannot open %1: is a directory</source>
        <translation type="vanished">Nie można otworzyć %1: jest to katalog</translation>
    </message>
    <message>
        <source>Logging in to %1 failed: authentication required</source>
        <translation type="vanished">Błąd podczas logowania do %1: wymagana autoryzacja</translation>
    </message>
    <message>
        <source>Error while downloading %1: %2</source>
        <translation type="vanished">Błąd podczas pobierania %1: %2</translation>
    </message>
    <message>
        <source>Error while uploading %1: %2</source>
        <translation type="vanished">Błąd podczas wysyłania %1: %2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessHttpBackend</name>
    <message>
        <source>No suitable proxy found</source>
        <translation type="vanished">Nie odnaleziono odpowiedniego pośrednika</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessManager</name>
    <message>
        <source>Network access is disabled.</source>
        <translation type="vanished">Dostęp do sieci wyłączony.</translation>
    </message>
</context>
<context>
    <name>QNetworkReply</name>
    <message>
        <source>Error downloading %1 - server replied: %2</source>
        <translation type="vanished">Błąd podczas pobierania %1 - odpowiedź serwera: %2</translation>
    </message>
    <message>
        <source>Protocol &quot;%1&quot; is unknown</source>
        <translation type="vanished">Protokół &quot;%1&quot; nie jest znany</translation>
    </message>
    <message>
        <source>Network session error.</source>
        <translation type="vanished">Błąd sesji sieciowej.</translation>
    </message>
    <message>
        <source>Temporary network failure.</source>
        <translation type="vanished">Chwilowy błąd w sieci.</translation>
    </message>
</context>
<context>
    <name>QNetworkReplyImpl</name>
    <message>
        <source>Operation canceled</source>
        <translation type="vanished">Operacja anulowana</translation>
    </message>
</context>
<context>
    <name>QNetworkSession</name>
    <message>
        <source>Invalid configuration.</source>
        <translation type="vanished">Niepoprawna konfiguracja.</translation>
    </message>
</context>
<context>
    <name>QNetworkSessionPrivateImpl</name>
    <message>
        <source>Roaming error</source>
        <translation type="vanished">Błąd roamingu</translation>
    </message>
    <message>
        <source>Session aborted by user or system</source>
        <translation type="vanished">Sesja przerwana przez użytkownika lub system</translation>
    </message>
    <message>
        <source>Unidentified Error</source>
        <translation type="vanished">Niezidentyfikowany błąd</translation>
    </message>
    <message>
        <source>Unknown session error.</source>
        <translation type="vanished">Nieznany błąd sesji.</translation>
    </message>
    <message>
        <source>The session was aborted by the user or system.</source>
        <translation type="vanished">Sesja została przerwana przez użytkownika lub system.</translation>
    </message>
    <message>
        <source>The requested operation is not supported by the system.</source>
        <translation type="vanished">Zażądana operacja nie jest obsługiwana przez system.</translation>
    </message>
    <message>
        <source>The specified configuration cannot be used.</source>
        <translation type="vanished">Podana konfiguracja nie może być użyta.</translation>
    </message>
    <message>
        <source>Roaming was aborted or is not possible.</source>
        <translation type="vanished">Roaming przerwany albo niemożliwy.</translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <source>Unable to initialize</source>
        <comment>QOCIDriver</comment>
        <translation type="vanished">Nie można dokonać inicjalizacji</translation>
    </message>
    <message>
        <source>Unable to logon</source>
        <translation type="vanished">Nie można się zalogować</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="vanished">Nie można rozpocząć transakcji</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Nie można dokonać transakcji</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Nie można wycofać transakcji</translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <source>Unable to bind column for batch execute</source>
        <translation type="vanished">Nie można powiązać kolumny dla wykonania zestawu poleceń</translation>
    </message>
    <message>
        <source>Unable to execute batch statement</source>
        <translation type="vanished">Nie można wykonać polecenia wsadowego</translation>
    </message>
    <message>
        <source>Unable to goto next</source>
        <translation type="vanished">Nie można przejść do kolejnego wiersza danych</translation>
    </message>
    <message>
        <source>Unable to alloc statement</source>
        <translation type="vanished">Nie można przydzielić miejsca na polecenie</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Nie można przygotować polecenia</translation>
    </message>
    <message>
        <source>Unable to get statement type</source>
        <translation type="vanished">Nie można pobrać typu polecenia</translation>
    </message>
    <message>
        <source>Unable to bind value</source>
        <translation type="vanished">Nie można powiązać wartości</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Nie można wykonać polecenia</translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Nie można dokonać transakcji</translation>
    </message>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Nie można nawiązać połączenia</translation>
    </message>
    <message>
        <source>Unable to disable autocommit</source>
        <translation type="vanished">Nie można wyłączyć trybu automatycznego dokonywania transakcji</translation>
    </message>
    <message>
        <source>Unable to enable autocommit</source>
        <translation type="vanished">Nie można włączyć trybu automatycznego dokonywania transakcji</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Nie można wycofać transakcji</translation>
    </message>
    <message>
        <source>Unable to connect - Driver doesn&apos;t support all functionality required</source>
        <translation type="vanished">Nie można nawiązać połączenia - sterownik nie obsługuje całej potrzebnej funkcjonalności</translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation type="vanished">QODBCResult::reset: Nie można ustawić &apos;SQL_CURSOR_STATIC&apos; jako atrybutu polecenia. Proszę sprawdzić konfiguracje sterownika ODBC</translation>
    </message>
    <message>
        <source>Unable to bind variable</source>
        <translation type="vanished">Nie można powiązać zmiennej</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Nie można wykonać polecenia</translation>
    </message>
    <message>
        <source>Unable to fetch next</source>
        <translation type="vanished">Nie można pobrać kolejnych danych</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Nie można przygotować polecenia</translation>
    </message>
    <message>
        <source>Unable to fetch last</source>
        <translation type="vanished">Nie można pobrać ostatnich danych</translation>
    </message>
    <message>
        <source>Unable to fetch</source>
        <translation type="vanished">Nie można pobrać</translation>
    </message>
    <message>
        <source>Unable to fetch first</source>
        <translation type="vanished">Nie można pobrać pierwszych danych</translation>
    </message>
    <message>
        <source>Unable to fetch previous</source>
        <translation type="vanished">Nie można pobrać poprzednich danych</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>PulseAudio Sound Server</source>
        <translation type="vanished">Serwer dźwięku PulseAudio</translation>
    </message>
    <message>
        <source>&quot;%1&quot; duplicates a previous role name and will be disabled.</source>
        <translation type="vanished">&quot;%1&quot; powiela poprzednią nazwę roli i zostanie wyłączone.</translation>
    </message>
    <message>
        <source>invalid query: &quot;%1&quot;</source>
        <translation type="vanished">Niepoprawne zapytanie: &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation type="vanished">Host nie znaleziony</translation>
    </message>
    <message>
        <source>Could not read image data</source>
        <translation type="vanished">Nie można odczytać danych obrazu</translation>
    </message>
    <message>
        <source>Sequential device (eg socket) for image read not supported</source>
        <translation type="vanished">Brak obsługi odczytu obrazu z urządzenia sekwencyjnego (np. gniazda)</translation>
    </message>
    <message>
        <source>Seek file/device for image read failed</source>
        <translation type="vanished">Przeszukiwanie pliku / urządzenia w celu odczytu obrazu zakończone błędem</translation>
    </message>
    <message>
        <source>Image mHeader read failed</source>
        <translation type="vanished">Błąd odczytu pola mHeader w obrazie</translation>
    </message>
    <message>
        <source>Image type not supported</source>
        <translation type="vanished">Nieobsługiwany typ obrazu</translation>
    </message>
    <message>
        <source>Image depth not valid</source>
        <translation type="vanished">Niepoprawna głębokość obrazu</translation>
    </message>
    <message>
        <source>Could not seek to image read footer</source>
        <translation type="vanished">Nie można odnaleźć nagłówka do odczytu obrazu</translation>
    </message>
    <message>
        <source>Could not read footer</source>
        <translation type="vanished">Nie można odczytać nagłówka</translation>
    </message>
    <message>
        <source>Image type (non-TrueVision 2.0) not supported</source>
        <translation type="vanished">Nieobsługiwany typ obrazu (inny od TrueVision 2.0)</translation>
    </message>
    <message>
        <source>Could not reset to start position</source>
        <translation type="vanished">Nie można zresetować do pozycji startowej</translation>
    </message>
</context>
<context>
    <name>QPPDOptionsModel</name>
    <message>
        <source>Name</source>
        <translation type="vanished">Nazwa</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Wartość</translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <source>Could not begin transaction</source>
        <translation type="vanished">Nie można rozpocząć transakcji</translation>
    </message>
    <message>
        <source>Could not commit transaction</source>
        <translation type="vanished">Nie można dokonać transakcji</translation>
    </message>
    <message>
        <source>Could not rollback transaction</source>
        <translation type="vanished">Nie można wycofać transakcji</translation>
    </message>
    <message>
        <source>Unable to connect</source>
        <translation type="vanished">Nie można nawiązać połączenia</translation>
    </message>
    <message>
        <source>Unable to subscribe</source>
        <translation type="vanished">Nie można wykonać subskrypcji</translation>
    </message>
    <message>
        <source>Unable to unsubscribe</source>
        <translation type="vanished">Nie można zrezygnować z subskrypcji</translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <source>Unable to create query</source>
        <translation type="vanished">Nie można utworzyć zapytania</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="vanished">Nie można przygotować polecenia</translation>
    </message>
</context>
<context>
    <name>QPageSetupWidget</name>
    <message>
        <source>Centimeters (cm)</source>
        <translation type="vanished">Centymetry (cm)</translation>
    </message>
    <message>
        <source>Millimeters (mm)</source>
        <translation type="vanished">Milimetry (mm)</translation>
    </message>
    <message>
        <source>Inches (in)</source>
        <translation type="vanished">Cale (in)</translation>
    </message>
    <message>
        <source>Points (pt)</source>
        <translation type="vanished">Punkty (pt)</translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="vanished">Formularz</translation>
    </message>
    <message>
        <source>Paper</source>
        <translation type="vanished">Papier</translation>
    </message>
    <message>
        <source>Page size:</source>
        <translation type="vanished">Rozmiar strony:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation type="vanished">Szerokość:</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation type="vanished">Wysokość:</translation>
    </message>
    <message>
        <source>Paper source:</source>
        <translation type="vanished">Źródło papieru:</translation>
    </message>
    <message>
        <source>Orientation</source>
        <translation type="vanished">Położenie</translation>
    </message>
    <message>
        <source>Portrait</source>
        <translation type="vanished">Portret</translation>
    </message>
    <message>
        <source>Landscape</source>
        <translation type="vanished">Pejzaż</translation>
    </message>
    <message>
        <source>Reverse landscape</source>
        <translation type="vanished">Odwrócony pejzaż</translation>
    </message>
    <message>
        <source>Reverse portrait</source>
        <translation type="vanished">Odwrócony portret</translation>
    </message>
    <message>
        <source>Margins</source>
        <translation type="vanished">Marginesy</translation>
    </message>
    <message>
        <source>top margin</source>
        <translation type="vanished">Górny margines</translation>
    </message>
    <message>
        <source>left margin</source>
        <translation type="vanished">Lewy margines</translation>
    </message>
    <message>
        <source>right margin</source>
        <translation type="vanished">Prawy margines</translation>
    </message>
    <message>
        <source>bottom margin</source>
        <translation type="vanished">Dolny margines</translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Nieznany błąd</translation>
    </message>
    <message>
        <source>The plugin was not loaded.</source>
        <translation type="vanished">Wtyczka nie została załadowana.</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <source>A0 (841 x 1189 mm)</source>
        <translation type="vanished">A0 (841 x 1189 mm)</translation>
    </message>
    <message>
        <source>A1 (594 x 841 mm)</source>
        <translation type="vanished">A1 (594 x 841 mm)</translation>
    </message>
    <message>
        <source>A2 (420 x 594 mm)</source>
        <translation type="vanished">A2 (420 x 594 mm)</translation>
    </message>
    <message>
        <source>A3 (297 x 420 mm)</source>
        <translation type="vanished">A3 (297 x 420 mm)</translation>
    </message>
    <message>
        <source>A4 (210 x 297 mm, 8.26 x 11.7 inches)</source>
        <translation type="vanished">A4 (210 x 297 mm, 8.26 x 11.7 cali)</translation>
    </message>
    <message>
        <source>A5 (148 x 210 mm)</source>
        <translation type="vanished">A5 (148 x 210 mm)</translation>
    </message>
    <message>
        <source>A6 (105 x 148 mm)</source>
        <translation type="vanished">A6 (105 x 148 mm)</translation>
    </message>
    <message>
        <source>A7 (74 x 105 mm)</source>
        <translation type="vanished">A7 (74 x 105 mm)</translation>
    </message>
    <message>
        <source>A8 (52 x 74 mm)</source>
        <translation type="vanished">A8 (52 x 74 mm)</translation>
    </message>
    <message>
        <source>A9 (37 x 52 mm)</source>
        <translation type="vanished">A9 (37 x 52 mm)</translation>
    </message>
    <message>
        <source>Print current page</source>
        <translation type="vanished">Wydrukuj bieżącą stronę</translation>
    </message>
    <message>
        <source>Aliases: %1</source>
        <translation type="vanished">Aliasy: %1</translation>
    </message>
    <message>
        <source>B0 (1000 x 1414 mm)</source>
        <translation type="vanished">B0 (1000 x 1414 mm)</translation>
    </message>
    <message>
        <source>B1 (707 x 1000 mm)</source>
        <translation type="vanished">B1 (707 x 1000 mm)</translation>
    </message>
    <message>
        <source>B10 (31 x 44 mm)</source>
        <translation type="vanished">B10 (31 x 44 mm)</translation>
    </message>
    <message>
        <source>B2 (500 x 707 mm)</source>
        <translation type="vanished">B2 (500 x 707 mm)</translation>
    </message>
    <message>
        <source>B3 (353 x 500 mm)</source>
        <translation type="vanished">B3 (353 x 500 mm)</translation>
    </message>
    <message>
        <source>B4 (250 x 353 mm)</source>
        <translation type="vanished">B4 (250 x 353 mm)</translation>
    </message>
    <message>
        <source>B5 (176 x 250 mm, 6.93 x 9.84 inches)</source>
        <translation type="vanished">B5 (176 x 250 mm, 6.93 x 9.84 cali)</translation>
    </message>
    <message>
        <source>B6 (125 x 176 mm)</source>
        <translation type="vanished">B6 (125 x 176 mm)</translation>
    </message>
    <message>
        <source>B7 (88 x 125 mm)</source>
        <translation type="vanished">B7 (88 x 125 mm)</translation>
    </message>
    <message>
        <source>B8 (62 x 88 mm)</source>
        <translation type="vanished">B8 (62 x 88 mm)</translation>
    </message>
    <message>
        <source>B9 (44 x 62 mm)</source>
        <translation type="vanished">B9 (44 x 62 mm)</translation>
    </message>
    <message>
        <source>C5E (163 x 229 mm)</source>
        <translation type="vanished">C5E (163 x 229 mm)</translation>
    </message>
    <message>
        <source>DLE (110 x 220 mm)</source>
        <translation type="vanished">DLE (110 x 220 mm)</translation>
    </message>
    <message>
        <source>locally connected</source>
        <translation type="vanished">podłączony lokalnie</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Print all</source>
        <translation type="vanished">Drukuj wszystko</translation>
    </message>
    <message>
        <source>Print range</source>
        <translation type="vanished">Drukuj zakres</translation>
    </message>
    <message>
        <source>unknown</source>
        <translation type="vanished">nieznany</translation>
    </message>
    <message>
        <source>Executive (7.5 x 10 inches, 191 x 254 mm)</source>
        <translation type="vanished">Executive (7.5 x 10 cali, 191 x 254 mm)</translation>
    </message>
    <message>
        <source>Folio (210 x 330 mm)</source>
        <translation type="vanished">Folio (210 x 330 mm)</translation>
    </message>
    <message>
        <source>Ledger (432 x 279 mm)</source>
        <translation type="vanished">Ledger (432 x 279 mm)</translation>
    </message>
    <message>
        <source>Legal (8.5 x 14 inches, 216 x 356 mm)</source>
        <translation type="vanished">Legal (8.5 x 14 cali, 216 x 356 mm)</translation>
    </message>
    <message>
        <source>Letter (8.5 x 11 inches, 216 x 279 mm)</source>
        <translation type="vanished">Letter (8.5 x 11 cali, 216 x 279 mm)</translation>
    </message>
    <message>
        <source>Tabloid (279 x 432 mm)</source>
        <translation type="vanished">Tabloid (279 x 432 mm)</translation>
    </message>
    <message>
        <source>US Common #10 Envelope (105 x 241 mm)</source>
        <translation type="vanished">US Common #10 Envelope (105 x 241 mm)</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="vanished">Drukowanie</translation>
    </message>
    <message>
        <source>Print To File ...</source>
        <translation type="vanished">Drukuj do pliku ...</translation>
    </message>
    <message>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation type="vanished">Plik %1 jest plikiem tylko do odczytu.
Proszę wybrać inną nazwę pliku.</translation>
    </message>
    <message>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation type="vanished">%1 już istnieje.
Czy chcesz nadpisać?</translation>
    </message>
    <message>
        <source>%1 is a directory.
Please choose a different file name.</source>
        <translation type="vanished">%1 jest katalogiem.
Proszę wybrać inną nazwę pliku.</translation>
    </message>
    <message>
        <source>File exists</source>
        <translation type="vanished">Plik istnieje</translation>
    </message>
    <message>
        <source>&lt;qt&gt;Do you want to overwrite it?&lt;/qt&gt;</source>
        <translation type="vanished">&lt;qt&gt;Czy chcesz nadpisać?&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>Print selection</source>
        <translation type="vanished">Drukuj zaznaczone</translation>
    </message>
    <message>
        <source>A0</source>
        <translation type="vanished">A0</translation>
    </message>
    <message>
        <source>A1</source>
        <translation type="vanished">A1</translation>
    </message>
    <message>
        <source>A2</source>
        <translation type="vanished">A2</translation>
    </message>
    <message>
        <source>A3</source>
        <translation type="vanished">A3</translation>
    </message>
    <message>
        <source>A4</source>
        <translation type="vanished">A4</translation>
    </message>
    <message>
        <source>A5</source>
        <translation type="vanished">A5</translation>
    </message>
    <message>
        <source>A6</source>
        <translation type="vanished">A6</translation>
    </message>
    <message>
        <source>A7</source>
        <translation type="vanished">A7</translation>
    </message>
    <message>
        <source>A8</source>
        <translation type="vanished">A8</translation>
    </message>
    <message>
        <source>A9</source>
        <translation type="vanished">A9</translation>
    </message>
    <message>
        <source>B0</source>
        <translation type="vanished">B0</translation>
    </message>
    <message>
        <source>B1</source>
        <translation type="vanished">B1</translation>
    </message>
    <message>
        <source>B2</source>
        <translation type="vanished">B2</translation>
    </message>
    <message>
        <source>B3</source>
        <translation type="vanished">B3</translation>
    </message>
    <message>
        <source>B4</source>
        <translation type="vanished">B4</translation>
    </message>
    <message>
        <source>B5</source>
        <translation type="vanished">B5</translation>
    </message>
    <message>
        <source>B6</source>
        <translation type="vanished">B6</translation>
    </message>
    <message>
        <source>B7</source>
        <translation type="vanished">B7</translation>
    </message>
    <message>
        <source>B8</source>
        <translation type="vanished">B8</translation>
    </message>
    <message>
        <source>B9</source>
        <translation type="vanished">B9</translation>
    </message>
    <message>
        <source>B10</source>
        <translation type="vanished">B10</translation>
    </message>
    <message>
        <source>C5E</source>
        <translation type="vanished">C5E</translation>
    </message>
    <message>
        <source>DLE</source>
        <translation type="vanished">DLE</translation>
    </message>
    <message>
        <source>Executive</source>
        <translation type="vanished">Executive</translation>
    </message>
    <message>
        <source>Folio</source>
        <translation type="vanished">Folio</translation>
    </message>
    <message>
        <source>Ledger</source>
        <translation type="vanished">Ledger</translation>
    </message>
    <message>
        <source>Legal</source>
        <translation type="vanished">Legal</translation>
    </message>
    <message>
        <source>Letter</source>
        <translation type="vanished">Letter</translation>
    </message>
    <message>
        <source>Tabloid</source>
        <translation type="vanished">Tabloid</translation>
    </message>
    <message>
        <source>US Common #10 Envelope</source>
        <translation type="vanished">US Common #10 Envelope</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="vanished">Niestandardowy</translation>
    </message>
    <message>
        <source>&amp;Options &gt;&gt;</source>
        <translation type="vanished">&amp;Opcje &gt;&gt;</translation>
    </message>
    <message>
        <source>&amp;Print</source>
        <translation type="vanished">Wy&amp;drukuj</translation>
    </message>
    <message>
        <source>&amp;Options &lt;&lt;</source>
        <translation type="vanished">&amp;Opcje &lt;&lt;</translation>
    </message>
    <message>
        <source>Print to File (PDF)</source>
        <translation type="vanished">Drukuj do pliku (PDF)</translation>
    </message>
    <message>
        <source>Print to File (Postscript)</source>
        <translation type="vanished">Drukuj do pliku (Postscript)</translation>
    </message>
    <message>
        <source>Local file</source>
        <translation type="vanished">Plik lokalny</translation>
    </message>
    <message>
        <source>Write %1 file</source>
        <translation type="vanished">Zapisz %1 plik</translation>
    </message>
    <message>
        <source>The &apos;From&apos; value cannot be greater than the &apos;To&apos; value.</source>
        <translation type="vanished">Wartość &quot;od&quot; nie może być większa od wartości &quot;do&quot;.</translation>
    </message>
</context>
<context>
    <name>QPrintPreviewDialog</name>
    <message>
        <source>%1%</source>
        <translation type="vanished">%1%</translation>
    </message>
    <message>
        <source>Print Preview</source>
        <translation type="vanished">Podgląd wydruku</translation>
    </message>
    <message>
        <source>Next page</source>
        <translation type="vanished">Następna strona</translation>
    </message>
    <message>
        <source>Previous page</source>
        <translation type="vanished">Poprzednia strona</translation>
    </message>
    <message>
        <source>First page</source>
        <translation type="vanished">Pierwsza strona</translation>
    </message>
    <message>
        <source>Last page</source>
        <translation type="vanished">Ostatnia strona</translation>
    </message>
    <message>
        <source>Fit width</source>
        <translation type="vanished">Dopasuj szerokość</translation>
    </message>
    <message>
        <source>Fit page</source>
        <translation type="vanished">Dopasuj stronę</translation>
    </message>
    <message>
        <source>Zoom in</source>
        <translation type="vanished">Powiększ</translation>
    </message>
    <message>
        <source>Zoom out</source>
        <translation type="vanished">Pomniejsz</translation>
    </message>
    <message>
        <source>Portrait</source>
        <translation type="vanished">Portret</translation>
    </message>
    <message>
        <source>Landscape</source>
        <translation type="vanished">Pejzaż</translation>
    </message>
    <message>
        <source>Show single page</source>
        <translation type="vanished">Pokaż pojedynczą stronę</translation>
    </message>
    <message>
        <source>Show facing pages</source>
        <translation type="vanished">Pokaż sąsiednie strony</translation>
    </message>
    <message>
        <source>Show overview of all pages</source>
        <translation type="vanished">Pokaż wszystkie strony</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="vanished">Wydrukuj</translation>
    </message>
    <message>
        <source>Page setup</source>
        <translation type="vanished">Ustawienia strony</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
    <message>
        <source>Export to PDF</source>
        <translation type="vanished">Wyeksportuj do PDF</translation>
    </message>
    <message>
        <source>Export to PostScript</source>
        <translation type="vanished">Wyeksportuj do PostScript</translation>
    </message>
    <message>
        <source>Page Setup</source>
        <translation type="vanished">Ustawienia strony</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesWidget</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Forma</translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="vanished">Strona</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">Zaawansowane</translation>
    </message>
</context>
<context>
    <name>QPrintSettingsOutput</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Forma</translation>
    </message>
    <message>
        <source>Copies</source>
        <translation type="vanished">Liczba kopii</translation>
    </message>
    <message>
        <source>Print range</source>
        <translation type="vanished">Zakres wydruku</translation>
    </message>
    <message>
        <source>Print all</source>
        <translation type="vanished">Drukuj wszystko</translation>
    </message>
    <message>
        <source>Pages from</source>
        <translation type="vanished">Strony od</translation>
    </message>
    <message>
        <source>to</source>
        <translation type="vanished">do</translation>
    </message>
    <message>
        <source>Selection</source>
        <translation type="vanished">Wybrane strony</translation>
    </message>
    <message>
        <source>Output Settings</source>
        <translation type="vanished">Ustawienia wyjściowe</translation>
    </message>
    <message>
        <source>Copies:</source>
        <translation type="vanished">Kopie:</translation>
    </message>
    <message>
        <source>Collate</source>
        <translation type="vanished">Parami</translation>
    </message>
    <message>
        <source>Reverse</source>
        <translation type="vanished">Odwróć</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">Opcje</translation>
    </message>
    <message>
        <source>Color Mode</source>
        <translation type="vanished">Tryb koloru</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Kolor</translation>
    </message>
    <message>
        <source>Grayscale</source>
        <translation type="vanished">Skala szarości</translation>
    </message>
    <message>
        <source>Duplex Printing</source>
        <translation type="vanished">Drukowanie dupleksowe</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">Brak</translation>
    </message>
    <message>
        <source>Long side</source>
        <translation type="vanished">Długa strona</translation>
    </message>
    <message>
        <source>Short side</source>
        <translation type="vanished">Krótka strona</translation>
    </message>
    <message>
        <source>Current Page</source>
        <translation type="vanished">Bieżąca strona</translation>
    </message>
</context>
<context>
    <name>QPrintWidget</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Forma</translation>
    </message>
    <message>
        <source>Printer</source>
        <translation type="vanished">Drukarka</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation type="vanished">&amp;Nazwa:</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">&amp;Właściwości</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">Położenie:</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation type="vanished">Podgląd</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation type="vanished">Typ:</translation>
    </message>
    <message>
        <source>Output &amp;file:</source>
        <translation type="vanished">&amp;Plik wyjściowy:</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
</context>
<context>
    <name>QProcess</name>
    <message>
        <source>Could not open input redirection for reading</source>
        <translation type="vanished">Nie można otworzyć wejściowego przekierowania do odczytu</translation>
    </message>
    <message>
        <source>Could not open output redirection for writing</source>
        <translation type="vanished">Nie można otworzyć wyjściowego przekierowania do zapisu</translation>
    </message>
    <message>
        <source>Resource error (fork failure): %1</source>
        <translation type="vanished">Błąd zasobów (błąd forkowania): %1</translation>
    </message>
    <message>
        <source>Process operation timed out</source>
        <translation type="vanished">Przekroczony czas operacji procesu</translation>
    </message>
    <message>
        <source>Error reading from process</source>
        <translation type="vanished">Błąd odczytywania z procesu</translation>
    </message>
    <message>
        <source>Error writing to process</source>
        <translation type="vanished">Błąd zapisywania do procesu</translation>
    </message>
    <message>
        <source>Process crashed</source>
        <translation type="vanished">Wystąpił błąd w procesie - proces zakończony</translation>
    </message>
    <message>
        <source>No program defined</source>
        <translation type="vanished">Nie zdefiniowano programu</translation>
    </message>
    <message>
        <source>Process failed to start: %1</source>
        <translation type="vanished">Nie można rozpocząć procesu: %1</translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Anuluj</translation>
    </message>
</context>
<context>
    <name>QPushButton</name>
    <message>
        <source>Open</source>
        <translation type="vanished">Otwórz</translation>
    </message>
</context>
<context>
    <name>QQmlAnonymousComponentResolver</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmltypecompiler.cpp" line="+1457"/>
        <source>Component objects cannot declare new functions.</source>
        <translation type="unfinished">Instancje komponentu nie mogą deklarować nowych funkcji.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component objects cannot declare new properties.</source>
        <translation type="unfinished">Instancje komponentu nie mogą deklarować nowych właściwości.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component objects cannot declare new signals.</source>
        <translation type="unfinished">Instancje komponentu nie mogą deklarować nowych sygnałów.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot create empty component specification</source>
        <translation type="unfinished">Nie można utworzyć pustej specyfikacji komponentu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Component elements may not contain properties other than id</source>
        <translation type="unfinished">Elementy komponentu nie mogą posiadać właściwości innych niż &quot;id&quot;</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid component body specification</source>
        <translation type="unfinished">Niepoprawna specyfikacja &quot;body&quot; komponentu</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>id is not unique</source>
        <translation type="unfinished">Wartość &quot;id&quot; nie jest unikatowa</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Invalid alias reference. Unable to find id &quot;%1&quot;</source>
        <translation type="unfinished">Niepoprawna referencja aliasu. Nie można odnaleźć identyfikatora &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+14"/>
        <location line="+9"/>
        <source>Invalid alias location</source>
        <translation type="unfinished">Niepoprawne położenie aliasu</translation>
    </message>
</context>
<context>
    <name>QQmlCodeGenerator</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmlirbuilder.cpp" line="+317"/>
        <location line="+689"/>
        <source>Property value set multiple times</source>
        <translation type="unfinished">Wartość właściwości ustawiona wielokrotnie</translation>
    </message>
    <message>
        <location line="-629"/>
        <location line="+656"/>
        <source>Expected type name</source>
        <translation type="unfinished">Oczekiwana nazwa typu</translation>
    </message>
    <message>
        <location line="-411"/>
        <source>Signal names cannot begin with an upper case letter</source>
        <translation type="unfinished">Nazwy sygnałów nie mogą rozpoczynać się wielką literą</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Illegal signal name</source>
        <translation type="unfinished">Niepoprawna nazwa sygnału</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>No property alias location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+6"/>
        <location line="+4"/>
        <source>Invalid alias reference. An alias reference must be specified as &lt;id&gt;, &lt;id&gt;.&lt;property&gt; or &lt;id&gt;.&lt;value property&gt;.&lt;property&gt;</source>
        <translation type="unfinished">Niepoprawna referencja aliasu. Referencja aliasu musi być podana jako &lt;id&gt;, &lt;id&gt;.&lt;property&gt; lub &lt;id&gt;.&lt;value property&gt;.&lt;property&gt;</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Invalid alias location</source>
        <translation type="unfinished">Niepoprawne położenie aliasu</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Illegal property name</source>
        <translation type="unfinished">Niepoprawna nazwa właściwości</translation>
    </message>
    <message>
        <location line="+187"/>
        <source>Invalid component id specification</source>
        <translation type="unfinished">Niepoprawna specyfikacja &quot;id&quot; komponentu</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Invalid empty ID</source>
        <translation type="unfinished">Niepoprawny pusty identyfikator</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>IDs cannot start with an uppercase letter</source>
        <translation type="unfinished">Identyfikatory nie mogą rozpoczynać się wielką literą</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>IDs must start with a letter or underscore</source>
        <translation type="unfinished">Identyfikatory muszą rozpoczynać się literą lub znakiem podkreślenia</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>IDs must contain only letters, numbers, and underscores</source>
        <translation type="unfinished">Identyfikatory mogą zawierać jedynie litery, cyfry i znaki podkreślenia</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>ID illegally masks global JavaScript property</source>
        <translation type="unfinished">Identyfikator nielegalnie maskuje globalną właściwość JavaScript</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Invalid use of id property</source>
        <translation type="unfinished">Niepoprawne użycie właściwości &quot;id&quot;</translation>
    </message>
</context>
<context>
    <name>QQmlComponent</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlcomponent.cpp" line="+668"/>
        <source>Invalid empty URL</source>
        <translation type="unfinished">Niepoprawny pusty URL</translation>
    </message>
    <message>
        <location line="+552"/>
        <location line="+126"/>
        <source>createObject: value is not an object</source>
        <translation type="unfinished">&quot;createObject&quot;: wartość nie jest obiektem</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlincubator.cpp" line="+290"/>
        <source>Object destroyed during incubation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQmlConnections</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/types/qqmlconnections.cpp" line="+207"/>
        <location line="+57"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation type="unfinished">Nie można przypisać wartości do nieistniejącej właściwości &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="-49"/>
        <source>Connections: nested objects not allowed</source>
        <translation type="unfinished">Połączenia: zagnieżdżone obiekty nie są dozwolone</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Connections: syntax error</source>
        <translation type="unfinished">Połączenia: błąd składni</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connections: script expected</source>
        <translation type="unfinished">Połączenia: oczekiwano skryptu</translation>
    </message>
</context>
<context>
    <name>QQmlDelegateModel</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/types/qqmldelegatemodel.cpp" line="+398"/>
        <source>The delegate of a DelegateModel cannot be changed within onUpdated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+206"/>
        <source>The maximum number of supported DelegateModelGroups is 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+109"/>
        <source>The group of a DelegateModel cannot be changed within onChanged</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQmlDelegateModelGroup</name>
    <message>
        <location line="-412"/>
        <source>Group names must start with a lower case letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2164"/>
        <source>get: index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+93"/>
        <source>insert: index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+85"/>
        <source>create: index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+53"/>
        <source>resolve: from index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>resolve: from index invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>resolve: to index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>resolve: to index invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>resolve: from is not an unresolved item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>resolve: to is not a model item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+70"/>
        <source>remove: invalid index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>remove: index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>remove: invalid count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+55"/>
        <source>addGroups: index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>addGroups: invalid count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>removeGroups: index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>removeGroups: invalid count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>setGroups: index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>setGroups: invalid count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+35"/>
        <source>move: invalid from index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>move: invalid to index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>move: invalid count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>move: from index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>move: to index out of range</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQmlEngine</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlengine.cpp" line="+211"/>
        <location line="+583"/>
        <source>Locale cannot be instantiated.  Use Qt.locale()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-225"/>
        <source>There are still &quot;%1&quot; items in the process of being created at engine destruction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/imports/localstorage/plugin.cpp" line="+271"/>
        <source>executeSql called outside transaction()</source>
        <translation type="unfinished">&quot;executeSql&quot; zawołane na zewnątrz &quot;transation()&quot;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Read-only Transaction</source>
        <translation type="unfinished">Transakcja tylko do odczytu</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Version mismatch: expected %1, found %2</source>
        <translation type="unfinished">Niezgodność wersji: oczekiwano %1, znaleziono %2</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>SQL transaction failed</source>
        <translation type="unfinished">Transakcja SQL zakończona błędem</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>transaction: missing callback</source>
        <translation type="unfinished">transakcja: brak wywołania zwrotnego</translation>
    </message>
    <message>
        <location line="+227"/>
        <source>SQL: can&apos;t create database, offline storage is disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+14"/>
        <source>SQL: database version mismatch</source>
        <translation type="unfinished">SQL: niezgodność wersji bazy danych</translation>
    </message>
</context>
<context>
    <name>QQmlEnumTypeResolver</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmltypecompiler.cpp" line="-504"/>
        <source>Invalid property assignment: &quot;%1&quot; is a read-only property</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: &quot;%1&quot; jest właściwością tylko do odczytu</translation>
    </message>
</context>
<context>
    <name>QQmlImportDatabase</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlimport.cpp" line="+558"/>
        <source>&quot;%1&quot; is ambiguous. Found in %2 and in %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+149"/>
        <source>- %1 is not a namespace</source>
        <translation type="unfinished">- %1 nie jest przestrzenią nazw</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>- nested namespaces not allowed</source>
        <translation type="unfinished">- zagnieżdżone przestrzenie nazw nie są dozwolone</translation>
    </message>
    <message>
        <location line="+58"/>
        <location line="+4"/>
        <source>local directory</source>
        <translation type="unfinished">lokalny katalog</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>is ambiguous. Found in %1 and in %2</source>
        <translation type="unfinished">jest niejednoznaczny. Znaleziono w %1 i w %2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>is ambiguous. Found in %1 in version %2.%3 and %4.%5</source>
        <translation type="unfinished">jest niejednoznaczny. Znaleziono w %1 w wersji %2.%3 i %4.%5</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>is instantiated recursively</source>
        <translation type="unfinished">jest zinstancjonowany rekurencyjnie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>is not a type</source>
        <translation type="unfinished">nie jest typem</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>static plugin for module &quot;%1&quot; with name &quot;%2&quot; has no metadata URI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+41"/>
        <source>module does not support the designer &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>plugin cannot be loaded for module &quot;%1&quot;: %2</source>
        <translation type="unfinished">wtyczka nie może zostać załadowana dla modułu &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>static plugin for module &quot;%1&quot; with name &quot;%2&quot; cannot be loaded: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>could not resolve all plugins for module &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>module &quot;%1&quot; plugin &quot;%2&quot; not found</source>
        <translation type="unfinished">wtyczka &quot;%2&quot; modułu &quot;%1&quot; nie została odnaleziona</translation>
    </message>
    <message>
        <location line="+166"/>
        <location line="+24"/>
        <source>&quot;%1&quot; version %2.%3 is defined more than once in module &quot;%4&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+87"/>
        <location line="+127"/>
        <source>module &quot;%1&quot; version %2.%3 is not installed</source>
        <translation type="unfinished">wersja %2.%3 modułu %1 nie jest zainstalowana</translation>
    </message>
    <message>
        <location line="-125"/>
        <location line="+127"/>
        <source>module &quot;%1&quot; is not installed</source>
        <translation type="unfinished">moduł &quot;%1&quot; nie jest zainstalowany</translation>
    </message>
    <message>
        <location line="-80"/>
        <source>&quot;%1&quot;: no such directory</source>
        <translation type="unfinished">&quot;%1&quot;: brak katalogu</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>import &quot;%1&quot; has no qmldir and no namespace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+473"/>
        <source>Module loaded for URI &apos;%1&apos; does not implement QQmlTypesExtensionInterface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Module namespace &apos;%1&apos; does not match import URI &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Namespace &apos;%1&apos; has already been used for type registration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Module &apos;%1&apos; does not contain a module identifier directive - it cannot be protected from external registrations.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+102"/>
        <source>File name case mismatch for &quot;%1&quot;</source>
        <translation type="unfinished">Niezgodność wielkości liter w &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QQmlListModel</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/types/qqmllistmodel.cpp" line="+1854"/>
        <source>unable to enable dynamic roles as this model is not empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>unable to enable static roles as this model is not empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>dynamic role setting must be made from the main thread, before any worker scripts are created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+62"/>
        <source>remove: indices [%1 - %2] out of range [0 - %3]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>remove: incorrect number of arguments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>insert: index %1 out of range</source>
        <translation type="unfinished">insert: indeks %1 poza zakresem</translation>
    </message>
    <message>
        <location line="+32"/>
        <location line="+3"/>
        <source>insert: value is not an object</source>
        <translation type="unfinished">insert: wartość nie jest obiektem</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>move: out of range</source>
        <translation type="unfinished">move: poza zakresem</translation>
    </message>
    <message>
        <location line="+89"/>
        <location line="+3"/>
        <source>append: value is not an object</source>
        <translation type="unfinished">append: wartość nie jest obiektem</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>set: value is not an object</source>
        <translation type="unfinished">set: wartość nie jest obiektem</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+46"/>
        <source>set: index %1 out of range</source>
        <translation type="unfinished">set: indeks %1 poza zakresem</translation>
    </message>
    <message>
        <location line="+50"/>
        <location line="+15"/>
        <source>ListElement: cannot contain nested elements</source>
        <translation type="unfinished">ListElement: nie może zawierać zagnieżdżonych elementów</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>ListElement: cannot use reserved &quot;id&quot; property</source>
        <translation type="unfinished">ListElement: nie można używać zarezerwowanej właściwości &quot;id&quot;</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>ListElement: cannot use script for property value</source>
        <translation type="unfinished">ListElement: nie można używać skryptu jako wartości właściwości</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>ListModel: undefined property &apos;%1&apos;</source>
        <translation type="unfinished">ListModel: niezdefiniowana właściwość &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QQmlObjectCreator</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlobjectcreator.cpp" line="+630"/>
        <source>Cannot assign value %1 to property %2</source>
        <translation type="unfinished">Nie można przypisać wartości %1 do właściwości %2</translation>
    </message>
    <message>
        <location line="+164"/>
        <location line="+12"/>
        <source>Cannot set properties on %1 as it is null</source>
        <translation type="unfinished">Nie można ustawić właściwości dla %1 ponieważ jest on zerowy</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Cannot assign an object to signal property %1</source>
        <translation type="unfinished">Nie można przypisać obiektu do właściwości sygnału %1</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cannot assign object type %1 with no default method</source>
        <translation type="unfinished">Nie można przypisać obiektu typu %1 który nie posiada domyślnej metody</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot connect mismatched signal/slot %1 %vs. %2</source>
        <translation type="unfinished">Nie można podłączyć niezgodnych sygnałów / slotów (%1 i %2)</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Cannot assign object to interface property</source>
        <translation type="unfinished">Nie można przypisać obiektu do właściwości interfejsu</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Cannot assign object to read only list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot assign primitives to lists</source>
        <translation type="unfinished">Nie można przypisać elementu do listy</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Unable to create object of type %1</source>
        <translation type="unfinished">Nie można utworzyć obiektu typu %1</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Composite Singleton Type %1 is not creatable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQmlParser</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmlirbuilder.cpp" line="-799"/>
        <source>Unexpected object definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+216"/>
        <source>Invalid import qualifier ID</source>
        <translation type="unfinished">Niepoprawny kwalifikator ID importu</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Reserved name &quot;Qt&quot; cannot be used as an qualifier</source>
        <translation type="unfinished">&quot;Qt&quot; jest nazwą zarezerwowaną i nie może być użyta jako kwalifikator</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Script import qualifiers must be unique.</source>
        <translation type="unfinished">Kwalifikator importu skryptu musi być unikatowy.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Script import requires a qualifier</source>
        <translation type="unfinished">Import skryptu wymaga użycia kwalifikatora</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Library import requires a version</source>
        <translation type="unfinished">Import biblioteki wymaga podania wersji</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <source>Pragma requires a valid qualifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+81"/>
        <source>Expected parameter type</source>
        <translation type="unfinished">Oczekiwany typ parametru</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Invalid signal parameter type: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Invalid property type modifier</source>
        <translation type="unfinished">Niepoprawny modyfikator typu właściwości</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unexpected property type modifier</source>
        <translation type="unfinished">Nieoczekiwany modyfikator typu właściwości</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Expected property type</source>
        <translation type="unfinished">Oczekiwany typ właściwości</translation>
    </message>
    <message>
        <location line="+111"/>
        <source>JavaScript declaration outside Script element</source>
        <translation type="unfinished">Deklaracja &quot;JavaScript&quot; na zewnątrz elementu &quot;Script&quot;</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/parser/qqmljslexer.cpp" line="+592"/>
        <location line="+416"/>
        <source>Illegal syntax for exponential number</source>
        <translation type="unfinished">Niepoprawna składnia liczby o postaci wykładniczej</translation>
    </message>
    <message>
        <location line="-323"/>
        <source>Stray newline in string literal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+46"/>
        <location line="+92"/>
        <location line="+25"/>
        <source>Illegal unicode escape sequence</source>
        <translation type="unfinished">Niepoprawny znak w sekwencji escape</translation>
    </message>
    <message>
        <location line="-106"/>
        <source>Illegal hexadecimal escape sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Octal escape sequences are not allowed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Unclosed string at end of line</source>
        <translation type="unfinished">Niedomknięty ciąg na końcu linii</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Decimal numbers can&apos;t start with &apos;0&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>At least one hexadecimal digit is required after &apos;0%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Invalid regular expression flag &apos;%0&apos;</source>
        <translation type="unfinished">Niepoprawna flaga &quot;%0&quot; w wyrażeniu regularnym</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+22"/>
        <source>Unterminated regular expression backslash sequence</source>
        <translation type="unfinished">Niedokończone sekwencja backslash wyrażenia regularnego</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unterminated regular expression class</source>
        <translation type="unfinished">Niedokończona klasa wyrażenia regularnego</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unterminated regular expression literal</source>
        <translation type="unfinished">Niedokończone wyrażenie regularne</translation>
    </message>
    <message>
        <location line="+204"/>
        <location line="+10"/>
        <location line="+117"/>
        <location filename="../../qtdeclarative/src/qml/parser/qqmljsparser.cpp" line="+1840"/>
        <location line="+67"/>
        <source>Syntax error</source>
        <translation type="unfinished">Błąd składni</translation>
    </message>
    <message>
        <location line="-93"/>
        <source>Imported file must be a script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+10"/>
        <location line="+12"/>
        <source>Invalid module URI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Module import requires a version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+15"/>
        <source>File import requires a qualifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+15"/>
        <source>Module import requires a qualifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid import qualifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/parser/qqmljsparser.cpp" line="-65"/>
        <source>Unexpected token `%1&apos;</source>
        <translation type="unfinished">Nieoczekiwany znak &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+24"/>
        <source>Expected token `%1&apos;</source>
        <translation type="unfinished">Oczekiwany znak &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QQmlPartsModel</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/types/qqmldelegatemodel.cpp" line="+58"/>
        <source>The group of a DelegateModel cannot be changed within onChanged</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Delegate component must be Package type.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQmlPropertyCacheCreator</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmltypecompiler.cpp" line="-638"/>
        <source>Fully dynamic types cannot declare new properties.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Fully dynamic types cannot declare new signals.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Fully Dynamic types cannot declare new functions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Non-existent attached object</source>
        <translation type="unfinished">Nieistniejący dołączony obiekt</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Cannot override FINAL property</source>
        <translation type="unfinished">Nie można nadpisać właściwości &quot;FINAL&quot;</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Invalid signal parameter type: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Duplicate signal name: invalid override of property change signal or superclass signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Duplicate method name: invalid override of property change signal or superclass signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Invalid property type</source>
        <translation type="unfinished">Niepoprawny typ właściwości</translation>
    </message>
</context>
<context>
    <name>QQmlPropertyValidator</name>
    <message>
        <location line="+999"/>
        <source>Property assignment expected</source>
        <translation type="unfinished">Oczekiwano przypisania wartości</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Invalid use of namespace</source>
        <translation type="unfinished">Niepoprawne użycie przestrzeni nazw</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid attached object assignment</source>
        <translation type="unfinished">Niepoprawne przypisanie dołączonego obiektu</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>&quot;%1.%2&quot; is not available in %3 %4.%5.</source>
        <translation type="unfinished">&quot;%1.%2&quot; nie jest dostępne w %3 %4.%5.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&quot;%1.%2&quot; is not available due to component versioning.</source>
        <translation type="unfinished">&quot;%1.%2&quot; nie jest dostępny z powodu niekompatybilności wersji komponentu.</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+46"/>
        <location line="+27"/>
        <source>Cannot assign a value directly to a grouped property</source>
        <translation type="unfinished">Nie można bezpośrednio przypisać wartości do zgrupowanej właściwości</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>Attached properties cannot be used here</source>
        <translation type="unfinished">Dołączone właściwości nie mogą być tutaj użyte</translation>
    </message>
    <message>
        <location line="+19"/>
        <location line="+39"/>
        <source>Invalid property assignment: &quot;%1&quot; is a read-only property</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: &quot;%1&quot; jest właściwością tylko do odczytu</translation>
    </message>
    <message>
        <location line="-32"/>
        <source>Cannot assign multiple values to a script property</source>
        <translation type="unfinished">Nie można przypisać wielu wartości do skryptowej właściwości</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot assign multiple values to a singular property</source>
        <translation type="unfinished">Nie można przypisać wielu wartości do pojedynczej właściwości</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Property has already been assigned a value</source>
        <translation type="unfinished">Wartość została już przypisana do właściwości</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+5"/>
        <source>Invalid grouped property access</source>
        <translation type="unfinished">Błędny dostęp do zgrupowanej właściwości</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Cannot assign to non-existent default property</source>
        <translation type="unfinished">Nie można przypisać wartości do nieistniejącej domyślnej właściwości</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation type="unfinished">Nie można przypisać wartości do nieistniejącej właściwości &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Cannot assign primitives to lists</source>
        <translation type="unfinished">Nie można przypisać elementu do listy</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Invalid property assignment: unknown enumeration</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: nieznana wartość wyliczeniowa</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid property assignment: string expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano ciągu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: string or string list expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: byte array expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: url expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano url</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid property assignment: unsigned int expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano liczby naturalnej</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Invalid property assignment: int expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano liczby całkowitej</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+7"/>
        <source>Invalid property assignment: number expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano liczby</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: color expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano koloru</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Invalid property assignment: date expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano daty</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: time expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano czasu</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: datetime expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano daty i czasu</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+9"/>
        <location line="+36"/>
        <source>Invalid property assignment: point expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano punktu</translation>
    </message>
    <message>
        <location line="-27"/>
        <location line="+9"/>
        <source>Invalid property assignment: size expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano rozmiaru</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: rect expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano prostokąta</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Invalid property assignment: boolean expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano wartości boolowskiej</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid property assignment: 3D vector expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano wektora 3D</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid property assignment: 4D vector expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano wektora 3D {4D?}</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: regular expression expected; use /pattern/ syntax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: number or array of numbers expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid property assignment: int or array of ints expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid property assignment: bool or array of bools expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: url or array of urls expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: string or array of strings expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid property assignment: unsupported type &quot;%1&quot;</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: nieobsługiwany typ &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&quot;%1&quot; cannot operate on &quot;%2&quot;</source>
        <translation type="unfinished">&quot;%1&quot; nie może operować na &quot;%2&quot;</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Cannot assign object to list property &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unexpected object assignment</source>
        <translation type="unfinished">Nieoczekiwane przypisanie obiektu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid property assignment: script expected</source>
        <translation type="unfinished">Niepoprawne przypisanie wartości: oczekiwano skryptu</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Cannot assign object to property</source>
        <translation type="unfinished">Nie można przypisać obiektu dla właściwości</translation>
    </message>
</context>
<context>
    <name>QQmlRewrite</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlpropertycache.cpp" line="+1166"/>
        <source>Signal uses unnamed parameter followed by named parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Signal parameter &quot;%1&quot; hides global variable.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQmlTypeCompiler</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmltypecompiler.cpp" line="-2301"/>
        <source>Composite Singleton Type %1 is not creatable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Element is not creatable.</source>
        <translation type="unfinished">Nie można utworzyć elementu (&quot;creatable&quot; wyłączone).</translation>
    </message>
</context>
<context>
    <name>QQmlTypeLoader</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlimport.cpp" line="-539"/>
        <source>Cannot update qmldir content for &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmltypeloader.cpp" line="+1415"/>
        <source>No matching type found, pragma Singleton files cannot be used by QQmlComponent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>pragma Singleton used with a non composite singleton type %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+592"/>
        <location line="+614"/>
        <source>Script %1 unavailable</source>
        <translation type="unfinished">Skrypt &quot;%1&quot; nie jest dostępny</translation>
    </message>
    <message>
        <location line="-595"/>
        <location line="+18"/>
        <source>Type %1 unavailable</source>
        <translation type="unfinished">Typ %1 nie jest dostępny</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>qmldir defines type as singleton, but no pragma Singleton found in type %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+138"/>
        <source>module &quot;%1&quot; is not installed</source>
        <translation type="unfinished">moduł &quot;%1&quot; nie jest zainstalowany</translation>
    </message>
    <message>
        <location line="+129"/>
        <location line="+62"/>
        <source>Namespace %1 cannot be used as a type</source>
        <translation type="unfinished">Przestrzeń nazw %1 nie może być użyta jako typ</translation>
    </message>
    <message>
        <location line="-55"/>
        <location line="+62"/>
        <source>Unreported error adding script import to import database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-59"/>
        <location line="+62"/>
        <source>%1 %2</source>
        <translation type="unfinished">%1 %2</translation>
    </message>
</context>
<context>
    <name>QQuickAbstractAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickanimation.cpp" line="+185"/>
        <source>Cannot animate non-existent property &quot;%1&quot;</source>
        <translation type="unfinished">Nie można animować nieistniejącej właściwości &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot animate read-only property &quot;%1&quot;</source>
        <translation type="unfinished">Nie można animować właściwości (tylko do odczytu): &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickutilmodule.cpp" line="+2"/>
        <source>Animation is an abstract class</source>
        <translation type="unfinished">&quot;Animation&quot; jest klasą abstrakcyjną</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Animator is an abstract class</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickAccessibleAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="+247"/>
        <source>Accessible is only available via attached properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickAnchorAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemanimation.cpp" line="+469"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation type="unfinished">Nie można ustawić ujemnego czasu trwania</translation>
    </message>
</context>
<context>
    <name>QQuickAnchors</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickanchors.cpp" line="+182"/>
        <source>Possible anchor loop detected on fill.</source>
        <translation type="unfinished">Wykryto możliwe zapętlenie dla kotwicy &quot;fill&quot;.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Possible anchor loop detected on centerIn.</source>
        <translation type="unfinished">Wykryto możliwe zapętlenie dla kotwicy &quot;centerIn&quot;.</translation>
    </message>
    <message>
        <location line="+258"/>
        <location line="+36"/>
        <location line="+733"/>
        <location line="+37"/>
        <source>Cannot anchor to an item that isn&apos;t a parent or sibling.</source>
        <translation type="unfinished">Nie można doczepić kotwicy do elementu który nie jest rodzicem ani rodzeństwem.</translation>
    </message>
    <message>
        <location line="-662"/>
        <source>Possible anchor loop detected on vertical anchor.</source>
        <translation type="unfinished">Wykryto możliwe zapętlenie dla pionowej kotwicy.</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Possible anchor loop detected on horizontal anchor.</source>
        <translation type="unfinished">Wykryto możliwe zapętlenie dla poziomej kotwicy.</translation>
    </message>
    <message>
        <location line="+514"/>
        <source>Cannot specify left, right, and horizontalCenter anchors at the same time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+37"/>
        <source>Cannot anchor to a null item.</source>
        <translation type="unfinished">Nie można doczepić kotwicy do zerowego elementu.</translation>
    </message>
    <message>
        <location line="-34"/>
        <source>Cannot anchor a horizontal edge to a vertical edge.</source>
        <translation type="unfinished">Nie można doczepić poziomej krawędzi do pionowej.</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+37"/>
        <source>Cannot anchor item to self.</source>
        <translation type="unfinished">Nie można doczepić kotwicy do tego samego elementu.</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>Cannot specify top, bottom, and verticalCenter anchors at the same time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Baseline anchor cannot be used in conjunction with top, bottom, or verticalCenter anchors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot anchor a vertical edge to a horizontal edge.</source>
        <translation type="unfinished">Nie można doczepić pionowej krawędzi do poziomej.</translation>
    </message>
</context>
<context>
    <name>QQuickAnimatedImage</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="-116"/>
        <source>Qt was built without support for QMovie</source>
        <translation type="unfinished">Qt zostało zbudowane bez obsługi QMovie</translation>
    </message>
</context>
<context>
    <name>QQuickApplication</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/qtquick2.cpp" line="+182"/>
        <source>Application is an abstract class</source>
        <translation type="unfinished">&quot;Application&quot; jest klasą abstrakcyjną</translation>
    </message>
</context>
<context>
    <name>QQuickBehavior</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickbehavior.cpp" line="+126"/>
        <source>Cannot change the animation assigned to a Behavior.</source>
        <translation type="unfinished">Nie można zmienić animacji przypisanej do &quot;Zachowania&quot;.</translation>
    </message>
</context>
<context>
    <name>QQuickDragAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="+108"/>
        <source>Drag is only available via attached properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickFlipable</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickflipable.cpp" line="+150"/>
        <source>front is a write-once property</source>
        <translation type="unfinished">&quot;front&quot; jest właściwością tylko do odczytu</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>back is a write-once property</source>
        <translation type="unfinished">&quot;back&quot; jest właściwością tylko do odczytu</translation>
    </message>
</context>
<context>
    <name>QQuickItemView</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="+13"/>
        <location line="+1"/>
        <source>ItemView is an abstract base class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemview.cpp" line="+2283"/>
        <source>Delegate must be of Item type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickKeyNavigationAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="-48"/>
        <source>KeyNavigation is only available via attached properties</source>
        <translation type="unfinished">&quot;KeyNavigation&quot; jest dostępny jedynie poprzez dołączone właściwości</translation>
    </message>
</context>
<context>
    <name>QQuickKeysAttached</name>
    <message>
        <location line="+1"/>
        <source>Keys is only available via attached properties</source>
        <translation type="unfinished">&quot;Keys&quot; jest dostępny jedynie poprzez dołączone właściwości</translation>
    </message>
</context>
<context>
    <name>QQuickLayoutMirroringAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitem.cpp" line="+1514"/>
        <source>LayoutDirection attached property only works with Items</source>
        <translation type="unfinished">Dołączona właściwość &quot;LayoutDirection&quot; działa tylko z &quot;Item&quot;</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="+1"/>
        <source>LayoutMirroring is only available via attached properties</source>
        <translation type="unfinished">&quot;LayoutMirroring&quot; dostępny jest tylko poprzez dołączone właściwości</translation>
    </message>
</context>
<context>
    <name>QQuickLoader</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickloader.cpp" line="+947"/>
        <source>setSource: value is not an object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickOpenGLInfo</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="+62"/>
        <source>OpenGLInfo is only available via attached properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickPaintedItem</name>
    <message>
        <location line="-50"/>
        <source>Cannot create instance of abstract class PaintedItem</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickParentAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemanimation.cpp" line="-170"/>
        <source>Unable to preserve appearance under complex transform</source>
        <translation type="unfinished">Nie można utrzymać wyglądu przy złożonej transformacji</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+7"/>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation type="unfinished">Nie można utrzymać wyglądu przy niejednolitej skali</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation type="unfinished">Nie można utrzymać wyglądu przy zerowej skali</translation>
    </message>
</context>
<context>
    <name>QQuickParentChange</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickstateoperations.cpp" line="+75"/>
        <source>Unable to preserve appearance under complex transform</source>
        <translation type="unfinished">Nie można utrzymać wyglądu przy złożonej transformacji</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+7"/>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation type="unfinished">Nie można utrzymać wyglądu przy niejednolitej skali</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation type="unfinished">Nie można utrzymać wyglądu przy zerowej skali</translation>
    </message>
</context>
<context>
    <name>QQuickPathAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemanimation.cpp" line="+282"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation type="unfinished">Nie można ustawić ujemnego czasu trwania</translation>
    </message>
</context>
<context>
    <name>QQuickPathView</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickpathview.cpp" line="+140"/>
        <source>Delegate must be of Item type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickPauseAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickanimation.cpp" line="+510"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation type="unfinished">Nie można ustawić ujemnego czasu trwania</translation>
    </message>
</context>
<context>
    <name>QQuickPixmap</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickpixmapcache.cpp" line="+393"/>
        <source>Error decoding: %1: %2</source>
        <translation type="unfinished">Błąd dekodowania: %1: %2</translation>
    </message>
    <message>
        <location line="+174"/>
        <location line="+461"/>
        <source>Invalid image provider: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-449"/>
        <location line="+12"/>
        <location line="+467"/>
        <source>Failed to get image from provider: %1</source>
        <translation type="unfinished">Pobieranie obrazka od dostawcy zakończone błędem: %1</translation>
    </message>
    <message>
        <location line="-455"/>
        <source>Failed to get texture from provider: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+452"/>
        <source>Cannot open: %1</source>
        <translation type="unfinished">Nie można otworzyć: %1</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Invalid image data: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickPropertyAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickanimation.cpp" line="+1368"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation type="unfinished">Nie można ustawić ujemnego czasu trwania</translation>
    </message>
</context>
<context>
    <name>QQuickPropertyChanges</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickpropertychanges.cpp" line="+236"/>
        <source>PropertyChanges does not support creating state-specific objects.</source>
        <translation type="unfinished">&quot;PropertyChanges&quot; nie obsługuje tworzenia obiektów charakterystycznych dla stanów.</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation type="unfinished">Nie można przypisać wartości do nieistniejącej właściwości &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot assign to read-only property &quot;%1&quot;</source>
        <translation type="unfinished">Nie można przypisać wartości do właściwości (tylko do odczytu): &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QQuickRepeater</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickrepeater.cpp" line="+401"/>
        <source>Delegate must be of Item type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickShaderEffectMesh</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="-3"/>
        <source>Cannot create instance of abstract class ShaderEffectMesh.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickTextUtil</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquicktextutil.cpp" line="+58"/>
        <source>%1 does not support loading non-visual cursor delegates.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Could not load cursor delegate</source>
        <translation type="unfinished">Nie można załadować delegata kursora</translation>
    </message>
</context>
<context>
    <name>QQuickViewTransitionAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="-8"/>
        <source>ViewTransition is only available via attached properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickWindow</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickwindow.cpp" line="+2492"/>
        <source>Failed to create %1 context for format %2.
This is most likely caused by not having the necessary graphics drivers installed.

Install a driver providing OpenGL 2.0 or higher, or, if this is not possible, make sure the ANGLE Open GL ES 2.0 emulation libraries (%3, %4 and d3dcompiler_*.dll) are available in the application executable&apos;s directory or in a location listed in PATH.</source>
        <extracomment>%1 Context type (Open GL, EGL), %2 format, ANGLE %3, %4 library names</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failed to create %1 context for format %2</source>
        <extracomment>%1 Context type (Open GL, EGL), %2 format specification</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickWindowQmlImpl</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickwindowmodule.cpp" line="+146"/>
        <source>Conflicting properties &apos;visible&apos; and &apos;visibility&apos; for Window &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicting properties &apos;visible&apos; and &apos;visibility&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickXmlListModel</name>
    <message>
        <location filename="../../qtdeclarative/src/imports/xmllistmodel/qqmlxmllistmodel.cpp" line="+593"/>
        <source>&quot;%1&quot; duplicates a previous role name and will be disabled.</source>
        <translation type="unfinished">&quot;%1&quot; powiela poprzednią nazwę roli i zostanie wyłączone.</translation>
    </message>
    <message>
        <location line="+540"/>
        <location line="+4"/>
        <source>invalid query: &quot;%1&quot;</source>
        <translation type="unfinished">Niepoprawne zapytanie: &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QQuickXmlListModelRole</name>
    <message>
        <location filename="../../qtdeclarative/src/imports/xmllistmodel/qqmlxmllistmodel_p.h" line="+163"/>
        <source>An XmlRole query must not start with &apos;/&apos;</source>
        <translation type="unfinished">Zapytanie XmlRole nie może rozpoczynać się od &quot;/&quot;</translation>
    </message>
</context>
<context>
    <name>QQuickXmlRoleList</name>
    <message>
        <location filename="../../qtdeclarative/src/imports/xmllistmodel/qqmlxmllistmodel.cpp" line="-293"/>
        <source>An XmlListModel query must start with &apos;/&apos; or &quot;//&quot;</source>
        <translation type="unfinished">Zapytanie XmlListModel nie może rozpoczynać się od &quot;/&quot; ani od &quot;//&quot;</translation>
    </message>
</context>
<context>
    <name>QRadioButton</name>
    <message>
        <source>Check</source>
        <translation type="vanished">Zaznacz</translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <source>bad char class syntax</source>
        <translation type="vanished">niepoprawna składnia klasy znakowej</translation>
    </message>
    <message>
        <source>bad lookahead syntax</source>
        <translation type="vanished">niepoprawna składnia &quot;lookahead&quot;</translation>
    </message>
    <message>
        <source>lookbehinds not supported, see QTBUG-2371</source>
        <translation type="vanished">&quot;lookbehinds&quot; nie jest obsługiwane, zobacz QTBUG-2371</translation>
    </message>
    <message>
        <source>bad repetition syntax</source>
        <translation type="vanished">niepoprawna składnia powtórzenia</translation>
    </message>
    <message>
        <source>invalid interval</source>
        <translation type="vanished">Niepoprawny interwał</translation>
    </message>
    <message>
        <source>invalid category</source>
        <translation type="vanished">Niepoprawna kategoria</translation>
    </message>
    <message>
        <source>disabled feature used</source>
        <translation type="vanished">użyta funkcja została wyłączona</translation>
    </message>
    <message>
        <source>invalid octal value</source>
        <translation type="vanished">niepoprawna wartość ósemkowa</translation>
    </message>
    <message>
        <source>met internal limit</source>
        <translation type="vanished">napotkano wewnętrzne ograniczenie</translation>
    </message>
    <message>
        <source>missing left delim</source>
        <translation type="vanished">brakujący lewy separator</translation>
    </message>
    <message>
        <source>no error occurred</source>
        <translation type="vanished">nie pojawił się żaden błąd</translation>
    </message>
    <message>
        <source>unexpected end</source>
        <translation type="vanished">nieoczekiwany koniec</translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <source>Error opening database</source>
        <translation type="vanished">Błąd otwierania bazy danych</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="vanished">Nie można rozpocząć transakcji</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Nie można dokonać transakcji</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Nie można wycofać transakcji</translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Nie można wykonać polecenia</translation>
    </message>
    <message>
        <source>Unable to fetch results</source>
        <translation type="vanished">Nie można pobrać wyników</translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <source>Error closing database</source>
        <translation type="vanished">Błąd zamykania bazy danych</translation>
    </message>
    <message>
        <source>Error opening database</source>
        <translation type="vanished">Błąd otwierania bazy danych</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="vanished">Nie można rozpocząć transakcji</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Nie można dokonać transakcji</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Nie można wycofać transakcji</translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <source>Parameter count mismatch</source>
        <translation type="vanished">Niezgodna liczba parametrów</translation>
    </message>
    <message>
        <source>Unable to bind parameters</source>
        <translation type="vanished">Nie można powiązać parametrów</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Nie można wykonać polecenia</translation>
    </message>
    <message>
        <source>Unable to fetch row</source>
        <translation type="vanished">Nie można pobrać wiersza danych</translation>
    </message>
    <message>
        <source>Unable to execute multiple statements at a time</source>
        <translation type="vanished">Nie można wykonać wielu poleceń jednocześnie</translation>
    </message>
    <message>
        <source>Unable to reset statement</source>
        <translation type="vanished">Nie można zresetować polecenia</translation>
    </message>
    <message>
        <source>No query</source>
        <translation type="vanished">Brak zapytania</translation>
    </message>
</context>
<context>
    <name>QScriptBreakpointsModel</name>
    <message>
        <source>ID</source>
        <translation type="vanished">Identyfikator</translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="vanished">Położenie</translation>
    </message>
    <message>
        <source>Condition</source>
        <translation type="vanished">Warunek</translation>
    </message>
    <message>
        <source>Ignore-count</source>
        <translation type="vanished">Licznik pominięć</translation>
    </message>
    <message>
        <source>Single-shot</source>
        <translation type="vanished">Pojedyncze trafienie</translation>
    </message>
    <message>
        <source>Hit-count</source>
        <translation type="vanished">Licznik trafień</translation>
    </message>
</context>
<context>
    <name>QScriptBreakpointsWidget</name>
    <message>
        <source>New</source>
        <translation type="vanished">Nowy</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Usuń</translation>
    </message>
</context>
<context>
    <name>QScriptDebugger</name>
    <message>
        <source>Go to Line</source>
        <translation type="vanished">Przejdź do linii</translation>
    </message>
    <message>
        <source>Line:</source>
        <translation type="vanished">Linia:</translation>
    </message>
    <message>
        <source>Interrupt</source>
        <translation type="vanished">Przerwij</translation>
    </message>
    <message>
        <source>Shift+F5</source>
        <translation type="vanished">Shift+F5</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">Kontynuuj</translation>
    </message>
    <message>
        <source>F5</source>
        <translation type="vanished">F5</translation>
    </message>
    <message>
        <source>Step Into</source>
        <translation type="vanished">Wskocz do wnętrza</translation>
    </message>
    <message>
        <source>F11</source>
        <translation type="vanished">F11</translation>
    </message>
    <message>
        <source>Step Over</source>
        <translation type="vanished">Przeskocz</translation>
    </message>
    <message>
        <source>F10</source>
        <translation type="vanished">F10</translation>
    </message>
    <message>
        <source>Step Out</source>
        <translation type="vanished">Wyskocz na zewnątrz</translation>
    </message>
    <message>
        <source>Shift+F11</source>
        <translation type="vanished">Shift+F11</translation>
    </message>
    <message>
        <source>Run to Cursor</source>
        <translation type="vanished">Uruchom do kursora</translation>
    </message>
    <message>
        <source>Ctrl+F10</source>
        <translation type="vanished">Ctrl+F10</translation>
    </message>
    <message>
        <source>Run to New Script</source>
        <translation type="vanished">Uruchom do nowego skryptu</translation>
    </message>
    <message>
        <source>Toggle Breakpoint</source>
        <translation type="vanished">Przełącz ustawienie pułapki</translation>
    </message>
    <message>
        <source>F9</source>
        <translation type="vanished">F9</translation>
    </message>
    <message>
        <source>Clear Debug Output</source>
        <translation type="vanished">Wyczyść wyjście debuggera</translation>
    </message>
    <message>
        <source>Clear Error Log</source>
        <translation type="vanished">Wyczyść log z błędami</translation>
    </message>
    <message>
        <source>Clear Console</source>
        <translation type="vanished">Wyczyść konsolę</translation>
    </message>
    <message>
        <source>&amp;Find in Script...</source>
        <translation type="vanished">&amp;Znajdź w skrypcie...</translation>
    </message>
    <message>
        <source>Ctrl+F</source>
        <translation type="vanished">Ctrl+F</translation>
    </message>
    <message>
        <source>Find &amp;Next</source>
        <translation type="vanished">Znajdź &amp;następne</translation>
    </message>
    <message>
        <source>F3</source>
        <translation type="vanished">F3</translation>
    </message>
    <message>
        <source>Find &amp;Previous</source>
        <translation type="vanished">Znajdź &amp;poprzednie</translation>
    </message>
    <message>
        <source>Shift+F3</source>
        <translation type="vanished">Shift+F3</translation>
    </message>
    <message>
        <source>Ctrl+G</source>
        <translation type="vanished">Ctrl+G</translation>
    </message>
    <message>
        <source>Debug</source>
        <translation type="vanished">Debuguj</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerCodeFinderWidget</name>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="vanished">Poprzednie</translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="vanished">Następne</translation>
    </message>
    <message>
        <source>Case Sensitive</source>
        <translation type="vanished">Uwzględniaj wielkość liter</translation>
    </message>
    <message>
        <source>Whole words</source>
        <translation type="vanished">Całe słowa</translation>
    </message>
    <message>
        <source>&lt;img src=&quot;:/qt/scripttools/debugging/images/wrap.png&quot;&gt;&amp;nbsp;Search wrapped</source>
        <translation type="vanished">&lt;img src=&quot;:/qt/scripttools/debugging/images/wrap.png&quot;&gt;&amp;nbsp;Przeszukano od początku</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerLocalsModel</name>
    <message>
        <source>Name</source>
        <translation type="vanished">Nazwa</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Wartość</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerStackModel</name>
    <message>
        <source>Level</source>
        <translation type="vanished">Poziom</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nazwa</translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="vanished">Położenie</translation>
    </message>
</context>
<context>
    <name>QScriptEdit</name>
    <message>
        <source>Toggle Breakpoint</source>
        <translation type="vanished">Przełącz ustawienie pułapki</translation>
    </message>
    <message>
        <source>Disable Breakpoint</source>
        <translation type="vanished">Wyłącz pułapkę</translation>
    </message>
    <message>
        <source>Enable Breakpoint</source>
        <translation type="vanished">Włącz pułapkę</translation>
    </message>
    <message>
        <source>Breakpoint Condition:</source>
        <translation type="vanished">Warunek dla pułapki:</translation>
    </message>
</context>
<context>
    <name>QScriptEngineDebugger</name>
    <message>
        <source>Loaded Scripts</source>
        <translation type="vanished">Załadowane skrypty</translation>
    </message>
    <message>
        <source>Breakpoints</source>
        <translation type="vanished">Pułapki</translation>
    </message>
    <message>
        <source>Stack</source>
        <translation type="vanished">Stos</translation>
    </message>
    <message>
        <source>Locals</source>
        <translation type="vanished">Zmienne lokalne</translation>
    </message>
    <message>
        <source>Console</source>
        <translation type="vanished">Konsola</translation>
    </message>
    <message>
        <source>Debug Output</source>
        <translation type="vanished">Wyjście debuggera</translation>
    </message>
    <message>
        <source>Error Log</source>
        <translation type="vanished">Log z błędami</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Szukaj</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="vanished">Widok</translation>
    </message>
    <message>
        <source>Qt Script Debugger</source>
        <translation type="vanished">Debugger Qt Script</translation>
    </message>
</context>
<context>
    <name>QScriptNewBreakpointWidget</name>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <source>Bottom</source>
        <translation type="vanished">W dół</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="vanished">Lewa krawędź</translation>
    </message>
    <message>
        <source>Line down</source>
        <translation type="vanished">Linia w dół</translation>
    </message>
    <message>
        <source>Line up</source>
        <translation type="vanished">Linia w górę</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="vanished">Strona w dół</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="vanished">Strona w lewo</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="vanished">Strona w prawo</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="vanished">Strona do góry</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="vanished">Pozycja</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="vanished">Prawa krawędź</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="vanished">Przewiń w dół</translation>
    </message>
    <message>
        <source>Scroll here</source>
        <translation type="vanished">Przewiń tutaj</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="vanished">Przewiń w lewo</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="vanished">Przewiń w prawo</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="vanished">Przewiń do góry</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="vanished">Do góry</translation>
    </message>
</context>
<context>
    <name>QSharedMemory</name>
    <message>
        <source>%1: create size is less then 0</source>
        <translation type="vanished">%1: rozmiar przy tworzeniu mniejszy od 0</translation>
    </message>
    <message>
        <source>%1: unable to lock</source>
        <translation type="vanished">%1: nie można zablokować</translation>
    </message>
    <message>
        <source>%1: unable to unlock</source>
        <translation type="vanished">%1: nie można odblokować</translation>
    </message>
    <message>
        <source>%1: permission denied</source>
        <translation type="vanished">%1: brak uprawnień</translation>
    </message>
    <message>
        <source>%1: already exists</source>
        <translation type="vanished">%1: już istnieje</translation>
    </message>
    <message>
        <source>%1: doesn&apos;t exists</source>
        <translation type="vanished">%1: nie istnieje</translation>
    </message>
    <message>
        <source>%1: out of resources</source>
        <translation type="vanished">%1: zasoby wyczerpane</translation>
    </message>
    <message>
        <source>%1: unknown error %2</source>
        <translation type="vanished">%1: nieznany błąd %2</translation>
    </message>
    <message>
        <source>%1: key is empty</source>
        <translation type="vanished">%1: klucz jest pusty</translation>
    </message>
    <message>
        <source>%1: ftok failed</source>
        <translation type="vanished">%1: wystąpił błąd w funkcji ftok()</translation>
    </message>
    <message>
        <source>%1: unable to make key</source>
        <translation type="vanished">%1: nie można utworzyć klucza</translation>
    </message>
    <message>
        <source>%1: doesn&apos;t exist</source>
        <translation type="vanished">%1: nie istnieje</translation>
    </message>
    <message>
        <source>%1: UNIX key file doesn&apos;t exist</source>
        <translation type="vanished">%1: unixowy plik z kluczem nie istnieje</translation>
    </message>
    <message>
        <source>%1: system-imposed size restrictions</source>
        <translation type="vanished">%1: ograniczenia rozmiarów narzucone przez system</translation>
    </message>
    <message>
        <source>%1: bad name</source>
        <translation type="vanished">%1: zła nazwa</translation>
    </message>
    <message>
        <source>%1: not attached</source>
        <translation type="vanished">%1: niedołączony</translation>
    </message>
    <message>
        <source>%1: invalid size</source>
        <translation type="vanished">%1: niepoprawny rozmiar</translation>
    </message>
    <message>
        <source>%1: size query failed</source>
        <translation type="vanished">%1: zapytanie o rozmiar zakończone błędem</translation>
    </message>
    <message>
        <source>%1: unable to set key on lock</source>
        <translation type="vanished">%1: nie można ustawić klucza na zablokowanym segmencie pamięci współdzielonej</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
    <message>
        <source>Alt</source>
        <translation type="vanished">Alt</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Back</translation>
    </message>
    <message>
        <source>Backspace</source>
        <translation type="vanished">Backspace</translation>
    </message>
    <message>
        <source>Backtab</source>
        <translation type="vanished">Backtab</translation>
    </message>
    <message>
        <source>Bass Boost</source>
        <translation type="vanished">Wzmocnienie basów</translation>
    </message>
    <message>
        <source>Bass Down</source>
        <translation type="vanished">Basy w dół</translation>
    </message>
    <message>
        <source>Bass Up</source>
        <translation type="vanished">Basy w górę</translation>
    </message>
    <message>
        <source>Call</source>
        <extracomment>Button to start a call (note: a separate button is used to end the call)</extracomment>
        <translation type="vanished">Wywołaj</translation>
    </message>
    <message>
        <source>Caps Lock</source>
        <translation type="vanished">Caps Lock</translation>
    </message>
    <message>
        <source>CapsLock</source>
        <translation type="vanished">CapsLock</translation>
    </message>
    <message>
        <source>Media Pause</source>
        <extracomment>Media player pause button</extracomment>
        <translation type="vanished">Media pauza</translation>
    </message>
    <message>
        <source>Toggle Media Play/Pause</source>
        <extracomment>Media player button to toggle between playing and paused</extracomment>
        <translation type="vanished">Przełącz: odtwarzanie / pauza</translation>
    </message>
    <message>
        <source>Monitor Brightness Up</source>
        <translation type="vanished">Zwiększ jasność monitora</translation>
    </message>
    <message>
        <source>Monitor Brightness Down</source>
        <translation type="vanished">Zmniejsz jasność monitora</translation>
    </message>
    <message>
        <source>Keyboard Light On/Off</source>
        <translation type="vanished">Włącz/wyłącz podświetlenie klawiatury</translation>
    </message>
    <message>
        <source>Keyboard Brightness Up</source>
        <translation type="vanished">Zwiększ jasność klawiatury</translation>
    </message>
    <message>
        <source>Keyboard Brightness Down</source>
        <translation type="vanished">Zmniejsz jasność klawiatury</translation>
    </message>
    <message>
        <source>Power Off</source>
        <translation type="vanished">Wyłącz zasilanie</translation>
    </message>
    <message>
        <source>Wake Up</source>
        <translation type="vanished">Przebudzenie</translation>
    </message>
    <message>
        <source>Eject</source>
        <translation type="vanished">Wysuń</translation>
    </message>
    <message>
        <source>Screensaver</source>
        <translation type="vanished">Wygaszacz ekranu</translation>
    </message>
    <message>
        <source>WWW</source>
        <translation type="vanished">WWW</translation>
    </message>
    <message>
        <source>Sleep</source>
        <translation type="vanished">Uśpienie</translation>
    </message>
    <message>
        <source>LightBulb</source>
        <translation type="vanished">Żarówka</translation>
    </message>
    <message>
        <source>Shop</source>
        <translation type="vanished">Sklep</translation>
    </message>
    <message>
        <source>History</source>
        <translation type="vanished">Historia</translation>
    </message>
    <message>
        <source>Add Favorite</source>
        <translation type="vanished">Dodaj do ulubionych</translation>
    </message>
    <message>
        <source>Hot Links</source>
        <translation type="vanished">Popularne łącza</translation>
    </message>
    <message>
        <source>Adjust Brightness</source>
        <translation type="vanished">Ustaw jasność</translation>
    </message>
    <message>
        <source>Finance</source>
        <translation type="vanished">Finanse</translation>
    </message>
    <message>
        <source>Community</source>
        <translation type="vanished">Społeczność</translation>
    </message>
    <message>
        <source>Audio Rewind</source>
        <translation type="vanished">Przewijanie do tyłu</translation>
    </message>
    <message>
        <source>Book</source>
        <translation type="vanished">Książka</translation>
    </message>
    <message>
        <source>CD</source>
        <translation type="vanished">CD</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation type="vanished">Kalkulator</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Wyczyść</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">Kopiuj</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="vanished">Wytnij</translation>
    </message>
    <message>
        <source>Display</source>
        <translation type="vanished">Wyświetlacz</translation>
    </message>
    <message>
        <source>DOS</source>
        <translation type="vanished">DOS</translation>
    </message>
    <message>
        <source>Documents</source>
        <translation type="vanished">Dokumenty</translation>
    </message>
    <message>
        <source>Spreadsheet</source>
        <translation type="vanished">Arkusz kalkulacyjny</translation>
    </message>
    <message>
        <source>Browser</source>
        <translation type="vanished">Przeglądarka</translation>
    </message>
    <message>
        <source>Game</source>
        <translation type="vanished">Gra</translation>
    </message>
    <message>
        <source>Go</source>
        <translation type="vanished">Przejdź</translation>
    </message>
    <message>
        <source>iTouch</source>
        <translation type="vanished">iTouch</translation>
    </message>
    <message>
        <source>Logoff</source>
        <translation type="vanished">Wyloguj</translation>
    </message>
    <message>
        <source>Market</source>
        <translation type="vanished">Rynek</translation>
    </message>
    <message>
        <source>Meeting</source>
        <translation type="vanished">Spotkanie</translation>
    </message>
    <message>
        <source>Keyboard Menu</source>
        <translation type="vanished">Menu klawiatury</translation>
    </message>
    <message>
        <source>Menu PB</source>
        <translation type="vanished">Menu PB</translation>
    </message>
    <message>
        <source>My Sites</source>
        <translation type="vanished">Moje strony</translation>
    </message>
    <message>
        <source>News</source>
        <translation type="vanished">Wiadomości</translation>
    </message>
    <message>
        <source>Home Office</source>
        <translation type="vanished">Biuro domowe</translation>
    </message>
    <message>
        <source>Option</source>
        <translation type="vanished">Opcje</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Wklej</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="vanished">Telefon</translation>
    </message>
    <message>
        <source>Reply</source>
        <translation type="vanished">Odpowiedz</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="vanished">Przeładuj</translation>
    </message>
    <message>
        <source>Rotate Windows</source>
        <translation type="vanished">Obróć okna</translation>
    </message>
    <message>
        <source>Rotation PB</source>
        <translation type="vanished">Obrót PB</translation>
    </message>
    <message>
        <source>Rotation KB</source>
        <translation type="vanished">Obrót KB</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Zachowaj</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">Wyślij</translation>
    </message>
    <message>
        <source>Spellchecker</source>
        <translation type="vanished">Sprawdzanie pisowni</translation>
    </message>
    <message>
        <source>Split Screen</source>
        <translation type="vanished">Podziel ekran</translation>
    </message>
    <message>
        <source>Support</source>
        <translation type="vanished">Pomoc techniczna</translation>
    </message>
    <message>
        <source>Task Panel</source>
        <translation type="vanished">Panel zadań</translation>
    </message>
    <message>
        <source>Terminal</source>
        <translation type="vanished">Terminal</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="vanished">Narzędzia</translation>
    </message>
    <message>
        <source>Travel</source>
        <translation type="vanished">Podróże</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">Wideo</translation>
    </message>
    <message>
        <source>Word Processor</source>
        <translation type="vanished">Procesor tekstu</translation>
    </message>
    <message>
        <source>XFer</source>
        <translation type="vanished">XFer</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="vanished">Powiększ</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="vanished">Pomniejsz</translation>
    </message>
    <message>
        <source>Messenger</source>
        <translation type="vanished">Komunikator</translation>
    </message>
    <message>
        <source>WebCam</source>
        <translation type="vanished">WebCam</translation>
    </message>
    <message>
        <source>Mail Forward</source>
        <translation type="vanished">Przekazanie poczty</translation>
    </message>
    <message>
        <source>Pictures</source>
        <translation type="vanished">Zdjęcia</translation>
    </message>
    <message>
        <source>Music</source>
        <translation type="vanished">Muzyka</translation>
    </message>
    <message>
        <source>Battery</source>
        <translation type="vanished">Bateria</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">Bluetooth</translation>
    </message>
    <message>
        <source>Wireless</source>
        <translation type="vanished">Bezprzewodowy</translation>
    </message>
    <message>
        <source>Ultra Wide Band</source>
        <translation type="vanished">Ultraszerokie pasmo</translation>
    </message>
    <message>
        <source>Audio Forward</source>
        <translation type="vanished">Przewijanie do przodu</translation>
    </message>
    <message>
        <source>Audio Repeat</source>
        <translation type="vanished">Powtarzanie</translation>
    </message>
    <message>
        <source>Audio Random Play</source>
        <translation type="vanished">Odtwarzanie losowe</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation type="vanished">Napisy</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">Czas</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="vanished">Widok</translation>
    </message>
    <message>
        <source>Top Menu</source>
        <translation type="vanished">Menu główne</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation type="vanished">Wstrzymanie</translation>
    </message>
    <message>
        <source>Hibernate</source>
        <translation type="vanished">Hibernacja</translation>
    </message>
    <message>
        <source>Context1</source>
        <translation type="vanished">Kontekst1</translation>
    </message>
    <message>
        <source>Context2</source>
        <translation type="vanished">Kontekst2</translation>
    </message>
    <message>
        <source>Context3</source>
        <translation type="vanished">Kontekst3</translation>
    </message>
    <message>
        <source>Context4</source>
        <translation type="vanished">Kontekst4</translation>
    </message>
    <message>
        <source>Toggle Call/Hangup</source>
        <extracomment>Button that will hang up if we&apos;re in call, or make a call if we&apos;re not.</extracomment>
        <translation type="vanished">Przełącz dzwoń/zawieś</translation>
    </message>
    <message>
        <source>Voice Dial</source>
        <extracomment>Button to trigger voice dialing</extracomment>
        <translation type="vanished">Wybieranie głosowe</translation>
    </message>
    <message>
        <source>Last Number Redial</source>
        <extracomment>Button to redial the last number called</extracomment>
        <translation type="vanished">Wykręć ostatni numer</translation>
    </message>
    <message>
        <source>Camera Shutter</source>
        <extracomment>Button to trigger the camera shutter (take a picture)</extracomment>
        <translation type="vanished">Migawka aparatu</translation>
    </message>
    <message>
        <source>Camera Focus</source>
        <extracomment>Button to focus the camera</extracomment>
        <translation type="vanished">Ostrość aparatu</translation>
    </message>
    <message>
        <source>Kanji</source>
        <translation type="vanished">Kanji</translation>
    </message>
    <message>
        <source>Muhenkan</source>
        <translation type="vanished">Muhenkan</translation>
    </message>
    <message>
        <source>Henkan</source>
        <translation type="vanished">Henkan</translation>
    </message>
    <message>
        <source>Romaji</source>
        <translation type="vanished">Romaji</translation>
    </message>
    <message>
        <source>Hiragana</source>
        <translation type="vanished">Hiragana</translation>
    </message>
    <message>
        <source>Katakana</source>
        <translation type="vanished">Katakana</translation>
    </message>
    <message>
        <source>Hiragana Katakana</source>
        <translation type="vanished">Hiragana Katakana</translation>
    </message>
    <message>
        <source>Zenkaku</source>
        <translation type="vanished">Zenkaku</translation>
    </message>
    <message>
        <source>Hankaku</source>
        <translation type="vanished">Hankaku</translation>
    </message>
    <message>
        <source>Zenkaku Hankaku</source>
        <translation type="vanished">Zenkaku Hankaku</translation>
    </message>
    <message>
        <source>Touroku</source>
        <translation type="vanished">Touroku</translation>
    </message>
    <message>
        <source>Massyo</source>
        <translation type="vanished">Massyo</translation>
    </message>
    <message>
        <source>Kana Lock</source>
        <translation type="vanished">Kana Lock</translation>
    </message>
    <message>
        <source>Kana Shift</source>
        <translation type="vanished">Kana Shift</translation>
    </message>
    <message>
        <source>Eisu Shift</source>
        <translation type="vanished">Eisu Shift</translation>
    </message>
    <message>
        <source>Eisu toggle</source>
        <translation type="vanished">Eisu toggle</translation>
    </message>
    <message>
        <source>Code input</source>
        <translation type="vanished">Wprowadzanie kodu</translation>
    </message>
    <message>
        <source>Multiple Candidate</source>
        <translation type="vanished">Kandydat wielokrotny</translation>
    </message>
    <message>
        <source>Previous Candidate</source>
        <translation type="vanished">Poprzedni kandydat</translation>
    </message>
    <message>
        <source>Hangul</source>
        <translation type="vanished">Hangul</translation>
    </message>
    <message>
        <source>Hangul Start</source>
        <translation type="vanished">Hangul Start</translation>
    </message>
    <message>
        <source>Hangul End</source>
        <translation type="vanished">Hangul End</translation>
    </message>
    <message>
        <source>Hangul Hanja</source>
        <translation type="vanished">Hangul Hanja</translation>
    </message>
    <message>
        <source>Hangul Jamo</source>
        <translation type="vanished">Hangul Jamo</translation>
    </message>
    <message>
        <source>Hangul Romaja</source>
        <translation type="vanished">Hangul Romaja</translation>
    </message>
    <message>
        <source>Hangul Jeonja</source>
        <translation type="vanished">Hangul Jeonja</translation>
    </message>
    <message>
        <source>Hangul Banja</source>
        <translation type="vanished">Hangul Banja</translation>
    </message>
    <message>
        <source>Hangul PreHanja</source>
        <translation type="vanished">Hangul PreHanja</translation>
    </message>
    <message>
        <source>Hangul PostHanja</source>
        <translation type="vanished">Hangul PostHanja</translation>
    </message>
    <message>
        <source>Hangul Special</source>
        <translation type="vanished">Hangul Special</translation>
    </message>
    <message>
        <source>Ctrl</source>
        <translation type="vanished">Ctrl</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">Del</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Delete</translation>
    </message>
    <message>
        <source>Down</source>
        <translation type="vanished">Dół</translation>
    </message>
    <message>
        <source>End</source>
        <translation type="vanished">End</translation>
    </message>
    <message>
        <source>Enter</source>
        <translation type="vanished">Enter</translation>
    </message>
    <message>
        <source>Esc</source>
        <translation type="vanished">Esc</translation>
    </message>
    <message>
        <source>Escape</source>
        <translation type="vanished">Escape</translation>
    </message>
    <message>
        <source>F%1</source>
        <translation type="vanished">F%1</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Ulubione</translation>
    </message>
    <message>
        <source>Flip</source>
        <translation type="vanished">Odwróć</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">Do przodu</translation>
    </message>
    <message>
        <source>Hangup</source>
        <extracomment>Button to end a call (note: a separate button is used to start the call)</extracomment>
        <translation type="vanished">Zawieś</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Pomoc</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="vanished">Home</translation>
    </message>
    <message>
        <source>Home Page</source>
        <translation type="vanished">Strona startowa</translation>
    </message>
    <message>
        <source>Ins</source>
        <translation type="vanished">Ins</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="vanished">Insert</translation>
    </message>
    <message>
        <source>Launch (0)</source>
        <translation type="vanished">Uruchom (0)</translation>
    </message>
    <message>
        <source>Launch (1)</source>
        <translation type="vanished">Uruchom (1)</translation>
    </message>
    <message>
        <source>Launch (2)</source>
        <translation type="vanished">Uruchom (2)</translation>
    </message>
    <message>
        <source>Launch (3)</source>
        <translation type="vanished">Uruchom (3)</translation>
    </message>
    <message>
        <source>Launch (4)</source>
        <translation type="vanished">Uruchom (4)</translation>
    </message>
    <message>
        <source>Launch (5)</source>
        <translation type="vanished">Uruchom (5)</translation>
    </message>
    <message>
        <source>Launch (6)</source>
        <translation type="vanished">Uruchom (6)</translation>
    </message>
    <message>
        <source>Launch (7)</source>
        <translation type="vanished">Uruchom (7)</translation>
    </message>
    <message>
        <source>Launch (8)</source>
        <translation type="vanished">Uruchom (8)</translation>
    </message>
    <message>
        <source>Launch (9)</source>
        <translation type="vanished">Uruchom (9)</translation>
    </message>
    <message>
        <source>Launch (A)</source>
        <translation type="vanished">Uruchom (A)</translation>
    </message>
    <message>
        <source>Launch (B)</source>
        <translation type="vanished">Uruchom (B)</translation>
    </message>
    <message>
        <source>Launch (C)</source>
        <translation type="vanished">Uruchom (C)</translation>
    </message>
    <message>
        <source>Launch (D)</source>
        <translation type="vanished">Uruchom (D)</translation>
    </message>
    <message>
        <source>Launch (E)</source>
        <translation type="vanished">Uruchom (E)</translation>
    </message>
    <message>
        <source>Launch (F)</source>
        <translation type="vanished">Uruchom (F)</translation>
    </message>
    <message>
        <source>Launch Mail</source>
        <translation type="vanished">Uruchom program pocztowy</translation>
    </message>
    <message>
        <source>Launch Media</source>
        <translation type="vanished">Uruchom przeglądarkę mediów</translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="vanished">Lewo</translation>
    </message>
    <message>
        <source>Media Next</source>
        <translation type="vanished">Następna ścieżka</translation>
    </message>
    <message>
        <source>Media Play</source>
        <translation type="vanished">Odtwarzaj</translation>
    </message>
    <message>
        <source>Media Previous</source>
        <translation type="vanished">Poprzednia ścieżka</translation>
    </message>
    <message>
        <source>Media Record</source>
        <translation type="vanished">Nagrywaj</translation>
    </message>
    <message>
        <source>Media Stop</source>
        <translation type="vanished">Zatrzymaj</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">Menu</translation>
    </message>
    <message>
        <source>Meta</source>
        <translation type="vanished">Meta</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">Nie</translation>
    </message>
    <message>
        <source>Num Lock</source>
        <translation type="vanished">Num Lock</translation>
    </message>
    <message>
        <source>Number Lock</source>
        <translation type="vanished">Number Lock</translation>
    </message>
    <message>
        <source>NumLock</source>
        <translation type="vanished">NumLock</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation type="vanished">Otwórz adres</translation>
    </message>
    <message>
        <source>Page Down</source>
        <translation type="vanished">Strona do góry</translation>
    </message>
    <message>
        <source>Page Up</source>
        <translation type="vanished">Strona w dół</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="vanished">Pauza</translation>
    </message>
    <message>
        <source>PgDown</source>
        <translation type="vanished">PgDown</translation>
    </message>
    <message>
        <source>PgUp</source>
        <translation type="vanished">PgUp</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="vanished">Wydrukuj</translation>
    </message>
    <message>
        <source>Print Screen</source>
        <translation type="vanished">Wydrukuj zawartość ekranu</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Odśwież</translation>
    </message>
    <message>
        <source>Return</source>
        <translation type="vanished">Powrót</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="vanished">Prawo</translation>
    </message>
    <message>
        <source>Scroll Lock</source>
        <translation type="vanished">Scroll Lock</translation>
    </message>
    <message>
        <source>ScrollLock</source>
        <translation type="vanished">ScrollLock</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Szukaj</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Wybierz</translation>
    </message>
    <message>
        <source>Shift</source>
        <translation type="vanished">Shift</translation>
    </message>
    <message>
        <source>Space</source>
        <extracomment>This and all following &quot;incomprehensible&quot; strings in QShortcut context are key names. Please use the localized names appearing on actual keyboards or whatever is commonly used.</extracomment>
        <translation type="vanished">Spacja</translation>
    </message>
    <message>
        <source>Standby</source>
        <translation type="vanished">Tryb oczekiwania</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Zatrzymaj</translation>
    </message>
    <message>
        <source>SysReq</source>
        <translation type="vanished">SysReq</translation>
    </message>
    <message>
        <source>System Request</source>
        <translation type="vanished">Żądanie systemu</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">Tabulator</translation>
    </message>
    <message>
        <source>Treble Down</source>
        <translation type="vanished">Soprany w dół</translation>
    </message>
    <message>
        <source>Treble Up</source>
        <translation type="vanished">Soprany w górę</translation>
    </message>
    <message>
        <source>Up</source>
        <translation type="vanished">Góra</translation>
    </message>
    <message>
        <source>Volume Down</source>
        <translation type="vanished">Przycisz</translation>
    </message>
    <message>
        <source>Volume Mute</source>
        <translation type="vanished">Wycisz</translation>
    </message>
    <message>
        <source>Volume Up</source>
        <translation type="vanished">Zrób głośniej</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Tak</translation>
    </message>
</context>
<context>
    <name>QSlider</name>
    <message>
        <source>Page down</source>
        <translation type="vanished">Strona w dół</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="vanished">Strona w lewo</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="vanished">Strona w prawo</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="vanished">Strona do góry</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="vanished">Położenie</translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <source>Connection to proxy refused</source>
        <translation type="vanished">Odmowa połączenia z pośrednikiem</translation>
    </message>
    <message>
        <source>Connection to proxy closed prematurely</source>
        <translation type="vanished">Przedwczesne zakończenie połączenia z pośrednikiem</translation>
    </message>
    <message>
        <source>Proxy host not found</source>
        <translation type="vanished">Nie odnaleziono hosta pośredniczącego</translation>
    </message>
    <message>
        <source>Connection to proxy timed out</source>
        <translation type="vanished">Przekroczony czas połączenia do pośrednika</translation>
    </message>
    <message>
        <source>Proxy authentication failed</source>
        <translation type="vanished">Autoryzacja pośrednika zakończona błędem</translation>
    </message>
    <message>
        <source>Proxy authentication failed: %1</source>
        <translation type="vanished">Autoryzacja pośrednika zakończona błędem: %1</translation>
    </message>
    <message>
        <source>SOCKS version 5 protocol error</source>
        <translation type="vanished">Błąd protokołu SOCKS wersji 5</translation>
    </message>
    <message>
        <source>General SOCKSv5 server failure</source>
        <translation type="vanished">Generalny błąd serwera SOCKS wersji 5</translation>
    </message>
    <message>
        <source>Connection not allowed by SOCKSv5 server</source>
        <translation type="vanished">Połączenie niedozwolone przez serwer SOCKS wersji 5</translation>
    </message>
    <message>
        <source>TTL expired</source>
        <translation type="vanished">TTL stracił ważność</translation>
    </message>
    <message>
        <source>SOCKSv5 command not supported</source>
        <translation type="vanished">Nieobsługiwana komenda SOCKS wersji 5</translation>
    </message>
    <message>
        <source>Address type not supported</source>
        <translation type="vanished">Nieobsługiwany typ adresu</translation>
    </message>
    <message>
        <source>Unknown SOCKSv5 proxy error code 0x%1</source>
        <translation type="vanished">Nieznany kod błędu (0x%1) pośrednika SOCKS wersji 5</translation>
    </message>
    <message>
        <source>Network operation timed out</source>
        <translation type="vanished">Przekroczony czas operacji sieciowej</translation>
    </message>
</context>
<context>
    <name>QSoftKeyManager</name>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Wybierz</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="vanished">Zrobione</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">Opcje</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Anuluj</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Wyjście</translation>
    </message>
</context>
<context>
    <name>QSpinBox</name>
    <message>
        <source>Less</source>
        <translation type="vanished">Mniej</translation>
    </message>
    <message>
        <source>More</source>
        <translation type="vanished">Więcej</translation>
    </message>
</context>
<context>
    <name>QSql</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Anuluj</translation>
    </message>
    <message>
        <source>Cancel your edits?</source>
        <translation type="vanished">Anulować zmiany?</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Potwierdź</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Usuń</translation>
    </message>
    <message>
        <source>Delete this record?</source>
        <translation type="vanished">Skasować ten rekord?</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="vanished">Wstaw</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">Nie</translation>
    </message>
    <message>
        <source>Save edits?</source>
        <translation type="vanished">Zachować zmiany?</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Uaktualnij</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Tak</translation>
    </message>
</context>
<context>
    <name>QSslSocket</name>
    <message>
        <source>Unable to decrypt data: %1</source>
        <translation type="vanished">Nie można odszyfrować danych: %1</translation>
    </message>
    <message>
        <source>Error while reading: %1</source>
        <translation type="vanished">Błąd podczas czytania: %1</translation>
    </message>
    <message>
        <source>Error during SSL handshake: %1</source>
        <translation type="vanished">Błąd podczas nawiązania sesji SSL: %1</translation>
    </message>
    <message>
        <source>Error creating SSL context (%1)</source>
        <translation type="vanished">Błąd tworzenia kontekstu (%1)</translation>
    </message>
    <message>
        <source>Invalid or empty cipher list (%1)</source>
        <translation type="vanished">Niepoprawna lub pusta lista szyfrów (%1)</translation>
    </message>
    <message>
        <source>Private key does not certify public key, %1</source>
        <translation type="vanished">Prywatny klucz nie uwiarygodnia publicznego, %1</translation>
    </message>
    <message>
        <source>Error creating SSL session, %1</source>
        <translation type="vanished">Błąd tworzenia sesji SSL, %1</translation>
    </message>
    <message>
        <source>Error creating SSL session: %1</source>
        <translation type="vanished">Błąd tworzenia sesji SSL: %1</translation>
    </message>
    <message>
        <source>The peer certificate is blacklisted</source>
        <translation type="vanished">Element równorzędny widnieje na czarnej liście</translation>
    </message>
    <message>
        <source>Cannot provide a certificate with no key, %1</source>
        <translation type="vanished">Nie można dostarczyć certyfikatu bez klucza, %1</translation>
    </message>
    <message>
        <source>Error loading local certificate, %1</source>
        <translation type="vanished">Błąd ładowania lokalnego certyfikatu, %1</translation>
    </message>
    <message>
        <source>Error loading private key, %1</source>
        <translation type="vanished">Błąd ładowania prywatnego klucza, %1</translation>
    </message>
    <message>
        <source>No error</source>
        <translation type="vanished">Brak błędu</translation>
    </message>
    <message>
        <source>The issuer certificate could not be found</source>
        <translation type="vanished">Nie można odnaleźć wydawcy certyfikatu</translation>
    </message>
    <message>
        <source>The certificate signature could not be decrypted</source>
        <translation type="vanished">Nie można odszyfrować podpisu certyfikatu</translation>
    </message>
    <message>
        <source>The public key in the certificate could not be read</source>
        <translation type="vanished">Nie można odczytać publicznego klucza w certyfikacie</translation>
    </message>
    <message>
        <source>The signature of the certificate is invalid</source>
        <translation type="vanished">Niepoprawny podpis certyfikatu</translation>
    </message>
    <message>
        <source>The certificate is not yet valid</source>
        <translation type="vanished">Certyfikat nie jest jeszcze ważny</translation>
    </message>
    <message>
        <source>The certificate has expired</source>
        <translation type="vanished">Certyfikat utracił ważność</translation>
    </message>
    <message>
        <source>The certificate&apos;s notBefore field contains an invalid time</source>
        <translation type="vanished">Pole &quot;notBefore&quot; certyfikatu zawiera niepoprawną datę</translation>
    </message>
    <message>
        <source>The certificate&apos;s notAfter field contains an invalid time</source>
        <translation type="vanished">Pole &quot;notAfter&quot; certyfikatu zawiera niepoprawną datę</translation>
    </message>
    <message>
        <source>The certificate is self-signed, and untrusted</source>
        <translation type="vanished">Niewiarygodny certyfikat z podpisem własnym</translation>
    </message>
    <message>
        <source>The root certificate of the certificate chain is self-signed, and untrusted</source>
        <translation type="vanished">Główny certyfikat łańcucha zaufania ma własny podpis i jest niewiarygodny</translation>
    </message>
    <message>
        <source>The issuer certificate of a locally looked up certificate could not be found</source>
        <translation type="vanished">Nie można odnaleźć certyfikatu wydawcy wyszukanego lokalnie certyfikatu</translation>
    </message>
    <message>
        <source>No certificates could be verified</source>
        <translation type="vanished">Nie można zweryfikować żadnych certyfikatów</translation>
    </message>
    <message>
        <source>One of the CA certificates is invalid</source>
        <translation type="vanished">Jeden z certyfikatów urzędu certyfikacji jest nieprawidłowy</translation>
    </message>
    <message>
        <source>The basicConstraints path length parameter has been exceeded</source>
        <translation type="vanished">Długość ścieżki określona w podstawowych warunkach ograniczających została przekroczona</translation>
    </message>
    <message>
        <source>The supplied certificate is unsuitable for this purpose</source>
        <translation type="vanished">Dostarczony certyfikat jest nieodpowiedni dla tego przeznaczenia</translation>
    </message>
    <message>
        <source>The root CA certificate is not trusted for this purpose</source>
        <translation type="vanished">Główny certyfikat urzędu certyfikacji nie jest wiarygodny dla tego przeznaczenia</translation>
    </message>
    <message>
        <source>The root CA certificate is marked to reject the specified purpose</source>
        <translation type="vanished">Główny certyfikat urzędu certyfikacji jest wyznaczony do odrzucania podanego przeznaczenia</translation>
    </message>
    <message>
        <source>The current candidate issuer certificate was rejected because its subject name did not match the issuer name of the current certificate</source>
        <translation type="vanished">Certyfikat wydawcy obecnego kandydata został odrzucony, ponieważ nazwa podmiotu nie odpowiadała nazwie wydawcy obecnego certyfikatu</translation>
    </message>
    <message>
        <source>The current candidate issuer certificate was rejected because its issuer name and serial number was present and did not match the authority key identifier of the current certificate</source>
        <translation type="vanished">Certyfikat wydawcy obecnego kandydata został odrzucony, ponieważ nazwa wydawcy i przedstawiony numer seryjny nie odpowiadały identyfikatorowi klucza urzędu certyfikacji obecnego certyfikatu</translation>
    </message>
    <message>
        <source>The peer did not present any certificate</source>
        <translation type="vanished">Element równorzędny nie przedstawił żadnego certyfikatu</translation>
    </message>
    <message>
        <source>The host name did not match any of the valid hosts for this certificate</source>
        <translation type="vanished">Nazwa hosta nie odpowiadała żadnemu z poprawnych hostów dla tego certyfikatu</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Nieznany błąd</translation>
    </message>
</context>
<context>
    <name>QStateMachine</name>
    <message>
        <source>Missing initial state in compound state &apos;%1&apos;</source>
        <translation type="vanished">Brak stanu początkowego w stanie złożonym &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Missing default state in history state &apos;%1&apos;</source>
        <translation type="vanished">Brak domyślnego stanu w historycznym stanie &quot;%1&quot;</translation>
    </message>
    <message>
        <source>No common ancestor for targets and source of transition from state &apos;%1&apos;</source>
        <translation type="vanished">Brak wspólnego przodka dla stanów docelowych i stanu źródłowego w przejściu ze stanu &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Nieznany błąd</translation>
    </message>
</context>
<context>
    <name>QSymSQLDriver</name>
    <message>
        <source>Invalid option: </source>
        <translation type="vanished">Niepoprawna opcja: </translation>
    </message>
    <message>
        <source>Error opening database</source>
        <translation type="vanished">Błąd otwierania bazy danych</translation>
    </message>
    <message>
        <source>POLICY_DB_DEFAULT must be defined before any other POLICY definitions can be used</source>
        <translation type="vanished">POLICY_DB_DEFAULT musi być zdefiniowane przed użyciem definicji POLICY</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="vanished">Nie można rozpocząć transakcji</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="vanished">Nie można dokonać transakcji</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="vanished">Nie można wycofać transakcji</translation>
    </message>
</context>
<context>
    <name>QSymSQLResult</name>
    <message>
        <source>Error retrieving column count</source>
        <translation type="vanished">Błąd pobierania liczby kolumn</translation>
    </message>
    <message>
        <source>Error retrieving column name</source>
        <translation type="vanished">Błąd pobierania nazwy kolumny</translation>
    </message>
    <message>
        <source>Error retrieving column type</source>
        <translation type="vanished">Błąd pobierania typu kolumny</translation>
    </message>
    <message>
        <source>Unable to fetch row</source>
        <translation type="vanished">Nie można pobrać wiersza danych</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation type="vanished">Nie można wykonać polecenia</translation>
    </message>
    <message>
        <source>Statement is not prepared</source>
        <translation type="vanished">Polecenie nie jest przygotowane</translation>
    </message>
    <message>
        <source>Unable to reset statement</source>
        <translation type="vanished">Nie można zresetować polecenia</translation>
    </message>
    <message>
        <source>Unable to bind parameters</source>
        <translation type="vanished">Nie można powiązać parametrów</translation>
    </message>
    <message>
        <source>Parameter count mismatch</source>
        <translation type="vanished">Niezgodna liczba parametrów</translation>
    </message>
</context>
<context>
    <name>QSymbianSocketEngine</name>
    <message>
        <source>Unable to initialize non-blocking socket</source>
        <translation type="vanished">Nie można zainicjalizować gniazda w trybie nieblokującym</translation>
    </message>
    <message>
        <source>Unable to initialize broadcast socket</source>
        <translation type="vanished">Nie można zainicjalizować gniazda rozsyłającego</translation>
    </message>
    <message>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation type="vanished">Próba użycia IPv6 na platformie bez obsługi IPv6</translation>
    </message>
    <message>
        <source>The remote host closed the connection</source>
        <translation type="vanished">Zdalny host zakończył połączenie</translation>
    </message>
    <message>
        <source>Network operation timed out</source>
        <translation type="vanished">Przekroczony czas operacji sieciowej</translation>
    </message>
    <message>
        <source>Out of resources</source>
        <translation type="vanished">Zasoby wyczerpane</translation>
    </message>
    <message>
        <source>Unsupported socket operation</source>
        <translation type="vanished">Nieobsługiwana operacja gniazda</translation>
    </message>
    <message>
        <source>Protocol type not supported</source>
        <translation type="vanished">Nieobsługiwany typ protokołu</translation>
    </message>
    <message>
        <source>Invalid socket descriptor</source>
        <translation type="vanished">Niepoprawny opis gniazda</translation>
    </message>
    <message>
        <source>Host unreachable</source>
        <translation type="vanished">Host niedostępny</translation>
    </message>
    <message>
        <source>Network unreachable</source>
        <translation type="vanished">Sieć niedostępna</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Brak uprawnień</translation>
    </message>
    <message>
        <source>Connection timed out</source>
        <translation type="vanished">Przekroczony czas połączenia</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation type="vanished">Połączenie odrzucone</translation>
    </message>
    <message>
        <source>The bound address is already in use</source>
        <translation type="vanished">Adres jest aktualnie w użyciu</translation>
    </message>
    <message>
        <source>The address is not available</source>
        <translation type="vanished">Adres nie jest dostępny</translation>
    </message>
    <message>
        <source>The address is protected</source>
        <translation type="vanished">Adres jest zabezpieczony</translation>
    </message>
    <message>
        <source>Datagram was too large to send</source>
        <translation type="vanished">Datagram za długi do wysłania</translation>
    </message>
    <message>
        <source>Unable to send a message</source>
        <translation type="vanished">Nie można wysłać wiadomości</translation>
    </message>
    <message>
        <source>Unable to receive a message</source>
        <translation type="vanished">Nie można odebrać wiadomości</translation>
    </message>
    <message>
        <source>Unable to write</source>
        <translation type="vanished">Nie można zapisać</translation>
    </message>
    <message>
        <source>Network error</source>
        <translation type="vanished">Błąd sieci</translation>
    </message>
    <message>
        <source>Another socket is already listening on the same port</source>
        <translation type="vanished">Inne gniazdo nasłuchuje już na tym porcie</translation>
    </message>
    <message>
        <source>Operation on non-socket</source>
        <translation type="vanished">Operacja na nieprawidłowym gnieździe</translation>
    </message>
    <message>
        <source>The proxy type is invalid for this operation</source>
        <translation type="vanished">Typ pośrednika nie jest poprawny dla tej operacji</translation>
    </message>
    <message>
        <source>The address is invalid for this operation</source>
        <translation type="vanished">Niepoprawny adres dla tej operacji</translation>
    </message>
    <message>
        <source>The specified network session is not opened</source>
        <translation type="vanished">Podana sesja sieciowa nie jest otwarta</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Nieznany błąd</translation>
    </message>
</context>
<context>
    <name>QSystemSemaphore</name>
    <message>
        <source>%1: out of resources</source>
        <translation type="vanished">%1: zasoby wyczerpane</translation>
    </message>
    <message>
        <source>%1: permission denied</source>
        <translation type="vanished">%1: brak uprawnień</translation>
    </message>
    <message>
        <source>%1: already exists</source>
        <translation type="vanished">%1: już istnieje</translation>
    </message>
    <message>
        <source>%1: does not exist</source>
        <translation type="vanished">%1: nie istnieje</translation>
    </message>
    <message>
        <source>%1: name error</source>
        <translation type="vanished">%1: błąd nazwy</translation>
    </message>
    <message>
        <source>%1: unknown error %2</source>
        <translation type="vanished">%1: nieznany błąd %2</translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <source>Unable to open connection</source>
        <translation type="vanished">Nie można otworzyć połączenia</translation>
    </message>
    <message>
        <source>Unable to use database</source>
        <translation type="vanished">Nie można użyć bazy danych</translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <source>Scroll Left</source>
        <translation type="vanished">Przewiń w lewo</translation>
    </message>
    <message>
        <source>Scroll Right</source>
        <translation type="vanished">Przewiń w prawo</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
    <message>
        <source>Activate</source>
        <translation type="vanished">Uaktywnij</translation>
    </message>
    <message>
        <source>Press</source>
        <translation type="vanished">Wciśnij</translation>
    </message>
    <message>
        <source>Close the tab</source>
        <translation type="vanished">Zamknij kartę</translation>
    </message>
    <message>
        <source>Activate the tab</source>
        <translation type="vanished">Uaktywnij kartę</translation>
    </message>
</context>
<context>
    <name>QTcpServer</name>
    <message>
        <source>Operation on socket is not supported</source>
        <translation type="vanished">Operacja na gnieździe nieobsługiwana</translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">&amp;Kopiuj</translation>
    </message>
    <message>
        <source>Copy &amp;Link Location</source>
        <translation type="vanished">Kopiuj &amp;adres odsyłacza</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">W&amp;ytnij</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Usuń</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">&amp;Wklej</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation type="vanished">&amp;Przywróć</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Zaznacz wszystko</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation type="vanished">&amp;Cofnij</translation>
    </message>
</context>
<context>
    <name>QToolButton</name>
    <message>
        <source>Open</source>
        <translation type="vanished">Otwórz</translation>
    </message>
    <message>
        <source>Press</source>
        <translation type="vanished">Wciśnij</translation>
    </message>
</context>
<context>
    <name>QUdpSocket</name>
    <message>
        <source>This platform does not support IPv6</source>
        <translation type="vanished">Ta platforma nie obsługuje IPv6</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <source>Undo %1</source>
        <translation type="vanished">Cofnij %1</translation>
    </message>
    <message>
        <source>Undo</source>
        <comment>Default text for undo action</comment>
        <translation type="vanished">Cofnij</translation>
    </message>
    <message>
        <source>Redo %1</source>
        <translation type="vanished">Przywróć %1</translation>
    </message>
    <message>
        <source>Redo</source>
        <comment>Default text for redo action</comment>
        <translation type="vanished">Przywróć</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <source>&lt;empty&gt;</source>
        <translation type="vanished">&lt;pusty&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <source>Undo %1</source>
        <translation type="vanished">Cofnij %1</translation>
    </message>
    <message>
        <source>Undo</source>
        <comment>Default text for undo action</comment>
        <translation type="vanished">Cofnij</translation>
    </message>
    <message>
        <source>Redo %1</source>
        <translation type="vanished">Przywróć %1</translation>
    </message>
    <message>
        <source>Redo</source>
        <comment>Default text for redo action</comment>
        <translation type="vanished">Przywróć</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <source>Insert Unicode control character</source>
        <translation type="vanished">Wstaw znak kontroli Unicode</translation>
    </message>
    <message>
        <source>LRE Start of left-to-right embedding</source>
        <translation type="vanished">LRE Początek osadzania od lewej do prawej</translation>
    </message>
    <message>
        <source>LRM Left-to-right mark</source>
        <translation type="vanished">LRM znacznik od prawej do lewej </translation>
    </message>
    <message>
        <source>LRO Start of left-to-right override</source>
        <translation type="vanished">LRO Początek nadpisania od lewej do prawej</translation>
    </message>
    <message>
        <source>PDF Pop directional formatting</source>
        <translation type="vanished">PDF Formatowanie kierunkowe pop</translation>
    </message>
    <message>
        <source>RLE Start of right-to-left embedding</source>
        <translation type="vanished">RLE Początek osadzania od prawej do lewej</translation>
    </message>
    <message>
        <source>RLM Right-to-left mark</source>
        <translation type="vanished">RLM Znacznik od prawej do lewej</translation>
    </message>
    <message>
        <source>RLO Start of right-to-left override</source>
        <translation type="vanished">RLO Początek nadpisania od prawej do lewej</translation>
    </message>
    <message>
        <source>ZWJ Zero width joiner</source>
        <translation type="vanished">ZWJ Łącznik zerowej długości</translation>
    </message>
    <message>
        <source>ZWNJ Zero width non-joiner</source>
        <translation type="vanished">ZWNJ Rozdzielnik zerowej długości</translation>
    </message>
    <message>
        <source>ZWSP Zero width space</source>
        <translation type="vanished">ZWSP Przerwa zerowej długości</translation>
    </message>
</context>
<context>
    <name>QWebFrame</name>
    <message>
        <source>Request cancelled</source>
        <translation type="vanished">Prośba anulowana</translation>
    </message>
    <message>
        <source>Request canceled</source>
        <translation type="vanished">Prośba anulowana</translation>
    </message>
    <message>
        <source>Request blocked</source>
        <translation type="vanished">Prośba zablokowana</translation>
    </message>
    <message>
        <source>Cannot show URL</source>
        <translation type="vanished">Nie można pokazać URL</translation>
    </message>
    <message>
        <source>Frame load interrupted by policy change</source>
        <translation type="vanished">Ładowanie ramki przerwane przez zmianę strategii</translation>
    </message>
    <message>
        <source>Cannot show mimetype</source>
        <translation type="vanished">Nie można pokazać typu MIME</translation>
    </message>
    <message>
        <source>File does not exist</source>
        <translation type="vanished">Plik nie istnieje</translation>
    </message>
    <message>
        <source>Loading is handled by the media engine</source>
        <translation type="vanished">Ładowanie jest obsługiwane przez silnik mediów</translation>
    </message>
</context>
<context>
    <name>QWebPage</name>
    <message>
        <source>Submit</source>
        <comment>default label for Submit buttons in forms on web pages</comment>
        <translation type="vanished">Wyślij</translation>
    </message>
    <message>
        <source>Submit</source>
        <comment>Submit (input element) alt text for &lt;input&gt; elements with no alt, title, or value</comment>
        <translation type="vanished">Wyślij</translation>
    </message>
    <message>
        <source>Reset</source>
        <comment>default label for Reset buttons in forms on web pages</comment>
        <translation type="vanished">Zresetuj</translation>
    </message>
    <message>
        <source>Choose File</source>
        <comment>title for file button used in HTML forms</comment>
        <translation type="vanished">Wybierz plik</translation>
    </message>
    <message>
        <source>No file selected</source>
        <comment>text to display in file button used in HTML forms when no file is selected</comment>
        <translation type="vanished">Nie zaznaczono pliku</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>text to display in &lt;details&gt; tag when it has no &lt;summary&gt; child</comment>
        <translation type="vanished">Szczegóły</translation>
    </message>
    <message>
        <source>Open in New Window</source>
        <comment>Open in New Window context menu item</comment>
        <translation type="vanished">Otwórz w nowym oknie</translation>
    </message>
    <message>
        <source>Save Link...</source>
        <comment>Download Linked File context menu item</comment>
        <translation type="vanished">Zachowaj odsyłacz...</translation>
    </message>
    <message>
        <source>Copy Link</source>
        <comment>Copy Link context menu item</comment>
        <translation type="vanished">Kopiuj odsyłacz</translation>
    </message>
    <message>
        <source>Open Image</source>
        <comment>Open Image in New Window context menu item</comment>
        <translation type="vanished">Otwórz obrazek</translation>
    </message>
    <message>
        <source>Save Image</source>
        <comment>Download Image context menu item</comment>
        <translation type="vanished">Zachowaj obrazek</translation>
    </message>
    <message>
        <source>Copy Image</source>
        <comment>Copy Link context menu item</comment>
        <translation type="vanished">Kopiuj obrazek</translation>
    </message>
    <message>
        <source>Copy Image Address</source>
        <comment>Copy Image Address menu item</comment>
        <translation type="vanished">Kopiuj adres obrazka</translation>
    </message>
    <message>
        <source>Open Video</source>
        <comment>Open Video in New Window</comment>
        <translation type="vanished">Otwórz wideo</translation>
    </message>
    <message>
        <source>Open Audio</source>
        <comment>Open Audio in New Window</comment>
        <translation type="vanished">Otwórz dźwięk</translation>
    </message>
    <message>
        <source>Copy Video</source>
        <comment>Copy Video Link Location</comment>
        <translation type="vanished">Kopiuj wideo</translation>
    </message>
    <message>
        <source>Copy Audio</source>
        <comment>Copy Audio Link Location</comment>
        <translation type="vanished">Kopiuj dźwięk</translation>
    </message>
    <message>
        <source>Enter Fullscreen</source>
        <comment>Switch Video to Fullscreen</comment>
        <translation type="vanished">Pełny ekran</translation>
    </message>
    <message>
        <source>Play</source>
        <comment>Play</comment>
        <translation type="vanished">Odtwórz</translation>
    </message>
    <message>
        <source>Pause</source>
        <comment>Pause</comment>
        <translation type="vanished">Pauza</translation>
    </message>
    <message>
        <source>Mute</source>
        <comment>Mute</comment>
        <translation type="vanished">Wycisz</translation>
    </message>
    <message>
        <source>Open Frame</source>
        <comment>Open Frame in New Window context menu item</comment>
        <translation type="vanished">Otwórz ramkę</translation>
    </message>
    <message>
        <source>Copy</source>
        <comment>Copy context menu item</comment>
        <translation type="vanished">Kopiuj</translation>
    </message>
    <message>
        <source>Go Back</source>
        <comment>Back context menu item</comment>
        <translation type="vanished">Wróć</translation>
    </message>
    <message>
        <source>Go Forward</source>
        <comment>Forward context menu item</comment>
        <translation type="vanished">Przejdź dalej</translation>
    </message>
    <message>
        <source>Stop</source>
        <comment>Stop context menu item</comment>
        <translation type="vanished">Zatrzymaj</translation>
    </message>
    <message>
        <source>Reload</source>
        <comment>Reload context menu item</comment>
        <translation type="vanished">Przeładuj</translation>
    </message>
    <message>
        <source>Cut</source>
        <comment>Cut context menu item</comment>
        <translation type="vanished">Wytnij</translation>
    </message>
    <message>
        <source>Paste</source>
        <comment>Paste context menu item</comment>
        <translation type="vanished">Wklej</translation>
    </message>
    <message>
        <source>Select All</source>
        <comment>Select All context menu item</comment>
        <translation type="vanished">Zaznacz wszystko</translation>
    </message>
    <message>
        <source>No Guesses Found</source>
        <comment>No Guesses Found context menu item</comment>
        <translation type="vanished">Nie odnaleziono podpowiedzi</translation>
    </message>
    <message>
        <source>Ignore</source>
        <comment>Ignore Spelling context menu item</comment>
        <translation type="vanished">Zignoruj</translation>
    </message>
    <message>
        <source>Add To Dictionary</source>
        <comment>Learn Spelling context menu item</comment>
        <translation type="vanished">Dodaj do słownika</translation>
    </message>
    <message>
        <source>Search The Web</source>
        <comment>Search The Web context menu item</comment>
        <translation type="vanished">Wyszukaj w sieci</translation>
    </message>
    <message>
        <source>Look Up In Dictionary</source>
        <comment>Look Up in Dictionary context menu item</comment>
        <translation type="vanished">Poszukaj w słowniku</translation>
    </message>
    <message>
        <source>Open Link</source>
        <comment>Open Link context menu item</comment>
        <translation type="vanished">Otwórz odsyłacz</translation>
    </message>
    <message>
        <source>Ignore</source>
        <comment>Ignore Grammar context menu item</comment>
        <translation type="vanished">Zignoruj</translation>
    </message>
    <message>
        <source>Spelling</source>
        <comment>Spelling and Grammar context sub-menu item</comment>
        <translation type="vanished">Pisownia</translation>
    </message>
    <message>
        <source>Show Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation type="vanished">Pokaż pisownię i gramatykę</translation>
    </message>
    <message>
        <source>Hide Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation type="vanished">Schowaj pisownię i gramatykę</translation>
    </message>
    <message>
        <source>Check Spelling</source>
        <comment>Check spelling context menu item</comment>
        <translation type="vanished">Sprawdź pisownię</translation>
    </message>
    <message>
        <source>Check Spelling While Typing</source>
        <comment>Check spelling while typing context menu item</comment>
        <translation type="vanished">Sprawdzaj pisownię podczas pisania</translation>
    </message>
    <message>
        <source>Check Grammar With Spelling</source>
        <comment>Check grammar with spelling context menu item</comment>
        <translation type="vanished">Sprawdzaj gramatykę wraz z pisownią</translation>
    </message>
    <message>
        <source>Fonts</source>
        <comment>Font context sub-menu item</comment>
        <translation type="vanished">Czcionki</translation>
    </message>
    <message>
        <source>Bold</source>
        <comment>Bold context menu item</comment>
        <translation type="vanished">Pogrubiony</translation>
    </message>
    <message>
        <source>Italic</source>
        <comment>Italic context menu item</comment>
        <translation type="vanished">Kursywa</translation>
    </message>
    <message>
        <source>Underline</source>
        <comment>Underline context menu item</comment>
        <translation type="vanished">Podkreślenie</translation>
    </message>
    <message>
        <source>Outline</source>
        <comment>Outline context menu item</comment>
        <translation type="vanished">Kontur</translation>
    </message>
    <message>
        <source>Direction</source>
        <comment>Writing direction context sub-menu item</comment>
        <translation type="vanished">Kierunek</translation>
    </message>
    <message>
        <source>Text Direction</source>
        <comment>Text direction context sub-menu item</comment>
        <translation type="vanished">Kierunek tekstu</translation>
    </message>
    <message>
        <source>Default</source>
        <comment>Default writing direction context menu item</comment>
        <translation type="vanished">Domyślny</translation>
    </message>
    <message>
        <source>Missing Plug-in</source>
        <comment>Label text to be used when a plug-in is missing</comment>
        <translation type="vanished">Brakująca wtyczka</translation>
    </message>
    <message>
        <source>Loading...</source>
        <comment>Media controller status message when the media is loading</comment>
        <translation type="vanished">Ładowanie...</translation>
    </message>
    <message>
        <source>Live Broadcast</source>
        <comment>Media controller status message when watching a live broadcast</comment>
        <translation type="vanished">Transmisja na żywo</translation>
    </message>
    <message>
        <source>Audio Element</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Element dźwiękowy</translation>
    </message>
    <message>
        <source>Video Element</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Element wideo</translation>
    </message>
    <message>
        <source>Mute Button</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przycisk wyłączania głosu</translation>
    </message>
    <message>
        <source>Unmute Button</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przycisk włączania głosu</translation>
    </message>
    <message>
        <source>Play Button</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przycisk odtwarzania</translation>
    </message>
    <message>
        <source>Pause Button</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przycisk pauzy</translation>
    </message>
    <message>
        <source>Slider</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Suwak</translation>
    </message>
    <message>
        <source>Slider Thumb</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Uchwyt suwaka</translation>
    </message>
    <message>
        <source>Rewind Button</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przycisk przewijania</translation>
    </message>
    <message>
        <source>Return to Real-time Button</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przycisk powrotu do czasu rzeczywistego</translation>
    </message>
    <message>
        <source>Elapsed Time</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Czas który upłynął</translation>
    </message>
    <message>
        <source>Remaining Time</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Czas który pozostał</translation>
    </message>
    <message>
        <source>Status Display</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Wyświetlacz stanu</translation>
    </message>
    <message>
        <source>Fullscreen Button</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przycisk trybu pełnoekranowego</translation>
    </message>
    <message>
        <source>Seek Forward Button</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przycisk przeszukiwania do przodu</translation>
    </message>
    <message>
        <source>Seek Back Button</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przycisk przeszukiwania do tyłu</translation>
    </message>
    <message>
        <source>Audio element playback controls and status display</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Kontrolki odtwarzania dźwięku i wyświetlacz stanu</translation>
    </message>
    <message>
        <source>Video element playback controls and status display</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Kontrolki odtwarzania wideo i wyświetlacz stanu</translation>
    </message>
    <message>
        <source>Mute audio tracks</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Wyłącz ścieżkę dźwiękową</translation>
    </message>
    <message>
        <source>Unmute audio tracks</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Włącz ścieżkę dźwiękową</translation>
    </message>
    <message>
        <source>Begin playback</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Rozpocznij odtwarzanie</translation>
    </message>
    <message>
        <source>Pause playback</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Zatrzymaj odtwarzanie</translation>
    </message>
    <message>
        <source>Movie time scrubber</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Suwak czasu</translation>
    </message>
    <message>
        <source>Movie time scrubber thumb</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Uchwyt suwaka czasu</translation>
    </message>
    <message>
        <source>Rewind movie</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przewiń film</translation>
    </message>
    <message>
        <source>Return streaming movie to real-time</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przywróć przesyłanie filmu do czasu rzeczywistego</translation>
    </message>
    <message>
        <source>Current movie time</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Czas bieżącego filmu</translation>
    </message>
    <message>
        <source>Remaining movie time</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Czas do końca filmu</translation>
    </message>
    <message>
        <source>Current movie status</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Stan bieżącego filmu</translation>
    </message>
    <message>
        <source>Play movie in full-screen mode</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Odtwarzaj film w trybie pełnoekranowym</translation>
    </message>
    <message>
        <source>Seek quickly back</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przeszukaj szybko do tyłu</translation>
    </message>
    <message>
        <source>Seek quickly forward</source>
        <comment>Media controller element</comment>
        <translation type="vanished">Przeszukaj szybko do przodu</translation>
    </message>
    <message>
        <source>Indefinite time</source>
        <comment>Media time description</comment>
        <translation type="vanished">Nieokreślony czas</translation>
    </message>
    <message>
        <source>%1 days %2 hours %3 minutes %4 seconds</source>
        <comment>Media time description</comment>
        <translation type="vanished">%1 dni %2 godzin %3 minut %4 sekund</translation>
    </message>
    <message>
        <source>%1 hours %2 minutes %3 seconds</source>
        <comment>Media time description</comment>
        <translation type="vanished">%1 godzin %2 minut %3 sekund</translation>
    </message>
    <message>
        <source>%1 minutes %2 seconds</source>
        <comment>Media time description</comment>
        <translation type="vanished">%1 minut %2 sekund</translation>
    </message>
    <message>
        <source>%1 seconds</source>
        <comment>Media time description</comment>
        <translation type="vanished">%1 sekund</translation>
    </message>
    <message>
        <source>Inspect</source>
        <comment>Inspect Element context menu item</comment>
        <translation type="vanished">Zwiedzaj</translation>
    </message>
    <message>
        <source>No recent searches</source>
        <comment>Label for only item in menu that appears when clicking on the search field image, when no searches have been performed</comment>
        <translation type="vanished">Brak ostatnich wyszukiwań</translation>
    </message>
    <message>
        <source>Recent searches</source>
        <comment>label for first item in the menu that appears when clicking on the search field image, used as embedded menu title</comment>
        <translation type="vanished">Ostatnie wyszukiwania</translation>
    </message>
    <message>
        <source>Clear recent searches</source>
        <comment>menu item in Recent Searches menu that empties menu&apos;s contents</comment>
        <translation type="vanished">Wyczyść ostatnie wyszukiwania</translation>
    </message>
    <message>
        <source>Unknown</source>
        <comment>Unknown filesize FTP directory listing item</comment>
        <translation type="vanished">Nieznany</translation>
    </message>
    <message>
        <source>Web Inspector - %2</source>
        <translation type="vanished">Wizytator sieciowy - %2</translation>
    </message>
    <message>
        <source>Redirection limit reached</source>
        <translation type="vanished">Osiągnięto limit przekierowań</translation>
    </message>
    <message>
        <source>This is a searchable index. Enter search keywords: </source>
        <comment>text that appears at the start of nearly-obsolete web pages in the form of a &apos;searchable index&apos;</comment>
        <translation type="vanished">To jest indeks wyszukiwawczy. Podaj słowa do wyszukania:</translation>
    </message>
    <message>
        <source>Left to Right</source>
        <comment>Left to Right context menu item</comment>
        <translation type="vanished">Z lewej na prawą</translation>
    </message>
    <message>
        <source>Right to Left</source>
        <comment>Right to Left context menu item</comment>
        <translation type="vanished">Z prawej na lewą</translation>
    </message>
    <message>
        <source>%1 (%2x%3 pixels)</source>
        <comment>Title string for images</comment>
        <translation type="vanished">%1 (%2x%3 piksli)</translation>
    </message>
    <message>
        <source>Scroll here</source>
        <translation type="vanished">Przewiń tutaj</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="vanished">Lewa krawędź</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="vanished">Do góry</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="vanished">Prawa krawędź</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="vanished">W dół</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="vanished">Strona w lewo</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="vanished">Strona do góry</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="vanished">Strona w prawo</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="vanished">Strona w dół</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="vanished">Przewiń w lewo</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="vanished">Przewiń do góry</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="vanished">Przewiń w prawo</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="vanished">Przewiń w dół</translation>
    </message>
    <message numerus="yes">
        <source>%n file(s)</source>
        <comment>number of chosen file</comment>
        <translation type="vanished">
            <numerusform>%n plik</numerusform>
            <numerusform>%n pliki</numerusform>
            <numerusform>%n plików</numerusform>
        </translation>
    </message>
    <message>
        <source>JavaScript Alert - %1</source>
        <translation type="vanished">Ostrzeżenie JavaScript - %1</translation>
    </message>
    <message>
        <source>JavaScript Confirm - %1</source>
        <translation type="vanished">Potwierdzenie JavaScript - %1</translation>
    </message>
    <message>
        <source>JavaScript Prompt - %1</source>
        <translation type="vanished">Zachęta JavaScript - %1</translation>
    </message>
    <message>
        <source>JavaScript Problem - %1</source>
        <translation type="vanished">Problem JavaScript - %1</translation>
    </message>
    <message>
        <source>The script on this page appears to have a problem. Do you want to stop the script?</source>
        <translation type="vanished">Skrypt na tej stronie nie działa poprawnie. Czy chcesz przerwać ten skrypt?</translation>
    </message>
    <message>
        <source>Move the cursor to the next character</source>
        <translation type="vanished">Przesuń kursor do następnego znaku</translation>
    </message>
    <message>
        <source>Move the cursor to the previous character</source>
        <translation type="vanished">Przesuń kursor do poprzedniego znaku</translation>
    </message>
    <message>
        <source>Move the cursor to the next word</source>
        <translation type="vanished">Przesuń kursor do następnego słowa</translation>
    </message>
    <message>
        <source>Move the cursor to the previous word</source>
        <translation type="vanished">Przesuń kursor do poprzedniego słowa</translation>
    </message>
    <message>
        <source>Move the cursor to the next line</source>
        <translation type="vanished">Przesuń kursor do następnej linii</translation>
    </message>
    <message>
        <source>Move the cursor to the previous line</source>
        <translation type="vanished">Przesuń kursor do poprzedniej linii</translation>
    </message>
    <message>
        <source>Move the cursor to the start of the line</source>
        <translation type="vanished">Przesuń kursor do początku linii</translation>
    </message>
    <message>
        <source>Move the cursor to the end of the line</source>
        <translation type="vanished">Przesuń kursor do końca linii</translation>
    </message>
    <message>
        <source>Move the cursor to the start of the block</source>
        <translation type="vanished">Przesuń kursor do początku bloku</translation>
    </message>
    <message>
        <source>Move the cursor to the end of the block</source>
        <translation type="vanished">Przesuń kursor do końca bloku</translation>
    </message>
    <message>
        <source>Move the cursor to the start of the document</source>
        <translation type="vanished">Przesuń kursor do początku dokumentu</translation>
    </message>
    <message>
        <source>Move the cursor to the end of the document</source>
        <translation type="vanished">Przesuń kursor do końca dokumentu</translation>
    </message>
    <message>
        <source>Select to the next character</source>
        <translation type="vanished">Zaznacz do następnego znaku</translation>
    </message>
    <message>
        <source>Select to the previous character</source>
        <translation type="vanished">Zaznacz do poprzedniego znaku</translation>
    </message>
    <message>
        <source>Select to the next word</source>
        <translation type="vanished">Zaznacz do następnego słowa</translation>
    </message>
    <message>
        <source>Select to the previous word</source>
        <translation type="vanished">Zaznacz do poprzedniego słowa</translation>
    </message>
    <message>
        <source>Select to the next line</source>
        <translation type="vanished">Zaznacz do następnej linii</translation>
    </message>
    <message>
        <source>Select to the previous line</source>
        <translation type="vanished">Zaznacz do poprzedniej linii</translation>
    </message>
    <message>
        <source>Select to the start of the line</source>
        <translation type="vanished">Zaznacz do początku linii</translation>
    </message>
    <message>
        <source>Select to the end of the line</source>
        <translation type="vanished">Zaznacz do końca linii</translation>
    </message>
    <message>
        <source>Select to the start of the block</source>
        <translation type="vanished">Zaznacz do początku bloku</translation>
    </message>
    <message>
        <source>Select to the end of the block</source>
        <translation type="vanished">Zaznacz do końca bloku</translation>
    </message>
    <message>
        <source>Select to the start of the document</source>
        <translation type="vanished">Zaznacz do początku dokumentu</translation>
    </message>
    <message>
        <source>Select to the end of the document</source>
        <translation type="vanished">Zaznacz do końca dokumentu</translation>
    </message>
    <message>
        <source>Delete to the start of the word</source>
        <translation type="vanished">Usuń do początku słowa</translation>
    </message>
    <message>
        <source>Delete to the end of the word</source>
        <translation type="vanished">Usuń do końca słowa</translation>
    </message>
    <message>
        <source>Insert a new paragraph</source>
        <translation type="vanished">Wstaw nowy paragraf</translation>
    </message>
    <message>
        <source>Insert a new line</source>
        <translation type="vanished">Wstaw nową linię</translation>
    </message>
    <message>
        <source>Paste and Match Style</source>
        <translation type="vanished">Wklej i dopasuj styl</translation>
    </message>
    <message>
        <source>Remove formatting</source>
        <translation type="vanished">Usuń formatowanie</translation>
    </message>
    <message>
        <source>Strikethrough</source>
        <translation type="vanished">Przekreślenie</translation>
    </message>
    <message>
        <source>Subscript</source>
        <translation type="vanished">Indeks dolny</translation>
    </message>
    <message>
        <source>Superscript</source>
        <translation type="vanished">Indeks górny</translation>
    </message>
    <message>
        <source>Insert Bulleted List</source>
        <translation type="vanished">Wstaw listę wypunktowaną</translation>
    </message>
    <message>
        <source>Insert Numbered List</source>
        <translation type="vanished">Wstaw listę ponumerowaną</translation>
    </message>
    <message>
        <source>Indent</source>
        <translation type="vanished">Zwiększ wcięcie</translation>
    </message>
    <message>
        <source>Outdent</source>
        <translation type="vanished">Zmniejsz wcięcie</translation>
    </message>
    <message>
        <source>Center</source>
        <translation type="vanished">Wyśrodkuj</translation>
    </message>
    <message>
        <source>Justify</source>
        <translation type="vanished">Wyjustuj</translation>
    </message>
    <message>
        <source>Align Left</source>
        <translation type="vanished">Wyrównaj do lewej</translation>
    </message>
    <message>
        <source>Align Right</source>
        <translation type="vanished">Wyrównaj do prawej</translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <source>What&apos;s This?</source>
        <translation type="vanished">Co to jest?</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <source>*</source>
        <translation type="vanished">*</translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <source>Go Back</source>
        <translation type="vanished">Wróć</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">Kontynuuj</translation>
    </message>
    <message>
        <source>Commit</source>
        <translation type="vanished">Dokonaj</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="vanished">Zrobione</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Pomoc</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation type="vanished">&lt; &amp;Wstecz</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation type="vanished">&amp;Zakończ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Anuluj</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="vanished">&amp;Pomoc</translation>
    </message>
    <message>
        <source>&amp;Next</source>
        <translation type="vanished">&amp;Dalej</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation type="vanished">&amp;Dalej &gt;</translation>
    </message>
</context>
<context>
    <name>QWorkspace</name>
    <message>
        <source>%1 - [%2]</source>
        <translation type="vanished">%1 - [%2]</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="vanished">&amp;Zamknij</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation type="vanished">Zma&amp;ksymalizuj</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Zminimalizuj</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation type="vanished">Zmi&amp;nimalizuj</translation>
    </message>
    <message>
        <source>&amp;Move</source>
        <translation type="vanished">&amp;Przenieś</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">&amp;Przywróć</translation>
    </message>
    <message>
        <source>Restore Down</source>
        <translation type="vanished">Przywróć pod spód</translation>
    </message>
    <message>
        <source>Sh&amp;ade</source>
        <translation type="vanished">&amp;Zwiń</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation type="vanished">&amp;Rozmiar</translation>
    </message>
    <message>
        <source>Stay on &amp;Top</source>
        <translation type="vanished">Pozostaw na &amp;wierzchu</translation>
    </message>
    <message>
        <source>&amp;Unshade</source>
        <translation type="vanished">R&amp;ozwiń</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation type="vanished">oczekiwano deklaracji &quot;encoding&quot; lub &quot;standalone&quot; podczas odczytywania deklaracji XML</translation>
    </message>
    <message>
        <source>error in the text declaration of an external entity</source>
        <translation type="vanished">błąd w deklaracji &quot;text&quot; zewnętrznej jednostki</translation>
    </message>
    <message>
        <source>error occurred while parsing comment</source>
        <translation type="vanished">wystąpił błąd podczas parsowania komentarza</translation>
    </message>
    <message>
        <source>error occurred while parsing content</source>
        <translation type="vanished">wystąpił błąd podczas parsowania zawartości</translation>
    </message>
    <message>
        <source>error occurred while parsing document type definition</source>
        <translation type="vanished">wystąpił błąd podczas parsowania typu definicji dokumentu</translation>
    </message>
    <message>
        <source>error occurred while parsing element</source>
        <translation type="vanished">wystąpił błąd podczas parsowania elementu</translation>
    </message>
    <message>
        <source>error occurred while parsing reference</source>
        <translation type="vanished">wystąpił błąd podczas parsowania odwołania</translation>
    </message>
    <message>
        <source>error triggered by consumer</source>
        <translation type="vanished">błąd wywołany przez konsumenta</translation>
    </message>
    <message>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation type="vanished">odwołanie do jednostki ogólnej zewnętrznie przetworzonej nie dozwolone dla wartości atrybutu</translation>
    </message>
    <message>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation type="vanished">odwołanie do jednostki ogólnej zewnętrznie przetworzonej nie dozwolone w DTD</translation>
    </message>
    <message>
        <source>internal general entity reference not allowed in DTD</source>
        <translation type="vanished">odwołanie do jednostki ogólnej wewnętrznej nie dozwolone w DTD</translation>
    </message>
    <message>
        <source>invalid name for processing instruction</source>
        <translation type="vanished">niepoprawna nazwa dla instrukcji przetwarzającej</translation>
    </message>
    <message>
        <source>letter is expected</source>
        <translation type="vanished">oczekiwana jest litera</translation>
    </message>
    <message>
        <source>more than one document type definition</source>
        <translation type="vanished">więcej niż jedna definicja typu dokumentu</translation>
    </message>
    <message>
        <source>no error occurred</source>
        <translation type="vanished">nie pojawił się żaden błąd</translation>
    </message>
    <message>
        <source>recursive entities</source>
        <translation type="vanished">jednostki rekurencyjne</translation>
    </message>
    <message>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation type="vanished">deklaracja &quot;standalone&quot; oczekiwana podczas czytania deklaracji XML</translation>
    </message>
    <message>
        <source>tag mismatch</source>
        <translation type="vanished">niezgodny tag</translation>
    </message>
    <message>
        <source>unexpected character</source>
        <translation type="vanished">nieoczekiwany znak</translation>
    </message>
    <message>
        <source>unexpected end of file</source>
        <translation type="vanished">nieoczekiwany koniec pliku</translation>
    </message>
    <message>
        <source>unparsed entity reference in wrong context</source>
        <translation type="vanished">odwołanie do jednostki nieprzetworzonej w złym kontekście</translation>
    </message>
    <message>
        <source>version expected while reading the XML declaration</source>
        <translation type="vanished">oczekiwano wersji podczas czytania deklaracji XML</translation>
    </message>
    <message>
        <source>wrong value for standalone declaration</source>
        <translation type="vanished">błędna wartość dla deklaracji &quot;standalone&quot;</translation>
    </message>
</context>
<context>
    <name>QXmlPatternistCLI</name>
    <message>
        <source>Warning in %1, at line %2, column %3: %4</source>
        <translation type="vanished">Ostrzeżenie w %1, wiersz %2, kolumna %3: %4</translation>
    </message>
    <message>
        <source>Warning in %1: %2</source>
        <translation type="vanished">Ostrzeżenie w %1: %2</translation>
    </message>
    <message>
        <source>Unknown location</source>
        <translation type="vanished">Nieznane położenie</translation>
    </message>
    <message>
        <source>Error %1 in %2, at line %3, column %4: %5</source>
        <translation type="vanished">Błąd %1 w %2, wiersz %3, kolumna %4: %5</translation>
    </message>
    <message>
        <source>Error %1 in %2: %3</source>
        <translation type="vanished">Błąd %1 w %2: %3</translation>
    </message>
</context>
<context>
    <name>QXmlStream</name>
    <message>
        <source>Extra content at end of document.</source>
        <translation type="vanished">Dodatkowa treść na końcu dokumentu.</translation>
    </message>
    <message>
        <source>Invalid entity value.</source>
        <translation type="vanished">Niepoprawna wartość jednostki.</translation>
    </message>
    <message>
        <source>Invalid XML character.</source>
        <translation type="vanished">Niepoprawny znak XML.</translation>
    </message>
    <message>
        <source>Sequence &apos;]]&gt;&apos; not allowed in content.</source>
        <translation type="vanished">Ciąg &apos;]]&gt;&apos; niedozwolony w treści.</translation>
    </message>
    <message>
        <source>Namespace prefix &apos;%1&apos; not declared</source>
        <translation type="vanished">Przedrostek przestrzeni nazw &apos;%1&apos; nie został zadeklarowany</translation>
    </message>
    <message>
        <source>Attribute redefined.</source>
        <translation type="vanished">Atrybut zdefiniowany wielokrotnie.</translation>
    </message>
    <message>
        <source>Unexpected character &apos;%1&apos; in public id literal.</source>
        <translation type="vanished">Nieoczekiwany znak &apos;%1&apos; w publicznym literale.</translation>
    </message>
    <message>
        <source>Invalid XML version string.</source>
        <translation type="vanished">Niepoprawna wersja XML.</translation>
    </message>
    <message>
        <source>Unsupported XML version.</source>
        <translation type="vanished">Nieobsługiwana wersja XML.</translation>
    </message>
    <message>
        <source>%1 is an invalid encoding name.</source>
        <translation type="vanished">%1 jest niepoprawną nazwą kodowania.</translation>
    </message>
    <message>
        <source>Encoding %1 is unsupported</source>
        <translation type="vanished">Kodowanie %1 jest nieobsługiwane</translation>
    </message>
    <message>
        <source>Standalone accepts only yes or no.</source>
        <translation type="vanished">Tylko wartości &quot;tak&quot; lub &quot;nie&quot; są akceptowane przez &quot;standalone&quot;.</translation>
    </message>
    <message>
        <source>Invalid attribute in XML declaration.</source>
        <translation type="vanished">Niepoprawny atrybut w deklaracji XML.</translation>
    </message>
    <message>
        <source>Premature end of document.</source>
        <translation type="vanished">Przedwczesne zakończenie dokumentu.</translation>
    </message>
    <message>
        <source>Invalid document.</source>
        <translation type="vanished">Niepoprawny dokument.</translation>
    </message>
    <message>
        <source>Expected </source>
        <translation type="vanished">Oczekiwano </translation>
    </message>
    <message>
        <source>, but got &apos;</source>
        <translation type="vanished">, ale otrzymano &apos;</translation>
    </message>
    <message>
        <source>Unexpected &apos;</source>
        <translation type="vanished">Nieoczekiwany &apos;</translation>
    </message>
    <message>
        <source>Expected character data.</source>
        <translation type="vanished">Oczekiwana dana znakowa.</translation>
    </message>
    <message>
        <source>Recursive entity detected.</source>
        <translation type="vanished">Wykryto jednostkę rekurencyjną.</translation>
    </message>
    <message>
        <source>Start tag expected.</source>
        <translation type="vanished">Oczekiwano rozpoczęcia tagu.</translation>
    </message>
    <message>
        <source>XML declaration not at start of document.</source>
        <translation type="vanished">Deklaracja XML nie jest na początku dokumentu.</translation>
    </message>
    <message>
        <source>NDATA in parameter entity declaration.</source>
        <translation type="vanished">NDATA w deklaracji parametru obiektu.</translation>
    </message>
    <message>
        <source>%1 is an invalid processing instruction name.</source>
        <translation type="vanished">%1 jest niepoprawną nazwą instrukcji przetwarzającej.</translation>
    </message>
    <message>
        <source>Invalid processing instruction name.</source>
        <translation type="vanished">Niepoprawna nazwa instrukcji przetwarzającej.</translation>
    </message>
    <message>
        <source>Illegal namespace declaration.</source>
        <translation type="vanished">Niepoprawna deklaracja przestrzeni nazw.</translation>
    </message>
    <message>
        <source>Invalid XML name.</source>
        <translation type="vanished">Niepoprawna nazwa XML.</translation>
    </message>
    <message>
        <source>Opening and ending tag mismatch.</source>
        <translation type="vanished">Niezgodne tagi początku i końca.</translation>
    </message>
    <message>
        <source>Reference to unparsed entity &apos;%1&apos;.</source>
        <translation type="vanished">Odwołanie do nieprzetworzonej jednostki &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Entity &apos;%1&apos; not declared.</source>
        <translation type="vanished">Jednostka &apos;%1&apos; nie zadeklarowana.</translation>
    </message>
    <message>
        <source>Reference to external entity &apos;%1&apos; in attribute value.</source>
        <translation type="vanished">Odwołanie do zewnętrznej jednostki &apos;%1&apos; jako wartość atrybutu.</translation>
    </message>
    <message>
        <source>Invalid character reference.</source>
        <translation type="vanished">Niepoprawny znak odwołania.</translation>
    </message>
    <message>
        <source>Encountered incorrectly encoded content.</source>
        <translation type="vanished">Natrafiono na niepoprawnie zakodowaną treść.</translation>
    </message>
    <message>
        <source>The standalone pseudo attribute must appear after the encoding.</source>
        <translation type="vanished">Pseudo atrybut &quot;standalone&quot; musi pojawić się po &quot;encoding&quot;.</translation>
    </message>
    <message>
        <source>%1 is an invalid PUBLIC identifier.</source>
        <translation type="vanished">%1 jest niepoprawnym publicznym identyfikatorem.</translation>
    </message>
</context>
<context>
    <name>QmlJSDebugger::LiveSelectionTool</name>
    <message>
        <source>Items</source>
        <translation type="vanished">Elementy</translation>
    </message>
</context>
<context>
    <name>QmlJSDebugger::QmlToolBar</name>
    <message>
        <source>Inspector Mode</source>
        <translation type="vanished">Tryb inspekcji</translation>
    </message>
    <message>
        <source>Play/Pause Animations</source>
        <translation type="vanished">Odtwórz / wstrzymaj animacje</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Wybierz</translation>
    </message>
    <message>
        <source>Select (Marquee)</source>
        <translation type="vanished">Wybierz (Marquee)</translation>
    </message>
    <message>
        <source>Zoom</source>
        <translation type="vanished">Powiększ</translation>
    </message>
    <message>
        <source>Color Picker</source>
        <translation type="vanished">Wybieracz kolorów</translation>
    </message>
    <message>
        <source>Apply Changes to QML Viewer</source>
        <translation type="vanished">Zastosuj zmiany do QML Viewera</translation>
    </message>
    <message>
        <source>Apply Changes to Document</source>
        <translation type="vanished">Zastosuj zmiany do dokumentu</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="vanished">Narzędzia</translation>
    </message>
    <message>
        <source>1x</source>
        <translation type="vanished">1x</translation>
    </message>
    <message>
        <source>0.5x</source>
        <translation type="vanished">0.5x</translation>
    </message>
    <message>
        <source>0.25x</source>
        <translation type="vanished">0.25x</translation>
    </message>
    <message>
        <source>0.125x</source>
        <translation type="vanished">0.125x</translation>
    </message>
    <message>
        <source>0.1x</source>
        <translation type="vanished">0.1x</translation>
    </message>
</context>
<context>
    <name>QmlJSDebugger::ToolBarColorBox</name>
    <message>
        <source>Copy Color</source>
        <translation type="vanished">Kopiuj kolor</translation>
    </message>
</context>
<context>
    <name>QmlJSDebugger::ZoomTool</name>
    <message>
        <source>Zoom to &amp;100%</source>
        <translation type="vanished">Powiększ do &amp;100%</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="vanished">Powiększ</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="vanished">Pomniejsz</translation>
    </message>
</context>
<context>
    <name>QtXmlPatterns</name>
    <message>
        <source>Network timeout.</source>
        <translation type="vanished">Przekroczony czas połączenia.</translation>
    </message>
    <message>
        <source>Element %1 can&apos;t be serialized because it appears outside the document element.</source>
        <translation type="vanished">Element %1 nie może być zserializowany ponieważ pojawił się poza elementem &quot;document&quot;.</translation>
    </message>
    <message>
        <source>Attribute %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation type="vanished">Atrybut %1 nie może być zserializowany ponieważ pojawił się na najwyższym poziomie.</translation>
    </message>
    <message>
        <source>Year %1 is invalid because it begins with %2.</source>
        <translation type="vanished">Rok %1 jest niepoprawny ponieważ rozpoczyna się: %2.</translation>
    </message>
    <message>
        <source>Day %1 is outside the range %2..%3.</source>
        <translation type="vanished">Dzień %1 jest poza zakresem %2..%3.</translation>
    </message>
    <message>
        <source>Month %1 is outside the range %2..%3.</source>
        <translation type="vanished">Miesiąc %1 jest poza zakresem %2..%3.</translation>
    </message>
    <message>
        <source>Overflow: Can&apos;t represent date %1.</source>
        <translation type="vanished">Przepełnienie: Nie można wyrazić daty %1.</translation>
    </message>
    <message>
        <source>Day %1 is invalid for month %2.</source>
        <translation type="vanished">Dzień %1 jest niepoprawny dla miesiąca %2.</translation>
    </message>
    <message>
        <source>Time 24:%1:%2.%3 is invalid. Hour is 24, but minutes, seconds, and milliseconds are not all 0; </source>
        <translation type="vanished">Czas 24:%1:%2:%3 jest niepoprawny. Godzina jest 24, ale minuty, sekundy i milisekundy nie są równocześnie zerami;</translation>
    </message>
    <message>
        <source>Time %1:%2:%3.%4 is invalid.</source>
        <translation type="vanished">Czas %1:%2:%3.%4 jest niepoprawny.</translation>
    </message>
    <message>
        <source>Overflow: Date can&apos;t be represented.</source>
        <translation type="vanished">Przepełnienie: Data nie może być wyrażona.</translation>
    </message>
    <message>
        <source>At least one component must be present.</source>
        <translation type="vanished">Przynajmniej jeden komponent musi być obecny.</translation>
    </message>
    <message>
        <source>At least one time component must appear after the %1-delimiter.</source>
        <translation type="vanished">Przynajmniej jeden komponent musi wystąpić po nawiasie %1.</translation>
    </message>
    <message>
        <source>%1 is not a valid value of type %2.</source>
        <translation type="vanished">%1 nie jest poprawną wartością dla typu %2.</translation>
    </message>
    <message>
        <source>When casting to %1 from %2, the source value cannot be %3.</source>
        <translation type="vanished">W rzutowaniu %1 na %2 wartość źródłowa nie może być %3.</translation>
    </message>
    <message>
        <source>Dividing a value of type %1 by %2 (not-a-number) is not allowed.</source>
        <translation type="vanished">Dzielenie wartości typu %1 przez %2 (typ nienumeryczny) jest niedozwolone.</translation>
    </message>
    <message>
        <source>Dividing a value of type %1 by %2 or %3 (plus or minus zero) is not allowed.</source>
        <translation type="vanished">Dzielenie wartości typu %1 przez %2 lub %3 (plus lub minus zero) jest niedozwolone.</translation>
    </message>
    <message>
        <source>Multiplication of a value of type %1 by %2 or %3 (plus or minus infinity) is not allowed.</source>
        <translation type="vanished">Mnożenie wartości typu %1 przez %2 lub %3 (plus lub minus nieskończoność) jest niedozwolone.</translation>
    </message>
    <message>
        <source>A value of type %1 cannot have an Effective Boolean Value.</source>
        <translation type="vanished">Wartość typu %1 nie może posiadać efektywnej wartości boolowskiej (EBV).</translation>
    </message>
    <message>
        <source>Effective Boolean Value cannot be calculated for a sequence containing two or more atomic values.</source>
        <translation type="vanished">Efektywna wartość boolowska (EBV) nie może być obliczona dla sekwencji zawierającej dwie lub więcej wartości atomowe.</translation>
    </message>
    <message>
        <source>Value %1 of type %2 exceeds maximum (%3).</source>
        <translation type="vanished">Wartość %1 typu %2 przekracza maksimum (%3).</translation>
    </message>
    <message>
        <source>Value %1 of type %2 is below minimum (%3).</source>
        <translation type="vanished">Wartość %1 typu %2 jest poniżej minimum (%3).</translation>
    </message>
    <message>
        <source>A value of type %1 must contain an even number of digits. The value %2 does not.</source>
        <translation type="vanished">Wartość typu %1 musi zawierać parzystą liczbę cyfr. Wartość %2 nie zawiera.</translation>
    </message>
    <message>
        <source>%1 is not valid as a value of type %2.</source>
        <translation type="vanished">Wartość %1 nie jest poprawna jako wartość typu %2.</translation>
    </message>
    <message>
        <source>Operator %1 cannot be used on type %2.</source>
        <translation type="vanished">Operator %1 nie może być użyty dla typu %2.</translation>
    </message>
    <message>
        <source>Operator %1 cannot be used on atomic values of type %2 and %3.</source>
        <translation type="vanished">Operator %1 nie może być użyty dla atomowych wartości typu %2 i %3.</translation>
    </message>
    <message>
        <source>The namespace URI in the name for a computed attribute cannot be %1.</source>
        <translation type="vanished">Przestrzeń nazw URI nie może być %1 w nazwie dla obliczonego atrybutu.</translation>
    </message>
    <message>
        <source>The name for a computed attribute cannot have the namespace URI %1 with the local name %2.</source>
        <translation type="vanished">Nazwa dla wyliczonego atrybutu nie może zawierać przestrzeni nazw URI %1 z lokalną nazwą %2.</translation>
    </message>
    <message>
        <source>Type error in cast, expected %1, received %2.</source>
        <translation type="vanished">Błąd typów w rzutowaniu: oczekiwano %1, otrzymano %2.</translation>
    </message>
    <message>
        <source>When casting to %1 or types derived from it, the source value must be of the same type, or it must be a string literal. Type %2 is not allowed.</source>
        <translation type="vanished">Podczas rzutowania na %1 lub na typ pochodny, wartość źródłowa musi być tego samego typu lub musi być literałem znakowym. Typ %2 nie jest dozwolony.</translation>
    </message>
    <message>
        <source>A comment cannot contain %1</source>
        <translation type="vanished">Komentarz nie może zawierać %1</translation>
    </message>
    <message>
        <source>A comment cannot end with a %1.</source>
        <translation type="vanished">Komentarz nie może kończyć się: %1.</translation>
    </message>
    <message>
        <source>An attribute node cannot be a child of a document node. Therefore, the attribute %1 is out of place.</source>
        <translation type="vanished">Węzeł &quot;attribute&quot; nie może być podelementem węzła &quot;document&quot;. Dlatego atrybut %1 jest w złym miejscu.</translation>
    </message>
    <message>
        <source>A library module cannot be evaluated directly. It must be imported from a main module.</source>
        <translation type="vanished">Moduł biblioteki nie może być bezpośrednio oceniony. On musi być zaimportowany z głównego modułu.</translation>
    </message>
    <message>
        <source>No template by name %1 exists.</source>
        <translation type="vanished">Szablon o nazwie %1 nie istnieje.</translation>
    </message>
    <message>
        <source>A value of type %1 cannot be a predicate. A predicate must have either a numeric type or an Effective Boolean Value type.</source>
        <translation type="vanished">Wartość typu %1 nie może być predykatem. Predykat musi być typu liczbowego lub Efektywną Wartość Logiczną.</translation>
    </message>
    <message>
        <source>A positional predicate must evaluate to a single numeric value.</source>
        <translation type="vanished">Wynikiem predykatu pozycyjnego musi być pojedyncza wartość liczbowa.</translation>
    </message>
    <message>
        <source>The target name in a processing instruction cannot be %1 in any combination of upper and lower case. Therefore, %2 is invalid.</source>
        <translation type="vanished">Docelowa nazwa w instrukcji przetwarzania nie może być %1 w żadnej kombinacji wielkich i małych liter. Dlatego nazwa %2 jest niepoprawna.</translation>
    </message>
    <message>
        <source>%1 is not a valid target name in a processing instruction. It must be a %2 value, e.g. %3.</source>
        <translation type="vanished">%1 nie jest poprawną nazwą docelową w instrukcji przetwarzania. Nazwa musi być wartością %2, np. %3.</translation>
    </message>
    <message>
        <source>The last step in a path must contain either nodes or atomic values. It cannot be a mixture between the two.</source>
        <translation type="vanished">Ostatni krok w ścieżce musi zawierać albo węzły albo wartości atomowe. Nie może zawierać obu jednocześnie.</translation>
    </message>
    <message>
        <source>The data of a processing instruction cannot contain the string %1</source>
        <translation type="vanished">Dane instrukcji przetwarzania nie mogą zawierać ciągu %1</translation>
    </message>
    <message>
        <source>No namespace binding exists for the prefix %1</source>
        <translation type="vanished">Żadna przestrzeń nazw nie jest powiązana z przedrostkiem %1</translation>
    </message>
    <message>
        <source>No namespace binding exists for the prefix %1 in %2</source>
        <translation type="vanished">Żadna przestrzeń nazw nie jest powiązana z przedrostkiem %1 w %2</translation>
    </message>
    <message>
        <source>%1 is an invalid %2</source>
        <translation type="vanished">%1 jest niepoprawnym %2</translation>
    </message>
    <message>
        <source>The first argument to %1 cannot be of type %2. It must be a numeric type, xs:yearMonthDuration or xs:dayTimeDuration.</source>
        <translation type="vanished">Pierwszy argument w %1 nie może być typu %2. Musi on być typu liczbowego: xs:yearMonthDuration lub xs:dayTimeDuration.</translation>
    </message>
    <message>
        <source>The first argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation type="vanished">Pierwszy argument w %1 nie może być typu %2. Musi on być typu: %3, %4 lub %5.</translation>
    </message>
    <message>
        <source>The second argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation type="vanished">Drugi argument w %1 nie może być typu %2. Musi on być typu: %3, %4 lub %5.</translation>
    </message>
    <message>
        <source>%1 is not a valid XML 1.0 character.</source>
        <translation type="vanished">%1 nie jest poprawnym znakiem XML 1.0.</translation>
    </message>
    <message>
        <source>If both values have zone offsets, they must have the same zone offset. %1 and %2 are not the same.</source>
        <translation type="vanished">Jeśli oba argumenty mają przesunięcia strefowe, muszą one być takie same. %1 i %2 nie są takie same.</translation>
    </message>
    <message>
        <source>%1 was called.</source>
        <translation type="vanished">Wywołano %1.</translation>
    </message>
    <message>
        <source>%1 must be followed by %2 or %3, not at the end of the replacement string.</source>
        <translation type="vanished">Po %1 musi następować %2 lub %3, lecz nie na końcu zastępczego ciągu.</translation>
    </message>
    <message>
        <source>In the replacement string, %1 must be followed by at least one digit when not escaped.</source>
        <translation type="vanished">W ciągu zastępczym, po %1 musi następować przynajmniej jedna cyfra pod warunkiem, że nie jest ona w sekwencji escape.</translation>
    </message>
    <message>
        <source>In the replacement string, %1 can only be used to escape itself or %2, not %3</source>
        <translation type="vanished">W ciągu zastępczym, %1 może być użyte tylko do zabezpieczenia samej siebie lub %2, nigdy %3</translation>
    </message>
    <message>
        <source>%1 matches newline characters</source>
        <translation type="vanished">%1 dopasowało znak nowej linii</translation>
    </message>
    <message>
        <source>%1 and %2 match the start and end of a line.</source>
        <translation type="vanished">%1 i %2 dopasowały początek i koniec linii.</translation>
    </message>
    <message>
        <source>Matches are case insensitive</source>
        <translation type="vanished">Dopasowania uwzględniają wielkość liter</translation>
    </message>
    <message>
        <source>Whitespace characters are removed, except when they appear in character classes</source>
        <translation type="vanished">Spacje są usuwane z wyjątkiem kiedy pojawią się w klasach znakowych</translation>
    </message>
    <message>
        <source>%1 is an invalid regular expression pattern: %2</source>
        <translation type="vanished">%1 jest niepoprawnym wzorcem wyrażenia regularnego: %2</translation>
    </message>
    <message>
        <source>%1 is an invalid flag for regular expressions. Valid flags are:</source>
        <translation type="vanished">%1 jest niepoprawną flagą dla wyrażeń regularnych. Poprawnymi flagami są:</translation>
    </message>
    <message>
        <source>If the first argument is the empty sequence or a zero-length string (no namespace), a prefix cannot be specified. Prefix %1 was specified.</source>
        <translation type="vanished">Jeśli pierwszy argument jest pustą sekwencją lub zerowej długości ciągiem (przy braku przestrzeni nazw), przedrostek nie może wystąpić. Podano przedrostek %1.</translation>
    </message>
    <message>
        <source>It will not be possible to retrieve %1.</source>
        <translation type="vanished">Nie będzie można odzyskać %1.</translation>
    </message>
    <message>
        <source>The default collection is undefined</source>
        <translation type="vanished">Domyślna kolekcja jest niezdefiniowana</translation>
    </message>
    <message>
        <source>%1 cannot be retrieved</source>
        <translation type="vanished">%1 nie może być odzyskane</translation>
    </message>
    <message>
        <source>The normalization form %1 is unsupported. The supported forms are %2, %3, %4, and %5, and none, i.e. the empty string (no normalization).</source>
        <translation type="vanished">Znormalizowana forma %1 nie jest obsługiwana. Obsługiwanymi formami są: %2, %3, %4 i %5 oraz pusta forma (brak normalizacji).</translation>
    </message>
    <message>
        <source>A zone offset must be in the range %1..%2 inclusive. %3 is out of range.</source>
        <translation type="vanished">Przesunięcie strefowe musi być w zakresie %1..%2 włącznie. %3 jest poza tym zakresem.</translation>
    </message>
    <message>
        <source>Required cardinality is %1; got cardinality %2.</source>
        <translation type="vanished">Wymagana liczność wynosi %1; otrzymano %2.</translation>
    </message>
    <message>
        <source>The item %1 did not match the required type %2.</source>
        <translation type="vanished">Element %1 nie został dopasowany do wymaganego typu %2.</translation>
    </message>
    <message>
        <source>%1 is an unknown schema type.</source>
        <translation type="vanished">%1 jest nieznanym typem schematu.</translation>
    </message>
    <message>
        <source>A template with name %1 has already been declared.</source>
        <translation type="vanished">Szablon o nazwie %1 został już zadeklarowany.</translation>
    </message>
    <message>
        <source>Only one %1 declaration can occur in the query prolog.</source>
        <translation type="vanished">Tylko jedna deklaracja %1 może się pojawić w prologu zapytania.</translation>
    </message>
    <message>
        <source>The initialization of variable %1 depends on itself</source>
        <translation type="vanished">Inicjalizacja zmiennej %1 zależy od niej samej</translation>
    </message>
    <message>
        <source>The variable %1 is unused</source>
        <translation type="vanished">Zmienna %1 jest nieużywana</translation>
    </message>
    <message>
        <source>Version %1 is not supported. The supported XQuery version is 1.0.</source>
        <translation type="vanished">Wersja %1 nie jest obsługiwana. Obsługiwaną wersją XQuery jest wersja 1.0.</translation>
    </message>
    <message>
        <source>No function with signature %1 is available</source>
        <translation type="vanished">Żadna funkcja w postaci %1 nie jest dostępna</translation>
    </message>
    <message>
        <source>It is not possible to redeclare prefix %1.</source>
        <translation type="vanished">Nie jest możliwe ponowne zadeklarowanie przedrostka %1.</translation>
    </message>
    <message>
        <source>Prefix %1 is already declared in the prolog.</source>
        <translation type="vanished">Przedrostek %1 jest już zadeklarowany w prologu.</translation>
    </message>
    <message>
        <source>The name of an option must have a prefix. There is no default namespace for options.</source>
        <translation type="vanished">Nazwa opcji musi posiadać przedrostek. Nie istnieje domyślna przestrzeń nazw dla opcji.</translation>
    </message>
    <message>
        <source>The Schema Import feature is not supported, and therefore %1 declarations cannot occur.</source>
        <translation type="vanished">Cecha &quot;Import schematu&quot; nie jest obsługiwana, dlatego deklaracje %1 nie mogą pojawić.</translation>
    </message>
    <message>
        <source>The target namespace of a %1 cannot be empty.</source>
        <translation type="vanished">Docelowa przestrzeń nazw dla %1 nie może być pusta.</translation>
    </message>
    <message>
        <source>The module import feature is not supported</source>
        <translation type="vanished">Cecha &quot;Import modułu&quot; nie jest obsługiwana</translation>
    </message>
    <message>
        <source>The namespace for a user defined function cannot be empty (try the predefined prefix %1, which exists for cases like this)</source>
        <translation type="vanished">Przestrzeń nazw dla funkcji zdefiniowanej przez użytkownika nie może być pusta (spróbuj predefiniowany przedrostek %1, który stworzono specjalnie do takich sytuacji)</translation>
    </message>
    <message>
        <source>The namespace %1 is reserved; therefore user defined functions may not use it. Try the predefined prefix %2, which exists for these cases.</source>
        <translation type="vanished">Przestrzeń nazw %1 jest zarezerwowana, dlatego funkcje zdefiniowane przez użytkownika nie mogą jej użyć. Spróbuj predefiniowany przedrostek %2, który istnieje w takich przypadkach.</translation>
    </message>
    <message>
        <source>The namespace of a user defined function in a library module must be equivalent to the module namespace. In other words, it should be %1 instead of %2</source>
        <translation type="vanished">Przestrzeń nazw dla funkcji zdefiniowanej przez użytkownika w module bibliotecznym musi odpowiadać przestrzeni nazw modułu. Powinna to być %1 zamiast %2</translation>
    </message>
    <message>
        <source>A function already exists with the signature %1.</source>
        <translation type="vanished">Funkcja w postaci %1 już istnieje.</translation>
    </message>
    <message>
        <source>No external functions are supported. All supported functions can be used directly, without first declaring them as external</source>
        <translation type="vanished">Zewnętrzne funkcje nie są obsługiwane. Wszystkie obsługiwane funkcje mogą być używane bezpośrednio, bez ich uprzedniego deklarowania jako zewnętrzne</translation>
    </message>
    <message>
        <source>The name of a variable bound in a for-expression must be different from the positional variable. Hence, the two variables named %1 collide.</source>
        <translation type="vanished">Nazwa zmiennej powiązanej w wyrażeniu &quot;for&quot; musi być inna od zmiennej pozycjonującej. W związku z tym dwie zmienne o nazwie %1 kolidują ze sobą.</translation>
    </message>
    <message>
        <source>The Schema Validation Feature is not supported. Hence, %1-expressions may not be used.</source>
        <translation type="vanished">Cecha &quot;Walidacja schematu&quot; nie jest obsługiwana. Dlatego też wyrażenia %1 nie mogą być użyte.</translation>
    </message>
    <message>
        <source>None of the pragma expressions are supported. Therefore, a fallback expression must be present</source>
        <translation type="vanished">Wyrażenia &quot;pragma&quot; nie są obsługiwane. Dlatego musi wystąpić wyrażenie zastępcze</translation>
    </message>
    <message>
        <source>The %1-axis is unsupported in XQuery</source>
        <translation type="vanished">Oś %1 nie jest obsługiwana w XQuery</translation>
    </message>
    <message>
        <source>%1 is not a valid numeric literal.</source>
        <translation type="vanished">%1 nie jest poprawnym literałem liczbowym.</translation>
    </message>
    <message>
        <source>W3C XML Schema identity constraint selector</source>
        <translation type="vanished">Selektor narzucenia niepowtarzalności W3C XML Schema</translation>
    </message>
    <message>
        <source>W3C XML Schema identity constraint field</source>
        <translation type="vanished">Pole narzucenia niepowtarzalności W3C XML Schema</translation>
    </message>
    <message>
        <source>A construct was encountered which is disallowed in the current language(%1).</source>
        <translation type="vanished">Wystąpiła konstrukcja która jest niedozwolona w bieżącym języku (%1).</translation>
    </message>
    <message>
        <source>The keyword %1 cannot occur with any other mode name.</source>
        <translation type="vanished">Słowo kluczowe %1 nie może wystąpić z inną nazwą trybu.</translation>
    </message>
    <message>
        <source>No variable with name %1 exists</source>
        <translation type="vanished">Zmienna o nazwie %1 nie istnieje</translation>
    </message>
    <message>
        <source>The value of attribute %1 must be of type %2, which %3 isn&apos;t.</source>
        <translation type="vanished">Wartość atrybutu %1 musi być typu %2, którym nie jest %3.</translation>
    </message>
    <message>
        <source>The prefix %1 cannot be bound. By default, it is already bound to the namespace %2.</source>
        <translation type="vanished">Przedrostek %1 nie może być powiązany. Jest on domyślnie powiązany z przestrzenią nazw %2.</translation>
    </message>
    <message>
        <source>A variable with name %1 has already been declared.</source>
        <translation type="vanished">Zmienna o nazwie %1 została już zadeklarowana.</translation>
    </message>
    <message>
        <source>No value is available for the external variable with name %1.</source>
        <translation type="vanished">Brak wartości dla zewnętrznej zmiennej o nazwie %1.</translation>
    </message>
    <message>
        <source>A stylesheet function must have a prefixed name.</source>
        <translation type="vanished">Funkcja arkusza stylu musi zawierać nazwę z przedrostkiem.</translation>
    </message>
    <message>
        <source>An argument with name %1 has already been declared. Every argument name must be unique.</source>
        <translation type="vanished">Argument o nazwie %1 został już zadeklarowany. Każda nazwa argumentu musi być unikatowa.</translation>
    </message>
    <message>
        <source>When function %1 is used for matching inside a pattern, the argument must be a variable reference or a string literal.</source>
        <translation type="vanished">Gdy funkcja %1 jest wykorzystana do dopasowania wewnątrz wzorca, jej argument musi być referencją do zmiennej lub literałem znakowym.</translation>
    </message>
    <message>
        <source>In an XSL-T pattern, the first argument to function %1 must be a string literal, when used for matching.</source>
        <translation type="vanished">We wzorze XSL-T pierwszy argument w funkcji %1 musi być literałem znakowym podczas dopasowywania.</translation>
    </message>
    <message>
        <source>In an XSL-T pattern, the first argument to function %1 must be a literal or a variable reference, when used for matching.</source>
        <translation type="vanished">We wzorze XSL-T pierwszy argument w funkcji %1 musi być literałem znakowym lub nazwą zmiennej podczas dopasowywania.</translation>
    </message>
    <message>
        <source>In an XSL-T pattern, function %1 cannot have a third argument.</source>
        <translation type="vanished">We wzorze XSL-T funkcja %1 nie może zawierać trzeciego argumentu.</translation>
    </message>
    <message>
        <source>In an XSL-T pattern, only function %1 and %2, not %3, can be used for matching.</source>
        <translation type="vanished">We wzorze XSL-T tylko funkcje %1 i %2 mogą być użyte do dopasowania, zaś funkcja %3 nie.</translation>
    </message>
    <message>
        <source>In an XSL-T pattern, axis %1 cannot be used, only axis %2 or %3 can.</source>
        <translation type="vanished">We wzorze XSL-T tylko osie %2 i %3 mogą być użyte, zaś oś %1 nie.</translation>
    </message>
    <message>
        <source>%1 is an invalid template mode name.</source>
        <translation type="vanished">%1 nie jest poprawną nazwa trybu szablonu.</translation>
    </message>
    <message>
        <source>Each name of a template parameter must be unique; %1 is duplicated.</source>
        <translation type="vanished">Każda nazwa parametru szablonu musi być unikatowa; %1 się powtarza.</translation>
    </message>
    <message>
        <source>No function with name %1 is available.</source>
        <translation type="vanished">Żadna funkcja o nazwie %1 nie jest dostępna.</translation>
    </message>
    <message>
        <source>An attribute with name %1 has already appeared on this element.</source>
        <translation type="vanished">Atrybut o nazwie %1 już się pojawił w tym elemencie.</translation>
    </message>
    <message>
        <source>The namespace URI cannot be the empty string when binding to a prefix, %1.</source>
        <translation type="vanished">Przestrzeń nazw URI nie może być pustym ciągiem w powiązaniu z przedrostkiem, %1.</translation>
    </message>
    <message>
        <source>%1 is an invalid namespace URI.</source>
        <translation type="vanished">%1 jest niepoprawną przestrzenią nazw URI.</translation>
    </message>
    <message>
        <source>It is not possible to bind to the prefix %1</source>
        <translation type="vanished">Nie jest możliwe powiązanie z przedrostkiem %1</translation>
    </message>
    <message>
        <source>Namespace %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation type="vanished">Przestrzeń nazw %1 może być jedynie powiązana z %2 (w przeciwnym wypadku jest ona domyślnie zadeklarowana).</translation>
    </message>
    <message>
        <source>Prefix %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation type="vanished">Przedrostek %1 może być jedynie powiązany z %2 (w przeciwnym wypadku jest on domyślnie zadeklarowany).</translation>
    </message>
    <message>
        <source>Two namespace declaration attributes have the same name: %1.</source>
        <translation type="vanished">Atrybuty deklaracji przestrzeni nazw mają tą samą nazwę: %1.</translation>
    </message>
    <message>
        <source>The namespace URI must be a constant and cannot use enclosed expressions.</source>
        <translation type="vanished">Przestrzeń nazw URI nie może być stałą i nie może używać zawartych w niej wyrażeń.</translation>
    </message>
    <message>
        <source>A direct element constructor is not well-formed. %1 is ended with %2.</source>
        <translation type="vanished">Konstruktor elementu bezpośredniego nie jest dobrze sformatowany. %1 jest zakończony %2.</translation>
    </message>
    <message>
        <source>The name %1 does not refer to any schema type.</source>
        <translation type="vanished">Nazwa %1 nie odpowiada żadnemu typowi schematu.</translation>
    </message>
    <message>
        <source>%1 is an complex type. Casting to complex types is not possible. However, casting to atomic types such as %2 works.</source>
        <translation type="vanished">%1 jest typem złożonym. Rzutowanie na typy złożone nie jest możliwe. Jednakże rzutowanie na typy atomowe np.: %2 jest dozwolone.</translation>
    </message>
    <message>
        <source>%1 is not an atomic type. Casting is only possible to atomic types.</source>
        <translation type="vanished">%1 nie jest typem atomowym. Możliwe jest rzutowanie tylko na typy atomowe.</translation>
    </message>
    <message>
        <source>%1 is not a valid name for a processing-instruction.</source>
        <translation type="vanished">%1 nie jest poprawną nazwą dla instrukcji przetwarzającej.</translation>
    </message>
    <message>
        <source>%1 is not in the in-scope attribute declarations. Note that the schema import feature is not supported.</source>
        <translation type="vanished">%1 nie jest wewnątrz zakresu deklaracji atrybutów. Zwróć uwagę że importowanie schematów nie jest obsługiwane.</translation>
    </message>
    <message>
        <source>The name of an extension expression must be in a namespace.</source>
        <translation type="vanished">Nazwa dodatkowego wyrażenia musi znajdować sie w przestrzeni nazw.</translation>
    </message>
    <message>
        <source>empty</source>
        <translation type="vanished">pusty</translation>
    </message>
    <message>
        <source>zero or one</source>
        <translation type="vanished">zero lub jeden</translation>
    </message>
    <message>
        <source>exactly one</source>
        <translation type="vanished">dokładnie jeden</translation>
    </message>
    <message>
        <source>one or more</source>
        <translation type="vanished">jeden lub więcej</translation>
    </message>
    <message>
        <source>zero or more</source>
        <translation type="vanished">zero lub więcej</translation>
    </message>
    <message>
        <source>Required type is %1, but %2 was found.</source>
        <translation type="vanished">Odnaleziono typ %2, lecz wymaganym typem jest %1.</translation>
    </message>
    <message>
        <source>Promoting %1 to %2 may cause loss of precision.</source>
        <translation type="vanished">Przekształcenie %1 do %2 może spowodować utratę precyzji.</translation>
    </message>
    <message>
        <source>The focus is undefined.</source>
        <translation type="vanished">Fokus jest niezdefiniowany.</translation>
    </message>
    <message>
        <source>It&apos;s not possible to add attributes after any other kind of node.</source>
        <translation type="vanished">Dodanie atrybutu poza węzłami nie jest możliwe.</translation>
    </message>
    <message>
        <source>An attribute by name %1 has already been created.</source>
        <translation type="vanished">Atrybut o nazwie %1 został już utworzony.</translation>
    </message>
    <message>
        <source>Only the Unicode Codepoint Collation is supported(%1). %2 is unsupported.</source>
        <translation type="vanished">Obsługiwane jest jedynie &quot;Unicode Codepoint Collation&quot; (%1), %2 nie jest obsługiwane.</translation>
    </message>
    <message>
        <source>Integer division (%1) by zero (%2) is undefined.</source>
        <translation type="vanished">Dzielenie w dziedzinie liczb całkowitych (%1) przez zero (%2) jest niezdefiniowane.</translation>
    </message>
    <message>
        <source>Division (%1) by zero (%2) is undefined.</source>
        <translation type="vanished">Dzielenie (%1) przez zero (%2) jest niezdefiniowane.</translation>
    </message>
    <message>
        <source>Modulus division (%1) by zero (%2) is undefined.</source>
        <translation type="vanished">Dzielenie modulo (%1) przez zero (%2) jest niezdefiniowane.</translation>
    </message>
    <message numerus="yes">
        <source>%1 takes at most %n argument(s). %2 is therefore invalid.</source>
        <translation type="vanished">
            <numerusform>%1 przyjmuje co najwyżej %n argument. %2 jest dlatego niepoprawne.</numerusform>
            <numerusform>%1 przyjmuje co najwyżej %n argumenty. %2 jest dlatego niepoprawne.</numerusform>
            <numerusform>%1 przyjmuje co najwyżej %n argumentów. %2 jest dlatego niepoprawne.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%1 requires at least %n argument(s). %2 is therefore invalid.</source>
        <translation type="vanished">
            <numerusform>%1 wymaga przynajmniej %n argumentu. %2 jest dlatego niepoprawne.</numerusform>
            <numerusform>%1 wymaga przynajmniej %n argumentów. %2 jest dlatego niepoprawne.</numerusform>
            <numerusform>%1 wymaga przynajmniej %n argumentów. %2 jest dlatego niepoprawne.</numerusform>
        </translation>
    </message>
    <message>
        <source>The root node of the second argument to function %1 must be a document node. %2 is not a document node.</source>
        <translation type="vanished">Głównym węzłem drugiego argumentu w funkcji %1 musi być węzeł &quot;document&quot;. %2 nie jest węzłem &quot;document&quot;.</translation>
    </message>
    <message>
        <source>%1 is not a whole number of minutes.</source>
        <translation type="vanished">%1 nie jest całkowitą liczbą minut.</translation>
    </message>
    <message>
        <source>The encoding %1 is invalid. It must contain Latin characters only, must not contain whitespace, and must match the regular expression %2.</source>
        <translation type="vanished">Enkodowanie %1 jest niepoprawne. Może ono zawierać jedynie znaki alfabetu łacińskiego, nie może zawierać spacji i musi być dopasowane do wyrażenia regularnego %2.</translation>
    </message>
    <message>
        <source>A default namespace declaration must occur before function, variable, and option declarations.</source>
        <translation type="vanished">Domyślna deklaracja przestrzeni nazw musi pojawić się przed deklaracjami funkcji, zmiennych i opcji.</translation>
    </message>
    <message>
        <source>Namespace declarations must occur before function, variable, and option declarations.</source>
        <translation type="vanished">Deklaracje przestrzeni nazw muszą pojawić się przed deklaracjami funkcji, zmiennych i opcji.</translation>
    </message>
    <message>
        <source>Module imports must occur before function, variable, and option declarations.</source>
        <translation type="vanished">Importy modułów muszą pojawić się przed deklaracjami funkcji, zmiennych i opcji.</translation>
    </message>
    <message>
        <source>%1 is an unsupported encoding.</source>
        <translation type="vanished">Nieobsługiwane kodowanie %1.</translation>
    </message>
    <message>
        <source>%1 contains octets which are disallowed in the requested encoding %2.</source>
        <translation type="vanished">%1 zawiera bity które są niedozwolone w zażądanym kodowaniu %2.</translation>
    </message>
    <message>
        <source>The codepoint %1, occurring in %2 using encoding %3, is an invalid XML character.</source>
        <translation type="vanished">Kod %1 który pojawił się w %2 i który używa kodowania %3 jest niepoprawnym znakiem XML.</translation>
    </message>
    <message>
        <source>Ambiguous rule match.</source>
        <translation type="vanished">Dopasowano niejednoznaczną regułę.</translation>
    </message>
    <message>
        <source>In a namespace constructor, the value for a namespace cannot be an empty string.</source>
        <translation type="vanished">W konstruktorze przestrzeni nazw wartość przestrzeni nazw nie może być pustym ciągiem.</translation>
    </message>
    <message>
        <source>The prefix must be a valid %1, which %2 is not.</source>
        <translation type="vanished">Przedrostek musi być poprawnym %1, którym %2 nie jest.</translation>
    </message>
    <message>
        <source>The prefix %1 cannot be bound.</source>
        <translation type="vanished">Przedrostek %1 nie może być powiązany.</translation>
    </message>
    <message>
        <source>Only the prefix %1 can be bound to %2 and vice versa.</source>
        <translation type="vanished">Tylko przedrostek %1 może być powiązany z %2 i vice versa.</translation>
    </message>
    <message>
        <source>The parameter %1 is required, but no corresponding %2 is supplied.</source>
        <translation type="vanished">Wymagany jest parametr %1 lecz żaden odpowiadający mu  %2 nie został dostarczony.</translation>
    </message>
    <message>
        <source>The parameter %1 is passed, but no corresponding %2 exists.</source>
        <translation type="vanished">Przekazany jest parametr %1 lecz żaden odpowiadający mu %2 nie istnieje.</translation>
    </message>
    <message>
        <source>The URI cannot have a fragment</source>
        <translation type="vanished">URI nie może posiadać fragmentu</translation>
    </message>
    <message>
        <source>Element %1 is not allowed at this location.</source>
        <translation type="vanished">Element %1 jest niedozwolony w tym miejscu.</translation>
    </message>
    <message>
        <source>Text nodes are not allowed at this location.</source>
        <translation type="vanished">Węzły tekstowe są niedozwolone w tym miejscu.</translation>
    </message>
    <message>
        <source>Parse error: %1</source>
        <translation type="vanished">Błąd parsowania: %1</translation>
    </message>
    <message>
        <source>The value of the XSL-T version attribute must be a value of type %1, which %2 isn&apos;t.</source>
        <translation type="vanished">Wartość atrybutu wersji XSL-T musi być typu %1, którym %2 nie jest.</translation>
    </message>
    <message>
        <source>Running an XSL-T 1.0 stylesheet with a 2.0 processor.</source>
        <translation type="vanished">Przetwarzanie arkusza XSL-T w wersji 1.0 przez procesor w wersji 2.0.</translation>
    </message>
    <message>
        <source>Unknown XSL-T attribute %1.</source>
        <translation type="vanished">Nieznany atrybut %1 XSL-T.</translation>
    </message>
    <message>
        <source>Attribute %1 and %2 are mutually exclusive.</source>
        <translation type="vanished">Atrybuty %1 i %2 wzajemnie się wykluczającą.</translation>
    </message>
    <message>
        <source>In a simplified stylesheet module, attribute %1 must be present.</source>
        <translation type="vanished">W uproszczonym module arkuszu stylu musi wystąpić atrybut %1.</translation>
    </message>
    <message>
        <source>If element %1 has no attribute %2, it cannot have attribute %3 or %4.</source>
        <translation type="vanished">Jeśli element %1 nie posiada atrybutu %2, nie może on również posiadać atrybutu %3 ani %4.</translation>
    </message>
    <message>
        <source>Element %1 must have at least one of the attributes %2 or %3.</source>
        <translation type="vanished">Element %1 musi posiadać przynajmniej jeden z atrybutów: %2 lub %3.</translation>
    </message>
    <message>
        <source>At least one mode must be specified in the %1-attribute on element %2.</source>
        <translation type="vanished">Przynajmniej jeden tryb musi być podany w atrybucie %1 elementu %2.</translation>
    </message>
    <message>
        <source>Element %1 must come last.</source>
        <translation type="vanished">Element %1 musi wystąpić jako ostatni.</translation>
    </message>
    <message>
        <source>At least one %1-element must occur before %2.</source>
        <translation type="vanished">Przynajmniej jeden element %1 musi wystąpić przed %2.</translation>
    </message>
    <message>
        <source>Only one %1-element can appear.</source>
        <translation type="vanished">Może wystąpić tylko jeden element %1.</translation>
    </message>
    <message>
        <source>At least one %1-element must occur inside %2.</source>
        <translation type="vanished">Przynajmniej jeden element %1 musi wystąpić wewnątrz %2.</translation>
    </message>
    <message>
        <source>When attribute %1 is present on %2, a sequence constructor cannot be used.</source>
        <translation type="vanished">Kiedy atrybut %1 występuje w %2 konstruktor sekwencyjny nie może być użyty.</translation>
    </message>
    <message>
        <source>Element %1 must have either a %2-attribute or a sequence constructor.</source>
        <translation type="vanished">Element %1 musi posiadać albo atrybut %2 albo sekwencyjny konstruktor.</translation>
    </message>
    <message>
        <source>When a parameter is required, a default value cannot be supplied through a %1-attribute or a sequence constructor.</source>
        <translation type="vanished">Kiedy wymagany jest parametr, domyślna wartość nie może być dostarczona przez atrybut %1 ani przez sekwencyjny konstruktor.</translation>
    </message>
    <message>
        <source>Element %1 cannot have children.</source>
        <translation type="vanished">Element %1 nie może posiadać potomków.</translation>
    </message>
    <message>
        <source>Element %1 cannot have a sequence constructor.</source>
        <translation type="vanished">Element %1 nie może posiadać sekwencyjnego konstruktora.</translation>
    </message>
    <message>
        <source>The attribute %1 cannot appear on %2, when it is a child of %3.</source>
        <translation type="vanished">Atrybut %1 nie może wystąpić w %2 kiedy jest on potomkiem %3.</translation>
    </message>
    <message>
        <source>A parameter in a function cannot be declared to be a tunnel.</source>
        <translation type="vanished">Parametr funkcji nie może być zadeklarowany jako tunelowy.</translation>
    </message>
    <message>
        <source>This processor is not Schema-aware and therefore %1 cannot be used.</source>
        <translation type="vanished">Procesor nie obsługuje schematów, więc %1 nie może zostać użyte.</translation>
    </message>
    <message>
        <source>Top level stylesheet elements must be in a non-null namespace, which %1 isn&apos;t.</source>
        <translation type="vanished">Elementy arkusza stylu najwyższego poziomu muszą być w niezerowej przestrzeni nazw, którą %1 nie jest.</translation>
    </message>
    <message>
        <source>The value for attribute %1 on element %2 must either be %3 or %4, not %5.</source>
        <translation type="vanished">Wartością atrybutu %1 w elemencie %2 musi być %3 albo %4, lecz nie %5.</translation>
    </message>
    <message>
        <source>Attribute %1 cannot have the value %2.</source>
        <translation type="vanished">Atrybut %1 nie może posiadać wartości %2.</translation>
    </message>
    <message>
        <source>The attribute %1 can only appear on the first %2 element.</source>
        <translation type="vanished">Atrybut %1 może wystąpić jedynie w pierwszym elemencie %2.</translation>
    </message>
    <message>
        <source>At least one %1 element must appear as child of %2.</source>
        <translation type="vanished">Przynajmniej jeden element %1 musi wystąpić jako potomek %2.</translation>
    </message>
    <message>
        <source>%1 has inheritance loop in its base type %2.</source>
        <translation type="vanished">%1 ma pętlę w dziedziczeniu w jego podstawowym typie %2.</translation>
    </message>
    <message>
        <source>Circular inheritance of base type %1.</source>
        <translation type="vanished">Cykliczne dziedziczenie podstawowego typu %1.</translation>
    </message>
    <message>
        <source>Circular inheritance of union %1.</source>
        <translation type="vanished">Cykliczne dziedziczenie unii %1.</translation>
    </message>
    <message>
        <source>%1 is not allowed to derive from %2 by restriction as the latter defines it as final.</source>
        <translation type="vanished">Nie można wywieść %1 z %2 ograniczając go ponieważ jest on zdefiniowany jako końcowy.</translation>
    </message>
    <message>
        <source>%1 is not allowed to derive from %2 by extension as the latter defines it as final.</source>
        <translation type="vanished">Nie można wywieść %1 z %2 rozszerzając go ponieważ jest on zdefiniowany jako końcowy.</translation>
    </message>
    <message>
        <source>Base type of simple type %1 cannot be complex type %2.</source>
        <translation type="vanished">Typ podstawowy dla typu prostego %1 nie może być typem złożonym %2.</translation>
    </message>
    <message>
        <source>Simple type %1 cannot have direct base type %2.</source>
        <translation type="vanished">Typ prosty %1 nie może mieć bezpośredniego typu podstawowego %2.</translation>
    </message>
    <message>
        <source>Simple type %1 is not allowed to have base type %2.</source>
        <translation type="vanished">Typ prosty %1 nie może mieć typu podstawowego %2.</translation>
    </message>
    <message>
        <source>Simple type %1 can only have simple atomic type as base type.</source>
        <translation type="vanished">Typem podstawowym typu prostego %1 może być tylko typ atomowy.</translation>
    </message>
    <message>
        <source>Simple type %1 cannot derive from %2 as the latter defines restriction as final.</source>
        <translation type="vanished">Typ prosty %1 nie może wywodzić się z %2 ponieważ ten ostatni jest zdefiniowany jako końcowy.</translation>
    </message>
    <message>
        <source>Variety of item type of %1 must be either atomic or union.</source>
        <translation type="vanished">Typem elementów listy %1 musi być albo typ atomowy albo unia.</translation>
    </message>
    <message>
        <source>Variety of member types of %1 must be atomic.</source>
        <translation type="vanished">Typy składników %1 muszą być atomowe.</translation>
    </message>
    <message>
        <source>%1 is not allowed to derive from %2 by list as the latter defines it as final.</source>
        <translation type="vanished">Nie można wywieść %1 z %2 poprzez listę ponieważ jest to zdefiniowane ostatecznie w typie podstawowym.</translation>
    </message>
    <message>
        <source>Simple type %1 is only allowed to have %2 facet.</source>
        <translation type="vanished">Typ prosty %1 może jedynie posiadać aspekt %2.</translation>
    </message>
    <message>
        <source>Base type of simple type %1 must have variety of type list.</source>
        <translation type="vanished">Typ podstawowy dla typu prostego %1 musi być listą typów.</translation>
    </message>
    <message>
        <source>Base type of simple type %1 has defined derivation by restriction as final.</source>
        <translation type="vanished">Typ podstawowy dla typu prostego %1 ma zdefiniowane wywodzenie poprzez ograniczenie jako końcowe.</translation>
    </message>
    <message>
        <source>Item type of base type does not match item type of %1.</source>
        <translation type="vanished">Typ elementów listy typu podstawowego nie pasuje do typu elementów listy %1.</translation>
    </message>
    <message>
        <source>Simple type %1 contains not allowed facet type %2.</source>
        <translation type="vanished">Typ prosty %1 posiada niedozwolony aspekt %2.</translation>
    </message>
    <message>
        <source>%1 is not allowed to derive from %2 by union as the latter defines it as final.</source>
        <translation type="vanished">Nie można wywieść %1 z %2 poprzez unię ponieważ jest to zdefiniowane ostatecznie w typie podstawowym.</translation>
    </message>
    <message>
        <source>%1 is not allowed to have any facets.</source>
        <translation type="vanished">%1 nie może posiadać żadnych aspektów.</translation>
    </message>
    <message>
        <source>Base type %1 of simple type %2 must have variety of union.</source>
        <translation type="vanished">Typ podstawowy %1 dla typu prostego %2 musi być unią.</translation>
    </message>
    <message>
        <source>Base type %1 of simple type %2 is not allowed to have restriction in %3 attribute.</source>
        <translation type="vanished">Typ podstawowy %1 dla typu prostego %2 nie może posiadać ograniczenia dla atrybutu %3.</translation>
    </message>
    <message>
        <source>Member type %1 cannot be derived from member type %2 of %3&apos;s base type %4.</source>
        <translation type="vanished">Typ %1 składnika nie może być wywiedziony z typu %2 który jest typem składnika %3 typu podstawowego %4.</translation>
    </message>
    <message>
        <source>Derivation method of %1 must be extension because the base type %2 is a simple type.</source>
        <translation type="vanished">Metodą wywodzenia z %1 musi być rozszerzenie ponieważ typ podstawowy %2 jest typem prostym.</translation>
    </message>
    <message>
        <source>Complex type %1 has duplicated element %2 in its content model.</source>
        <translation type="vanished">Typ złożony %1 posiada powielony element %2 w jego modelu zawartości.</translation>
    </message>
    <message>
        <source>Complex type %1 has non-deterministic content.</source>
        <translation type="vanished">Typ złożony %1 posiada nieokreśloną zawartość.</translation>
    </message>
    <message>
        <source>Attributes of complex type %1 are not a valid extension of the attributes of base type %2: %3.</source>
        <translation type="vanished">Atrybuty typu złożonego %1 nie są poprawnym rozszerzeniem atrybutów typu podstawowego %2: %3.</translation>
    </message>
    <message>
        <source>Content model of complex type %1 is not a valid extension of content model of %2.</source>
        <translation type="vanished">Model zawartości typu złożonego %1 nie jest poprawnym rozszerzeniem modelu zawartości %2.</translation>
    </message>
    <message>
        <source>Complex type %1 must have simple content.</source>
        <translation type="vanished">Typ złożony %1 musi mieć prostą zawartość.</translation>
    </message>
    <message>
        <source>Complex type %1 must have the same simple type as its base class %2.</source>
        <translation type="vanished">Typ złożony %1 musi posiadać ten sam prosty typ jaki posiada jego klasa podstawowa %2.</translation>
    </message>
    <message>
        <source>Complex type %1 cannot be derived from base type %2%3.</source>
        <translation type="vanished">Typ złożony %1 nie może być wywiedziony z typu %2%3.</translation>
    </message>
    <message>
        <source>Attributes of complex type %1 are not a valid restriction from the attributes of base type %2: %3.</source>
        <translation type="vanished">Atrybuty typu złożonego %1 nie są poprawnym ograniczeniem atrybutów typu podstawowego %2: %3.</translation>
    </message>
    <message>
        <source>Complex type %1 with simple content cannot be derived from complex base type %2.</source>
        <translation type="vanished">Typ złożony %1 z prostą zawartością nie może być wywiedziony z podstawowego typu złożonego %2.</translation>
    </message>
    <message>
        <source>Item type of simple type %1 cannot be a complex type.</source>
        <translation type="vanished">Typ elementów listy w prostym typie %1 nie może być typem złożonym.</translation>
    </message>
    <message>
        <source>Member type of simple type %1 cannot be a complex type.</source>
        <translation type="vanished">Typ składnika typu prostego %1 nie może być typem złożonym.</translation>
    </message>
    <message>
        <source>%1 is not allowed to have a member type with the same name as itself.</source>
        <translation type="vanished">%1 nie może posiadać typu składnika o tej samej nazwie jaką on sam posiada.</translation>
    </message>
    <message>
        <source>%1 facet collides with %2 facet.</source>
        <translation type="vanished">Aspekt %1 koliduje z aspektem %2.</translation>
    </message>
    <message>
        <source>%1 facet must have the same value as %2 facet of base type.</source>
        <translation type="vanished">Aspekt %1 musi mieć tą samą wartość jaką ma aspekt %2 typu podstawowego.</translation>
    </message>
    <message>
        <source>%1 facet must be equal or greater than %2 facet of base type.</source>
        <translation type="vanished">Wartość aspektu %1 musi większa od lub równa wartości aspektu %2 typu podstawowego.</translation>
    </message>
    <message>
        <source>%1 facet must be less than or equal to %2 facet of base type.</source>
        <translation type="vanished">Wartość aspektu %1 musi być mniejsza od lub równa wartości aspektu %2 typu podstawowego.</translation>
    </message>
    <message>
        <source>%1 facet contains invalid regular expression</source>
        <translation type="vanished">Aspekt %1 zawiera niepoprawne wyrażenie regularne</translation>
    </message>
    <message>
        <source>Unknown notation %1 used in %2 facet.</source>
        <translation type="vanished">Nieznany zapis %1 użyty w aspekcie %2.</translation>
    </message>
    <message>
        <source>%1 facet contains invalid value %2: %3.</source>
        <translation type="vanished">Aspekt %1 zawiera niepoprawną wartość %2: %3.</translation>
    </message>
    <message>
        <source>%1 facet cannot be %2 or %3 if %4 facet of base type is %5.</source>
        <translation type="vanished">Aspektem %1 nie może być %2 ani %3 jeśli aspektem %4 typu podstawowego jest %5.</translation>
    </message>
    <message>
        <source>%1 facet cannot be %2 if %3 facet of base type is %4.</source>
        <translation type="vanished">Aspektem %1 nie może być %2 jeśli aspektem %3 typu podstawowego jest %4.</translation>
    </message>
    <message>
        <source>%1 facet must be less than or equal to %2 facet.</source>
        <translation type="vanished">Wartość aspektu %1 musi być mniejsza od lub równa wartości aspektu %2.</translation>
    </message>
    <message>
        <source>%1 facet must be less than %2 facet of base type.</source>
        <translation type="vanished">Wartość aspektu %1 musi być mniejsza od wartości aspektu %2 typu podstawowego.</translation>
    </message>
    <message>
        <source>%1 facet and %2 facet cannot appear together.</source>
        <translation type="vanished">Aspekty %1 i %2 nie mogą wystąpić jednocześnie.</translation>
    </message>
    <message>
        <source>%1 facet must be greater than %2 facet of base type.</source>
        <translation type="vanished">Wartość aspektu %1 musi być większa od wartości aspektu %2 typu podstawowego.</translation>
    </message>
    <message>
        <source>%1 facet must be less than %2 facet.</source>
        <translation type="vanished">Wartość aspektu %1 musi być mniejsza od wartości aspektu %2.</translation>
    </message>
    <message>
        <source>%1 facet must be greater than or equal to %2 facet of base type.</source>
        <translation type="vanished">Wartość aspektu %1 musi być większa od lub równa wartości aspektu %2 typu podstawowego.</translation>
    </message>
    <message>
        <source>Simple type contains not allowed facet %1.</source>
        <translation type="vanished">Typ prosty zawiera niedozwolony aspekt %1.</translation>
    </message>
    <message>
        <source>%1, %2, %3, %4, %5 and %6 facets are not allowed when derived by list.</source>
        <translation type="vanished">Aspekty %1, %2, %3, %4, %5 i %6 nie są dozwolone podczas wywodzenia z listy.</translation>
    </message>
    <message>
        <source>Only %1 and %2 facets are allowed when derived by union.</source>
        <translation type="vanished">Dozwolone są jedynie aspekty %1 i %2 podczas wywodzenia z unii.</translation>
    </message>
    <message>
        <source>%1 contains %2 facet with invalid data: %3.</source>
        <translation type="vanished">%1 zawiera aspekt %2 z niepoprawnymi danymi: %3.</translation>
    </message>
    <message>
        <source>Attribute group %1 contains attribute %2 twice.</source>
        <translation type="vanished">Grupa atrybutów %1 zawiera dwukrotnie atrybut %2.</translation>
    </message>
    <message>
        <source>Attribute group %1 contains two different attributes that both have types derived from %2.</source>
        <translation type="vanished">Grupa atrybutów %1 zawiera dwa różne atrybuty których typy są wywiedzione z %2.</translation>
    </message>
    <message>
        <source>Attribute group %1 contains attribute %2 that has value constraint but type that inherits from %3.</source>
        <translation type="vanished">Grupa atrybutów %1 zawiera atrybut %2 który ma ograniczenie wartości ale typ wywodzi się z %3.</translation>
    </message>
    <message>
        <source>Complex type %1 contains attribute %2 twice.</source>
        <translation type="vanished">Typ złożony %1 zawiera atrybut %2 dwukrotnie.</translation>
    </message>
    <message>
        <source>Complex type %1 contains two different attributes that both have types derived from %2.</source>
        <translation type="vanished">Typ złożony %1 zawiera dwa różne atrybuty których typy są wywiedzione z %2.</translation>
    </message>
    <message>
        <source>Complex type %1 contains attribute %2 that has value constraint but type that inherits from %3.</source>
        <translation type="vanished">Typ złożony %1 zawiera atrybut %2 który ma ograniczenie wartości ale typ wywodzi się z %3.</translation>
    </message>
    <message>
        <source>Element %1 is not allowed to have a value constraint if its base type is complex.</source>
        <translation type="vanished">Element %1 nie może zawierać ograniczenia wartości gdy jego typ podstawowy jest złożony.</translation>
    </message>
    <message>
        <source>Element %1 is not allowed to have a value constraint if its type is derived from %2.</source>
        <translation type="vanished">Element %1 nie może zawierać ograniczenia wartości gdy jego typ jest wywiedziony z %2.</translation>
    </message>
    <message>
        <source>Value constraint of element %1 is not of elements type: %2.</source>
        <translation type="vanished">Ograniczenie wartości elementu %1 nie jest typu: %2.</translation>
    </message>
    <message>
        <source>Element %1 is not allowed to have substitution group affiliation as it is no global element.</source>
        <translation type="vanished">Element %1 nie może przynależeć do grupy zastępującej ponieważ nie jest on elementem globalnym.</translation>
    </message>
    <message>
        <source>Type of element %1 cannot be derived from type of substitution group affiliation.</source>
        <translation type="vanished">Typ elementu %1 nie może być wywiedziony z typu przynależnego do grupy zastępującej.</translation>
    </message>
    <message>
        <source>Value constraint of attribute %1 is not of attributes type: %2.</source>
        <translation type="vanished">Ograniczenie wartości atrybutu %1 nie jest typu: %2.</translation>
    </message>
    <message>
        <source>Attribute %1 has value constraint but has type derived from %2.</source>
        <translation type="vanished">Atrybut %1 posiada ograniczenie wartości lecz jego typ wywodzi się z %2.</translation>
    </message>
    <message>
        <source>%1 attribute in derived complex type must be %2 like in base type.</source>
        <translation type="vanished">Atrybut %1 w wywiedzionym typie złożonym musi być %2 jak w typie podstawowym.</translation>
    </message>
    <message>
        <source>Attribute %1 in derived complex type must have %2 value constraint like in base type.</source>
        <translation type="vanished">Atrybut %1 w wywiedzionym typie złożonym musi zawierać ograniczenie wartości %2 jak w typie podstawowym.</translation>
    </message>
    <message>
        <source>Attribute %1 in derived complex type must have the same %2 value constraint like in base type.</source>
        <translation type="vanished">Atrybut %1 w wywiedzionym typie złożonym musi zawierać te same ograniczenie wartości %2 jak w typie podstawowym.</translation>
    </message>
    <message>
        <source>Attribute %1 in derived complex type must have %2 value constraint.</source>
        <translation type="vanished">Atrybut %1 w wywiedzionym typie złożonym musi zawierać ograniczenie wartości %2.</translation>
    </message>
    <message>
        <source>processContent of base wildcard must be weaker than derived wildcard.</source>
        <translation type="vanished">&quot;processContent&quot; podstawowego dżokera musi być słabszy od wywiedzionego dżokera.</translation>
    </message>
    <message>
        <source>Element %1 exists twice with different types.</source>
        <translation type="vanished">Istnieją dwa elementy %1 o różnych typach.</translation>
    </message>
    <message>
        <source>Particle contains non-deterministic wildcards.</source>
        <translation type="vanished">Element zawiera nieokreślone dżokery.</translation>
    </message>
    <message>
        <source>Base attribute %1 is required but derived attribute is not.</source>
        <translation type="vanished">Wymagany jest bazowy atrybut %1, wywiedziony zaś nie.</translation>
    </message>
    <message>
        <source>Type of derived attribute %1 cannot be validly derived from type of base attribute.</source>
        <translation type="vanished">Typ wywiedzionego atrybutu %1 nie może być poprawnie wywiedziony z typu podstawowego atrybutu.</translation>
    </message>
    <message>
        <source>Value constraint of derived attribute %1 does not match value constraint of base attribute.</source>
        <translation type="vanished">Ograniczenie wartości wywiedzionego atrybutu %1 nie pasuje do ograniczenia wartości podstawowego atrybutu.</translation>
    </message>
    <message>
        <source>Derived attribute %1 does not exist in the base definition.</source>
        <translation type="vanished">Wywiedziony atrybut %1 nie istnieje w podstawowej definicji.</translation>
    </message>
    <message>
        <source>Derived attribute %1 does not match the wildcard in the base definition.</source>
        <translation type="vanished">Wywiedziony atrybut %1 nie pasuje do dżokera w podstawowej definicji.</translation>
    </message>
    <message>
        <source>Base attribute %1 is required but missing in derived definition.</source>
        <translation type="vanished">Brak wymaganego bazowego atrybutu %1 w wywiedzionej definicji.</translation>
    </message>
    <message>
        <source>Derived definition contains an %1 element that does not exists in the base definition</source>
        <translation type="vanished">Wywiedziona definicja zawiera element %1 który nie istnieje w definicji podstawowej</translation>
    </message>
    <message>
        <source>Derived wildcard is not a subset of the base wildcard.</source>
        <translation type="vanished">Wywiedziony dżoker nie jest podzbiorem podstawowego dżokera.</translation>
    </message>
    <message>
        <source>%1 of derived wildcard is not a valid restriction of %2 of base wildcard</source>
        <translation type="vanished">%1 wywiedzionego dżokera nie jest poprawnym ograniczeniem %2 podstawowego dżokera</translation>
    </message>
    <message>
        <source>Attribute %1 from base type is missing in derived type.</source>
        <translation type="vanished">Brak atrybutu %1 typu bazowego w wywiedzionej definicji.</translation>
    </message>
    <message>
        <source>Type of derived attribute %1 differs from type of base attribute.</source>
        <translation type="vanished">Typ wywiedzionego atrybutu %1 różni się od typu podstawowego atrybutu.</translation>
    </message>
    <message>
        <source>Base definition contains an %1 element that is missing in the derived definition</source>
        <translation type="vanished">Podstawowa definicja zawiera element %1 którego brakuje w wywiedzionej definicji</translation>
    </message>
    <message>
        <source>%1 references unknown %2 or %3 element %4.</source>
        <translation type="vanished">%1 odwołuje się do nieznanego elementu %2 lub %3: %4.</translation>
    </message>
    <message>
        <source>%1 references identity constraint %2 that is no %3 or %4 element.</source>
        <translation type="vanished">%1 odwołuje się do narzucenia niepowtarzalności %2 które nie jest elementem %3 ani %4.</translation>
    </message>
    <message>
        <source>%1 has a different number of fields from the identity constraint %2 that it references.</source>
        <translation type="vanished">%1 posiada inna liczbę pól od narzucenia niepowtarzalności %2 które się do niego odwołuje.</translation>
    </message>
    <message>
        <source>Base type %1 of %2 element cannot be resolved.</source>
        <translation type="vanished">Nie można rozwiązać typu podstawowego %1 elementu %2.</translation>
    </message>
    <message>
        <source>Item type %1 of %2 element cannot be resolved.</source>
        <translation type="vanished">Nie można rozwiązać typu elementów listy %1 w elemencie %2.</translation>
    </message>
    <message>
        <source>Member type %1 of %2 element cannot be resolved.</source>
        <translation type="vanished">Nie można rozwiązać typu %1 składnika elementu %2.</translation>
    </message>
    <message>
        <source>Type %1 of %2 element cannot be resolved.</source>
        <translation type="vanished">Nie można rozwiązać typu %1 elementu %2.</translation>
    </message>
    <message>
        <source>Base type %1 of complex type cannot be resolved.</source>
        <translation type="vanished">Nie można rozwiązać typu podstawowego %1 dla typu złożonego.</translation>
    </message>
    <message>
        <source>%1 cannot have complex base type that has a %2.</source>
        <translation type="vanished">%1 nie może mieć złożonego typu podstawowego który ma %2.</translation>
    </message>
    <message>
        <source>Content model of complex type %1 contains %2 element, so it cannot be derived by extension from a non-empty type.</source>
        <translation type="vanished">Model zawartości typu złożonego %1, posiada element %2, więc nie może być on wywiedziony poprzez rozszerzenie niepustego typu.</translation>
    </message>
    <message>
        <source>Complex type %1 cannot be derived by extension from %2 as the latter contains %3 element in its content model.</source>
        <translation type="vanished">Typ złożony %1 nie może być wywiedziony z %2 poprzez rozszerzenie ponieważ ten ostatni zawiera element %3 w jego modelu zawartości.</translation>
    </message>
    <message>
        <source>Type of %1 element must be a simple type, %2 is not.</source>
        <translation type="vanished">Typem elementu %1 musi być typ prosty, %2 nim nie jest.</translation>
    </message>
    <message>
        <source>Substitution group %1 of %2 element cannot be resolved.</source>
        <translation type="vanished">Nie można rozwiązać grupy zastępującej %1 elementu %2.</translation>
    </message>
    <message>
        <source>Substitution group %1 has circular definition.</source>
        <translation type="vanished">Grupa zastępująca %1 posiada cykliczną definicję.</translation>
    </message>
    <message>
        <source>Duplicated element names %1 in %2 element.</source>
        <translation type="vanished">Powielona nazwa elementu %1 w elemencie %2.</translation>
    </message>
    <message>
        <source>Reference %1 of %2 element cannot be resolved.</source>
        <translation type="vanished">Nie można rozwiązać odwołania %1 do elementu %2.</translation>
    </message>
    <message>
        <source>Circular group reference for %1.</source>
        <translation type="vanished">Cykliczne odwołanie do grupy dla %1.</translation>
    </message>
    <message>
        <source>%1 element is not allowed in this scope</source>
        <translation type="vanished">Element %1 nie jest dozwolony w tym zakresie</translation>
    </message>
    <message>
        <source>%1 element cannot have %2 attribute with value other than %3.</source>
        <translation type="vanished">Element %1 nie może mieć atrybutu %2 z wartością inną niż %3.</translation>
    </message>
    <message>
        <source>%1 element cannot have %2 attribute with value other than %3 or %4.</source>
        <translation type="vanished">Element %1 nie może mieć atrybutu %2 z wartością inną niż %3 lub %4.</translation>
    </message>
    <message>
        <source>%1 or %2 attribute of reference %3 does not match with the attribute declaration %4.</source>
        <translation type="vanished">Atrybut %1 lub %2 odwołania %3 nie pasuje do deklaracji atrybutu %4.</translation>
    </message>
    <message>
        <source>Attribute group %1 has circular reference.</source>
        <translation type="vanished">Grupa atrybutów %1 posiada cykliczne odwołanie.</translation>
    </message>
    <message>
        <source>%1 attribute in %2 must have %3 use like in base type %4.</source>
        <translation type="vanished">Atrybut %1 w %2 powinien używać %3 jak w typie podstawowym %4.</translation>
    </message>
    <message>
        <source>Attribute wildcard of %1 is not a valid restriction of attribute wildcard of base type %2.</source>
        <translation type="vanished">Atrybut dżokera %1 nie jest poprawnym ograniczeniem atrybutu dżokera typu podstawowego %2.</translation>
    </message>
    <message>
        <source>%1 has attribute wildcard but its base type %2 has not.</source>
        <translation type="vanished">%1 posiada atrybut dżokera lecz jego typ podstawowy %2 go nie posiada.</translation>
    </message>
    <message>
        <source>Union of attribute wildcard of type %1 and attribute wildcard of its base type %2 is not expressible.</source>
        <translation type="vanished">Nie można wyrazić unii atrybutu dżokera typu %1 i atrybutu dżokera jego typu podstawowego %2.</translation>
    </message>
    <message>
        <source>Enumeration facet contains invalid content: {%1} is not a value of type %2.</source>
        <translation type="vanished">Aspekt &quot;enumeration&quot; posiada niepoprawną zawartość: {%1} nie jest wartością typu %2.</translation>
    </message>
    <message>
        <source>Namespace prefix of qualified name %1 is not defined.</source>
        <translation type="vanished">Przedrostek przestrzeni nazw występujący w pełnej nazwie %1 nie jest zdefiniowany.</translation>
    </message>
    <message>
        <source>%1 element %2 is not a valid restriction of the %3 element it redefines: %4.</source>
        <translation type="vanished">Element %1 (%2) nie jest poprawnym ograniczeniem elementu %3 który redefiniuje: %4.</translation>
    </message>
    <message>
        <source>Empty particle cannot be derived from non-empty particle.</source>
        <translation type="vanished">Pusty element nie może być wywiedziony z niepustego elementu.</translation>
    </message>
    <message>
        <source>Derived particle is missing element %1.</source>
        <translation type="vanished">Brak elementu %1 w wywiedzionym elemencie.</translation>
    </message>
    <message>
        <source>Derived element %1 is missing value constraint as defined in base particle.</source>
        <translation type="vanished">Brak ograniczenia wartości w wywiedzionym elemencie %1 takiego jak w podstawowym elemencie.</translation>
    </message>
    <message>
        <source>Derived element %1 has weaker value constraint than base particle.</source>
        <translation type="vanished">Wywiedziony element %1 posiada słabsze ograniczenie wartości niż element podstawowy.</translation>
    </message>
    <message>
        <source>Fixed value constraint of element %1 differs from value constraint in base particle.</source>
        <translation type="vanished">Ograniczenie stałej wartości elementu %1 różni się od ograniczenia wartości w podstawowym elemencie.</translation>
    </message>
    <message>
        <source>Derived element %1 cannot be nillable as base element is not nillable.</source>
        <translation type="vanished">Wywiedziony element %1 może być zerowalny ponieważ element podstawowy nie jest zerowalny.</translation>
    </message>
    <message>
        <source>Block constraints of derived element %1 must not be more weaker than in the base element.</source>
        <translation type="vanished">Ograniczenia blokujące dla wywiedzionego elementu %1 nie mogą być słabsze od ograniczeń w elemencie podstawowym.</translation>
    </message>
    <message>
        <source>Simple type of derived element %1 cannot be validly derived from base element.</source>
        <translation type="vanished">Typ prosty w elemencie wywiedzionym %1 nie może być poprawnie wywiedziony z elementu podstawowego.</translation>
    </message>
    <message>
        <source>Complex type of derived element %1 cannot be validly derived from base element.</source>
        <translation type="vanished">Typ złożony w elemencie wywiedzionym %1 nie może być poprawnie wywiedziony z elementu podstawowego.</translation>
    </message>
    <message>
        <source>Element %1 is missing in derived particle.</source>
        <translation type="vanished">Brak elementu %1 w wywiedzionym elemencie.</translation>
    </message>
    <message>
        <source>Element %1 does not match namespace constraint of wildcard in base particle.</source>
        <translation type="vanished">Element %1 nie pasuje do ograniczenia przestrzeni nazw dżokera w elemencie podstawowym.</translation>
    </message>
    <message>
        <source>Wildcard in derived particle is not a valid subset of wildcard in base particle.</source>
        <translation type="vanished">Dżoker w wywiedzionym elemencie nie jest poprawnym podzbiorem dżokera w elemencie podstawowym.</translation>
    </message>
    <message>
        <source>processContent of wildcard in derived particle is weaker than wildcard in base particle.</source>
        <translation type="vanished">&quot;processContent&quot; dżokera w wywiedzionym elemencie jest słabszy od dżokera w podstawowym elemencie.</translation>
    </message>
    <message>
        <source>Derived particle allows content that is not allowed in the base particle.</source>
        <translation type="vanished">Wywiedziony element pozwala na zawartość która jest niedozwolona w podstawowym elemencie.</translation>
    </message>
    <message>
        <source>Can not process unknown element %1, expected elements are: %2.</source>
        <translation type="vanished">Nie można przetworzyć nieznanego elementu %1, oczekiwanymi elementami są: %2.</translation>
    </message>
    <message>
        <source>Element %1 is not allowed in this scope, possible elements are: %2.</source>
        <translation type="vanished">Element %1 jest niedozwolony w tym zakresie, możliwymi elementami są: %2.</translation>
    </message>
    <message>
        <source>Child element is missing in that scope, possible child elements are: %1.</source>
        <translation type="vanished">Brak podelementu w tym zakresie, możliwymi podelementami są: %1.</translation>
    </message>
    <message>
        <source>Document is not a XML schema.</source>
        <translation type="vanished">Dokument nie jest schematem XML.</translation>
    </message>
    <message>
        <source>%1 attribute of %2 element contains invalid content: {%3} is not a value of type %4.</source>
        <translation type="vanished">Atrybut %1 elementu %2 posiada niepoprawną zawartość: {%3} nie jest wartością typu %4.</translation>
    </message>
    <message>
        <source>%1 attribute of %2 element contains invalid content: {%3}.</source>
        <translation type="vanished">Atrybut %1 elementu %2 posiada niepoprawną zawartość: {%3}.</translation>
    </message>
    <message>
        <source>Target namespace %1 of included schema is different from the target namespace %2 as defined by the including schema.</source>
        <translation type="vanished">Docelowa przestrzeń nazw %1 załączonego schematu jest różna od docelowej przestrzeni nazw %2 która jest zdefiniowana w schemacie załączającym.</translation>
    </message>
    <message>
        <source>Target namespace %1 of imported schema is different from the target namespace %2 as defined by the importing schema.</source>
        <translation type="vanished">Docelowa przestrzeń nazw %1 zaimportowanego schematu jest różna od docelowej przestrzeni nazw %2 która jest zdefiniowana w schemacie importującym.</translation>
    </message>
    <message>
        <source>%1 element is not allowed to have the same %2 attribute value as the target namespace %3.</source>
        <translation type="vanished">Element %1 nie może zawierać tej samej wartości atrybutu %2 co docelowa przestrzeń nazw %3.</translation>
    </message>
    <message>
        <source>%1 element without %2 attribute is not allowed inside schema without target namespace.</source>
        <translation type="vanished">Element %1 bez atrybutu %2 jest niedozwolony wewnątrz schematu bez docelowej przestrzeni nazw.</translation>
    </message>
    <message>
        <source>%1 element is not allowed inside %2 element if %3 attribute is present.</source>
        <translation type="vanished">Element %1 jest niedozwolony wewnątrz elementu %2 jeśli jest obecny atrybut %3.</translation>
    </message>
    <message>
        <source>%1 element has neither %2 attribute nor %3 child element.</source>
        <translation type="vanished">Element %1 nie posiada ani atrybutu %2 ani podelementu %3.</translation>
    </message>
    <message>
        <source>%1 element with %2 child element must not have a %3 attribute.</source>
        <translation type="vanished">Element %1 z podelementem %2 nie może mieć atrybutu %3.</translation>
    </message>
    <message>
        <source>%1 attribute of %2 element must be %3 or %4.</source>
        <translation type="vanished">Atrybutem %1 elementu %2 musi być %3 lub %4.</translation>
    </message>
    <message>
        <source>%1 attribute of %2 element must have a value of %3.</source>
        <translation type="vanished">Atrybut %1 elementu %2 musi posiadać wartość %3.</translation>
    </message>
    <message>
        <source>%1 attribute of %2 element must have a value of %3 or %4.</source>
        <translation type="vanished">Atrybut %1 elementu %2 musi posiadać wartość %3 lub %4.</translation>
    </message>
    <message>
        <source>%1 element must not have %2 and %3 attribute together.</source>
        <translation type="vanished">Element %1 nie może posiadać jednocześnie atrybutów %2 i %3.</translation>
    </message>
    <message>
        <source>Content of %1 attribute of %2 element must not be from namespace %3.</source>
        <translation type="vanished">Zawartość atrybutu %1 elementu %2 nie może pochodzić z przestrzeni nazw %3.</translation>
    </message>
    <message>
        <source>%1 attribute of %2 element must not be %3.</source>
        <translation type="vanished">Atrybut %1 elementu %2 nie może być %3.</translation>
    </message>
    <message>
        <source>%1 attribute of %2 element must have the value %3 because the %4 attribute is set.</source>
        <translation type="vanished">Atrybut %1 elementu %2 musi zawierać wartość %3 ponieważ atrybut %4 jest ustawiony.</translation>
    </message>
    <message>
        <source>Specifying use=&apos;prohibited&apos; inside an attribute group has no effect.</source>
        <translation type="vanished">Podawanie: use=&apos;prohibited&apos; wewnątrz grupy atrybutów nie przynosi żadnego efektu.</translation>
    </message>
    <message>
        <source>%1 element must have either %2 or %3 attribute.</source>
        <translation type="vanished">Element %1 musi zawierać atrybut %2 albo %3.</translation>
    </message>
    <message>
        <source>%1 element must have either %2 attribute or %3 or %4 as child element.</source>
        <translation type="vanished">Element %1 musi zawierać albo atrybut %2 albo %3 lub %4 jako podelement.</translation>
    </message>
    <message>
        <source>%1 element requires either %2 or %3 attribute.</source>
        <translation type="vanished">Element %1 wymaga atrybutu %2 albo %3.</translation>
    </message>
    <message>
        <source>Text or entity references not allowed inside %1 element</source>
        <translation type="vanished">Tekst ani odwołanie nie są dozwolone wewnątrz elementu %1</translation>
    </message>
    <message>
        <source>%1 attribute of %2 element must contain %3, %4 or a list of URIs.</source>
        <translation type="vanished">Atrybut %1 elementu %2 musi zawierać %3, %4 lub listę URI.</translation>
    </message>
    <message>
        <source>%1 element is not allowed in this context.</source>
        <translation type="vanished">Element %1 jest niedozwolony w tym kontekście.</translation>
    </message>
    <message>
        <source>%1 attribute of %2 element has larger value than %3 attribute.</source>
        <translation type="vanished">Atrybut %1 elementu %2 posiada większą wartość niż atrybut %3.</translation>
    </message>
    <message>
        <source>Prefix of qualified name %1 is not defined.</source>
        <translation type="vanished">Przedrostek w pełnej nazwie %1 nie jest zdefiniowany.</translation>
    </message>
    <message>
        <source>%1 attribute of %2 element must either contain %3 or the other values.</source>
        <translation type="vanished">Atrybut %1 elementu %2 musi zawierać albo %3 albo inne wartości.</translation>
    </message>
    <message>
        <source>Component with ID %1 has been defined previously.</source>
        <translation type="vanished">Komponent o identyfikatorze %1 został uprzednio zdefiniowany.</translation>
    </message>
    <message>
        <source>Element %1 already defined.</source>
        <translation type="vanished">Element %1 jest już zdefiniowany.</translation>
    </message>
    <message>
        <source>Attribute %1 already defined.</source>
        <translation type="vanished">Atrybut %1 jest już zdefiniowany.</translation>
    </message>
    <message>
        <source>Type %1 already defined.</source>
        <translation type="vanished">Typ %1 jest już zdefiniowany.</translation>
    </message>
    <message>
        <source>Attribute group %1 already defined.</source>
        <translation type="vanished">Grupa atrybutów %1 jest już zdefiniowana.</translation>
    </message>
    <message>
        <source>Element group %1 already defined.</source>
        <translation type="vanished">Grupa elementów %1 jest już zdefiniowana.</translation>
    </message>
    <message>
        <source>Notation %1 already defined.</source>
        <translation type="vanished">Zapis %1 jest już zdefiniowany.</translation>
    </message>
    <message>
        <source>Identity constraint %1 already defined.</source>
        <translation type="vanished">Narzucenie niepowtarzalności %1 jest już zdefiniowane.</translation>
    </message>
    <message>
        <source>Duplicated facets in simple type %1.</source>
        <translation type="vanished">Powielone aspekty w prostym typie %1.</translation>
    </message>
    <message>
        <source>%1 is not valid according to %2.</source>
        <translatorcomment>Ponieważ nie wiadomo co jest podmiotem nie można stwierdzić czy to ma być &quot;poprawnym&quot;, &quot;poprawną&quot; czy &quot;poprawne&quot;</translatorcomment>
        <translation type="vanished">%1 nie jest poprawne według %2.</translation>
    </message>
    <message>
        <source>String content does not match the length facet.</source>
        <translation type="vanished">Wartość ciągu koliduje z aspektem &quot;length&quot;.</translation>
    </message>
    <message>
        <source>String content does not match the minLength facet.</source>
        <translation type="vanished">Wartość ciągu koliduje z aspektem &quot;minLength&quot;.</translation>
    </message>
    <message>
        <source>String content does not match the maxLength facet.</source>
        <translation type="vanished">Wartość ciągu koliduje z aspektem &quot;maxLength&quot;.</translation>
    </message>
    <message>
        <source>String content does not match pattern facet.</source>
        <translation type="vanished">Wartość ciągu koliduje z aspektem &quot;pattern&quot;.</translation>
    </message>
    <message>
        <source>String content is not listed in the enumeration facet.</source>
        <translation type="vanished">Wartość ciągu nie widnieje na liście aspektu &quot;enumeration&quot;.</translation>
    </message>
    <message>
        <source>Signed integer content does not match the maxInclusive facet.</source>
        <translation type="vanished">Wartość liczby całkowitej koliduje z aspektem &quot;maxInclusive&quot;.</translation>
    </message>
    <message>
        <source>Signed integer content does not match the maxExclusive facet.</source>
        <translation type="vanished">Wartość liczby całkowitej koliduje z aspektem &quot;maxExclusive&quot;.</translation>
    </message>
    <message>
        <source>Signed integer content does not match the minInclusive facet.</source>
        <translation type="vanished">Wartość liczby całkowitej koliduje z aspektem &quot;minInclusive&quot;.</translation>
    </message>
    <message>
        <source>Signed integer content does not match the minExclusive facet.</source>
        <translation type="vanished">Wartość liczby całkowitej koliduje z aspektem &quot;minExclusive&quot;.</translation>
    </message>
    <message>
        <source>Signed integer content is not listed in the enumeration facet.</source>
        <translation type="vanished">Wartość liczby całkowitej nie widnieje na liście aspektu &quot;enumeration&quot;.</translation>
    </message>
    <message>
        <source>Signed integer content does not match pattern facet.</source>
        <translation type="vanished">Wartość liczby całkowitej koliduje z aspektem &quot;pattern&quot;.</translation>
    </message>
    <message>
        <source>Signed integer content does not match in the totalDigits facet.</source>
        <translation type="vanished">Wartość liczby całkowitej koliduje z aspektem &quot;totalDigits&quot;.</translation>
    </message>
    <message>
        <source>Unsigned integer content does not match the maxInclusive facet.</source>
        <translation type="vanished">Wartość liczby naturalnej koliduje z aspektem &quot;maxInclusive&quot;.</translation>
    </message>
    <message>
        <source>Unsigned integer content does not match the maxExclusive facet.</source>
        <translation type="vanished">Wartość liczby naturalnej koliduje z aspektem &quot;maxExclusive&quot;.</translation>
    </message>
    <message>
        <source>Unsigned integer content does not match the minInclusive facet.</source>
        <translation type="vanished">Wartość liczby naturalnej koliduje z aspektem &quot;minInclusive&quot;.</translation>
    </message>
    <message>
        <source>Unsigned integer content does not match the minExclusive facet.</source>
        <translation type="vanished">Wartość liczby naturalnej koliduje z aspektem &quot;minExclusive&quot;.</translation>
    </message>
    <message>
        <source>Unsigned integer content is not listed in the enumeration facet.</source>
        <translation type="vanished">Wartość liczby naturalnej nie widnieje na liście aspektu &quot;enumeration&quot;.</translation>
    </message>
    <message>
        <source>Unsigned integer content does not match pattern facet.</source>
        <translation type="vanished">Wartość liczby naturalnej koliduje z aspektem &quot;pattern&quot;.</translation>
    </message>
    <message>
        <source>Unsigned integer content does not match in the totalDigits facet.</source>
        <translation type="vanished">Wartość liczby naturalnej koliduje z aspektem &quot;totalDigits&quot;.</translation>
    </message>
    <message>
        <source>Double content does not match the maxInclusive facet.</source>
        <translation type="vanished">Wartość liczby rzeczywistej koliduje z aspektem &quot;maxInclusive&quot;.</translation>
    </message>
    <message>
        <source>Double content does not match the maxExclusive facet.</source>
        <translation type="vanished">Wartość liczby rzeczywistej koliduje z aspektem &quot;maxExclusive&quot;.</translation>
    </message>
    <message>
        <source>Double content does not match the minInclusive facet.</source>
        <translation type="vanished">Wartość liczby rzeczywistej koliduje z aspektem &quot;minInclusive&quot;.</translation>
    </message>
    <message>
        <source>Double content does not match the minExclusive facet.</source>
        <translation type="vanished">Wartość liczby rzeczywistej koliduje z aspektem &quot;minExclusive&quot;.</translation>
    </message>
    <message>
        <source>Double content is not listed in the enumeration facet.</source>
        <translation type="vanished">Wartość liczby rzeczywistej nie widnieje na liście aspektu &quot;enumeration&quot;.</translation>
    </message>
    <message>
        <source>Double content does not match pattern facet.</source>
        <translation type="vanished">Wartość liczby rzeczywistej koliduje z aspektem &quot;pattern&quot;.</translation>
    </message>
    <message>
        <source>Decimal content does not match in the fractionDigits facet.</source>
        <translation type="vanished">Wartość liczby rzeczywistej koliduje z aspektem &quot;fractionDigits&quot;.</translation>
    </message>
    <message>
        <source>Decimal content does not match in the totalDigits facet.</source>
        <translation type="vanished">Wartość liczby rzeczywistej koliduje z aspektem &quot;totalDigits&quot;.</translation>
    </message>
    <message>
        <source>Date time content does not match the maxInclusive facet.</source>
        <translation type="vanished">Zawartość daty i czasu koliduje z aspektem &quot;maxInclusive&quot;.</translation>
    </message>
    <message>
        <source>Date time content does not match the maxExclusive facet.</source>
        <translation type="vanished">Zawartość daty i czasu koliduje z aspektem &quot;maxExclusive&quot;.</translation>
    </message>
    <message>
        <source>Date time content does not match the minInclusive facet.</source>
        <translation type="vanished">Zawartość daty i czasu koliduje z aspektem &quot;minInclusive&quot;.</translation>
    </message>
    <message>
        <source>Date time content does not match the minExclusive facet.</source>
        <translation type="vanished">Zawartość daty i czasu koliduje z aspektem &quot;minExclusive&quot;.</translation>
    </message>
    <message>
        <source>Date time content is not listed in the enumeration facet.</source>
        <translation type="vanished">Zawartość daty i czasu nie widnieje na liście aspektu &quot;enumeration&quot;.</translation>
    </message>
    <message>
        <source>Date time content does not match pattern facet.</source>
        <translation type="vanished">Zawartość daty i czasu koliduje z aspektem &quot;pattern&quot;.</translation>
    </message>
    <message>
        <source>Duration content does not match the maxInclusive facet.</source>
        <translation type="vanished">Wartość czasu trwania koliduje z aspektem &quot;maxInclusive&quot;.</translation>
    </message>
    <message>
        <source>Duration content does not match the maxExclusive facet.</source>
        <translation type="vanished">Wartość czasu trwania koliduje z aspektem &quot;maxExclusive&quot;.</translation>
    </message>
    <message>
        <source>Duration content does not match the minInclusive facet.</source>
        <translation type="vanished">Wartość czasu trwania koliduje z aspektem &quot;minInclusive&quot;.</translation>
    </message>
    <message>
        <source>Duration content does not match the minExclusive facet.</source>
        <translation type="vanished">Wartość czasu trwania koliduje z aspektem &quot;minExclusive&quot;.</translation>
    </message>
    <message>
        <source>Duration content is not listed in the enumeration facet.</source>
        <translation type="vanished">Wartość czasu trwania nie widnieje na liście aspektu &quot;enumeration&quot;.</translation>
    </message>
    <message>
        <source>Duration content does not match pattern facet.</source>
        <translation type="vanished">Wartość czasu trwania koliduje z aspektem &quot;pattern&quot;.</translation>
    </message>
    <message>
        <source>Boolean content does not match pattern facet.</source>
        <translation type="vanished">Wartość boolowska koliduje z aspektem &quot;pattern&quot;.</translation>
    </message>
    <message>
        <source>Binary content does not match the length facet.</source>
        <translation type="vanished">Wartość binarna koliduje z aspektem &quot;length&quot;.</translation>
    </message>
    <message>
        <source>Binary content does not match the minLength facet.</source>
        <translation type="vanished">Wartość binarna koliduje z aspektem &quot;minLength&quot;.</translation>
    </message>
    <message>
        <source>Binary content does not match the maxLength facet.</source>
        <translation type="vanished">Wartość binarna koliduje z aspektem &quot;maxLength&quot;.</translation>
    </message>
    <message>
        <source>Binary content is not listed in the enumeration facet.</source>
        <translation type="vanished">Wartość binarna nie widnieje na liście aspektu &quot;enumeration&quot;.</translation>
    </message>
    <message>
        <source>Invalid QName content: %1.</source>
        <translation type="vanished">Niepoprawna zawartość QName: %1.</translation>
    </message>
    <message>
        <source>QName content is not listed in the enumeration facet.</source>
        <translation type="vanished">Zawartość QName nie widnieje na liście aspektu &quot;enumeration&quot;.</translation>
    </message>
    <message>
        <source>QName content does not match pattern facet.</source>
        <translation type="vanished">Zawartość QName koliduje z aspektem &quot;pattern&quot;.</translation>
    </message>
    <message>
        <source>Notation content is not listed in the enumeration facet.</source>
        <translation type="vanished">Zapis zawartości nie widnieje na liście aspektu &quot;enumeration&quot;.</translation>
    </message>
    <message>
        <source>List content does not match length facet.</source>
        <translation type="vanished">Zawartość listy koliduje z aspektem &quot;length&quot;.</translation>
    </message>
    <message>
        <source>List content does not match minLength facet.</source>
        <translation type="vanished">Zawartość listy koliduje z aspektem &quot;minLength&quot;.</translation>
    </message>
    <message>
        <source>List content does not match maxLength facet.</source>
        <translation type="vanished">Zawartość listy koliduje z aspektem &quot;maxLength&quot;.</translation>
    </message>
    <message>
        <source>List content is not listed in the enumeration facet.</source>
        <translation type="vanished">Zawartość listy nie widnieje na liście aspektu &quot;enumeration&quot;.</translation>
    </message>
    <message>
        <source>List content does not match pattern facet.</source>
        <translation type="vanished">Zawartość listy koliduje z aspektem &quot;pattern&quot;.</translation>
    </message>
    <message>
        <source>Union content is not listed in the enumeration facet.</source>
        <translation type="vanished">Zawartość unii nie widnieje na liście aspektu &quot;enumeration&quot;.</translation>
    </message>
    <message>
        <source>Union content does not match pattern facet.</source>
        <translation type="vanished">Zawartość unii koliduje z aspektem &quot;pattern&quot;.</translation>
    </message>
    <message>
        <source>Data of type %1 are not allowed to be empty.</source>
        <translation type="vanished">Dane typu %1 nie mogą być puste.</translation>
    </message>
    <message>
        <source>Element %1 is missing child element.</source>
        <translation type="vanished">Brak wymaganego podelementu w elemencie %1.</translation>
    </message>
    <message>
        <source>There is one IDREF value with no corresponding ID: %1.</source>
        <translation type="vanished">Istnieje wartość IDREF bez odpowiadającej jej wartości ID: %1.</translation>
    </message>
    <message>
        <source>Loaded schema file is invalid.</source>
        <translation type="vanished">Załadowany plik nie jest poprawnym plikiem ze schematem.</translation>
    </message>
    <message>
        <source>%1 contains invalid data.</source>
        <translation type="vanished">%1 zawiera niepoprawne dane.</translation>
    </message>
    <message>
        <source>xsi:schemaLocation namespace %1 has already appeared earlier in the instance document.</source>
        <translation type="vanished">Przestrzeń nazw &quot;xsi:schemaLocation&quot; %1 wystąpiła już wcześniej w dokumencie.</translation>
    </message>
    <message>
        <source>xsi:noNamespaceSchemaLocation cannot appear after the first no-namespace element or attribute.</source>
        <translation type="vanished">&quot;xsi:noNamespaceSchemaLocation&quot; nie może wystąpić po pierwszym elemencie lub atrybucie który nie jest przestrzenią nazw.</translation>
    </message>
    <message>
        <source>No schema defined for validation.</source>
        <translation type="vanished">Brak zdefiniowanego schematu dla walidacji.</translation>
    </message>
    <message>
        <source>No definition for element %1 available.</source>
        <translation type="vanished">Brak dostępnej definicji dla elementu %1.</translation>
    </message>
    <message>
        <source>Specified type %1 is not known to the schema.</source>
        <translation type="vanished">Podany typ %1 nie jest schematowi znany.</translation>
    </message>
    <message>
        <source>Element %1 is not defined in this scope.</source>
        <translation type="vanished">Element %1 nie jest zdefiniowany w tym zakresie.</translation>
    </message>
    <message>
        <source>Declaration for element %1 does not exist.</source>
        <translation type="vanished">Brak deklaracji dla elementu %1.</translation>
    </message>
    <message>
        <source>Element %1 contains invalid content.</source>
        <translation type="vanished">Element %1 posiada niepoprawną zawartość.</translation>
    </message>
    <message>
        <source>Element %1 is declared as abstract.</source>
        <translation type="vanished">Element %1 jest zadeklarowany jako abstrakcyjny.</translation>
    </message>
    <message>
        <source>Element %1 is not nillable.</source>
        <translation type="vanished">Element %1 nie jest zerowalny.</translation>
    </message>
    <message>
        <source>Attribute %1 contains invalid data: %2</source>
        <translation type="vanished">Atrybut %1 zawiera niepoprawne dane: %2</translation>
    </message>
    <message>
        <source>Element contains content although it is nillable.</source>
        <translation type="vanished">Element posiada zawartość chociaż jest zerowalny.</translation>
    </message>
    <message>
        <source>Fixed value constraint not allowed if element is nillable.</source>
        <translation type="vanished">Ograniczenie stałej wartości jest niedozwolone gdy element jest zerowalny.</translation>
    </message>
    <message>
        <source>Specified type %1 is not validly substitutable with element type %2.</source>
        <translation type="vanished">Podany typ %1 nie jest poprawnie zastępowalny typem elementu %2.</translation>
    </message>
    <message>
        <source>Complex type %1 is not allowed to be abstract.</source>
        <translation type="vanished">Typ złożony %1 nie może być abstrakcyjny.</translation>
    </message>
    <message>
        <source>Element %1 contains not allowed attributes.</source>
        <translation type="vanished">Element %1 zawiera niedozwolone atrybuty.</translation>
    </message>
    <message>
        <source>Element %1 contains not allowed child element.</source>
        <translation type="vanished">Element %1 zawiera niedozwolony podelement.</translation>
    </message>
    <message>
        <source>Content of element %1 does not match its type definition: %2.</source>
        <translation type="vanished">Zawartość elementu %1 nie pasuje do jego definicji typu: %2.</translation>
    </message>
    <message>
        <source>Content of element %1 does not match defined value constraint.</source>
        <translation type="vanished">Zawartość elementu %1 nie pasuje do zdefiniowanego ograniczenia wartości.</translation>
    </message>
    <message>
        <source>Element %1 contains not allowed child content.</source>
        <translation type="vanished">Element %1 zawiera niedozwolony podelement.</translation>
    </message>
    <message>
        <source>Element %1 contains not allowed text content.</source>
        <translation type="vanished">Element %1 zawiera niedozwolony text.</translation>
    </message>
    <message>
        <source>Element %1 cannot contain other elements, as it has fixed content.</source>
        <translation type="vanished">Element %1 nie może zawierać innych elementów, ponieważ posiada on stałą zawartość.</translation>
    </message>
    <message>
        <source>Element %1 is missing required attribute %2.</source>
        <translation type="vanished">Brak wymaganego atrybutu %2 w elemencie %1.</translation>
    </message>
    <message>
        <source>Attribute %1 does not match the attribute wildcard.</source>
        <translation type="vanished">Atrybut %1 nie pasuje do atrybutu dżokera.</translation>
    </message>
    <message>
        <source>Declaration for attribute %1 does not exist.</source>
        <translation type="vanished">Brak deklaracji atrybutu %1.</translation>
    </message>
    <message>
        <source>Element %1 contains two attributes of type %2.</source>
        <translation type="vanished">Element %1 posiada dwa atrybuty typu %2.</translation>
    </message>
    <message>
        <source>Attribute %1 contains invalid content.</source>
        <translation type="vanished">Atrybut %1 posiada niepoprawną zawartość.</translation>
    </message>
    <message>
        <source>Element %1 contains unknown attribute %2.</source>
        <translation type="vanished">Element %1 posiada nieznany atrybut %2.</translation>
    </message>
    <message>
        <source>Content of attribute %1 does not match its type definition: %2.</source>
        <translation type="vanished">Zawartość atrybutu %1 nie pasuje do jego definicji typu: %2.</translation>
    </message>
    <message>
        <source>Content of attribute %1 does not match defined value constraint.</source>
        <translation type="vanished">Zawartość elementu %1 nie pasuje do zdefiniowanego ograniczenia wartości.</translation>
    </message>
    <message>
        <source>Non-unique value found for constraint %1.</source>
        <translation type="vanished">Znaleziono nieunikatową wartość dla ograniczenia %1.</translation>
    </message>
    <message>
        <source>Key constraint %1 contains absent fields.</source>
        <translation type="vanished">Ograniczenie klucza %1 zawiera nieobecne pola.</translation>
    </message>
    <message>
        <source>Key constraint %1 contains references nillable element %2.</source>
        <translation type="vanished">Ograniczenie klucza %1 zawiera odwołania do elementu zerowalnego %2.</translation>
    </message>
    <message>
        <source>No referenced value found for key reference %1.</source>
        <translation type="vanished">Brak wartości do której odwołuje się klucz %1.</translation>
    </message>
    <message>
        <source>More than one value found for field %1.</source>
        <translation type="vanished">Znaleziono więcej niż jedną wartość dla pola %1.</translation>
    </message>
    <message>
        <source>Field %1 has no simple type.</source>
        <translation type="vanished">Pole %1 nie posiada prostego typu.</translation>
    </message>
    <message>
        <source>ID value &apos;%1&apos; is not unique.</source>
        <translation type="vanished">Wartość ID &quot;%1&quot; nie jest unikatowa.</translation>
    </message>
    <message>
        <source>&apos;%1&apos; attribute contains invalid QName content: %2.</source>
        <translation type="vanished">Atrybut &quot;%1&quot; zawiera niepoprawną zawartość QName: %2.</translation>
    </message>
</context>
<context>
    <name>SignalHandlerConverter</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmltypecompiler.cpp" line="+844"/>
        <source>Non-existent attached object</source>
        <translation type="unfinished">Nieistniejący dołączony obiekt</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Signal uses unnamed parameter followed by named parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Signal parameter &quot;%1&quot; hides global variable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&quot;%1.%2&quot; is not available in %3 %4.%5.</source>
        <translation type="unfinished">&quot;%1.%2&quot; nie jest dostępne w %3 %4.%5.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&quot;%1.%2&quot; is not available due to component versioning.</source>
        <translation type="unfinished">&quot;%1.%2&quot; nie jest dostępny z powodu niekompatybilności wersji komponentu.</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Cannot assign a value to a signal (expecting a script to be run)</source>
        <translation type="unfinished">Nie można przypisać wartości do sygnału (oczekiwano uruchomienia skryptu)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Incorrectly specified signal assignment</source>
        <translation type="unfinished">Przypisanie sygnału błędnie podane</translation>
    </message>
</context>
<context>
    <name>qmlRegisterType</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlmetatype.cpp" line="+1175"/>
        <source>Invalid QML %1 name &quot;%2&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot install %1 &apos;%2&apos; into unregistered namespace &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot install %1 &apos;%2&apos; into protected namespace &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot install %1 &apos;%2&apos; into protected module &apos;%3&apos; version &apos;%4&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
