<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>CloseButton</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qtabbar.cpp" line="+2433"/>
        <source>Close Tab</source>
        <translation>Zavřít kartu</translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../../qtbase/src/plugins/platforms/cocoa/messages.cpp" line="+49"/>
        <source>Services</source>
        <translation>Služby</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide %1</source>
        <translation>Skrýt %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Others</source>
        <translation>Skrýt ostatní</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show All</source>
        <translation>Ukázat vše</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Preferences...</source>
        <translation>Nastavení...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit %1</source>
        <translation>Ukončit %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>About %1</source>
        <translation>O %1</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <location filename="../../qtbase/src/network/access/qhttpnetworkconnection.cpp" line="+788"/>
        <location filename="../../qtbase/src/network/socket/qabstractsocket.cpp" line="+2084"/>
        <source>Socket operation timed out</source>
        <translation>Časový limit pro socket operaci byl překročen</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/socket/qabstractsocket.cpp" line="-1448"/>
        <location line="+988"/>
        <location line="+222"/>
        <source>Operation on socket is not supported</source>
        <translation>Tato socket operace není podporována</translation>
    </message>
    <message>
        <location line="-803"/>
        <location filename="../../qtbase/src/network/socket/qhttpsocketengine.cpp" line="+667"/>
        <location filename="../../qtbase/src/network/socket/qsocks5socketengine.cpp" line="+658"/>
        <location line="+26"/>
        <source>Host not found</source>
        <translation>Nepodařilo se najít počítač</translation>
    </message>
    <message>
        <location line="+50"/>
        <location filename="../../qtbase/src/network/socket/qhttpsocketengine.cpp" line="+3"/>
        <location filename="../../qtbase/src/network/socket/qsocks5socketengine.cpp" line="+4"/>
        <source>Connection refused</source>
        <translation>Spojení bylo odmítnuto</translation>
    </message>
    <message>
        <location line="+131"/>
        <source>Connection timed out</source>
        <translation>Časový limit pro spojení byl překročen</translation>
    </message>
    <message>
        <location line="+370"/>
        <source>Trying to connect while connection is in progress</source>
        <translation>Při navázaném spojení došlo k dalšímu pokusu o spojení</translation>
    </message>
    <message>
        <location line="+872"/>
        <source>Socket is not connected</source>
        <translation>Socket není spojen</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/socket/qsocks5socketengine.cpp" line="-8"/>
        <source>Network unreachable</source>
        <translation>Síť není dosažitelná</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qabstractspinbox.cpp" line="+1237"/>
        <source>&amp;Select All</source>
        <translation>&amp;Vybrat vše</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Step up</source>
        <translation>&amp;Krok nahoru</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Step &amp;down</source>
        <translation>Krok &amp;dolů</translation>
    </message>
</context>
<context>
    <name>QAccessibleActionInterface</name>
    <message>
        <location filename="../../qtbase/src/gui/accessible/qaccessible.cpp" line="+2609"/>
        <source>Press</source>
        <translation>Stisknout</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Increase</source>
        <translation>Zvětšit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Decrease</source>
        <translation>Zmenšit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ShowMenu</source>
        <translation>Ukázat nabídku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SetFocus</source>
        <translation>Nastavit zaměření</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Toggle</source>
        <translation>Přepnout</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Left</source>
        <translation type="unfinished">Projíždět doleva</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Right</source>
        <translation type="unfinished">Projíždět doprava</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Triggers the action</source>
        <translation>Spouští činnost</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Increase the value</source>
        <translation>Zvýšit hodnotu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Decrease the value</source>
        <translation>Snížit hodnotu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shows the menu</source>
        <translation>Ukáže nabídku</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sets the focus</source>
        <translation>Nastaví zaměření</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Toggles the state</source>
        <translation>Přepne stav</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scrolls to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scrolls to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scrolls up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scrolls down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Goes back a page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Goes to the next page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QAndroidPlatformTheme</name>
    <message>
        <location filename="../../qtbase/src/plugins/platforms/android/qandroidplatformtheme.cpp" line="+491"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes to All</source>
        <translation>Ano, vše</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No to All</source>
        <translation>Ne, žádné</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qmessagebox.h" line="+317"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>Použití &apos;%1&apos; vyžaduje Qt %2; bylo ale nalezeno Qt %3.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Incompatible Qt Library Error</source>
        <translation>Nekompatibilní knihovna Qt</translation>
    </message>
</context>
<context>
    <name>QAxSelect</name>
    <message>
        <source>Select ActiveX Control</source>
        <translation type="vanished">Vybrat prvek ActiveX</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;Zrušit</translation>
    </message>
    <message>
        <source>COM &amp;Object:</source>
        <translation type="vanished">COM-&amp;Objekt:</translation>
    </message>
</context>
<context>
    <name>QCocoaMenuItem</name>
    <message>
        <location filename="../../qtbase/src/plugins/platforms/cocoa/messages.cpp" line="-11"/>
        <source>About Qt</source>
        <translation>O Qt</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>About</source>
        <translation>O</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Config</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Preference</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Options</source>
        <translation>Volby</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Setting</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Setup</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Quit</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Exit</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cut</source>
        <translation type="unfinished">Vyjmout</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy</source>
        <translation type="unfinished">Kopírovat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paste</source>
        <translation type="unfinished">Vložit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Select All</source>
        <translation type="unfinished">Vybrat vše</translation>
    </message>
</context>
<context>
    <name>QCocoaTheme</name>
    <message>
        <location line="+7"/>
        <source>Don&apos;t Save</source>
        <translation>Neukládat</translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qcolordialog.cpp" line="+1435"/>
        <source>Hu&amp;e:</source>
        <translation>&amp;Odstín:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Sat:</source>
        <translation>&amp;Sytost:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Val:</source>
        <translation>&amp;Hodnota:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Red:</source>
        <translation>&amp;Červená:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Green:</source>
        <translation>&amp;Zelená:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bl&amp;ue:</source>
        <translation>&amp;Modrá:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A&amp;lpha channel:</source>
        <translation>A&amp;lfa kanál:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;HTML:</source>
        <translation>&amp;HTML:</translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Cursor at %1, %2
Press ESC to cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+222"/>
        <source>&amp;Pick Screen Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cursor at %1, %2, color: %3
Press ESC to cancel</source>
        <translation type="vanished">Ukazatel na %1, %2, barva: %3
Stiskněte Esc pro zrušení</translation>
    </message>
    <message>
        <location line="-198"/>
        <source>Select Color</source>
        <translation>Vybrat barvu</translation>
    </message>
    <message>
        <source>Pick Screen Color</source>
        <translation type="vanished">Zvolit barvu obrazovky</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>&amp;Basic colors</source>
        <translation>Základní &amp;barvy</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Custom colors</source>
        <translation>&amp;Uživatelem stanovené barvy</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Add to Custom Colors</source>
        <translation>&amp;Přidat k uživatelem stanoveným barvám</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <location filename="../../qtbase/src/widgets/itemviews/qitemeditorfactory.cpp" line="+587"/>
        <source>False</source>
        <translation>Nesprávný</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>True</source>
        <translation>Pravdivý</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/widgets/accessible/complexwidgets.cpp" line="+341"/>
        <source>Open the combo box selection popup</source>
        <translation>Otevřít rozbalovací seznam</translation>
    </message>
</context>
<context>
    <name>QCommandLineParser</name>
    <message>
        <location filename="../../qtbase/src/corelib/tools/qcommandlineparser.cpp" line="+362"/>
        <source>Displays version information.</source>
        <translation>Zobrazit informace o verzi.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Displays this help.</source>
        <translation>Zobrazit tuto nápovědu.</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Unknown option &apos;%1&apos;.</source>
        <translation>Neznámá volba &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown options: %1.</source>
        <translation>Neznámé volby: %1.</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>Missing value after &apos;%1&apos;.</source>
        <translation>Chybějící hodnota po &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Unexpected value after &apos;%1&apos;.</source>
        <translation>Neočekávaná hodnota po &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+419"/>
        <source>[options]</source>
        <translation>[volby]</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Usage: %1</source>
        <translation>Použití: %1</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Options:</source>
        <translation>Volby:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Arguments:</source>
        <translation>Argumenty:</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../../qtbase/src/corelib/kernel/qsystemsemaphore_posix.cpp" line="+69"/>
        <location filename="../../qtbase/src/corelib/kernel/qsystemsemaphore_systemv.cpp" line="+69"/>
        <source>%1: key is empty</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1: Neplatný údaj u klíče (prázdný)</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/kernel/qsystemsemaphore_systemv.cpp" line="+12"/>
        <source>%1: unable to make key</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1: Nepodařilo se vytvořit klíč</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1: ftok failed</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1: Vyvolání ftok se nezdařilo</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/gui/kernel/qguiapplication.cpp" line="+179"/>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation>LTR</translation>
    </message>
</context>
<context>
    <name>QCupsJobWidget</name>
    <message>
        <location filename="../../qtbase/src/printsupport/widgets/qcupsjobwidget.ui"/>
        <source>Job</source>
        <translation>Úloha</translation>
    </message>
    <message>
        <location/>
        <source>Job Control</source>
        <translation>Ovládání úlohy</translation>
    </message>
    <message>
        <location/>
        <source>Scheduled printing:</source>
        <translation>Naplánovaný tisk:</translation>
    </message>
    <message>
        <location/>
        <source>Billing information:</source>
        <translation>Vyúčtování:</translation>
    </message>
    <message>
        <location/>
        <source>Job priority:</source>
        <translation>Přednost úlohy:</translation>
    </message>
    <message>
        <location/>
        <source>Banner Pages</source>
        <translation>Úvodní stránky</translation>
    </message>
    <message>
        <location/>
        <source>End:</source>
        <comment>Banner page at end</comment>
        <translation>Konec:</translation>
    </message>
    <message>
        <location/>
        <source>Start:</source>
        <comment>Banner page at start</comment>
        <translation>Začátek:</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/printsupport/widgets/qcupsjobwidget.cpp" line="+94"/>
        <source>Print Immediately</source>
        <translation>Vytisknout okamžitě</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hold Indefinitely</source>
        <translation>Pozdržet na neurčito</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Day (06:00 to 17:59)</source>
        <translation>Den (06:00 až 17:59)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Night (18:00 to 05:59)</source>
        <translation>Noc (18:00 až 05:59)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Second Shift (16:00 to 23:59)</source>
        <translation>Druhá směna (16:00 až 23:59)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Third Shift (00:00 to 07:59)</source>
        <translation>Třetí směna (10:00 až 07:59)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Weekend (Saturday to Sunday)</source>
        <translation>Sobota a neděle</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Specific Time</source>
        <translation>Určitý čas</translation>
    </message>
    <message>
        <location line="+68"/>
        <location line="+8"/>
        <source>None</source>
        <comment>CUPS Banner page</comment>
        <translation>Žádný</translation>
    </message>
    <message>
        <location line="-7"/>
        <location line="+8"/>
        <source>Standard</source>
        <comment>CUPS Banner page</comment>
        <translation>Standardní</translation>
    </message>
    <message>
        <location line="-7"/>
        <location line="+8"/>
        <source>Unclassified</source>
        <comment>CUPS Banner page</comment>
        <translation>Neutajovaný</translation>
    </message>
    <message>
        <location line="-7"/>
        <location line="+8"/>
        <source>Confidential</source>
        <comment>CUPS Banner page</comment>
        <translation>Důvěrný</translation>
    </message>
    <message>
        <location line="-7"/>
        <location line="+8"/>
        <source>Classified</source>
        <comment>CUPS Banner page</comment>
        <translation>Utajovaný</translation>
    </message>
    <message>
        <location line="-7"/>
        <location line="+8"/>
        <source>Secret</source>
        <comment>CUPS Banner page</comment>
        <translation>Tajný</translation>
    </message>
    <message>
        <location line="-7"/>
        <location line="+8"/>
        <source>Top Secret</source>
        <comment>CUPS Banner page</comment>
        <translation>Přísně tajný</translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <location filename="../../qtbase/src/sql/drivers/db2/qsql_db2.cpp" line="+1240"/>
        <source>Unable to connect</source>
        <translation>Nepodařilo se navázat spojení</translation>
    </message>
    <message>
        <location line="+304"/>
        <source>Unable to commit transaction</source>
        <translation>Transakci se nepodařilo zapsat</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Unable to rollback transaction</source>
        <translation>Transakci se nepodařilo vrátit</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Unable to set autocommit</source>
        <translation>&apos;autocommit&apos; se nepodařilo nastavit</translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <location line="-1032"/>
        <location line="+240"/>
        <source>Unable to execute statement</source>
        <translation>Příkaz se nepodařilo provést</translation>
    </message>
    <message>
        <location line="-203"/>
        <source>Unable to prepare statement</source>
        <translation>Příkaz se nepodařilo připravit</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Unable to bind variable</source>
        <translation>Proměnnou se nepodařilo provést</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Unable to fetch record %1</source>
        <translation>Datový záznam %1 se nepodařilo natáhnout</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to fetch next</source>
        <translation>Další datový záznam se nepodařilo natáhnout</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Unable to fetch first</source>
        <translation>První datový záznam se nepodařilo natáhnout</translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qdatetimeedit.cpp" line="+2320"/>
        <source>AM</source>
        <translation>AM</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>am</source>
        <translation>am</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PM</source>
        <translation>PM</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>pm</source>
        <translation>pm</translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qdialog.cpp" line="+636"/>
        <source>What&apos;s This?</source>
        <translation>Co je toto?</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qmessagebox.cpp" line="+2047"/>
        <location line="+446"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="vanished">&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="vanished">&amp;Uložit</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Uložit</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Otevřít</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;Zrušit</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Zrušit</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="vanished">&amp;Zavřít</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zavřít</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">Použít</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Vrátit</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Nápověda</translation>
    </message>
    <message>
        <source>Don&apos;t Save</source>
        <translation type="vanished">Neukládat</translation>
    </message>
    <message>
        <source>Close without Saving</source>
        <translation type="vanished">Zavřít bez uložení</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="vanished">Zahodit</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation type="vanished">&amp;Ano</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation type="vanished">Ano, &amp;vše</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="vanished">&amp;Ne</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation type="vanished">N&amp;e, žádné</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation type="vanished">Uložit vše</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="vanished">Zrušit</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Opakovat</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">Ignorovat</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation type="vanished">Obnovit výchozí</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <location filename="../../qtbase/src/widgets/itemviews/qdirmodel.cpp" line="+431"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>Druh</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>Typ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Date Modified</source>
        <translation>Datum změny</translation>
    </message>
</context>
<context>
    <name>QDnsLookup</name>
    <message>
        <location filename="../../qtbase/src/network/kernel/qdnslookup.cpp" line="+477"/>
        <source>Operation cancelled</source>
        <translation>Operace byla zrušena</translation>
    </message>
</context>
<context>
    <name>QDnsLookupRunnable</name>
    <message>
        <location line="+519"/>
        <source>Invalid domain name</source>
        <translation>Neplatný název domény</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/kernel/qdnslookup_android.cpp" line="+46"/>
        <source>Not yet supported on Android</source>
        <translation>Zatím ještě není na Androidu podporováno</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/kernel/qdnslookup_unix.cpp" line="+125"/>
        <source>Resolver functions not found</source>
        <translation>Funkce pro překlad adres nenalezeny</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Resolver initialization failed</source>
        <translation>Inicializace překladače adres se nezdařila</translation>
    </message>
    <message>
        <location line="+37"/>
        <location filename="../../qtbase/src/network/kernel/qdnslookup_win.cpp" line="+65"/>
        <source>IPv6 addresses for nameservers is currently not supported</source>
        <translation>Adresy IPv6 pro DNS servery nejsou v současnosti podporovány</translation>
    </message>
    <message>
        <location line="+23"/>
        <location filename="../../qtbase/src/network/kernel/qdnslookup_win.cpp" line="+10"/>
        <source>Server could not process query</source>
        <translation>Serveru se dotaz nepodařilo zpracovat</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../../qtbase/src/network/kernel/qdnslookup_win.cpp" line="+4"/>
        <source>Server failure</source>
        <translation>Neúspěch serveru</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../../qtbase/src/network/kernel/qdnslookup_win.cpp" line="+4"/>
        <source>Non existent domain</source>
        <translation>Neexistující doména</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../../qtbase/src/network/kernel/qdnslookup_win.cpp" line="+4"/>
        <source>Server refused to answer</source>
        <translation>Server odmítl odpovědět</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+7"/>
        <source>Invalid reply received</source>
        <translation>Přijata neplatná odpověď</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+11"/>
        <source>Could not expand domain name</source>
        <translatorcomment>Nepodařilo se rozvinout název domény</translatorcomment>
        <translation>Nepodařilo se rozšířit název domény</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Invalid IPv4 address record</source>
        <translation>Záznam neplatné adresy IPv4</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid IPv6 address record</source>
        <translation>Záznam neplatné adresy IPv6</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid canonical name record</source>
        <translation>Záznam kanonického názvu je neplatný</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid name server record</source>
        <translation>Záznam názvového serveru je neplatný</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid pointer record</source>
        <translation>Záznam ukazatele je neplatný</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid mail exchange record</source>
        <translation>Záznam směrování pošty je neplatný</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Invalid service record</source>
        <translation>Záznam služby je neplatný</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Invalid text record</source>
        <translation>Záznam textu je neplatný</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Resolver library can&apos;t be loaded: No runtime library loading support</source>
        <translation>Knihovnu překladače adres nelze nahrát: Žádná podpora pro nahrání běhové knihovny</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/kernel/qdnslookup_winrt.cpp" line="+72"/>
        <source>No hostname given</source>
        <translation>Nebyl zadán název serveru</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Invalid hostname</source>
        <translation>Neplatný název serveru</translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qdockwidget.cpp" line="+659"/>
        <source>Float</source>
        <extracomment>Accessible name for button undocking a dock widget (floating state)</extracomment>
        <translation>Uvolnit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Undocks and re-attaches the dock widget</source>
        <translation>Zruší ukotvení a znovupřipojí kotvící prvek</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Close</source>
        <extracomment>Accessible name for button closing a dock widget</extracomment>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Closes the dock widget</source>
        <translation>Zavře kotvící prvek</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qerrormessage.cpp" line="+182"/>
        <source>Debug Message:</source>
        <translation>Hlášení o odladění:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning:</source>
        <translation>Varování:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Fatal Error:</source>
        <translation>Kritická chyba:</translation>
    </message>
    <message>
        <location line="+194"/>
        <source>&amp;Show this message again</source>
        <translation>Toto hlášení &amp;ukázat ještě jednou</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
</context>
<context>
    <name>QFile</name>
    <message>
        <location filename="../../qtbase/src/corelib/io/qfile.cpp" line="+555"/>
        <source>Destination file is the same file.</source>
        <translation>Cílový soubor je týž soubor.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Source file does not exist.</source>
        <translation>Zdrojový soubor neexistuje.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+184"/>
        <source>Destination file exists</source>
        <translation>Cílový soubor již existuje</translation>
    </message>
    <message>
        <location line="-168"/>
        <source>Error while renaming.</source>
        <translation>Chyba při přejmenovávání.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Unable to restore from %1: %2</source>
        <translation>Nelze obnovit z %1: %2</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Will not rename sequential file using block copy</source>
        <translation>Nepřejmenuje sekvenční soubor pomocí kopie bloku</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Cannot remove source file</source>
        <translation>Nelze odstranit zdrojový soubor</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>Cannot open %1 for input</source>
        <translation>%1 se nepodařilo otevřít pro čtení</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cannot open for output</source>
        <translation>Nepodařilo se otevřít pro zápis</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Failure to write block</source>
        <translation>Datový blok se nepodařilo zapsat</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot create %1 for output</source>
        <translation>%1 se nepodařilo zřídit pro výstup</translation>
    </message>
</context>
<context>
    <name>QFileDevice</name>
    <message>
        <location filename="../../qtbase/src/corelib/io/qfiledevice.cpp" line="+738"/>
        <source>No file engine available or engine does not support UnMapExtension</source>
        <translation>Není dostupný žádný souborový stroj nebo stroj nepodporuje UnMapExtension</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qfiledialog.ui"/>
        <location filename="../../qtbase/src/widgets/dialogs/qfiledialog_embedded.ui"/>
        <source>Look in:</source>
        <translation>Hledat v:</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qtbase/src/widgets/dialogs/qfiledialog_embedded.ui"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location/>
        <source>Go back</source>
        <translation>Jít zpět</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qtbase/src/widgets/dialogs/qfiledialog_embedded.ui"/>
        <source>Forward</source>
        <translation>Dopředu</translation>
    </message>
    <message>
        <location/>
        <source>Go forward</source>
        <translation>Jít dopředu</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qtbase/src/widgets/dialogs/qfiledialog_embedded.ui"/>
        <source>Parent Directory</source>
        <translation>Nadřazený adresář</translation>
    </message>
    <message>
        <location/>
        <source>Go to the parent directory</source>
        <translation>Jít do rodičovského adresáře</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qtbase/src/widgets/dialogs/qfiledialog_embedded.ui"/>
        <source>Create New Folder</source>
        <translation>Vytvořit novou složku</translation>
    </message>
    <message>
        <location/>
        <source>Create a New Folder</source>
        <translation>Vytvořit novou složku</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qtbase/src/widgets/dialogs/qfiledialog_embedded.ui"/>
        <source>List View</source>
        <translation>Pohled se seznamem</translation>
    </message>
    <message>
        <location/>
        <source>Change to list view mode</source>
        <translation>Změnit na režim pohledu se seznamem</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qtbase/src/widgets/dialogs/qfiledialog_embedded.ui"/>
        <source>Detail View</source>
        <translation>Podrobný pohled</translation>
    </message>
    <message>
        <location/>
        <source>Change to detail view mode</source>
        <translation>Změnit na režim s podrobným pohledem</translation>
    </message>
    <message>
        <location/>
        <source>Sidebar</source>
        <translation>Postranní panel</translation>
    </message>
    <message>
        <location/>
        <source>List of places and bookmarks</source>
        <translation>Seznam míst a záložek</translation>
    </message>
    <message>
        <location/>
        <source>Files</source>
        <translation>Soubory</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qtbase/src/widgets/dialogs/qfiledialog_embedded.ui"/>
        <source>Files of type:</source>
        <translation>Soubory typu:</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qfiledialog.cpp" line="+621"/>
        <source>Find Directory</source>
        <translation>Najít adresář</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open</source>
        <translation>Otevřít</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save As</source>
        <translation>Uložit jako</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Directory:</source>
        <translation>Adresář:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>File &amp;name:</source>
        <translation>Název &amp;souboru:</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+13"/>
        <source>&amp;Open</source>
        <translation>&amp;Otevřít</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>&amp;Choose</source>
        <translation>&amp;Vybrat</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Save</source>
        <translation>&amp;Uložit</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+712"/>
        <source>All Files (*)</source>
        <translation>Všechny soubory (*)</translation>
    </message>
    <message>
        <location line="-700"/>
        <source>Show </source>
        <translation>Ukázat </translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Rename</source>
        <translation>&amp;Přejmenovat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>&amp;Smazat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show &amp;hidden files</source>
        <translation>&amp;Ukázat skryté soubory</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;New Folder</source>
        <translation>&amp;Nová složka</translation>
    </message>
    <message>
        <location line="+815"/>
        <source>All files (*)</source>
        <translation>Všechny soubory (*)</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Directories</source>
        <translation>Adresáře</translation>
    </message>
    <message>
        <location line="+929"/>
        <location line="+897"/>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation>%1
Adresář se nepodařilo nalézt.
Ověřte, prosím, že byl zadán správný název adresáře.</translation>
    </message>
    <message>
        <location line="-863"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>Soubor %1 již existuje.
Chcete jej nahradit?</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation>%1
Soubor se nepodařilo nalézt.
Ověřte, prosím, že byl zadán správný název souboru.</translation>
    </message>
    <message>
        <location line="+492"/>
        <source>New Folder</source>
        <translation>Nová složka</translation>
    </message>
    <message>
        <location line="+128"/>
        <location line="+5"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation>&apos;%1&apos; je chráněn proti zápisu.
Přesto chcete soubor smazat?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are you sure you want to delete &apos;%1&apos;?</source>
        <translation>Jste si jistý, že %1 chcete smazat?</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Could not delete directory.</source>
        <translation>Adresář se nepodařilo smazat.</translation>
    </message>
    <message>
        <location line="+441"/>
        <source>Recent Places</source>
        <translation>Naposledy navštívené</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qsidebar.cpp" line="+435"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/widgets/itemviews/qdirmodel.cpp" line="+405"/>
        <source>My Computer</source>
        <translation>Můj počítač</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/widgets/itemviews/qfileiconprovider.cpp" line="+361"/>
        <source>Drive</source>
        <translation>Disková jednotka</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 File</source>
        <extracomment>%1 is a file name suffix, for example txt</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>File Folder</source>
        <comment>Match Windows Explorer</comment>
        <translation>Souborová složka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Folder</source>
        <comment>All other platforms</comment>
        <translation>Složka</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Alias</source>
        <comment>Mac OS X Finder</comment>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shortcut</source>
        <comment>All other platforms</comment>
        <translation>Zkratka</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qfiledialog.ui"/>
        <source>Alt+Left</source>
        <translation>Alt+Left</translation>
    </message>
    <message>
        <location/>
        <source>Alt+Right</source>
        <translation>Alt+Right</translation>
    </message>
    <message>
        <location/>
        <source>Alt+Up</source>
        <translation>Alt+Up</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qfilesystemmodel.cpp" line="+751"/>
        <location filename="../../qtbase/src/widgets/itemviews/qdirmodel.cpp" line="+470"/>
        <source>%1 TB</source>
        <translation>%1 TB</translation>
    </message>
    <message>
        <location line="+2"/>
        <location filename="../../qtbase/src/widgets/itemviews/qdirmodel.cpp" line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+2"/>
        <location filename="../../qtbase/src/widgets/itemviews/qdirmodel.cpp" line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <location filename="../../qtbase/src/widgets/itemviews/qdirmodel.cpp" line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 bytes</source>
        <translation>%1 bytů</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Invalid filename</source>
        <translation>Neplatný název souboru</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;b&gt;The name &quot;%1&quot; can not be used.&lt;/b&gt;&lt;p&gt;Try using another name, with fewer characters or no punctuations marks.</source>
        <translation>&lt;b&gt;Název &quot;%1&quot; nelze použít.&lt;/b&gt;&lt;p&gt;Zkuste použít jiný název, s menším počtem znaků nebo bez zvláštních znaků.</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>Druh</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>Typ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Date Modified</source>
        <translation>Datum změny</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qfilesystemmodel_p.h" line="+251"/>
        <source>My Computer</source>
        <translation>Můj počítač</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Computer</source>
        <translation>Počítač</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/widgets/itemviews/qdirmodel.cpp" line="+1"/>
        <source>%1 byte(s)</source>
        <translation>%1 byt(ů)</translation>
    </message>
</context>
<context>
    <name>QFontDatabase</name>
    <message>
        <location filename="../../qtbase/src/gui/text/qfontdatabase.cpp" line="+1227"/>
        <source>Normal</source>
        <translation>Normální</translation>
    </message>
    <message>
        <location line="-1092"/>
        <location line="+1080"/>
        <source>Bold</source>
        <translation>Tučné</translation>
    </message>
    <message>
        <location line="-1077"/>
        <location line="+1079"/>
        <source>Demi Bold</source>
        <translation>Polotučné</translation>
    </message>
    <message>
        <location line="-1075"/>
        <location line="+1071"/>
        <source>Black</source>
        <translation>Černé</translation>
    </message>
    <message>
        <location line="-1056"/>
        <source>Demi</source>
        <translation>Polotučné</translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+1075"/>
        <source>Light</source>
        <translation>Jemné</translation>
    </message>
    <message>
        <location line="-1086"/>
        <source>Normal</source>
        <comment>The Normal or Regular font weight</comment>
        <translation type="unfinished">Normální</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Medium</source>
        <comment>The Medium font weight</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Thin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Extra Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Extra Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Extra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+100"/>
        <location line="+967"/>
        <source>Italic</source>
        <translation>Kurzíva</translation>
    </message>
    <message>
        <location line="-964"/>
        <location line="+966"/>
        <source>Oblique</source>
        <translation>Skloněné</translation>
    </message>
    <message>
        <location line="+723"/>
        <source>Any</source>
        <translation>Všechna</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Latin</source>
        <translation>Latinské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Greek</source>
        <translation>Řecké</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cyrillic</source>
        <translation>Cyrilské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Armenian</source>
        <translation>Arménské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hebrew</source>
        <translation>Hebrejské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Arabic</source>
        <translation>Arabské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Syriac</source>
        <translation>Syrské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thaana</source>
        <translation>Thaana</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Devanagari</source>
        <translation>Devanagari</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bengali</source>
        <translation>Bengálské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gurmukhi</source>
        <translation>Gurmukhi</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gujarati</source>
        <translation>Gujarati</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Oriya</source>
        <translation>Oriya</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tamil</source>
        <translation>Tamilské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Telugu</source>
        <translation>Telugu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kannada</source>
        <translation>Kannada</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Malayalam</source>
        <translation>Malayalam</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sinhala</source>
        <translation>Sinhálské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thai</source>
        <translation>Thajské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lao</source>
        <translation>Laoské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tibetan</source>
        <translation>Tibetské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Myanmar</source>
        <translation>Myanmar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Georgian</source>
        <translation>Gruzínské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Khmer</source>
        <translation>Khmerské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Simplified Chinese</source>
        <translation>Čínské zjednodušené</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Traditional Chinese</source>
        <translation>Čínské tradiční</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Japanese</source>
        <translation>Japonské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Korean</source>
        <translation>Korejské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Vietnamese</source>
        <translation>Větnamské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Symbol</source>
        <translation>Symbol</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ogham</source>
        <translation>Ogamské</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Runic</source>
        <translation>Runové</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>N&apos;Ko</source>
        <translation>N&apos;Ko</translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qfontdialog.cpp" line="+171"/>
        <source>Select Font</source>
        <translation>Vybrat písmo</translation>
    </message>
    <message>
        <location line="+593"/>
        <source>&amp;Font</source>
        <translation>&amp;Sada písma</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Font st&amp;yle</source>
        <translation>Řez pís&amp;ma</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>&amp;Velikost</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Effects</source>
        <translation>Efekty</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stri&amp;keout</source>
        <translation>Pře&amp;škrtnout</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Underline</source>
        <translation>&amp;Podtrhnout</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sample</source>
        <translation>Vzorek</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wr&amp;iting System</source>
        <translation>&amp;Písmo</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <location filename="../../qtbase/src/network/access/qftp.cpp" line="+835"/>
        <source>Not connected</source>
        <translation>Žádné spojení</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Host %1 not found</source>
        <translation>Počítač %1 se nepodařilo nalézt</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection refused to host %1</source>
        <translation>Spojení s počítačem %1 odmítnuto</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection timed out to host %1</source>
        <translation>Časový limit pro spojení s počítačem &apos;%1&apos; byl překročen</translation>
    </message>
    <message>
        <location line="+106"/>
        <source>Connected to host %1</source>
        <translation>Spojeno s počítačem %1</translation>
    </message>
    <message>
        <location line="+213"/>
        <source>Data Connection refused</source>
        <translation>Datové spojení bylo odmítnuto</translation>
    </message>
    <message>
        <location line="+180"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
    <message>
        <location line="+935"/>
        <source>Connecting to host failed:
%1</source>
        <translation>Spojení s počítačem se nezdařilo:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Login failed:
%1</source>
        <translation>Přihlášení se nezdařilo:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Listing directory failed:
%1</source>
        <translation>Obsah adresářů nelze ukázat:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Changing directory failed:
%1</source>
        <translation>Změna adresáře se nezdařila:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Downloading file failed:
%1</source>
        <translation>Stažení souboru se nezdařilo:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Uploading file failed:
%1</source>
        <translation>Nahrání souboru se nezdařilo:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Removing file failed:
%1</source>
        <translation>Odstranění souboru se nezdařilo:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Creating directory failed:
%1</source>
        <translation>Vytvoření adresářů se nezdařilo:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Removing directory failed:
%1</source>
        <translation>Odstranění adresáře se nezdařilo:
%1</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Connection closed</source>
        <translation>Spojení ukončeno</translation>
    </message>
</context>
<context>
    <name>QGnomeTheme</name>
    <message>
        <location filename="../../qtbase/src/platformsupport/themes/genericunix/qgenericunixthemes.cpp" line="+602"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Save</source>
        <translation>&amp;Uložit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Zrušit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Close</source>
        <translation>&amp;Zavřít</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close without Saving</source>
        <translation>Zavřít bez uložení</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <location filename="../../qtbase/src/network/kernel/qhostinfo.cpp" line="+164"/>
        <source>No host name given</source>
        <translation>Nebyl zadán název pro hostitelský počítač</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/kernel/qhostinfo_p.h" line="+99"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <location filename="../../qtbase/src/network/kernel/qhostinfo_unix.cpp" line="+193"/>
        <location filename="../../qtbase/src/network/kernel/qhostinfo_win.cpp" line="+177"/>
        <location filename="../../qtbase/src/network/kernel/qhostinfo_winrt.cpp" line="+71"/>
        <source>No host name given</source>
        <translation>Nebyl zadán název pro hostitelský počítač</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtbase/src/network/kernel/qhostinfo_win.cpp" line="+0"/>
        <location filename="../../qtbase/src/network/kernel/qhostinfo_winrt.cpp" line="+0"/>
        <source>Invalid hostname</source>
        <translation>Neplatný název pro hostitelský počítač</translation>
    </message>
    <message>
        <location line="+52"/>
        <location line="+39"/>
        <location filename="../../qtbase/src/network/kernel/qhostinfo_win.cpp" line="+30"/>
        <location line="+25"/>
        <source>Unknown address type</source>
        <translation>Neznámý typ adresy</translation>
    </message>
    <message>
        <location line="-26"/>
        <location line="+32"/>
        <location filename="../../qtbase/src/network/kernel/qhostinfo_win.cpp" line="-134"/>
        <source>Host not found</source>
        <translation>Nepodařilo se najít počítač</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/kernel/qhostinfo_win.cpp" line="+4"/>
        <source>Unknown error (%1)</source>
        <translation>Neznámá chyba (%1)</translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <location filename="../../qtbase/src/network/access/qhttpnetworkconnection.cpp" line="-11"/>
        <location line="+2"/>
        <source>Host %1 not found</source>
        <translation>Počítač %1 se nepodařilo nalézt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection refused</source>
        <translation>Spojení bylo odmítnuto</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection closed</source>
        <translation>Spojení ukončeno</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Proxy requires authentication</source>
        <translation>Proxy server požaduje autentizaci</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host requires authentication</source>
        <translation>Hostitelský počítač požaduje autentizaci</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data corrupted</source>
        <translation>Data jsou poškozena</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown protocol specified</source>
        <translation>Byl zadán neznámý protokol</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>SSL handshake failed</source>
        <translation>Během startu SSL protokolu se vyskytla chyba</translation>
    </message>
</context>
<context>
    <name>QHttpSocketEngine</name>
    <message>
        <location filename="../../qtbase/src/network/socket/qhttpsocketengine.cpp" line="-119"/>
        <source>Did not receive HTTP response from proxy</source>
        <translation>Žádná HTTP odpověď od proxy serveru</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Error parsing authentication request from proxy</source>
        <translation>Chyba při vyhodnocení autentizačního požadavku proxy serveru</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Authentication required</source>
        <translation>Požadována autentizace</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Proxy denied connection</source>
        <translation>Proxy server odmítl spojení</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error communicating with HTTP proxy</source>
        <translation>Chyba při spojení s proxy serverem</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Proxy server not found</source>
        <translation>Nepodařilo se najít žádný proxy server</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection refused</source>
        <translation>Proxy server odmítl navázání spojení</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy server connection timed out</source>
        <translation>Při spojení s proxy serverem byl překročen časový limit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection closed prematurely</source>
        <translation>Proxy server předčasně ukončil spojení</translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <location filename="../../qtbase/src/sql/drivers/ibase/qsql_ibase.cpp" line="+1527"/>
        <source>Error opening database</source>
        <translation>Nepodařilo se otevřít spojení s databází</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Could not start transaction</source>
        <translation>Nepodařilo se spustit žádnou transakci</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to commit transaction</source>
        <translation>Transakci se nepodařilo zapsat</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to rollback transaction</source>
        <translation>Transakci se nepodařilo vrátit</translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <location line="-1170"/>
        <source>Unable to create BLOB</source>
        <translation>Nepodařilo se vytvořit žádný BLOB</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to write BLOB</source>
        <translation>Nepodařilo se zapsat BLOB</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to open BLOB</source>
        <translation>Nepodařilo se otevřít BLOB</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Unable to read BLOB</source>
        <translation>BLOB se nepodařilo přečíst</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+187"/>
        <source>Could not find array</source>
        <translation>Nepodařilo se najít pole</translation>
    </message>
    <message>
        <location line="-155"/>
        <source>Could not get array data</source>
        <translation>Nepodařilo se přečíst data pole</translation>
    </message>
    <message>
        <location line="+210"/>
        <source>Could not get query info</source>
        <translation>Požadované informace k vyhledávání nejsou k dispozici</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Could not start transaction</source>
        <translation>Nepodařilo se spustit žádnou transakci</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>Transakci se nepodařilo zapsat</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Could not allocate statement</source>
        <translation>Přidělení příkazu se nezdařilo</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not prepare statement</source>
        <translation>Příkaz se nepodařilo připravit</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+11"/>
        <source>Could not describe input statement</source>
        <translation>Nepodařilo se získat žádný popis vstupního příkazu</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Could not describe statement</source>
        <translation>Nepodařilo se získat žádný popis příkazu</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Unable to close statement</source>
        <translation>Příkaz se nepodařilo zavřít</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Unable to execute query</source>
        <translation>Dotaz se nepodařilo provést</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Could not fetch next item</source>
        <translation>Další prvek se nepodařilo vyzvednout</translation>
    </message>
    <message>
        <location line="+204"/>
        <source>Could not get statement info</source>
        <translation>K dispozici není žádná informace k příkazu</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <location filename="../../qtbase/src/corelib/global/qglobal.cpp" line="+2893"/>
        <location filename="../../qtbase/src/corelib/kernel/qsystemerror.cpp" line="+116"/>
        <source>Permission denied</source>
        <translation>Přístup odepřen</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/corelib/kernel/qsystemerror.cpp" line="+3"/>
        <source>Too many open files</source>
        <translation>Příliš mnoho otevřených souborů</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/corelib/kernel/qsystemerror.cpp" line="+3"/>
        <source>No such file or directory</source>
        <translation>Nepodařilo se najít žádný takový soubor nebo adresář</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/corelib/kernel/qsystemerror.cpp" line="+3"/>
        <source>No space left on device</source>
        <translation>Na zařízení není žádný volný úložný prostor</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/io/qiodevice.cpp" line="+1580"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/io/qfsfileengine_unix.cpp" line="+140"/>
        <source>file to open is a directory</source>
        <translation>Soubor k otevření je adresářem</translation>
    </message>
</context>
<context>
    <name>QImageReader</name>
    <message>
        <location filename="../../qtbase/src/gui/image/qimagereader.cpp" line="+577"/>
        <source>Invalid device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>File not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Unsupported image format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+624"/>
        <source>Unable to read image data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Unknown error</source>
        <translation type="unfinished">Neznámá chyba</translation>
    </message>
</context>
<context>
    <name>QImageWriter</name>
    <message>
        <location filename="../../qtbase/src/gui/image/qimagewriter.cpp" line="+279"/>
        <source>Unknown error</source>
        <translation type="unfinished">Neznámá chyba</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Device is not set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Device not writable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+474"/>
        <source>Unsupported image format</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QInputDialog</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qinputdialog.cpp" line="+227"/>
        <source>Enter a value:</source>
        <translation>Zadejte hodnotu:</translation>
    </message>
</context>
<context>
    <name>QJsonParseError</name>
    <message>
        <location filename="../../qtbase/src/corelib/json/qjsonparser.cpp" line="+60"/>
        <source>no error occurred</source>
        <translation>žádná chyba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unterminated object</source>
        <translation>Neukončený objekt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing name separator</source>
        <translation>Chybějící oddělovač názvu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unterminated array</source>
        <translation>Neukončené pole</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing value separator</source>
        <translation>Chybějící oddělovač hodnoty</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>illegal value</source>
        <translation>Neplatná hodnota</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid termination by number</source>
        <translation>Neplatné ukončení podle čísla</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>illegal number</source>
        <translation>Neplatné číslo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid escape sequence</source>
        <translation>Neplatná úniková posloupnost</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid UTF8 string</source>
        <translation>Neplatný řetězec UTF8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unterminated string</source>
        <translation>Nekončící řetězec</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>object is missing after a comma</source>
        <translation>Objekt chybí po čárce</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>too deeply nested document</source>
        <translation>Příliš hluboko zanořený dokument</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>too large document</source>
        <translation>Příliš velký dokument</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>garbage at the end of the document</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QKeySequenceEdit</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qkeysequenceedit.cpp" line="+105"/>
        <source>Press shortcut</source>
        <translation>Stiskněte klávesovou zkratku</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>%1, ...</source>
        <extracomment>This text is an &quot;unfinished&quot; shortcut, expands like &quot;Ctrl+A, ...&quot;</extracomment>
        <translation>%1, ...</translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <location filename="../../qtbase/src/corelib/plugin/qelfparser_p.cpp" line="+69"/>
        <source>&apos;%1&apos; is not an ELF object (%2)</source>
        <translation>&apos;%1&apos; není objekt ELF (%2)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&apos;%1&apos; is not an ELF object</source>
        <translation>&apos;%1&apos; není objekt ELF</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+10"/>
        <location line="+6"/>
        <location line="+20"/>
        <location line="+12"/>
        <location line="+11"/>
        <location line="+15"/>
        <location line="+11"/>
        <location line="+22"/>
        <location line="+14"/>
        <location line="+13"/>
        <source>&apos;%1&apos; is an invalid ELF object (%2)</source>
        <translation>&apos;%1&apos; je neplatný objekt ELF (%2)</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/plugin/qlibrary.cpp" line="+315"/>
        <source>Plugin verification data mismatch in &apos;%1&apos;</source>
        <translation>Ověřovací data přídavného modulu nesouhlasí v &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+174"/>
        <location line="+222"/>
        <location line="+20"/>
        <source>The shared library was not found.</source>
        <translation>Nepodařilo se nalézt sdílenou knihovnu.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The file &apos;%1&apos; is not a valid Qt plugin.</source>
        <translation>Soubor &apos;%1&apos; není platným přídavným modulem Qt.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (%2.%3.%4) [%5]</source>
        <translation>Přídavný modul &apos;%1&apos; používá neslučitelnou Qt knihovnu. (%2.%3.%4) [%5]</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (Cannot mix debug and release libraries.)</source>
        <translation>Přídavný modul &apos;%1&apos; používá neslučitelnou Qt knihovnu. (Knihovny vytvořené v režimu ladění a vydání nemohou být používány společně.)</translation>
    </message>
    <message>
        <location line="+312"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/plugin/qlibrary_unix.cpp" line="+256"/>
        <location filename="../../qtbase/src/corelib/plugin/qlibrary_win.cpp" line="+127"/>
        <source>Cannot load library %1: %2</source>
        <translation>Knihovnu %1 nelze nahrát: %2</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+3"/>
        <location filename="../../qtbase/src/corelib/plugin/qlibrary_win.cpp" line="+24"/>
        <source>Cannot unload library %1: %2</source>
        <translation>Knihovnu %1 nelze vyjmout: %2</translation>
    </message>
    <message>
        <location line="+42"/>
        <location filename="../../qtbase/src/corelib/plugin/qlibrary_win.cpp" line="+15"/>
        <source>Cannot resolve symbol &quot;%1&quot; in %2: %3</source>
        <translation>Symbol &quot;%1&quot; nelze v %2 vyřešit: %3</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/plugin/qmachparser.cpp" line="+78"/>
        <source>&apos;%1&apos; is not a valid Mach-O binary (%2)</source>
        <translation>&apos;%1&apos; není platným spustitelným souborem Mach-O (%2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>file is corrupt</source>
        <translation>Soubor je poškozen</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+9"/>
        <source>file too small</source>
        <translation>Soubor je příliš malý</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>no suitable architecture in fat binary</source>
        <translation>Žádná vhodná architektura ve spustitelném souboru (fat binary)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>invalid magic %1</source>
        <translation>Neplatné magické číslo %1</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>wrong architecture</source>
        <translation>Nesprává architektura</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>not a dynamic library</source>
        <translation>Není dynamická knihovna</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>&apos;%1&apos; is not a Qt plugin</source>
        <translation>&apos;%1&apos; není přídavný modul Qt</translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qlineedit.cpp" line="+2105"/>
        <source>&amp;Undo</source>
        <translation>&amp;Zpět</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Redo</source>
        <translation>&amp;Znovu</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cu&amp;t</source>
        <translation>&amp;Vyjmout</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopírovat</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Paste</source>
        <translation>&amp;Vložit</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Select All</source>
        <translation>Vybrat vše</translation>
    </message>
</context>
<context>
    <name>QLocalServer</name>
    <message>
        <location filename="../../qtbase/src/network/socket/qlocalserver.cpp" line="+291"/>
        <location filename="../../qtbase/src/network/socket/qlocalserver_unix.cpp" line="+315"/>
        <source>%1: Name error</source>
        <translation>%1: Chybný název</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/socket/qlocalserver_unix.cpp" line="-8"/>
        <source>%1: Permission denied</source>
        <translation>%1: Přístup odepřen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: Address in use</source>
        <translation>%1: Adresa se již používá</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1: Unknown error %2</source>
        <translation>%1: Neznámá chyba %2</translation>
    </message>
</context>
<context>
    <name>QLocalSocket</name>
    <message>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_tcp.cpp" line="+124"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_unix.cpp" line="+131"/>
        <source>%1: Connection refused</source>
        <translation>%1: Spojení bylo odmítnuto</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Remote closed</source>
        <translation>%1: Spojení bylo protější stranou uzavřeno</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_win.cpp" line="+76"/>
        <location line="+62"/>
        <source>%1: Invalid name</source>
        <translation>%1: Neplatný název</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket access error</source>
        <translation>%1: Chyba při přístupu k socketu</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket resource error</source>
        <translation>%1: Chyba socketu - potíže se zdrojem</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket operation timed out</source>
        <translation>%1: Překročení času při operaci se socketem</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Datagram too large</source>
        <translation>%1: Datagram je příliš veliký</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_win.cpp" line="-67"/>
        <source>%1: Connection error</source>
        <translation>%1: Chyba spojení</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: The socket operation is not supported</source>
        <translation>%1: Tato operace se socketem není podporována</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Operation not permitted when socket is in this state</source>
        <translation>%1: Operace není povolena, když je socket v tomto stavu</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: Unknown error</source>
        <translation>%1: Neznámá chyba</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_unix.cpp" line="+4"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_win.cpp" line="+15"/>
        <source>%1: Unknown error %2</source>
        <translation>%1: Neznámá chyba %2</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_win.cpp" line="-5"/>
        <source>%1: Access denied</source>
        <translation>%1: Přístup odepřen</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_tcp.cpp" line="+58"/>
        <location filename="../../qtbase/src/network/socket/qlocalsocket_win.cpp" line="+46"/>
        <source>Trying to connect while connection is in progress</source>
        <translation>Při navázaném spojení došlo k dalšímu pokusu o spojení</translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <location filename="../../qtbase/src/sql/drivers/mysql/qsql_mysql.cpp" line="+1283"/>
        <source>Unable to open database &apos;%1&apos;</source>
        <translation>Nepodařilo se otevřít spojení s databází &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unable to connect</source>
        <translation>Nepodařilo se navázat spojení</translation>
    </message>
    <message>
        <location line="+159"/>
        <source>Unable to begin transaction</source>
        <translation>Transakci se nepodařilo spustit</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Unable to commit transaction</source>
        <translation>Transakci se nepodařilo zapsat</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Unable to rollback transaction</source>
        <translation>Transakci se nepodařilo vrátit</translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <location line="-990"/>
        <location line="+31"/>
        <source>Unable to fetch data</source>
        <translation>Nepodařilo se natáhnout žádná data</translation>
    </message>
    <message>
        <location line="+163"/>
        <source>Unable to execute query</source>
        <translation>Dotaz se nepodařilo provést</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to store result</source>
        <translation>Výsledek se nepodařilo uložit</translation>
    </message>
    <message>
        <location line="+103"/>
        <source>Unable to execute next query</source>
        <translation>Další dotaz nelze provést</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unable to store next result</source>
        <translation>Další výsledek nelze uložit</translation>
    </message>
    <message>
        <location line="+71"/>
        <location line="+8"/>
        <source>Unable to prepare statement</source>
        <translation>Příkaz se nepodařilo připravit</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Unable to reset statement</source>
        <translation>Příkaz se nepodařilo znovu nastavit</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>Unable to bind value</source>
        <translation>Pro hodnotu se nepodařilo vytvořit vazbu</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Unable to execute statement</source>
        <translation>Příkaz se nepodařilo provést</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+21"/>
        <source>Unable to bind outvalues</source>
        <translation>Pro výstupní hodnoty se nepodařilo vytvořit vazbu</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Unable to store statement results</source>
        <translation>Výsledky příkazu se nepodařilo uložit</translation>
    </message>
</context>
<context>
    <name>QMdiArea</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qmdiarea.cpp" line="+284"/>
        <source>(Untitled)</source>
        <translation>(Bez názvu)</translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qmdisubwindow.cpp" line="+264"/>
        <source>- [%1]</source>
        <translation>- [%1]</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Minimize</source>
        <translation>Zmenšit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Maximize</source>
        <translation>Zvětšit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unshade</source>
        <translation>Odvinout</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shade</source>
        <translation>Navinout</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Restore Down</source>
        <translation>Obnovit menší</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Restore</source>
        <translation>Obnovit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Menu</source>
        <translation>Nabídka</translation>
    </message>
    <message>
        <location line="+697"/>
        <source>&amp;Restore</source>
        <translation>&amp;Obnovit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Move</source>
        <translation>Po&amp;sunout</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>Změnit &amp;velikost</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mi&amp;nimize</source>
        <translation>Zmen&amp;šit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>Zvě&amp;tšit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stay on &amp;Top</source>
        <translation>Zůstat v &amp;popředí</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Close</source>
        <translation>&amp;Zavřít</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qmessagebox.cpp" line="-2328"/>
        <source>Show Details...</source>
        <translation>Ukázat podrobnosti...</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Hide Details...</source>
        <translation>Skrýt podrobnosti...</translation>
    </message>
    <message>
        <location line="+286"/>
        <location line="+921"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location line="+506"/>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;O Qt&lt;/h3&gt;&lt;p&gt;Tento program používá Qt-verze %1.&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across all major desktop operating systems. It is also available for embedded Linux and other embedded and mobile operating systems.&lt;/p&gt;&lt;p&gt;Qt is available under three different licensing options designed to accommodate the needs of our various users.&lt;/p&gt;&lt;p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of the GNU LGPL version 3 or GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 3 is appropriate for the development of Qt applications provided you can comply with the terms and conditions of the GNU LGPL version 3.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 2.1 is appropriate for the development of Qt applications provided you can comply with the terms and conditions of the GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://%2/&quot;&gt;%2&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) %1 Digia Plc and/or its subsidiary(-ies) and other contributors.&lt;/p&gt;&lt;p&gt;Qt and the Qt logo are trademarks of Digia Plc and/or its subsidiary(-ies).&lt;/p&gt;&lt;p&gt;Qt is a Digia product developed as an open source project. See &lt;a href=&quot;http://%3/&quot;&gt;%3&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across all major desktop operating systems. It is also available for embedded Linux and other embedded and mobile operating systems.&lt;/p&gt;&lt;p&gt;Qt is available under three different licensing options designed to accommodate the needs of our various users.&lt;/p&gt;&lt;p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of the GNU LGPL version 2.1 or GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 2.1 is appropriate for the development of Qt applications provided you can comply with the terms and conditions of the GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU General Public License version 3.0 is appropriate for the development of Qt applications where you wish to use such applications in combination with software subject to the terms of the GNU GPL version 3.0 or where you are otherwise willing to comply with the terms of the GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.digia.com/Product/Licensing/&quot;&gt;qt.digia.com/Product/Licensing&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies) and other contributors.&lt;/p&gt;&lt;p&gt;Qt and the Qt logo are trademarks of Digia Plc and/or its subsidiary(-ies).&lt;/p&gt;&lt;p&gt;Qt is developed as an open source project on &lt;a href=&quot;http://qt-project.org/&quot;&gt;qt-project.org&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;Qt is a Digia product. See &lt;a href=&quot;http://qt.digia.com/&quot;&gt;qt.digia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Qt je sadou softwarových nástrojů C++ určených pro víceplatformní vývoj aplikací.&lt;/p&gt;&lt;p&gt;Qt poskytuje snadnou přenositelnost a jednotný zdrojový kód pro MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux a všechny hlavní prodejní varianty systému Unix. Qt je rovněž dostupné pro vestavěná zařízení jako Qt pro Embedded Linux a Qt pro Windows CE.&lt;/p&gt;&lt;p&gt;Qt je dostupné s třemi rozdílnými licencemi, aby vyhovělo širokým řadám našich uživatelů.&lt;/p&gt;Qt s naší obchodní licenční smlouvou je vhodné pro vývoj soukromého/obchodního software, u kterého si nepřejete sdílet jakýkoli zdrojový kód se třetími stranami anebo z jiného důvodu nemůžete vyhovět podmínkám GNU LGPL ve verzi 2.1 nebo GNU GPL ve verzi 3.0.&lt;/p&gt;&lt;p&gt;Qt s licencí GNU LGPL ve verzi 2.1 je vhodné pro vývoj Qt aplikací (soukromých nebo s otevřeným zdrojovým kódem) za předpokladu, že můžete souhlasit s požadavky a podmínkami GNU LGPL verze 2.1.&lt;/p&gt;&lt;p&gt;Qt s licencí GNU General Public License ve verzi 3.0 je vhodné pro vývoj aplikací Qt, u nichž si přejete použít takovou aplikaci ve spojení se software, který podléhá požadavkům GNU GPL ve verzi 3.0, nebo kde jste jinak ochotni souhlasit s podmínkami GNU GPL ve verzi 3.0.&lt;/p&gt;&lt;p&gt;Více informací najdete na &lt;a href=&quot;http://qt.digia.com/Product/Licensing/&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;Autorské právo (C) 2013 Digia Plc a/nebo její dceřinná(é) společnost(i) a další přispěvatelé.&lt;/p&gt;&lt;p&gt;Qt a logo Qt jsou obchodní značky Digia Plc a/nebo její dceřinná(é) společnost(i).&lt;/p&gt;&lt;p&gt;Qt je vyvíjeno jako projekt s otevřeným zdrojovým kódem na &lt;a href=&quot;http://qt-project.org/&quot;&gt;qt-project.org&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;Qt je výrobkem společnosti Digia. Další informace najdete na &lt;a href=&quot;http://qt.digia.com/&quot;&gt;qt.digia.com&lt;/a&gt;. href=&quot;http://qt.digia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across all major desktop operating systems. It is also available for embedded Linux and other embedded and mobile operating systems.&lt;/p&gt;&lt;p&gt;Qt is available under three different licensing options designed to accommodate the needs of our various users.&lt;/p&gt;&lt;p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of the GNU LGPL version 2.1 or GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 2.1 is appropriate for the development of Qt applications provided you can comply with the terms and conditions of the GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU General Public License version 3.0 is appropriate for the development of Qt applications where you wish to use such applications in combination with software subject to the terms of the GNU GPL version 3.0 or where you are otherwise willing to comply with the terms of the GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.digia.com/Product/Licensing/&quot;&gt;qt.digia.com/Product/Licensing&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies) and other contributors.&lt;/p&gt;&lt;p&gt;Qt and the Qt logo are trademarks of Digia Plc and/or its subsidiary(-ies).&lt;/p&gt;&lt;p&gt;Qt is developed as an open source project on &lt;a href=&quot;http://qt-project.org/&quot;&gt;qt-project.org&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;Qt is a Digia product. See &lt;a href=&quot;http://qt.digia.com/&quot;&gt;qt.digia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Qt je sadou softwarových nástrojů C++ určených pro víceplatformní vývoj aplikací.&lt;/p&gt;&lt;p&gt;Qt poskytuje snadnou přenositelnost a jednotný zdrojový kód pro MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux a všechny hlavní prodejní varianty systému Unix. Qt je rovněž dostupné pro vestavěná zařízení jako Qt pro Embedded Linux a Qt pro Windows CE.&lt;/p&gt;&lt;p&gt;Qt je dostupné s třemi rozdílnými licencemi, aby vyhovělo širokým řadám našich uživatelů.&lt;/p&gt;Qt s naší obchodní licenční smlouvou je vhodné pro vývoj soukromého/obchodního software, u kterého si nepřejete sdílet jakýkoli zdrojový kód se třetími stranami anebo z jiného důvodu nemůžete vyhovět podmínkám GNU LGPL ve verzi 2.1 nebo GNU GPL ve verzi 3.0.&lt;/p&gt;&lt;p&gt;Qt s licencí GNU LGPL ve verzi 2.1 je vhodné pro vývoj Qt aplikací (soukromých nebo s otevřeným zdrojovým kódem) za předpokladu, že můžete souhlasit s požadavky a podmínkami GNU LGPL verze 2.1.&lt;/p&gt;&lt;p&gt;Qt s licencí GNU General Public License ve verzi 3.0 je vhodné pro vývoj aplikací Qt, u nichž si přejete použít takovou aplikaci ve spojení se software, který podléhá požadavkům GNU GPL ve verzi 3.0, nebo kde jste jinak ochotni souhlasit s podmínkami GNU GPL ve verzi 3.0.&lt;/p&gt;&lt;p&gt;Více informací najdete na &lt;a href=&quot;http://qt.digia.com/Product/Licensing/&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;Autorské právo (C) 2012 Digia Plc a/nebo její dceřinná(é) společnost(i).&lt;/p&gt;&lt;p&gt;Qt je výrobkem společnosti Nokia. Další informace najdete na &lt;a href=&quot;http://qt.digia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>About Qt</source>
        <translation>O Qt</translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine.cpp" line="+194"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+826"/>
        <source>Unable to initialize non-blocking socket</source>
        <translation>Neblokující socket se nepodařilo spustit</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Unable to initialize broadcast socket</source>
        <translation>Socket pro vysílání se nepodařilo spustit</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+4"/>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation>Vyzkoušelo se použít IPv6 socket na systému bez podpory IPv6</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>The remote host closed the connection</source>
        <translation>Vzdálený počítač uzavřel spojení</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Network operation timed out</source>
        <translation>Časový limit pro síťovou operaci byl překročen</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Out of resources</source>
        <translation>Nejsou dostupné žádné zdroje</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Unsupported socket operation</source>
        <translation>Nepodporovaná socket operace</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Protocol type not supported</source>
        <translation>Protokol tohoto typu není podporován</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Invalid socket descriptor</source>
        <translation>Neplatný deskriptor socketu</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Host unreachable</source>
        <translation>Cílový počítač je nedosažitelný</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Network unreachable</source>
        <translation>Síť je nedosažitelná</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Permission denied</source>
        <translation>Přístup odepřen</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Connection timed out</source>
        <translation>Časový limit pro spojení byl překročen</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Connection refused</source>
        <translation>Spojení bylo odmítnuto</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>The bound address is already in use</source>
        <translation>Uvedená adresa se už používá</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>The address is not available</source>
        <translation>Adresa není dostupná</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>The address is protected</source>
        <translation>Adresa je chráněna</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Datagram was too large to send</source>
        <translation>Datagram byl pro odeslání příliš veliký</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Unable to send a message</source>
        <translation>Nepodařilo se odeslat hlášení</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Unable to receive a message</source>
        <translation>Zprávu se nepodařilo přijmout</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Unable to write</source>
        <translation>Nepodařilo se zapsat</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Network error</source>
        <translation>Síťová chyba</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Another socket is already listening on the same port</source>
        <translation>Na tomto portu již naslouchá jiný socket</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Operation on non-socket</source>
        <translation>Operace mimo socket</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>The proxy type is invalid for this operation</source>
        <translation>Tuto operaci nelze s tímto typem proxy provést</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Temporary error</source>
        <translation>Dočasná chyba</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtbase/src/network/socket/qnativesocketengine_winrt.cpp" line="+3"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessCacheBackend</name>
    <message>
        <location filename="../../qtbase/src/network/access/qnetworkaccesscachebackend.cpp" line="+57"/>
        <source>Error opening %1</source>
        <translation>%1 se nepodařilo otevřít</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessDataBackend</name>
    <message>
        <location filename="../../qtbase/src/network/access/qnetworkreplydataimpl.cpp" line="+82"/>
        <source>Invalid URI: %1</source>
        <translation>Neplatný URI: %1</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessDebugPipeBackend</name>
    <message>
        <location filename="../../qtbase/src/network/access/qnetworkaccessdebugpipebackend.cpp" line="+194"/>
        <source>Write error writing to %1: %2</source>
        <translation>Při zápisu do souboru %1: %2 nastala chyba</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Socket error on %1: %2</source>
        <translation>Chyba socketu u %1: %2</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Remote host closed the connection prematurely on %1</source>
        <translation>Vzdálený počítač předčasně ukončil spojení s %1</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFileBackend</name>
    <message>
        <location filename="../../qtbase/src/network/access/qnetworkaccessfilebackend.cpp" line="+112"/>
        <location filename="../../qtbase/src/network/access/qnetworkreplyfileimpl.cpp" line="+71"/>
        <source>Request for opening non-local file %1</source>
        <translation>Požadavek na otevření souboru přes síť %1</translation>
    </message>
    <message>
        <location line="+51"/>
        <location filename="../../qtbase/src/network/access/qnetworkreplyfileimpl.cpp" line="+42"/>
        <source>Error opening %1: %2</source>
        <translation>%1 se nepodařilo otevřít: %2</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Write error writing to %1: %2</source>
        <translation>Při zápisu do souboru %1: %2 nastala chyba</translation>
    </message>
    <message>
        <location line="+42"/>
        <location filename="../../qtbase/src/network/access/qnetworkreplyfileimpl.cpp" line="-13"/>
        <source>Cannot open %1: Path is a directory</source>
        <translation>%1 nelze otevřít: Jedná se o adresář</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Read error reading from %1: %2</source>
        <translation>Při čtení ze souboru %1 nastala chyba: %2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFtpBackend</name>
    <message>
        <location filename="../../qtbase/src/network/access/qnetworkaccessftpbackend.cpp" line="+132"/>
        <source>No suitable proxy found</source>
        <translation>Nepodařilo se najít žádný vhodný proxy server</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot open %1: is a directory</source>
        <translation>%1 nelze otevřít: Jedná se o adresář</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>Logging in to %1 failed: authentication required</source>
        <translation>Přihlášení do %1 se nezdařilo: Je požadována autentizace</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Error while downloading %1: %2</source>
        <translation>Při stahování %1 se vyskytla chyba: %2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error while uploading %1: %2</source>
        <translation>Při nahrávání %1 se vyskytla chyba: %2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessManager</name>
    <message>
        <location filename="../../qtbase/src/network/access/qnetworkreplyimpl.cpp" line="+1141"/>
        <source>Network access is disabled.</source>
        <translation>Přístup k síti není dovolen.</translation>
    </message>
</context>
<context>
    <name>QNetworkReply</name>
    <message>
        <location filename="../../qtbase/src/network/access/qhttpthreaddelegate.cpp" line="+491"/>
        <location line="+23"/>
        <source>Error downloading %1 - server replied: %2</source>
        <translation>Při stahování %1 se vyskytla chyba - Odpověď serveru je: %2</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/access/qnetworkreplyhttpimpl.cpp" line="+1623"/>
        <location line="+228"/>
        <location filename="../../qtbase/src/network/access/qnetworkreplyimpl.cpp" line="-1048"/>
        <location line="+244"/>
        <source>Background request not allowed.</source>
        <translation>Požadavek na pozadí nepovolen.</translation>
    </message>
    <message>
        <location line="-203"/>
        <location line="+189"/>
        <location filename="../../qtbase/src/network/access/qnetworkreplyimpl.cpp" line="-219"/>
        <location line="+205"/>
        <source>Network session error.</source>
        <translation>Chyba při spojení přes síť.</translation>
    </message>
    <message>
        <location line="-181"/>
        <location filename="../../qtbase/src/network/access/qnetworkreplyimpl.cpp" line="-198"/>
        <source>backend start error.</source>
        <translation>Chyba spuštění podpůrné vrstvy.</translation>
    </message>
    <message>
        <location line="+281"/>
        <location filename="../../qtbase/src/network/access/qnetworkreplyimpl.cpp" line="+674"/>
        <source>Temporary network failure.</source>
        <translation>Síť dočasně vypadla.</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/access/qnetworkreplyimpl.cpp" line="-717"/>
        <source>Protocol &quot;%1&quot; is unknown</source>
        <translation>Protokol &quot;%1&quot; není znám</translation>
    </message>
</context>
<context>
    <name>QNetworkReplyHttpImpl</name>
    <message>
        <location filename="../../qtbase/src/network/access/qnetworkreplyhttpimpl.cpp" line="-1682"/>
        <location line="+16"/>
        <source>Operation canceled</source>
        <translation>Operace byla zrušena</translation>
    </message>
    <message>
        <location line="+394"/>
        <source>No suitable proxy found</source>
        <translation>Nepodařilo se najít žádný vhodný proxy server</translation>
    </message>
</context>
<context>
    <name>QNetworkReplyHttpImplPrivate</name>
    <message>
        <source>No suitable proxy found</source>
        <translation type="vanished">Nepodařilo se najít žádný vhodný proxy server</translation>
    </message>
</context>
<context>
    <name>QNetworkReplyImpl</name>
    <message>
        <location filename="../../qtbase/src/network/access/qnetworkreplyimpl.cpp" line="+846"/>
        <location line="+30"/>
        <source>Operation canceled</source>
        <translation>Operace byla zrušena</translation>
    </message>
</context>
<context>
    <name>QNetworkSession</name>
    <message>
        <location filename="../../qtbase/src/network/bearer/qnetworksession.cpp" line="+460"/>
        <source>Invalid configuration.</source>
        <translation>Neplatné nastavení.</translation>
    </message>
</context>
<context>
    <name>QNetworkSessionPrivateImpl</name>
    <message>
        <location filename="../../qtbase/src/plugins/bearer/qnetworksession_impl.cpp" line="+246"/>
        <source>Unknown session error.</source>
        <translation>Neznámá chyba při spojení přes síť.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The session was aborted by the user or system.</source>
        <translation>Spojení bylo zrušeno buď uživatelem nebo operačním systémem.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The requested operation is not supported by the system.</source>
        <translation>Požadovaná operace není systémem podporována.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The specified configuration cannot be used.</source>
        <translation>Zadané nastavení nelze použít.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Roaming was aborted or is not possible.</source>
        <translation>Toulání se (roaming) bylo buď zrušeno, nebo zde není možné.</translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <location filename="../../qtbase/src/sql/drivers/oci/qsql_oci.cpp" line="+2101"/>
        <source>Unable to initialize</source>
        <comment>QOCIDriver</comment>
        <translation>Spuštění se nezdařilo</translation>
    </message>
    <message>
        <location line="+146"/>
        <source>Unable to logon</source>
        <translation>Přihlášení se nezdařilo</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>Unable to begin transaction</source>
        <translation>Transakci se nepodařilo spustit</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to commit transaction</source>
        <translation>Transakci se nepodařilo zapsat</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to rollback transaction</source>
        <translation>Transakci se nepodařilo vrátit</translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <location line="-1022"/>
        <location line="+194"/>
        <location line="+15"/>
        <source>Unable to bind column for batch execute</source>
        <translation>Nepodařilo se spojit sloupec pro provedení příkazu dávkového zpracování</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to execute batch statement</source>
        <translation>Příkaz pro dávkové zpracování se nepodařilo provést</translation>
    </message>
    <message>
        <location line="+319"/>
        <source>Unable to goto next</source>
        <translation>Nelze jít k dalšímu prvku</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Unable to alloc statement</source>
        <translation>Přidělení příkazu se nepodařilo</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to prepare statement</source>
        <translation>Příkaz se nepodařilo připravit</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Unable to get statement type</source>
        <translation>Nepodařilo se získat typ příkazu</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to bind value</source>
        <translation>Hodnotu se nepodařilo spojit</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to execute statement</source>
        <translation>Příkaz se nepodařilo provést</translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <location filename="../../qtbase/src/sql/drivers/odbc/qsql_odbc.cpp" line="+1907"/>
        <source>Unable to connect</source>
        <translation>Nepodařilo se navázat spojení</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to connect - Driver doesn&apos;t support all functionality required</source>
        <translation>Nepodařilo se navázat spojení, protože ovladač nutnou funkcionalitu plně nepodporuje</translation>
    </message>
    <message>
        <location line="+282"/>
        <source>Unable to disable autocommit</source>
        <translation>&apos;autocommit&apos;, automatické zapsání, se nepodařilo zastavit</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Unable to commit transaction</source>
        <translation>Transakci se nepodařilo zapsat</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Unable to rollback transaction</source>
        <translation>Transakci se nepodařilo vrátit</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Unable to enable autocommit</source>
        <translation>&apos;autocommit&apos; se nepodařilo povolit</translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <location filename="../../qtbase/src/sql/drivers/db2/qsql_db2.cpp" line="+189"/>
        <location filename="../../qtbase/src/sql/drivers/odbc/qsql_odbc.cpp" line="-1127"/>
        <location line="+609"/>
        <source>Unable to fetch last</source>
        <translation>Poslední datový záznam se nepodařilo natáhnout</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/sql/drivers/odbc/qsql_odbc.cpp" line="-767"/>
        <location line="+339"/>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation>QODBCResult::reset: &apos;SQL_CURSOR_STATIC&apos; se nepodařilo nastavit jako příkaz vlastnosti . Ověřte, prosím, nastavení svého ODBC ovladače</translation>
    </message>
    <message>
        <location line="-329"/>
        <location line="+621"/>
        <source>Unable to execute statement</source>
        <translation>Příkaz se nepodařilo provést</translation>
    </message>
    <message>
        <location line="-567"/>
        <source>Unable to fetch</source>
        <translation>Nepodařilo se natáhnout žádná data</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Unable to fetch next</source>
        <translation>Další datový záznam se nepodařilo natáhnout</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Unable to fetch first</source>
        <translation>První datový záznam se nepodařilo natáhnout</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to fetch previous</source>
        <translation>Předchozí datový záznam se nepodařilo natáhnout</translation>
    </message>
    <message>
        <location line="+223"/>
        <source>Unable to prepare statement</source>
        <translation>Příkaz se nepodařilo spustit</translation>
    </message>
    <message>
        <location line="+273"/>
        <source>Unable to bind variable</source>
        <translation>Proměnnou se nepodařilo provést</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Could not read image data</source>
        <translation type="vanished">Nepodařilo se přečíst data obrazu</translation>
    </message>
    <message>
        <source>Sequential device (eg socket) for image read not supported</source>
        <translation type="vanished">Postupné zařízení (např. socket - komunikační kanál) pro čtení obrazu nepodporováno</translation>
    </message>
    <message>
        <source>Seek file/device for image read failed</source>
        <translation type="vanished">Vyhledání souboru/zařízení pro čtení obrazu se nezdařilo</translation>
    </message>
    <message>
        <source>Image mHeader read failed</source>
        <translation type="vanished">Čtení mHeader obrazu se nezdařilo</translation>
    </message>
    <message>
        <source>Image type not supported</source>
        <translation type="vanished">Typ obrazu nepodporován</translation>
    </message>
    <message>
        <source>Image dpeth not valid</source>
        <translation type="vanished">Hloubka obrazu neplatná</translation>
    </message>
    <message>
        <source>Could not seek to image read footer</source>
        <translation type="vanished">Nepodařilo se vyhledat zápatí čtení obrazu</translation>
    </message>
    <message>
        <source>Could not read footer</source>
        <translation type="vanished">Nepodařilo se přečíst zápatí</translation>
    </message>
    <message>
        <source>Image type (non-TrueVision 2.0) not supported</source>
        <translation type="vanished">Typ obrazu (non-TrueVision 2.0) nepodporován</translation>
    </message>
    <message>
        <source>Could not reset to read data</source>
        <translation type="vanished">Nepodařilo se nastavit znovu na čtení dat</translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <location filename="../../qtbase/src/sql/drivers/psql/qsql_psql.cpp" line="+891"/>
        <source>Unable to connect</source>
        <translation>Nepodařilo se navázat spojení</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Could not begin transaction</source>
        <translation>Transakci se nepodařilo spustit</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Could not commit transaction</source>
        <translation>Transakci se nepodařilo zapsat</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Could not rollback transaction</source>
        <translation>Transakci se nepodařilo vrátit</translation>
    </message>
    <message>
        <location line="+391"/>
        <source>Unable to subscribe</source>
        <translation>Registrace se nezdařila</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Unable to unsubscribe</source>
        <translation>Registraci se nepodařilo zrušit</translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <location line="-1168"/>
        <source>Unable to create query</source>
        <translation>Nepodařilo se vytvořit žádný dotaz</translation>
    </message>
    <message>
        <location line="+371"/>
        <source>Unable to prepare statement</source>
        <translation>Příkaz se nepodařilo připravit</translation>
    </message>
</context>
<context>
    <name>QPageSetupWidget</name>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qpagesetupwidget.ui"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
    <message>
        <location/>
        <source>Paper</source>
        <translation>Papír</translation>
    </message>
    <message>
        <location/>
        <source>Page size:</source>
        <translation>Velikost stran:</translation>
    </message>
    <message>
        <location/>
        <source>Width:</source>
        <translation>Šířka:</translation>
    </message>
    <message>
        <location/>
        <source>Height:</source>
        <translation>Výška:</translation>
    </message>
    <message>
        <location/>
        <source>Paper source:</source>
        <translation>Zdroj papíru:</translation>
    </message>
    <message>
        <location/>
        <source>Orientation</source>
        <translation>Zaměření</translation>
    </message>
    <message>
        <location/>
        <source>Portrait</source>
        <translation>Formát na výšku</translation>
    </message>
    <message>
        <location/>
        <source>Landscape</source>
        <translation>Formát na šířku</translation>
    </message>
    <message>
        <location/>
        <source>Reverse landscape</source>
        <translation>Obrácený formát na šířku</translation>
    </message>
    <message>
        <location/>
        <source>Reverse portrait</source>
        <translation>Obrácený formát na výšku</translation>
    </message>
    <message>
        <location/>
        <source>Margins</source>
        <translation>Okraje</translation>
    </message>
    <message>
        <location/>
        <source>top margin</source>
        <translation>Horní okraj</translation>
    </message>
    <message>
        <location/>
        <source>left margin</source>
        <translation>Levý okraj</translation>
    </message>
    <message>
        <location/>
        <source>right margin</source>
        <translation>Pravý okraj</translation>
    </message>
    <message>
        <location/>
        <source>bottom margin</source>
        <translation>Dolní okraj</translation>
    </message>
    <message>
        <source>Centimeters (cm)</source>
        <translation type="vanished">Centimetry (cm)</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qpagesetupdialog_unix.cpp" line="+277"/>
        <source>Millimeters (mm)</source>
        <translation>Milimetry (mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Inches (in)</source>
        <translation>Palce (in)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Points (pt)</source>
        <translation>Body (pt)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pica (P̸)</source>
        <translation>Pica (P̸)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Didot (DD)</source>
        <translation>Didot (DD)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cicero (CC)</source>
        <translation>Cicero (CC)</translation>
    </message>
    <message>
        <location line="+64"/>
        <location line="+10"/>
        <source>Custom</source>
        <translation>Stanovený uživatelem</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>mm</source>
        <extracomment>Unit &apos;Millimeter&apos;</extracomment>
        <translation>mm</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>pt</source>
        <extracomment>Unit &apos;Points&apos;</extracomment>
        <translation>pt</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>in</source>
        <extracomment>Unit &apos;Inch&apos;</extracomment>
        <translation>in</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>P̸</source>
        <extracomment>Unit &apos;Pica&apos;</extracomment>
        <translation>P̸</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>DD</source>
        <extracomment>Unit &apos;Didot&apos;</extracomment>
        <translation>DD</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>CC</source>
        <extracomment>Unit &apos;Cicero&apos;</extracomment>
        <translation>CC</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qpagesetupwidget.ui"/>
        <source>Page Layout</source>
        <translation>Rozvržení stran</translation>
    </message>
    <message>
        <location/>
        <source>Page order:</source>
        <translation>Pořadí stran:</translation>
    </message>
    <message>
        <location/>
        <source>Pages per sheet:</source>
        <translation>Stran na list:</translation>
    </message>
</context>
<context>
    <name>QPageSize</name>
    <message>
        <location filename="../../qtbase/src/gui/painting/qpagesize.cpp" line="+474"/>
        <source>Custom (%1mm x %2mm)</source>
        <extracomment>Custom size name in millimeters</extracomment>
        <translation>Vlastní (%1 mm x %2 mm)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Custom (%1pt x %2pt)</source>
        <extracomment>Custom size name in points</extracomment>
        <translation>Vlastní (%1 pt x %2 pt)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Custom (%1in x %2in)</source>
        <extracomment>Custom size name in inches</extracomment>
        <translation>Vlastní (%1 in x %2 in)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Custom (%1pc x %2pc)</source>
        <extracomment>Custom size name in picas</extracomment>
        <translation>Vlastní (%1 pc x %2 pc)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Custom (%1DD x %2DD)</source>
        <extracomment>Custom size name in didots</extracomment>
        <translation>Vlastní (%1 DD x %2 DD)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Custom (%1CC x %2CC)</source>
        <extracomment>Custom size name in ciceros</extracomment>
        <translation>Vlastní (%1 CC x %2 CC)</translation>
    </message>
    <message>
        <location line="+989"/>
        <source>%1 x %2 in</source>
        <extracomment>Page size in &apos;Inch&apos;.</extracomment>
        <translation>%1 x %2 in</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>A0</source>
        <translation>A0</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A1</source>
        <translation>A1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A2</source>
        <translation>A2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A6</source>
        <translation>A6</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A7</source>
        <translation>A7</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A8</source>
        <translation>A8</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A9</source>
        <translation>A9</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A10</source>
        <translation>A10</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B0</source>
        <translation>B0</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B1</source>
        <translation>B1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B2</source>
        <translation>B2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B3</source>
        <translation>B3</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B7</source>
        <translation>B7</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B8</source>
        <translation>B8</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B9</source>
        <translation>B9</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B10</source>
        <translation>B10</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Executive (7.5 x 10 in)</source>
        <translation>Executive (7.5 x 10 in)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Executive (7.25 x 10.5 in)</source>
        <translation>Executive (7.25 x 10.5 in)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Folio (8.27 x 13 in)</source>
        <translation>Folio (8.27 x 13 in)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Legal</source>
        <translation>US Legal</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Letter / ANSI A</source>
        <translation>Letter / ANSI A</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tabloid / ANSI B</source>
        <translation>Tabloid / ANSI B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ledger / ANSI B</source>
        <translation>Ledger / ANSI B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Custom</source>
        <translation>Vlastní</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A3 Extra</source>
        <translation>A3 Extra</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A4 Extra</source>
        <translation>A4 Extra</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A4 Plus</source>
        <translation>A4 Plus</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A4 Small</source>
        <translation>A4 malý</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A5 Extra</source>
        <translation>A5 Extra</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>B5 Extra</source>
        <translation>B5 Extra</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>JIS B0</source>
        <translation>JIS B0</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>JIS B1</source>
        <translation>JIS B1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>JIS B2</source>
        <translation>JIS B2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>JIS B3</source>
        <translation>JIS B3</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>JIS B4</source>
        <translation>JIS B4</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>JIS B5</source>
        <translation>JIS B5</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>JIS B6</source>
        <translation>JIS B6</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>JIS B7</source>
        <translation>JIS B7</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>JIS B8</source>
        <translation>JIS B8</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>JIS B9</source>
        <translation>JIS B9</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>JIS B10</source>
        <translation>JIS B10</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>ANSI C</source>
        <translation>ANSI C</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>ANSI D</source>
        <translation>ANSI D</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>ANSI E</source>
        <translation>ANSI E</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Legal Extra</source>
        <translation>Legal Extra</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Letter Extra</source>
        <translation>Letter Extra</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Letter Plus</source>
        <translation>Letter Plus</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Letter Small</source>
        <translation>Letter malý</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tabloid Extra</source>
        <translation>Tabloid Extra</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Architect A</source>
        <translation>Architect A</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Architect B</source>
        <translation>Architect B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Architect C</source>
        <translation>Architect C</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Architect D</source>
        <translation>Architect D</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Architect E</source>
        <translation>Architect E</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Quarto</source>
        <translation>Kvartový formát</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Statement</source>
        <translation>Statement</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Super A</source>
        <translation>Super A</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Super B</source>
        <translation>Super B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Postcard</source>
        <translation>Korespondenční lístek</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Double Postcard</source>
        <translation>Dvojitý korespondenční lístek</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PRC 16K</source>
        <translation>PRC 16K</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PRC 32K</source>
        <translation>PRC 32K</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PRC 32K Big</source>
        <translation>PRC 32K velký</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fan-fold US (14.875 x 11 in)</source>
        <translation>Fan-fold US (14.875 x 11 in)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fan-fold German (8.5 x 12 in)</source>
        <translation>Fan-fold německý (8.5 x 12 in)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fan-fold German Legal (8.5 x 13 in)</source>
        <translation>Fan-fold německý Legal (8.5 x 13 in)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope B4</source>
        <translation>Obálka B4</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope B5</source>
        <translation>Obálka B5</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope B6</source>
        <translation>Obálka B6</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope C0</source>
        <translation>Obálka C0</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope C1</source>
        <translation>Obálka C1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope C2</source>
        <translation>Obálka C2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope C3</source>
        <translation>Obálka C3</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope C4</source>
        <translation>Obálka C4</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope C5</source>
        <translation>Obálka C5</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope C6</source>
        <translation>Obálka C6</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope C65</source>
        <translation>Obálka C65</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope C7</source>
        <translation>Obálka C7</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope DL</source>
        <translation>Obálka DL</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope US 9</source>
        <translation>Obálka US 9</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope US 10</source>
        <translation>Obálka US 10</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope US 11</source>
        <translation>Obálka US 11</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope US 12</source>
        <translation>Obálka US 12</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope US 14</source>
        <translation>Obálka US 14</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope Monarch</source>
        <translation>Obálka Monarch</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope Personal</source>
        <translation>Obálka osobní</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope Chou 3</source>
        <translation>Obálka růžička 3</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope Chou 4</source>
        <translation>Obálka růžička 4</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope Invite</source>
        <translation>Obálka pozvánka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope Italian</source>
        <translation>Obálka italská</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope Kaku 2</source>
        <translation>Obálka Kaku 2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope Kaku 3</source>
        <translation>Obálka Kaku 3</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope PRC 1</source>
        <translation>Obálka PRC 1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope PRC 2</source>
        <translation>Obálka PRC 2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope PRC 3</source>
        <translation>Obálka PRC 3</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope PRC 4</source>
        <translation>Obálka PRC 4</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope PRC 5</source>
        <translation>Obálka PRC 5</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope PRC 6</source>
        <translation>Obálka PRC 6</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope PRC 7</source>
        <translation>Obálka PRC 7</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope PRC 8</source>
        <translation>Obálka PRC 8</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope PRC 9</source>
        <translation>Obálka PRC 9</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope PRC 10</source>
        <translation>Obálka PRC 10</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Envelope You 4</source>
        <translation>Obálka ty 4</translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <location filename="../../qtbase/src/gui/kernel/qplatformtheme.cpp" line="+656"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save All</source>
        <translation>Uložit vše</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open</source>
        <translation>Otevřít</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ano</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes to &amp;All</source>
        <translation>Ano, &amp;vše</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;No</source>
        <translation>&amp;Ne</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>N&amp;o to All</source>
        <translation>N&amp;e, žádné</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Abort</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Retry</source>
        <translation>Opakovat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore</source>
        <translation>Ignorovat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Discard</source>
        <translation>Zahodit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Apply</source>
        <translation>Použít</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset</source>
        <translation>Vrátit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Restore Defaults</source>
        <translation>Obnovit výchozí</translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <location filename="../../qtbase/src/corelib/plugin/qpluginloader.cpp" line="+259"/>
        <source>The plugin was not loaded.</source>
        <translation>Přídavný modul nebyl nahrán.</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qabstractprintdialog.cpp" line="+106"/>
        <location line="+16"/>
        <location filename="../../qtbase/src/printsupport/dialogs/qprintdialog_win.cpp" line="+258"/>
        <source>Print</source>
        <translation>Tisk</translation>
    </message>
    <message>
        <source>A0</source>
        <translation type="vanished">A0</translation>
    </message>
    <message>
        <source>A1</source>
        <translation type="vanished">A1</translation>
    </message>
    <message>
        <source>A2</source>
        <translation type="vanished">A2</translation>
    </message>
    <message>
        <source>A3</source>
        <translation type="vanished">A3</translation>
    </message>
    <message>
        <source>A4</source>
        <translation type="vanished">A4</translation>
    </message>
    <message>
        <source>A5</source>
        <translation type="vanished">A5</translation>
    </message>
    <message>
        <source>A6</source>
        <translation type="vanished">A6</translation>
    </message>
    <message>
        <source>A7</source>
        <translation type="vanished">A7</translation>
    </message>
    <message>
        <source>A8</source>
        <translation type="vanished">A8</translation>
    </message>
    <message>
        <source>A9</source>
        <translation type="vanished">A9</translation>
    </message>
    <message>
        <source>B0</source>
        <translation type="vanished">B0</translation>
    </message>
    <message>
        <source>B1</source>
        <translation type="vanished">B1</translation>
    </message>
    <message>
        <source>B2</source>
        <translation type="vanished">B2</translation>
    </message>
    <message>
        <source>B3</source>
        <translation type="vanished">B3</translation>
    </message>
    <message>
        <source>B4</source>
        <translation type="vanished">B4</translation>
    </message>
    <message>
        <source>B5</source>
        <translation type="vanished">B5</translation>
    </message>
    <message>
        <source>B6</source>
        <translation type="vanished">B6</translation>
    </message>
    <message>
        <source>B7</source>
        <translation type="vanished">B7</translation>
    </message>
    <message>
        <source>B8</source>
        <translation type="vanished">B8</translation>
    </message>
    <message>
        <source>B9</source>
        <translation type="vanished">B9</translation>
    </message>
    <message>
        <source>B10</source>
        <translation type="vanished">B10</translation>
    </message>
    <message>
        <source>C5E</source>
        <translation type="vanished">C5E</translation>
    </message>
    <message>
        <source>DLE</source>
        <translation type="vanished">DLE</translation>
    </message>
    <message>
        <source>Executive</source>
        <translation type="vanished">US Executive</translation>
    </message>
    <message>
        <source>Folio</source>
        <translation type="vanished">Folio</translation>
    </message>
    <message>
        <source>Ledger</source>
        <translation type="vanished">US Ledger</translation>
    </message>
    <message>
        <source>Legal</source>
        <translation type="vanished">US Legal</translation>
    </message>
    <message>
        <source>Letter</source>
        <translation type="vanished">US Letter</translation>
    </message>
    <message>
        <source>Tabloid</source>
        <translation type="vanished">US Tabloid</translation>
    </message>
    <message>
        <source>US Common #10 Envelope</source>
        <translation type="vanished">US běžná #10 obálka</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="vanished">Stanovený uživatelem</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qpagesetupdialog_unix.cpp" line="-136"/>
        <source>Left to Right, Top to Bottom</source>
        <translation>Zleva doprava, shora dolů</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Left to Right, Bottom to Top</source>
        <translation>Zleva doprava, zdola nahoru</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Right to Left, Bottom to Top</source>
        <translation>Zprava doleva, zdola nahoru</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Right to Left, Top to Bottom</source>
        <translation>Zprava doleva, shora dolů</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bottom to Top, Left to Right</source>
        <translation>Zdola nahoru, zleva doprava</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bottom to Top, Right to Left</source>
        <translation>Zdola nahoru, zprava doleva</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Top to Bottom, Left to Right</source>
        <translation>Shora dolů, zleva doprava</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Top to Bottom, Right to Left</source>
        <translation>Shora dolů, zprava doleva</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>1 (1x1)</source>
        <translation>1 (1x1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>2 (2x1)</source>
        <translation>2 (2x1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>4 (2x2)</source>
        <translation>4 (2x2)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>6 (2x3)</source>
        <translation>6 (2x3)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>9 (3x3)</source>
        <translation>9 (3x3)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>16 (4x4)</source>
        <translation>16 (4x4)</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qprintdialog_unix.cpp" line="+316"/>
        <source>All Pages</source>
        <translation>Všechny strany</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Odd Pages</source>
        <translation>Liché strany</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Even Pages</source>
        <translation>Sudé strany</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+161"/>
        <source>&amp;Options &gt;&gt;</source>
        <translation>&amp;Nastavení &gt;&gt;</translation>
    </message>
    <message>
        <location line="-156"/>
        <source>&amp;Print</source>
        <translation>&amp;Tisk</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>&amp;Options &lt;&lt;</source>
        <translation>&amp;Nastavení &lt;&lt; </translation>
    </message>
    <message>
        <location line="+211"/>
        <source>Print to File (PDF)</source>
        <translation>Tisk do souboru (PDF)</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Local file</source>
        <translation>Místní soubor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write PDF file</source>
        <translation>Zapsat soubor PDF</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Print To File ...</source>
        <translation>Tisk do souboru...</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>%1 is a directory.
Please choose a different file name.</source>
        <translation>%1 je adresář.
Zvolte, prosím, pro soubor jiný název.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation>Soubor %1 je chráněn proti zápisu.
Zvolte, prosím, pro soubor jiný název.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation>Soubor %1 již existuje.
Má se přepsat?</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Options &apos;Pages Per Sheet&apos; and &apos;Page Set&apos; cannot be used together.
Please turn one of those options off.</source>
        <translation>Volby &amp;apos;Stran na list&amp;apos; a &amp;apos;Sada stran&amp;apos; nelze použít zároveň.
Vypněte, prosím, jednu z voleb.</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qprintdialog_win.cpp" line="+1"/>
        <source>The &apos;From&apos; value cannot be greater than the &apos;To&apos; value.</source>
        <translation>Údaj pro první stranu nesmí být větší než údaj pro poslední stranu.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/printsupport/kernel/qplatformprintdevice.cpp" line="+298"/>
        <location line="+20"/>
        <source>Automatic</source>
        <translation>Automaticky</translation>
    </message>
</context>
<context>
    <name>QPrintPreviewDialog</name>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qpagesetupdialog_unix.cpp" line="+295"/>
        <location line="+8"/>
        <location filename="../../qtbase/src/printsupport/dialogs/qpagesetupdialog_win.cpp" line="+49"/>
        <location line="+7"/>
        <source>Page Setup</source>
        <translation>Nastavení strany</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qprintpreviewdialog.cpp" line="+250"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>Print Preview</source>
        <translation>Náhled tisku</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Next page</source>
        <translation>Další strana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous page</source>
        <translation>Předchozí strana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First page</source>
        <translation>První strana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last page</source>
        <translation>Poslední strana</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Fit width</source>
        <translation>Přizpůsobit šířku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fit page</source>
        <translation>Přizpůsobit stranu</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Zoom in</source>
        <translation>Zvětšit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom out</source>
        <translation>Zmenšit</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Portrait</source>
        <translation>Formát na výšku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Landscape</source>
        <translation>Formát na šířku</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Show single page</source>
        <translation>Ukázat jednotlivé strany</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show facing pages</source>
        <translation>Ukázat strany ležící naproti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show overview of all pages</source>
        <translation>Ukázat přehled všech stran</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Print</source>
        <translation>Tisk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page setup</source>
        <translation>Nastavení strany</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Export to PDF</source>
        <translation>Exportovat do PDF</translation>
    </message>
    <message>
        <source>Export to PostScript</source>
        <translation type="vanished">Exportovat do PostScriptu</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesDialog</name>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qprintdialog_unix.cpp" line="-637"/>
        <source>Job Options</source>
        <translation>Volby pro úlohy</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesWidget</name>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qprintpropertieswidget.ui"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
    <message>
        <location/>
        <source>Page</source>
        <translation>Strana</translation>
    </message>
</context>
<context>
    <name>QPrintSettingsOutput</name>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qprintsettingsoutput.ui"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
    <message>
        <location/>
        <source>Copies</source>
        <translation>Počet exemplářů</translation>
    </message>
    <message>
        <location/>
        <source>Print range</source>
        <translation>Tisk oblasti</translation>
    </message>
    <message>
        <location/>
        <source>Print all</source>
        <translation>Tisknout vše</translation>
    </message>
    <message>
        <location/>
        <source>Pages from</source>
        <translation>Strany od</translation>
    </message>
    <message>
        <location/>
        <source>to</source>
        <translation>do</translation>
    </message>
    <message>
        <location/>
        <source>Current Page</source>
        <translation>Nynější strana</translation>
    </message>
    <message>
        <location/>
        <source>Selection</source>
        <translation>Výběr</translation>
    </message>
    <message>
        <location/>
        <source>Output Settings</source>
        <translation>Nastavení výstupu</translation>
    </message>
    <message>
        <location/>
        <source>Copies:</source>
        <translation>Počet exemplářů:</translation>
    </message>
    <message>
        <location/>
        <source>Collate</source>
        <translation>Srovnat</translation>
    </message>
    <message>
        <location/>
        <source>Reverse</source>
        <translation>Obrácený</translation>
    </message>
    <message>
        <location/>
        <source>Options</source>
        <translation>Volby</translation>
    </message>
    <message>
        <location/>
        <source>Color Mode</source>
        <translation>Barevný režim</translation>
    </message>
    <message>
        <location/>
        <source>Color</source>
        <translation>Barva</translation>
    </message>
    <message>
        <location/>
        <source>Grayscale</source>
        <translation>Odstíny šedi</translation>
    </message>
    <message>
        <location/>
        <source>Duplex Printing</source>
        <translation>Oboustranný tisk</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>Žádný</translation>
    </message>
    <message>
        <location/>
        <source>Long side</source>
        <translation>Dlouhá strana</translation>
    </message>
    <message>
        <location/>
        <source>Short side</source>
        <translation>Krátká strana</translation>
    </message>
    <message>
        <location/>
        <source>Page Set:</source>
        <translation>Sada stran:</translation>
    </message>
</context>
<context>
    <name>QPrintWidget</name>
    <message>
        <location filename="../../qtbase/src/printsupport/dialogs/qprintwidget.ui"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
    <message>
        <location/>
        <source>Printer</source>
        <translation>Tiskárna</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Name:</source>
        <translation>&amp;Název:</translation>
    </message>
    <message>
        <location/>
        <source>P&amp;roperties</source>
        <translation>&amp;Vlastnosti</translation>
    </message>
    <message>
        <location/>
        <source>Location:</source>
        <translation>Umístění:</translation>
    </message>
    <message>
        <location/>
        <source>Preview</source>
        <translation>Náhled</translation>
    </message>
    <message>
        <location/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location/>
        <source>Output &amp;file:</source>
        <translation>Výstupní &amp;soubor:</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QProcess</name>
    <message>
        <location filename="../../qtbase/src/corelib/io/qprocess.cpp" line="+923"/>
        <source>Error reading from process</source>
        <translation>Čtení z procesu se nezdařilo</translation>
    </message>
    <message>
        <location line="+86"/>
        <location line="+923"/>
        <source>Error writing to process</source>
        <translation>Zápis do procesu se nezdařil</translation>
    </message>
    <message>
        <location line="-852"/>
        <source>Process crashed</source>
        <translation>Proces spadl</translation>
    </message>
    <message>
        <location line="+1114"/>
        <source>No program defined</source>
        <translation>Nestanoven žádný program</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/io/qprocess_unix.cpp" line="+429"/>
        <location filename="../../qtbase/src/corelib/io/qprocess_win.cpp" line="+223"/>
        <source>Could not open input redirection for reading</source>
        <translation>Vstupní přesměrování se nepodařilo otevřít pro čtení</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../../qtbase/src/corelib/io/qprocess_win.cpp" line="+20"/>
        <source>Could not open output redirection for writing</source>
        <translation>Výstupní přesměrování se nepodařilo otevřít pro zápis</translation>
    </message>
    <message>
        <location line="+271"/>
        <source>Resource error (fork failure): %1</source>
        <translation>Potíže se zdroji (selhání rozcestí - &quot;fork failure&quot;): %1</translation>
    </message>
    <message>
        <location line="+337"/>
        <location line="+71"/>
        <location line="+82"/>
        <location line="+74"/>
        <location filename="../../qtbase/src/corelib/io/qprocess_win.cpp" line="+370"/>
        <location line="+60"/>
        <location line="+68"/>
        <location line="+39"/>
        <location line="+46"/>
        <location filename="../../qtbase/src/corelib/io/qprocess_wince.cpp" line="+214"/>
        <location line="+41"/>
        <source>Process operation timed out</source>
        <translation>Překročení času u procesu</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/io/qprocess_win.cpp" line="-314"/>
        <source>Process failed to start: %1</source>
        <translation>Proces se nepodařilo spustit: %1</translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qprogressdialog.cpp" line="+177"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>QQnxFileDialogHelper</name>
    <message>
        <location filename="../../qtbase/src/plugins/platforms/qnx/qqnxfiledialoghelper_playbook.cpp" line="+190"/>
        <source>CANCEL</source>
        <translation>ZRUŠIT</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/plugins/platforms/qnx/qqnxfiledialoghelper_bb10.cpp" line="+103"/>
        <source>All files (*.*)</source>
        <translation>Všechny soubory (*.*)</translation>
    </message>
</context>
<context>
    <name>QQnxFilePicker</name>
    <message>
        <location filename="../../qtbase/src/plugins/platforms/qnx/qqnxfilepicker.cpp" line="+64"/>
        <source>Pick a file</source>
        <translation>Zvolte soubor</translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <location filename="../../qtbase/src/corelib/tools/qregexp.cpp" line="+58"/>
        <source>no error occurred</source>
        <translation>žádná chyba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>disabled feature used</source>
        <translation>byla použita zakázaná vlastnost</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad char class syntax</source>
        <translation>nesprávná syntax pro třídu znaků</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad lookahead syntax</source>
        <translation>nesprávná syntax pro dopředný výrok (lookahead)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>lookbehinds not supported, see QTBUG-2371</source>
        <translation>zpětné výroky (lookbehind) nejsou podporovány, viz QTBUG-2371</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad repetition syntax</source>
        <translation>nesprávná syntax pro opakování</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid octal value</source>
        <translation>neplatná osmičková hodnota</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing left delim</source>
        <translation>chybějící levý oddělovač</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end</source>
        <translation>neočekávaný konec</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>met internal limit</source>
        <translation>dosažena vnitřní mez</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid interval</source>
        <translation>neplatný interval</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid category</source>
        <translation>neplatná kategorie</translation>
    </message>
</context>
<context>
    <name>QRegularExpression</name>
    <message>
        <location filename="../../qtbase/src/corelib/tools/qregularexpression.cpp" line="+1681"/>
        <location line="+895"/>
        <source>no error</source>
        <translation>žádná chyba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>\ at end of pattern</source>
        <translation>\ na konci vzoru</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>\c at end of pattern</source>
        <translation>\c na konci vzoru</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unrecognized character follows \</source>
        <translation>Nerozpoznaný znak následuje \</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>numbers out of order in {} quantifier</source>
        <translatorcomment>operátor ?</translatorcomment>
        <translation>Nesprávné pořadí čísel v kvantifikátoru {}</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>number too big in {} quantifier</source>
        <translation>Příliš velké číslo v kvantifikátoru {}</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing terminating ] for character class</source>
        <translation>Chybějící ukončení ] pro třídu znaků</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid escape sequence in character class</source>
        <translation>Neplatná úniková posloupnost ve třídě znaků</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>range out of order in character class</source>
        <translation>Nesprávné pořadí mezí rozsahu ve třídě znaků</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>nothing to repeat</source>
        <translation>Nic k opakování</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal error: unexpected repeat</source>
        <translation>Vnitřní chyba: neočekávané opakování</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unrecognized character after (? or (?-</source>
        <translation>Nerozpoznaný znak po (? nebo (?-</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>POSIX named classes are supported only within a class</source>
        <translation>Pojmenované třídy POSIX jsou podporovány jen uvnitř třídy</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing )</source>
        <translation>Chybí )</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>reference to non-existent subpattern</source>
        <translation>Odkaz na neexistující podvzor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>erroffset passed as NULL</source>
        <translation>erroffset by předán jako NULL</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unknown option bit(s) set</source>
        <translation>Nastavena neznámá volba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing ) after comment</source>
        <translation>Chybí ) po poznámce</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>regular expression is too large</source>
        <translation>Regulární výraz je příliš velký</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>failed to get memory</source>
        <translation>Nepodařilo se získat paměť</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unmatched parentheses</source>
        <translation>Nespářené závorky</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal error: code overflow</source>
        <translation>Vnitřní chyba: přetečení kódu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unrecognized character after (?&lt;</source>
        <translation>Nerozpoznaný znak po (?&lt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>lookbehind assertion is not fixed length</source>
        <translation>Zpětný výrok (lookbehind) není pevné délky</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>malformed number or name after (?(</source>
        <translation>Chybné číslo nebo název po (?(</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>conditional group contains more than two branches</source>
        <translation>Závislá skupina obsahuje více než dvě větve</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>assertion expected after (?(</source>
        <translation>Výrok očekáván po (?(</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(?R or (?[+-]digits must be followed by )</source>
        <translation>Číslice (?R nebo (?[+-] musí následovat )</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unknown POSIX class name</source>
        <translation>Neznámý název třídy POSIX</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>POSIX collating elements are not supported</source>
        <translatorcomment>collating ?</translatorcomment>
        <translation>Prvky řazení POSIXu nejsou podporovány</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>this version of PCRE is not compiled with PCRE_UTF8 support</source>
        <translation>Tato verze PCRE není sestavena s podporou PCRE_UTF8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>character value in \x{...} sequence is too large</source>
        <translation>Hodnota znaku v sekvenci \x{...}  je příliš velká</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid condition (?(0)</source>
        <translation>Neplatná podmínka (?(0)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>\C not allowed in lookbehind assertion</source>
        <translation>\C nepovoleno v tvrzení zpětného výroku (lookbehind)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PCRE does not support \L, \l, \N{name}, \U, or \u</source>
        <translation>PCRE nepodporuje \L, \l, \N{name}, \U, nebo \u</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>number after (?C is &gt; 255</source>
        <translation>Číslo po (?C je &gt; 255</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>closing ) for (?C expected</source>
        <translation>Očekávána uzavírající ) pro (?C</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>recursive call could loop indefinitely</source>
        <translation>Rekurzivní volání by mohlo zůstat ve smyčce neurčitě dlouho</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unrecognized character after (?P</source>
        <translation>Nerozpoznaný znak po (?P</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>syntax error in subpattern name (missing terminator)</source>
        <translation>Chyba ve skladbě názvu podvzoru (chybějící ukončení)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>two named subpatterns have the same name</source>
        <translation>Dva pojmenované podvzory mají stejný název</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid UTF-8 string</source>
        <translation>Neplatný řetězec UTF-8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>support for \P, \p, and \X has not been compiled</source>
        <translation>Qt bylo sestaveno bez podpory pro \P, \p a \X</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>malformed \P or \p sequence</source>
        <translation>Chybně utvořená sekvence \P nebo \p</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unknown property name after \P or \p</source>
        <translation>Neznámý název vlastnosti po \P nebo \p</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>subpattern name is too long (maximum 32 characters)</source>
        <translation>Název podvzoru je příliš dlouhý (nejvíce může mít 32 znaků)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>too many named subpatterns (maximum 10000)</source>
        <translation>Příliš mnoho pojmenovaných podvzorů (nejvíce je 10000)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>octal value is greater than \377 (not in UTF-8 mode)</source>
        <translation>Osmičková hodnota je větší než \377 (ne v režimu UTF-8)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal error: overran compiling workspace</source>
        <translation>Vnitřní chyba: Přeběh při sestavování</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal error: previously-checked referenced subpattern not found</source>
        <translation>Vnitřní chyba: Předtím načtený odkazovaný podvzor nenalezen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DEFINE group contains more than one branch</source>
        <translation>Skupina DEFINE obsahuje více než jednu větev</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>repeating a DEFINE group is not allowed</source>
        <translation>Opakování skupiny DEFINE není povoleno</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>inconsistent NEWLINE options</source>
        <translation>Nekonzistentní volby NEWLINE</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>\g is not followed by a braced, angle-bracketed, or quoted name/number or by a plain number</source>
        <translation>\g není následováno názvem/číslem ve složených či špičatých závorkách anebo uvozovkách, ani prostým číslem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>a numbered reference must not be zero</source>
        <translation>Číslovaný odkaz nesmí být nulový</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>an argument is not allowed for (*ACCEPT), (*FAIL), or (*COMMIT)</source>
        <translation>Argument není povolen pro (*ACCEPT), (*FAIL), nebo (*COMMIT)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(*VERB) not recognized</source>
        <translation>(*VERB) nerozpoznán</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>number is too big</source>
        <translation>Číslo je příliš velké</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>subpattern name expected</source>
        <translation>Očekáván název podvzoru</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>digit expected after (?+</source>
        <translation>Po (?+ očekávána číslice</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>] is an invalid data character in JavaScript compatibility mode</source>
        <translation>] je neplatný datový znak v režimu kompatibility JavaScriptu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>different names for subpatterns of the same number are not allowed</source>
        <translation>Odlišné názvy podvzorů téhož čísla nejsou povoleny</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(*MARK) must have an argument</source>
        <translation>(*MARK) musí mít argument</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>this version of PCRE is not compiled with PCRE_UCP support</source>
        <translation>Tato verze PCRE není sestavena s podporou PCRE_UCP</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>\c must be followed by an ASCII character</source>
        <translation>\c musí být následováno znakem ASCII</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>\k is not followed by a braced, angle-bracketed, or quoted name</source>
        <translation>\k není následováno názvem ve složených či špičatých závorkách anebo uvozovkách</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal error: unknown opcode in find_fixedlength()</source>
        <translation>Vnitřní chyba: neznámý opcode v find_fixedlength()</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>\N is not supported in a class</source>
        <translation>\N není ve třídě podporováno</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>too many forward references</source>
        <translation>Příliš mnoho dopředných odkazů</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>disallowed Unicode code point (&gt;= 0xd800 &amp;&amp; &lt;= 0xdfff)</source>
        <translation>Nedovolený kódový bod Unicode (&gt;= 0xd800 &amp;&amp; &lt;= 0xdfff)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid UTF-16 string</source>
        <translation>Neplatný řetězec UTF-16</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>name is too long in (*MARK), (*PRUNE), (*SKIP), or (*THEN)</source>
        <translation>Název je příliš dlouhý v (*MARK), (*PRUNE), (*SKIP), nebo (*THEN)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>character value in \u.... sequence is too large</source>
        <translation>Hodnota znaku v sekvenci \u....  je příliš velká</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid UTF-32 string</source>
        <translation type="unfinished">Neplatný řetězec UTF-16 {32 ?}</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>setting UTF is disabled by the application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>non-hex character in \x{} (closing brace missing?)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>non-octal character in \o{} (closing brace missing?)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing opening brace after \o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>parentheses are too deeply nested</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid range in character class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>group name must start with a non-digit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>parentheses are too deeply nested (stack check)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <location filename="../../qtbase/src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="+420"/>
        <source>Error opening database</source>
        <translation>Nepodařilo se otevřít spojení s databází</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Unable to begin transaction</source>
        <translation>Transakci se nepodařilo spustit</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Unable to commit transaction</source>
        <translation>Transakci se nepodařilo zapsat</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Unable to rollback transaction</source>
        <translation>Transakci se nepodařilo vrátit</translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <location line="-329"/>
        <source>Unable to fetch results</source>
        <translation>Výsledek se nepodařilo natáhnout</translation>
    </message>
    <message>
        <location line="+144"/>
        <source>Unable to execute statement</source>
        <translation>Příkaz se nepodařilo provést</translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <location filename="../../qtbase/src/sql/drivers/sqlite/qsql_sqlite.cpp" line="+631"/>
        <source>Error opening database</source>
        <translation>Nepodařilo se otevřít spojení s databází</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Error closing database</source>
        <translation>Nepodařilo se uzavřít spojení s datatabází</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to begin transaction</source>
        <translation>Transakci se nepodařilo spustit</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to commit transaction</source>
        <translation>Transakci se nepodařilo zapsat</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to rollback transaction</source>
        <translation>Transakci se nepodařilo vrátit</translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <location line="-439"/>
        <location line="+63"/>
        <location line="+8"/>
        <source>Unable to fetch row</source>
        <translation>Řádek se nepodařilo natáhnout</translation>
    </message>
    <message>
        <location line="-70"/>
        <source>No query</source>
        <translation>Žádný požadavek</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Unable to execute statement</source>
        <translation>Příkaz se nepodařilo provést</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unable to execute multiple statements at a time</source>
        <translation>Více příkazů naráz se nepodařilo provést</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to reset statement</source>
        <translation>Příkaz se nepodařilo znovu nastavit</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Unable to bind parameters</source>
        <translation>Parametry se nepodařilo spojit</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Parameter count mismatch</source>
        <translation>Počet parametrů není správný</translation>
    </message>
</context>
<context>
    <name>QSaveFile</name>
    <message>
        <location filename="../../qtbase/src/corelib/io/qsavefile.cpp" line="+196"/>
        <source>Existing file %1 is not writable</source>
        <translation>Stávající soubor %1 není zapisovatelný</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Filename refers to a directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+132"/>
        <source>Writing canceled by application</source>
        <translation>Zápis zrušen programem</translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qscrollbar.cpp" line="+414"/>
        <source>Scroll here</source>
        <translation>Projíždět až sem</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Left edge</source>
        <translation>Levý okraj</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>Začátek</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>Pravý okraj</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>Konec</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Page left</source>
        <translation>O stranu doleva</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>O stranu nahoru</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>O stranu doprava</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>O stranu dolů</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scroll left</source>
        <translation>Projíždět doleva</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>Projíždět nahoru</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>Projíždět doprava</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>Projíždět dolů</translation>
    </message>
</context>
<context>
    <name>QSharedMemory</name>
    <message>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory.cpp" line="+245"/>
        <source>%1: unable to set key on lock</source>
        <translation>%1: Nepodařilo se nastavit klíč při uzamknutí</translation>
    </message>
    <message>
        <location line="+100"/>
        <source>%1: create size is less then 0</source>
        <translation>%1: Údaj o velikosti vytvoření je menší než nula</translation>
    </message>
    <message>
        <location line="+164"/>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_p.h" line="+141"/>
        <source>%1: unable to lock</source>
        <translation>%1: Uzamknutí nelze provést</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>%1: unable to unlock</source>
        <translation>%1: Odemknutí nelze provést</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_unix.cpp" line="+79"/>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_win.cpp" line="+80"/>
        <source>%1: permission denied</source>
        <translation>%1: Přístup odepřen</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_win.cpp" line="-22"/>
        <source>%1: already exists</source>
        <translation>%1: Již existuje</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: doesn&apos;t exist</source>
        <translation>%1: Neexistuje</translation>
    </message>
    <message>
        <location line="+6"/>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: out of resources</source>
        <translation>%1: Nejsou již použitelné zdroje</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_win.cpp" line="+7"/>
        <source>%1: unknown error %2</source>
        <translation>%1: Neznámá chyba %2</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_posix.cpp" line="+64"/>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_systemv.cpp" line="+73"/>
        <source>%1: key is empty</source>
        <translation>%1: Neplatný údaj u klíče (prázdný)</translation>
    </message>
    <message>
        <location line="+38"/>
        <location line="+44"/>
        <source>%1: bad name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_systemv.cpp" line="+7"/>
        <source>%1: UNIX key file doesn&apos;t exist</source>
        <translation>%1: Soubor s unixovým klíčem neexistuje</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1: ftok failed</source>
        <translation>%1: Vyvolání ftok se nezdařilo</translation>
    </message>
    <message>
        <location line="+48"/>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_win.cpp" line="+14"/>
        <source>%1: unable to make key</source>
        <translation>%1: Nepodařilo se vytvořit klíč</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1: system-imposed size restrictions</source>
        <translation>%1: Bylo dosaženo systémem podmíněné meze velikosti</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>%1: not attached</source>
        <translation>%1: Nepřipojen</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/kernel/qsharedmemory_win.cpp" line="-26"/>
        <source>%1: invalid size</source>
        <translation>%1: Neplatná velikost</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>%1: key error</source>
        <translation>%1: Chybný klíč</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>%1: size query failed</source>
        <translation>%1: Vyhledání velikosti se nezdařilo</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../../qtbase/src/gui/kernel/qkeysequence.cpp" line="+394"/>
        <source>Space</source>
        <extracomment>This and all following &quot;incomprehensible&quot; strings in QShortcut context are key names. Please use the localized names appearing on actual keyboards or whatever is commonly used.</extracomment>
        <translation>Mezerník</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tab</source>
        <translation>Tab</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backtab</source>
        <translation>Zpět-Tab</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Return</source>
        <translation>Return</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter</source>
        <translation>Enter</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ins</source>
        <translation>Insert</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SysReq</source>
        <translation>SysReq</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>End</source>
        <translation>End</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Left</source>
        <translation>Vlevo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Up</source>
        <translation>Nahoru</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right</source>
        <translation>Vpravo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Down</source>
        <translation>Dolů</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CapsLock</source>
        <translation>CapsLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>NumLock</source>
        <translation>NumLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ScrollLock</source>
        <translation>ScrollLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Forward</source>
        <translation>Dopředu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Refresh</source>
        <translation>Obnovit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Down</source>
        <translation>Hlasitost -</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Mute</source>
        <translation>Ztlumit hlasitost</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Up</source>
        <translation>Hlasitost +</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Boost</source>
        <translation>Zesílení basů</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Up</source>
        <translation>Basy +</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Down</source>
        <translation>Basy -</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Up</source>
        <translation>Výšky +</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Down</source>
        <translation>Výšky -</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Play</source>
        <translation>Přehrávání</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Stop</source>
        <translation>Zastavit přehrávání</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Previous</source>
        <translation>Předchozí</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Next</source>
        <translation>Další</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Record</source>
        <translation>Nahrát</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Media Pause</source>
        <extracomment>Media player pause button</extracomment>
        <translation>Pozastavení přehrávání</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Toggle Media Play/Pause</source>
        <extracomment>Media player button to toggle between playing and paused</extracomment>
        <translation>Přepnout přehrávat/pozastavit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Home Page</source>
        <translation>Domovská stránka</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Favorites</source>
        <translation>Oblíbené</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search</source>
        <translation>Hledat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Standby</source>
        <translation>V pohotovosti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open URL</source>
        <translation>Otevřít URL</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Mail</source>
        <translation>Spustit e-mail</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Media</source>
        <translation>Spustit přehrávač</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (0)</source>
        <translation>Spustit (0)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (1)</source>
        <translation>Spustit (1)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (2)</source>
        <translation>Spustit (2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (3)</source>
        <translation>Spustit (3)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (4)</source>
        <translation>Spustit (4)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (5)</source>
        <translation>Spustit (5)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (6)</source>
        <translation>Spustit (6)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (7)</source>
        <translation>Spustit (7)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (8)</source>
        <translation>Spustit (8)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (9)</source>
        <translation>Spustit (9)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (A)</source>
        <translation>Spustit (A)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (B)</source>
        <translation>Spustit (B)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (C)</source>
        <translation>Spustit (C)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (D)</source>
        <translation>Spustit (D)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (E)</source>
        <translation>Spustit (E)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (F)</source>
        <translation>Spustit (F)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Monitor Brightness Up</source>
        <translation>Zvýšit jas obrazovky</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Monitor Brightness Down</source>
        <translation>Snížit jas obrazovky</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Keyboard Light On/Off</source>
        <translation>Zapnout/Vypnout podsvícení klávesnice</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Keyboard Brightness Up</source>
        <translation>Zvýšit jas klávesnice</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Keyboard Brightness Down</source>
        <translation>Snížit jas klávesnice</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Power Off</source>
        <translation>Vypnout</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wake Up</source>
        <translation>Probudit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Eject</source>
        <translation>Vysunout</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Screensaver</source>
        <translation>Spořič/Šetřič obrazovky</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>WWW</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sleep</source>
        <translation>Režim spánku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LightBulb</source>
        <translation>Osvětlení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Shop</source>
        <translation>Obchod</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>History</source>
        <translation>Průběh</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Favorite</source>
        <translation>Přidat záložku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hot Links</source>
        <translation>Doporučené odkazy</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Adjust Brightness</source>
        <translation>Upravit jas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Finance</source>
        <translation>Finance</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Community</source>
        <translation>Společenství</translation>
    </message>
    <message>
        <source>Audio Rewind</source>
        <translation type="vanished">Zvuk přetočit zpět</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Back Forward</source>
        <translation>Zpět dopředu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Application Left</source>
        <translation>Aplikace vlevo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Application Right</source>
        <translation>Aplikace vpravo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Book</source>
        <translation>Kniha</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Calculator</source>
        <translation>Kalkulačka</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear Grab</source>
        <translation>Smazat přístup</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy</source>
        <translation>Kopírovat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cut</source>
        <translation>Vyjmout</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Display</source>
        <translation>Zobrazit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DOS</source>
        <translation>DOS</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Documents</source>
        <translation>Dokumenty</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Spreadsheet</source>
        <translation>Tabulkový dokument</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browser</source>
        <translation>Prohlížeč</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Game</source>
        <translation>Hra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Go</source>
        <translation>Do toho</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>iTouch</source>
        <translation>iTouch</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Logoff</source>
        <translation>Odhlásit se</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Market</source>
        <translation>Trh</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Meeting</source>
        <translation>Setkání</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Keyboard Menu</source>
        <translation>Nabídka klávesnice</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Menu PB</source>
        <translation>Nabídka PB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>My Sites</source>
        <translation>Moje místa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>News</source>
        <translation>Zprávy</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Home Office</source>
        <translation>Domácí kancelář</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Option</source>
        <translation>Volba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paste</source>
        <translation>Vložit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reply</source>
        <translation>Odpovědět</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reload</source>
        <translation>Nahrát znovu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rotate Windows</source>
        <translation>Otáčet okny</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rotation PB</source>
        <translation>Otáčení PB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rotation KB</source>
        <translation>Otáčení KB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Send</source>
        <translation>Poslat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Spellchecker</source>
        <translation>Ověření pravopisu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Split Screen</source>
        <translation>Rozdělit obrazovku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Support</source>
        <translation>Podpora</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Task Panel</source>
        <translation>Panel s úkoly</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Terminal</source>
        <translation>Terminál</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tools</source>
        <translation>Nástroje</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Travel</source>
        <translation>Cestování</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Word Processor</source>
        <translation>Zpracování textu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>XFer</source>
        <translation>XFer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom In</source>
        <translation>Přiblížit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom Out</source>
        <translation>Oddálit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Away</source>
        <translation>Pryč</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Messenger</source>
        <translation>Posel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>WebCam</source>
        <translation>Internetová kamera</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mail Forward</source>
        <translation>Předání dál</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pictures</source>
        <translation>Obrázky</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Music</source>
        <translation>Hudba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Battery</source>
        <translation>Baterie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bluetooth</source>
        <translation>Modrozub</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wireless</source>
        <translation>Bezdrát</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ultra Wide Band</source>
        <translation>Ultra široké pásmo</translation>
    </message>
    <message>
        <source>Audio Forward</source>
        <translation type="vanished">Zvuk přetočit dopředu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Audio Repeat</source>
        <translation>Opakovat zvuk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Audio Random Play</source>
        <translation>Zvuk přehrávat náhodně</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Subtitle</source>
        <translation>Titulky</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Audio Cycle Track</source>
        <translation>Změnit zvukovou stopu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time</source>
        <translation>Čas</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Select</source>
        <translation>Vybrat</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>View</source>
        <translation>Pohled</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Top Menu</source>
        <translation>Pruh nabídky</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Suspend</source>
        <translation>Uspat</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Hibernate</source>
        <translation>Hibernovat</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Media Rewind</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Media Fast Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Power Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Microphone Mute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Yellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Channel Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Channel Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Microphone Volume Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Microphone Volume Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open</source>
        <translation type="unfinished">Otevřít</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Undo</source>
        <translation type="unfinished">Zpět</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Redo</source>
        <translation type="unfinished">Znovu</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Print Screen</source>
        <translation>Print Screen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Up</source>
        <translation>Page Up</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Down</source>
        <translation>Obraz dolů</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Caps Lock</source>
        <translation>Caps Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Num Lock</source>
        <translation>Num Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Number Lock</source>
        <translation>Zahlen-Feststelltaste</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Lock</source>
        <translation>Scroll Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert</source>
        <translation>Insert</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Escape</source>
        <translation>Escape</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>System Request</source>
        <translation>Žádost systému</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Context1</source>
        <translation>Kontext1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context2</source>
        <translation>Kontext2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context3</source>
        <translation>Kontext3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context4</source>
        <translation>Kontext4</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Call</source>
        <extracomment>Button to start a call (note: a separate button is used to end the call)</extracomment>
        <translation>Volání</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hangup</source>
        <extracomment>Button to end a call (note: a separate button is used to start the call)</extracomment>
        <translation>Zavěsit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Toggle Call/Hangup</source>
        <extracomment>Button that will hang up if we&apos;re in call, or make a call if we&apos;re not.</extracomment>
        <translation>Přepnout volat/zavěsit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Flip</source>
        <translation>Obrátit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Voice Dial</source>
        <extracomment>Button to trigger voice dialing</extracomment>
        <translation>Hlasové vytáčení</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Last Number Redial</source>
        <extracomment>Button to redial the last number called</extracomment>
        <translation>Opakované vytáčení posledního čísla</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Camera Shutter</source>
        <extracomment>Button to trigger the camera shutter (take a picture)</extracomment>
        <translation>Závěrka kamery</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Camera Focus</source>
        <extracomment>Button to focus the camera</extracomment>
        <translation>Zaostření kamery</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Kanji</source>
        <translation>Kandži</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Muhenkan</source>
        <translation>Muhenkan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Henkan</source>
        <translation>Henkan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Romaji</source>
        <translation>Rómadži</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hiragana</source>
        <translation>Hiragana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Katakana</source>
        <translation>Katakana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hiragana Katakana</source>
        <translation>Hiragana Katakana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zenkaku</source>
        <translation>Zenkaku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hankaku</source>
        <translation>Hankaku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zenkaku Hankaku</source>
        <translation>Zenkaku Hankaku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Touroku</source>
        <translation>Touroku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Massyo</source>
        <translation>Massyo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Kana Lock</source>
        <translation>Kana Zámek</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Kana Shift</source>
        <translation>Kana Posun</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Eisu Shift</source>
        <translation>Eisu Posun</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Eisu toggle</source>
        <translation>Eisu Přepínač</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Code input</source>
        <translation>Vstup pro kód</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Multiple Candidate</source>
        <translation>Více návrhů</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Candidate</source>
        <translation>Předchozí návrh</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Hangul</source>
        <translation>Hangul</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangul Start</source>
        <translation>Hangul začátek</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangul End</source>
        <translation>Hangul konec</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangul Hanja</source>
        <translation>Hangul Hanja</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangul Jamo</source>
        <translation>Hangul Jamo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangul Romaja</source>
        <translation>Hangul Romaja</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangul Jeonja</source>
        <translation>Hangul Jeonja</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangul Banja</source>
        <translation>Hangul Banja</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangul PreHanja</source>
        <translation>Hangul PreHanja</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangul PostHanja</source>
        <translation>Hangul PostHanja</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangul Special</source>
        <translation>Hangul zvláštní</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Printer</source>
        <translation>Tiskárna</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Execute</source>
        <translation>Provést</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Play</source>
        <translation>Přehrát</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom</source>
        <translation>Zvětšení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Exit</source>
        <translation type="unfinished">Ukončit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Touchpad Toggle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Touchpad On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Touchpad Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+412"/>
        <location line="+159"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="-158"/>
        <location line="+162"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location line="-161"/>
        <location line="+159"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="-158"/>
        <location line="+154"/>
        <source>Meta</source>
        <translation>Meta</translation>
    </message>
    <message>
        <location line="-153"/>
        <location line="+162"/>
        <source>Num</source>
        <translation>Num</translation>
    </message>
    <message>
        <location line="-54"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>F%1</source>
        <translation>F%1</translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <location filename="../../qtbase/src/network/socket/qsocks5socketengine.cpp" line="-67"/>
        <source>Connection to proxy refused</source>
        <translation>Proxy server odmítl navázání spojení</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to proxy closed prematurely</source>
        <translation>Proxy server předčasně ukončil spojení</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Proxy host not found</source>
        <translation>Proxy server se nepodařilo najit</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Connection to proxy timed out</source>
        <translation>Při spojení s proxy serverem byl překročen časový limit</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Proxy authentication failed</source>
        <translation>Autentizace u proxy serveru se nezdařila</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Proxy authentication failed: %1</source>
        <translation>Autentizace u proxy serveru se nezdařila: %1</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>SOCKS version 5 protocol error</source>
        <translation>Chyba protokolu (SOCKS verze 5)</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>General SOCKSv5 server failure</source>
        <translation>Všeobecná chyba při spojení s SOCKSv5 serverem</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection not allowed by SOCKSv5 server</source>
        <translation>SOCKSv5 server odmítl spojení</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>TTL expired</source>
        <translation>Síť není dosažitelná</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>SOCKSv5 command not supported</source>
        <translation>Tento SOCKSv5 příkaz není podporován</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Address type not supported</source>
        <translation>Tento typ adresy není podporován</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown SOCKSv5 proxy error code 0x%1</source>
        <translation>Byl obdržen neznámý chybový kód od SOCKSv5 proxy serveru: 0x%1</translation>
    </message>
    <message>
        <location line="+710"/>
        <source>Network operation timed out</source>
        <translation>Časový limit pro síťovou operaci byl překročen</translation>
    </message>
</context>
<context>
    <name>QSpiAccessibleBridge</name>
    <message>
        <location filename="../../qtbase/src/platformsupport/linuxaccessibility/bridge.cpp" line="+114"/>
        <source>invalid role</source>
        <extracomment>Role of an accessible object - the object is in an invalid state or could not be constructed</extracomment>
        <translation>Neplatná role</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>title bar</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Titulkový pruh</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>menu bar</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Pruh nabídky</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>scroll bar</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Posuvník</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>grip</source>
        <extracomment>Role of an accessible object - the grip is usually used for resizing another object</extracomment>
        <translation>Úchop</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>sound</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Zvuk</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>cursor</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Ukazovátko myši</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>text caret</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Vsuvka pro text</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>alert message</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Oznámení</translation>
    </message>
    <message>
        <source>window</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation type="vanished">Okno</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>filler</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Plnič</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>popup menu</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Vyskakovací nabídka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>menu item</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Položka v nabídce</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>tool tip</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Nástrojová rada</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>application</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Program</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>document</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Dokument</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+8"/>
        <source>panel</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Panel</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>chart</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Graf</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>dialog</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Dialog</translation>
    </message>
    <message>
        <location line="-18"/>
        <location line="+20"/>
        <source>frame</source>
        <extracomment>Role of an accessible object: a window with frame and title
----------
Role of an accessible object</extracomment>
        <translation>Rámeček</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>separator</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Oddělovač</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>tool bar</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Nástrojový pruh</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>status bar</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Stavový řádek</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>table</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Tabulka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>column header</source>
        <extracomment>Role of an accessible object - part of a table</extracomment>
        <translation>Záhlaví sloupce</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>row header</source>
        <extracomment>Role of an accessible object - part of a table</extracomment>
        <translation>Záhlaví řádku</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>column</source>
        <extracomment>Role of an accessible object - part of a table</extracomment>
        <translation>Sloupec</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>row</source>
        <extracomment>Role of an accessible object - part of a table</extracomment>
        <translation>Řádek</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>cell</source>
        <extracomment>Role of an accessible object - part of a table</extracomment>
        <translation>Buňka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>link</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Odkaz</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>help balloon</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Bublinová nápověda</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>assistant</source>
        <extracomment>Role of an accessible object - a helper dialog</extracomment>
        <translation>Pomocník</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>list</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Seznam</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>list item</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Položka seznamu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>tree</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Strom</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>tree item</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Položka stromu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>page tab</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Karta strany</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>property page</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Strana vlastnosti</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>indicator</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Ukazatel</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>graphic</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Grafika</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>label</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Textové pole</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>text</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Text</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>push button</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Tlačítko</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>check box</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Zaškrtávací pole</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>radio button</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Přepínač</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>combo box</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Rozbalovací seznam</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>progress bar</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Ukazatel postupu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>dial</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Číselník</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>hotkey field</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Pole s klávesovou zkratkou</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>slider</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Posuvník</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>spin box</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Přírůstkové pole</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>canvas</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Plátno</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>animation</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Animace</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>equation</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Rovnice</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>button with drop down</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Tlačítko, které rozbalí okno</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>button menu</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Tlačítko s nabídkou</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>button with drop down grid</source>
        <extracomment>Role of an accessible object - a button that expands a grid.</extracomment>
        <translation>Tlačítko, které rozbalí okno, jež ukazuje mřížku</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>space</source>
        <extracomment>Role of an accessible object - blank space between other objects.</extracomment>
        <translation>Prázdný prostor</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>page tab list</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Seznam karet stran</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>clock</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Hodiny</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>splitter</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Dělitel oken</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>layered pane</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Panel s více vrstvami</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>web document</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>paragraph</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>section</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>color chooser</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>footer</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>form</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>heading</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>note</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>complementary content</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>unknown</source>
        <extracomment>Role of an accessible object</extracomment>
        <translation>Neznámý</translation>
    </message>
</context>
<context>
    <name>QSslSocket</name>
    <message>
        <location filename="../../qtbase/src/network/ssl/qsslerror.cpp" line="+215"/>
        <source>No error</source>
        <translation>Žádná chyba</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The issuer certificate could not be found</source>
        <translation>Osvědčení od vydavatele se nepodařilo nalézt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The certificate signature could not be decrypted</source>
        <translation>Podpis osvědčení se nepodařilo rozluštit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The public key in the certificate could not be read</source>
        <translation>Veřejný klíč v osvědčení se nepodařilo přečíst</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The signature of the certificate is invalid</source>
        <translation>Podpis osvědčení je neplatný</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The certificate is not yet valid</source>
        <translation>Osvědčení ještě není platné</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The certificate has expired</source>
        <translation>Platnost osvědčení uplynula</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The certificate&apos;s notBefore field contains an invalid time</source>
        <translation>Pole osvědčení &apos;notBefore&apos; obsahuje neplatný čas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The certificate&apos;s notAfter field contains an invalid time</source>
        <translation>Pole osvědčení &apos;notAfter&apos; obsahuje neplatný čas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The certificate is self-signed, and untrusted</source>
        <translation>Osvědčení je podepsáno samo sebou, a proto není důvěryhodné</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The root certificate of the certificate chain is self-signed, and untrusted</source>
        <translation>Kořenové osvědčení řetězce osvědčení je podepsáno samo sebou, a proto není důvěryhodné</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The issuer certificate of a locally looked up certificate could not be found</source>
        <translation>Osvědčení od vydavatele místně nalezeného osvědčení se nepodařilo najít</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No certificates could be verified</source>
        <translation>Žádný z osvědčení se nepodařilo ověřit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>One of the CA certificates is invalid</source>
        <translation>Jedno z osvědčení osvědčovacího místa (CA) je neplatné</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The basicConstraints path length parameter has been exceeded</source>
        <translation>Délka cesty &apos;basicConstraints&apos;byla překročena</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The supplied certificate is unsuitable for this purpose</source>
        <translation>Poskytnuté osvědčení nelze v tomto případě použít; není vhodné pro tento účel</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The root CA certificate is not trusted for this purpose</source>
        <translation>Kořenové osvědčení osvědčovacího místa není pro tento případ důvěryhodné</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The root CA certificate is marked to reject the specified purpose</source>
        <translation>Kořenové osvědčení osvědčovacího místa odmítá tento případ na základě zvláštního označení</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The current candidate issuer certificate was rejected because its subject name did not match the issuer name of the current certificate</source>
        <translation>Osvědčení sledovaného vydavatele bylo odmítnuto, protože jeho předmětný název neodpovídá názvu vydavatele současného osvědčení</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The current candidate issuer certificate was rejected because its issuer name and serial number was present and did not match the authority key identifier of the current certificate</source>
        <translation>Osvědčení sledovaného vydavatele bylo odmítnuto, protože název vydavatele a sériové číslo jsou přítomny a neodpovídají identifikátoru osvědčovacího místa současného osvědčení</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The peer did not present any certificate</source>
        <translation>Protější místo neudalo žádné osvědčení</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The host name did not match any of the valid hosts for this certificate</source>
        <translation>Název hostitelského počítače neodpovídá žádnému z hostitelů platných pro toto osvědčení, kteří jsou na seznamu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The peer certificate is blacklisted</source>
        <translation>Osvědčení protějšího místa je na černé listině</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/ssl/qsslcontext_openssl.cpp" line="+184"/>
        <source>Error creating SSL context (%1)</source>
        <translation>Nepodařilo se vytvořit žádný kontext SSL (%1)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unsupported protocol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Invalid or empty cipher list (%1)</source>
        <translation>Neplatný či prázdný seznam se šifrovacími klíči (%1)</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot provide a certificate with no key, %1</source>
        <translation>Bez klíče nelze poskytnout žádné osvědčení k volnému použití, %1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error loading local certificate, %1</source>
        <translation>Nepodařilo se nahrát místní osvědčení, %1</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Error loading private key, %1</source>
        <translation>Soukromý klíč se nepodařilo nahrát, %1</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Private key does not certify public key, %1</source>
        <translation>Soukromý klíč nedosvědčuje veřejný klíč, %1</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Error when setting the elliptic curves (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error when setting the elliptic curves (OpenSSL version too old, need at least v1.0.2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtbase/src/network/ssl/qsslsocket_openssl.cpp" line="+343"/>
        <source>Error creating SSL session, %1</source>
        <translation>Nepodařilo se vytvořit sezení SSL, %1</translation>
    </message>
    <message>
        <location line="+36"/>
        <location filename="../../qtbase/src/network/ssl/qsslsocket_winrt.cpp" line="+442"/>
        <source>Error creating SSL session: %1</source>
        <translation>Nepodařilo se vytvořit sezení SSL: %1</translation>
    </message>
    <message>
        <location line="+406"/>
        <location line="+16"/>
        <source>Unable to init SSL Context: %1</source>
        <translation>Nelze inicializovat SSL Context: %1</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Unable to write data: %1</source>
        <translation>Data se nepodařilo zapsat: %1</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Unable to decrypt data: %1</source>
        <translation>Data se nepodařilo rozluštit: %1</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>The TLS/SSL connection has been closed</source>
        <translation>Spojení TLS/SSL bylo zavřeno</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>Error while reading: %1</source>
        <translation>Při čtení se vyskytla chyba: %1</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Error during SSL handshake: %1</source>
        <translation>Během startu SSL protokolu se vyskytla chyba: %1</translation>
    </message>
</context>
<context>
    <name>QStandardPaths</name>
    <message>
        <location filename="../../qtbase/src/corelib/io/qstandardpaths.cpp" line="+546"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Documents</source>
        <translation>Dokumenty</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fonts</source>
        <translation>Písma</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Applications</source>
        <translation>Programy</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Music</source>
        <translation>Hudba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Movies</source>
        <translation>Filmy</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pictures</source>
        <translation>Obrázky</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Temporary Directory</source>
        <translation>Dočasný adresář</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Application Data</source>
        <translation>Data aplikací</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Application Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Cache</source>
        <translation>Vyrovnávací paměť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shared Data</source>
        <translation>Sdílená data</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Runtime</source>
        <translation>Běhový čas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Configuration</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shared Configuration</source>
        <translation>Sdílené nastavení</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shared Cache</source>
        <translation>Sdílená vyrovnávací paměť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Download</source>
        <translation>Stahování</translation>
    </message>
</context>
<context>
    <name>QStateMachine</name>
    <message>
        <location filename="../../qtbase/src/corelib/statemachine/qstatemachine.cpp" line="+1052"/>
        <source>Missing initial state in compound state &apos;%1&apos;</source>
        <translation>Chybí počáteční stav složeného stavu &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Missing default state in history state &apos;%1&apos;</source>
        <translation>Chybí výchozí stav ve vývoji stavu &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>No common ancestor for targets and source of transition from state &apos;%1&apos;</source>
        <translation>Cíl a zdroj přechodu ze stavu &apos;%1&apos; nemají žádný společný původ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
</context>
<context>
    <name>QSystemSemaphore</name>
    <message>
        <location filename="../../qtbase/src/corelib/kernel/qsystemsemaphore_unix.cpp" line="+70"/>
        <location filename="../../qtbase/src/corelib/kernel/qsystemsemaphore_win.cpp" line="+63"/>
        <source>%1: permission denied</source>
        <translation>%1: Přístup odepřen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: already exists</source>
        <translation>%1: Již existuje</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: does not exist</source>
        <translation>%1: Neexistuje</translation>
    </message>
    <message>
        <location line="+5"/>
        <location filename="../../qtbase/src/corelib/kernel/qsystemsemaphore_win.cpp" line="-4"/>
        <source>%1: out of resources</source>
        <translation>%1: Nejsou již použitelné zdroje</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../../qtbase/src/corelib/kernel/qsystemsemaphore_win.cpp" line="+7"/>
        <source>%1: unknown error %2</source>
        <translation>%1: Neznámá chyba %2</translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <location filename="../../qtbase/src/sql/drivers/tds/qsql_tds.cpp" line="+623"/>
        <source>Unable to open connection</source>
        <translation>Nepodařilo se otevřít spojení s databází</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unable to use database</source>
        <translation>Databázi se nepodařilo použít</translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qtabbar.cpp" line="-2049"/>
        <source>Scroll Left</source>
        <translation>Projíždět doleva</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Right</source>
        <translation>Projíždět doprava</translation>
    </message>
</context>
<context>
    <name>QTcpServer</name>
    <message>
        <location filename="../../qtbase/src/network/socket/qtcpserver.cpp" line="+291"/>
        <location line="+132"/>
        <source>Operation on socket is not supported</source>
        <translation>Tato socket operace není podporována</translation>
    </message>
</context>
<context>
    <name>QTgaFile</name>
    <message>
        <location filename="../../qtimageformats/src/plugins/imageformats/tga/qtgafile.cpp" line="+128"/>
        <source>Could not read image data</source>
        <translation>Nepodařilo se přečíst data obrazu</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sequential device (eg socket) for image read not supported</source>
        <translation>Postupné zařízení (např. socket - komunikační kanál) pro čtení obrazu nepodporováno</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Seek file/device for image read failed</source>
        <translation>Vyhledání souboru/zařízení pro čtení obrazu se nezdařilo</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Image header read failed</source>
        <translation>Nepodařilo se přečíst hlavičku obrazu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Image type not supported</source>
        <translation>Typ obrazu nepodporován</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Image depth not valid</source>
        <translation>Hloubka obrazu nepodporována</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not seek to image read footer</source>
        <translation>Nepodařilo se vyhledat zápatí čtení obrazu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Could not read footer</source>
        <translation>Nepodařilo se přečíst zápatí</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Image type (non-TrueVision 2.0) not supported</source>
        <translation>Typ obrazu (non-TrueVision 2.0) nepodporován</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Could not reset to read data</source>
        <translation>Nepodařilo se nastavit znovu na čtení dat</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <location filename="../../qtbase/src/widgets/util/qundogroup.cpp" line="+381"/>
        <source>Undo %1</source>
        <translation>Zpět %1</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Undo</source>
        <comment>Default text for undo action</comment>
        <translation>Zpět</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Redo %1</source>
        <translation>Znovu %1</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Redo</source>
        <comment>Default text for redo action</comment>
        <translation>Znovu</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <location filename="../../qtbase/src/widgets/util/qundoview.cpp" line="+93"/>
        <source>&lt;empty&gt;</source>
        <translation>&lt;prázdný&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <location filename="../../qtbase/src/widgets/util/qundostack.cpp" line="+873"/>
        <source>Undo %1</source>
        <translation>Zpět %1</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Undo</source>
        <comment>Default text for undo action</comment>
        <translation>Zpět</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Redo %1</source>
        <translation>Znovu %1</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Redo</source>
        <comment>Default text for redo action</comment>
        <translation>Znovu</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qwidgettextcontrol.cpp" line="+3216"/>
        <source>LRM Left-to-right mark</source>
        <translation>LRM Značka zleva doprava</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLM Right-to-left mark</source>
        <translation>RLM Značka zprava doleva</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWJ Zero width joiner</source>
        <translation>ZWJ Nulová mezera spojovací</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWNJ Zero width non-joiner</source>
        <translation>ZWNJ Nulová mezera nespojovací</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWSP Zero width space</source>
        <translation>ZWSP Nulová mezera</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRE Start of left-to-right embedding</source>
        <translation>LRE Začátek vložení zleva doprava</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLE Start of right-to-left embedding</source>
        <translation>RLE Začátek vložení zprava doleva</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRO Start of left-to-right override</source>
        <translation>LRO Začátek přepsání zleva doprava</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLO Start of right-to-left override</source>
        <translation>RLO Začátek přepsání zprava doleva</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PDF Pop directional formatting</source>
        <translation>PDF Konec vložení/přepsání směru</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRI Left-to-right isolate</source>
        <translation>LRI zleva doprava oddělit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLI Right-to-left isolate</source>
        <translation>RLI zprava doleva oddělit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>FSI First strong isolate</source>
        <translation>FSI první silný oddělit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PDI Pop directional isolate</source>
        <translation>PDI Pop směrově oddělit</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Insert Unicode control character</source>
        <translation>Vložit kontrolní znak Unicode</translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <location filename="../../qtbase/src/widgets/kernel/qwhatsthis.cpp" line="+495"/>
        <source>What&apos;s This?</source>
        <translation>Co je toto?</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <location filename="../../qtbase/src/widgets/kernel/qwidget.cpp" line="+5977"/>
        <source>*</source>
        <translation>*</translation>
    </message>
</context>
<context>
    <name>QWidgetTextControl</name>
    <message>
        <location filename="../../qtbase/src/widgets/widgets/qwidgettextcontrol.cpp" line="-1013"/>
        <source>&amp;Undo</source>
        <translation>&amp;Zpět</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Redo</source>
        <translation>&amp;Znovu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cu&amp;t</source>
        <translation>Vyj&amp;mout</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopírovat</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Link Location</source>
        <translation>&amp;Kopírovat adresu odkazu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Paste</source>
        <translation>&amp;Vložit</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Select All</source>
        <translation>Vybrat vše</translation>
    </message>
</context>
<context>
    <name>QWindowsDirect2DIntegration</name>
    <message>
        <location filename="../../qtbase/src/plugins/platforms/direct2d/qwindowsdirect2dintegration.cpp" line="+174"/>
        <source>Qt cannot load the direct2d platform plugin because the Direct2D version on this system is too old. The minimum system requirement for this platform plugin is Windows 7 SP1 with Platform Update.

The minimum Direct2D version required is %1.%2.%3.%4. The Direct2D version on this system is %5.%6.%7.%8.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Cannot load direct2d platform plugin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <location filename="../../qtbase/src/widgets/dialogs/qwizard.cpp" line="+697"/>
        <source>Go Back</source>
        <translation>Jít zpět</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp;Zpět</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Next</source>
        <translation>&amp;Další</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Next &gt;</source>
        <translation>&amp;Další &gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Commit</source>
        <translation>Použít</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Done</source>
        <translation>Hotovo</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Finish</source>
        <translation>Do&amp;končit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Help</source>
        <translation>&amp;Nápověda</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <location filename="../../qtbase/src/xml/sax/qxml.cpp" line="+52"/>
        <source>no error occurred</source>
        <translation>žádná chyba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error triggered by consumer</source>
        <translation>chyba spuštěná spotřebitelem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end of file</source>
        <translation>neočekávaný konec souboru</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>more than one document type definition</source>
        <translation>více definicí typu dokumentu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing element</source>
        <translation>při vyhodnocení prvku se vyskytla chyba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>tag mismatch</source>
        <translation>Značky prvků nejsou vkládány správně</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing content</source>
        <translation>při vyhodnocení obsahu se vyskytla chyba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected character</source>
        <translation>neočekávaný znak</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid name for processing instruction</source>
        <translation>neplatný název pro pokyn pro zpracování</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>version expected while reading the XML declaration</source>
        <translation>chybějící verze při čtení deklarace XML</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>wrong value for standalone declaration</source>
        <translation>nesprávná hodnota vlastnosti standalone</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation>chybějící deklarace kódování nebo deklarace samostatnosti při čtení deklarace XML</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation>chybějící deklarace samostatnosti při čtení deklarace XML</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing document type definition</source>
        <translation>při vyhodnocení definice typu dokumentu se vyskytla chyba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>letter is expected</source>
        <translation>na tomto místě je potřeba písmeno</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing comment</source>
        <translation>při vyhodnocení poznámky se vyskytla chyba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing reference</source>
        <translation>při vyhodnocení odkazu se vyskytla chyba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal general entity reference not allowed in DTD</source>
        <translation>v DTD nejsou dovoleny žádné odkazy na vnitřní obecnou entitu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation>v hodnotě vlastnosti nejsou dovoleny žádné odkazy na vnější obecnou entitu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation>v DTD nejsou dovoleny žádné odkazy na vnější obecnou entitu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unparsed entity reference in wrong context</source>
        <translation>používán nevyhodnocený odkaz na entitu v nesprávné souvislosti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>recursive entities</source>
        <translation>rekurzivní entity</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error in the text declaration of an external entity</source>
        <translation>Chyba v deklaraci textu vnější entity</translation>
    </message>
</context>
<context>
    <name>QXmlStream</name>
    <message>
        <location filename="../../qtbase/src/corelib/xml/qxmlstream.cpp" line="+599"/>
        <location filename="../../qtbase/src/corelib/xml/qxmlstream_p.h" line="+1763"/>
        <source>Extra content at end of document.</source>
        <translation>Přebytečný obsah za koncem dokumentu.</translation>
    </message>
    <message>
        <location line="+272"/>
        <source>Invalid entity value.</source>
        <translation>Neplatná hodnota entity.</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Invalid XML character.</source>
        <translation>Neplatný znak XML.</translation>
    </message>
    <message>
        <location line="+259"/>
        <source>Sequence &apos;]]&gt;&apos; not allowed in content.</source>
        <translation>Sled znaků &apos;]]&gt;&apos; není v obsahu povolen.</translation>
    </message>
    <message>
        <location line="+279"/>
        <location filename="../../qtbase/src/corelib/xml/qxmlstream_p.h" line="-691"/>
        <source>Encountered incorrectly encoded content.</source>
        <translation>Byl nalezen obsah s neplatným kódováním.</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Namespace prefix &apos;%1&apos; not declared</source>
        <translation>Předpona jmenného prostoru &apos;%1&apos; nebyla prohlášena</translation>
    </message>
    <message>
        <location line="+33"/>
        <location line="+12"/>
        <location filename="../../qtbase/src/corelib/xml/qxmlstream_p.h" line="+612"/>
        <location line="+53"/>
        <source>Illegal namespace declaration.</source>
        <translation>Neplatná deklarace jmenného prostoru.</translation>
    </message>
    <message>
        <source>Attribute redefined.</source>
        <translation type="vanished">Vlastnost byla nově vymezena.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Attribute &apos;%1&apos; redefined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unexpected character &apos;%1&apos; in public id literal.</source>
        <translation>&apos;%1&apos; není platným znakem v údaji veřejného id, který je tvořen písmeny.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Invalid XML version string.</source>
        <translation>Neplatný údaj o verzi XML.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unsupported XML version.</source>
        <translation>Tato verze XML není podporována.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The standalone pseudo attribute must appear after the encoding.</source>
        <translation>Pseudovlastnost standalone musí následovat bezprostředně po kódování.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 is an invalid encoding name.</source>
        <translation>%1 není platným názvem pro kódování.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Encoding %1 is unsupported</source>
        <translation>Kódování %1 není podporováno</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Standalone accepts only yes or no.</source>
        <translation>Hodnota pro vlastnost standalone může být pouze &quot;ano&quot; nebo &quot;ne&quot;.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid attribute in XML declaration.</source>
        <translation>Deklarace XML obsahuje neplatnou vlastnost.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Premature end of document.</source>
        <translation>Předčasný konec dokumentu.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid document.</source>
        <translation>Neplatný dokument.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Expected </source>
        <translation>Bylo </translation>
    </message>
    <message>
        <location line="+11"/>
        <source>, but got &apos;</source>
        <translation>očekáváno, namísto toho obdrženo &apos;</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unexpected &apos;</source>
        <translation>Na tomto místě neplatný &apos; </translation>
    </message>
    <message>
        <location line="+225"/>
        <source>Expected character data.</source>
        <translation>Byly očekávány údaje o znacích.</translation>
    </message>
    <message>
        <location filename="../../qtbase/src/corelib/xml/qxmlstream_p.h" line="-969"/>
        <source>Recursive entity detected.</source>
        <translation>Byla zjištěna rekurzivní entita.</translation>
    </message>
    <message>
        <location line="+516"/>
        <source>Start tag expected.</source>
        <translation>Očekáván otevírající prvek.</translation>
    </message>
    <message>
        <location line="+191"/>
        <source>NDATA in parameter entity declaration.</source>
        <translation>Deklarace parametrické entity nesmí obsahovat NDATA.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>XML declaration not at start of document.</source>
        <translation>Deklarace XML se nenachází na začátku dokumentu.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>%1 is an invalid processing instruction name.</source>
        <translation>%1 není platným názvem pokynu pro zpracování.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid processing instruction name.</source>
        <translation>Název pokynu pro zpracování je neplatný.</translation>
    </message>
    <message>
        <location line="+114"/>
        <source>%1 is an invalid PUBLIC identifier.</source>
        <translation>%1 není platným údajem VEŘEJNÉHO identifikátoru (id).</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Invalid XML name.</source>
        <translation>Neplatný název XML.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Opening and ending tag mismatch.</source>
        <translation>Počet otevírajících prvků neodpovídá počtu zavírajících prvků.</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+61"/>
        <location line="+40"/>
        <source>Entity &apos;%1&apos; not declared.</source>
        <translation>Entita &apos;%1&apos; není prohlášena.</translation>
    </message>
    <message>
        <location line="-88"/>
        <source>Reference to unparsed entity &apos;%1&apos;.</source>
        <translation>Odkaz na nevyhodnocenou entitu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Reference to external entity &apos;%1&apos; in attribute value.</source>
        <translation>V hodnotě vlastnosti byla odkazována entita &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Invalid character reference.</source>
        <translation>Odkaz na neplatný znak.</translation>
    </message>
</context>
<context>
    <name>QtAndroidDialogHelpers::QAndroidPlatformMessageDialogHelper</name>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Uložit</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Otevřít</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Zrušit</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zavřít</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">Použít</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Nastavit znovu</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Nápověda</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="vanished">Zahodit změny</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Ano</translation>
    </message>
    <message>
        <source>Yes to All</source>
        <translation type="vanished">Ano, vše</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">Ne</translation>
    </message>
    <message>
        <source>No to All</source>
        <translation type="vanished">Ne, žádné</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation type="vanished">Uložit vše</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="vanished">Zrušit</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Opakovat</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">Ignorovat</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation type="vanished">Obnovit výchozí</translation>
    </message>
</context>
</TS>
