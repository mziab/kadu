<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AudioCaptureSession</name>
    <message>
        <source>RAW file format</source>
        <translation type="vanished">Souborový formát RAW</translation>
    </message>
    <message>
        <source>WAV file format</source>
        <translation type="vanished">Souborový formát WAW</translation>
    </message>
</context>
<context>
    <name>AudioContainerControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/audiocapture/audiocontainercontrol.cpp" line="+69"/>
        <source>RAW (headerless) file format</source>
        <translation>Souborový formát RAW (bez hlavičky)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>WAV file format</source>
        <translation>Souborový formát WAW</translation>
    </message>
</context>
<context>
    <name>AudioEncoderControl</name>
    <message>
        <source>PCM audio data</source>
        <translation type="vanished">Data PCM audio</translation>
    </message>
    <message>
        <location filename="../../qtmultimedia/src/plugins/audiocapture/audioencodercontrol.cpp" line="+92"/>
        <source>Linear PCM audio data</source>
        <translation>Zvuková data PCM (lineární)</translation>
    </message>
</context>
<context>
    <name>BbCameraAudioEncoderSettingsControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbcameraaudioencodersettingscontrol.cpp" line="+53"/>
        <source>No compression</source>
        <translation>Žádná komprese</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AAC compression</source>
        <translation>Komprese AAC</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PCM uncompressed</source>
        <translation>Komprese PCM</translation>
    </message>
</context>
<context>
    <name>BbCameraMediaRecorderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbcameramediarecordercontrol.cpp" line="+105"/>
        <source>Unable to retrieve mute status</source>
        <translation>Nelze určit stav ztlumení</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to retrieve audio input volume</source>
        <translation>Nelze určit sílu hlasitosti zvukového vstupu</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Unable to set mute status</source>
        <translation>Nelze nastavit stav ztlumení</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Unable to set audio input volume</source>
        <translation>Nelze nastavit sílu hlasitosti zvukového vstupu</translation>
    </message>
</context>
<context>
    <name>BbCameraSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbcamerasession.cpp" line="+336"/>
        <source>Camera provides image in unsupported format</source>
        <translation>Kamera poskytuje obraz v nepodporovaném formátu</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Could not load JPEG data from frame</source>
        <translation>Nepodařilo se nahrát data JPEG ze snímku</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Camera not ready</source>
        <translation>Kamera není připravena</translation>
    </message>
    <message>
        <location line="+243"/>
        <source>Unable to apply video settings</source>
        <translation>Nelze použít nastavení pro obraz</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Could not open destination file:
%1</source>
        <translation>Nepodařilo se otevřít cílový soubor:
%1</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Unable to open camera</source>
        <translation>Nelze otevřít kameru</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to retrieve native camera orientation</source>
        <translation>Nelze určit přirozené zaměření kamery</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Unable to close camera</source>
        <translation>Nelze zavřít kameru</translation>
    </message>
    <message>
        <location line="+209"/>
        <source>Unable to start video recording</source>
        <translation>Nelze spustit nahrávání obrazu</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to stop video recording</source>
        <translation>Nelze zastavit nahrávání obrazu</translation>
    </message>
</context>
<context>
    <name>BbCameraVideoEncoderSettingsControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbcameravideoencodersettingscontrol.cpp" line="+63"/>
        <source>No compression</source>
        <translation>Žádná komprese</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AVC1 compression</source>
        <translation>Komprese AVC1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>H264 compression</source>
        <translation>Komprese H264</translation>
    </message>
</context>
<context>
    <name>BbImageEncoderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbimageencodercontrol.cpp" line="+53"/>
        <source>JPEG image</source>
        <translation>Soubor s obrázkem JPEG</translation>
    </message>
</context>
<context>
    <name>BbVideoDeviceSelectorControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbvideodeviceselectorcontrol.cpp" line="+104"/>
        <source>Front Camera</source>
        <translation>Čelní kamera</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Rear Camera</source>
        <translation>Zadní kamera</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Desktop Camera</source>
        <translation>Stolní kamera</translation>
    </message>
</context>
<context>
    <name>CameraBinImageCapture</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinimagecapture.cpp" line="+86"/>
        <source>Camera not ready</source>
        <translation>Kamera není připravena</translation>
    </message>
</context>
<context>
    <name>CameraBinImageEncoder</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinimageencoder.cpp" line="+66"/>
        <source>JPEG image</source>
        <translation>Soubor s obrázkem JPEG</translation>
    </message>
</context>
<context>
    <name>CameraBinRecorder</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinrecorder.cpp" line="+213"/>
        <source>QMediaRecorder::pause() is not supported by camerabin2.</source>
        <translation>QMediaRecorder::pause() není camerabin2 podporován.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Service has not been started</source>
        <translation>Služba nebyla spuštěna</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Recording permissions are not available</source>
        <translation>Oprávnění pro nahrávání nejsou dostupná</translation>
    </message>
</context>
<context>
    <name>CameraBinSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinsession.cpp" line="+855"/>
        <source>Camera error</source>
        <translation>Chyba kamery</translation>
    </message>
</context>
<context>
    <name>DSCameraSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/directshow/camera/dscamerasession.cpp" line="+343"/>
        <source>Camera not ready for capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Could not save image to file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MFPlayerSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/wmf/player/mfplayersession.cpp" line="+207"/>
        <source>Invalid stream source.</source>
        <translation>Neplatný zdroj proudu.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Attempting to play invalid Qt resource.</source>
        <translation>Pokus o přehrání neplatného prostředku Qt.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The system cannot find the file specified.</source>
        <translation>Systém nemůže najít zadaný soubor.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The specified server could not be found.</source>
        <translation>Zadaný server se nepodařilo najít.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unsupported media type.</source>
        <translation>Nepodporovaný multimediální typ.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to load source.</source>
        <translation>Nepodařilo se nahrát zdroj.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot create presentation descriptor.</source>
        <translation>Nelze vytvořit popisovač prezentace.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Failed to get stream count.</source>
        <translation>Nepodařilo se dostat počet proudů.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Failed to create topology.</source>
        <translation>Nepodařilo se vytvořit topologii.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Unable to play any stream.</source>
        <translation>Nelze přehrát žádný proud.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Unable to play.</source>
        <translation>Nelze přehrát.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Failed to set topology.</source>
        <translation>Nepodařilo se nastavit topologii.</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Unknown stream type.</source>
        <translation>Neznámý typ proudu.</translation>
    </message>
    <message>
        <location line="+570"/>
        <source>Failed to stop.</source>
        <translation>Nepodařilo se zastavit.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>failed to start playback</source>
        <translation>Nepodařilo se spustit přehrávání</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Failed to pause.</source>
        <translation>Nepodařilo se pozastavit.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Unable to create mediasession.</source>
        <translation>Nepodařilo se vytvořit multimediální sezení.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to pull session events.</source>
        <translation>Nepodařilo se přivést události sezení.</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Failed to seek.</source>
        <translation>Vyhledání selhalo.</translation>
    </message>
    <message>
        <location line="+393"/>
        <source>Media session non-fatal error.</source>
        <translation>Méně vážná chyba multimediálního sezení.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Media session serious error.</source>
        <translation>Závažná chyba multimediálního sezení.</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Unsupported media, a codec is missing.</source>
        <translation>Nepodporované médium. Chybí kodek.</translation>
    </message>
</context>
<context>
    <name>PLSParser</name>
    <message>
        <source>Error parsing playlist: %1, expected count = %2</source>
        <translation type="vanished">Chyba při zpracování seznamu skladeb: %1, očekávaný počet = %2</translation>
    </message>
    <message>
        <source>Error parsing playlist at line[%1], expected version = 2</source>
        <translation type="vanished">Chyba při zpracování seznamu skladeb na řádku [%1], očekávaná verze = %2</translation>
    </message>
    <message>
        <source>Error parsing playlist at line[%1]:%2</source>
        <translation type="vanished">Chyba při zpracování seznamu skladeb na řádku [%1]:%2</translation>
    </message>
    <message>
        <source>File%1</source>
        <translation type="vanished">Soubor %1</translation>
    </message>
    <message>
        <source>Title%1</source>
        <translation type="vanished">Název %1</translation>
    </message>
    <message>
        <source>Length%1</source>
        <translation type="vanished">Délka %1</translation>
    </message>
</context>
<context>
    <name>QAndroidAudioEncoderSettingsControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidaudioencodersettingscontrol.cpp" line="+54"/>
        <source>Adaptive Multi-Rate Narrowband (AMR-NB) audio codec</source>
        <translation>Zvukový kodek Adaptive Multi-Rate Narrowband (AMR-NB)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Adaptive Multi-Rate Wideband (AMR-WB) audio codec</source>
        <translation>Zvukový kodek Adaptive Multi-Rate Wideband (AMR-WB)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AAC Low Complexity (AAC-LC) audio codec</source>
        <translation>Zvukový kodek AAC Low Complexity (AAC-LC)</translation>
    </message>
</context>
<context>
    <name>QAndroidCameraSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidcamerasession.cpp" line="+319"/>
        <source>Camera cannot be started without a viewfinder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Camera not ready</source>
        <translation>Kamera není připravena</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Drive mode not supported</source>
        <extracomment>Drive mode is the camera&apos;s shutter mode, for example single shot, continuos exposure, etc.</extracomment>
        <translation>Režim spouště nepodporován</translation>
    </message>
    <message>
        <location line="+116"/>
        <source>Could not open destination file: %1</source>
        <translation>Nepodařilo se otevřít cílový soubor: %1</translation>
    </message>
</context>
<context>
    <name>QAndroidImageEncoderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidimageencodercontrol.cpp" line="+57"/>
        <source>JPEG image</source>
        <translation>Soubor s obrázkem JPEG</translation>
    </message>
</context>
<context>
    <name>QAndroidMediaContainerControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidmediacontainercontrol.cpp" line="+67"/>
        <source>MPEG4 media file format</source>
        <translation>Multimediální souborový formát MPEG4</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>3GPP media file format</source>
        <translation>Multimediální souborový formát 3GPP</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AMR NB file format</source>
        <translation>Souborový formát AMR NB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AMR WB file format</source>
        <translation>Souborový formát AMR WB</translation>
    </message>
</context>
<context>
    <name>QAndroidVideoEncoderSettingsControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidvideoencodersettingscontrol.cpp" line="+72"/>
        <source>H.263 compression</source>
        <translation>Komprese H.263</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>H.264 compression</source>
        <translation>Komprese H.264</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>MPEG-4 SP compression</source>
        <translation>Komprese MPEG-4 SP</translation>
    </message>
</context>
<context>
    <name>QAudioDecoder</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/audio/qaudiodecoder.cpp" line="+147"/>
        <location line="+58"/>
        <source>The QAudioDecoder object does not have a valid service</source>
        <translation>Objekt QAudioDecoder nemá platnou službu</translation>
    </message>
</context>
<context>
    <name>QCamera</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/camera/qcamera.cpp" line="+102"/>
        <location line="+92"/>
        <source>The camera service is missing</source>
        <translation>Chybí služba kamery</translation>
    </message>
</context>
<context>
    <name>QCameraImageCapture</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/camera/qcameraimagecapture.cpp" line="+542"/>
        <location line="+22"/>
        <source>Device does not support images capture.</source>
        <translation>Zařízení nepodporuje pořizování snímků.</translation>
    </message>
</context>
<context>
    <name>QCameraPrivate</name>
    <message>
        <source>The camera service is missing</source>
        <translation type="vanished">Chybí služba kamery</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAudio</name>
    <message>
        <location filename="../../qtmultimedia/src/imports/multimedia/qdeclarativeaudio.cpp" line="+281"/>
        <source>volume should be between 0.0 and 1.0</source>
        <translation>Hlasitost by měla být mezi 0.0 a 1.0</translation>
    </message>
</context>
<context>
    <name>QGstreamerAudioDecoderSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/audiodecoder/qgstreameraudiodecodersession.cpp" line="+224"/>
        <source>Cannot play stream of type: &lt;unknown&gt;</source>
        <translation>Nelze přehrát proud typu: &lt;neznámý&gt;</translation>
    </message>
</context>
<context>
    <name>QGstreamerAudioEncode</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreameraudioencode.cpp" line="+89"/>
        <source>Raw PCM audio</source>
        <translation>Nezpracované PCM audio</translation>
    </message>
</context>
<context>
    <name>QGstreamerAudioInputSelector</name>
    <message>
        <location filename="../../qtmultimedia/src/gsttools/qgstreameraudioinputselector.cpp" line="+101"/>
        <source>System default device</source>
        <translation>Výchozí zařízení systému</translation>
    </message>
</context>
<context>
    <name>QGstreamerCameraControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamercameracontrol.cpp" line="+120"/>
        <source>State not supported.</source>
        <translation>Stav nepodporován.</translation>
    </message>
</context>
<context>
    <name>QGstreamerCaptureSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamercapturesession.cpp" line="+225"/>
        <source>Could not create an audio source element</source>
        <translation>Nepodařilo se vytvořit prvek zdroje zvuku</translation>
    </message>
    <message>
        <location line="+402"/>
        <source>Failed to build media capture pipeline.</source>
        <translation>Nepodařilo se sestavit postup pro zpracování multimédií.</translation>
    </message>
</context>
<context>
    <name>QGstreamerImageCaptureControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamerimagecapturecontrol.cpp" line="+68"/>
        <source>Not ready to capture</source>
        <translation>Nepřipraven ke snímání</translation>
    </message>
</context>
<context>
    <name>QGstreamerImageEncode</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamerimageencode.cpp" line="+66"/>
        <source>JPEG image encoder</source>
        <translation>Kodér pro obrázek JPEG</translation>
    </message>
</context>
<context>
    <name>QGstreamerPlayerControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediaplayer/qgstreamerplayercontrol.cpp" line="+389"/>
        <source>Attempting to play invalid Qt resource</source>
        <translation>Pokus o přehrání neplatného prostředku Qt</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Attempting to play invalid user stream</source>
        <translation>Pokus o přehrání neplatného uživatelského proudu</translation>
    </message>
</context>
<context>
    <name>QGstreamerPlayerSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediaplayer/qgstreamerplayersession.cpp" line="+1167"/>
        <location line="+125"/>
        <source>Cannot play stream of type: &lt;unknown&gt;</source>
        <translation>Nelze přehrát proud typu: &lt;neznámý&gt;</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>UDP source timeout</source>
        <translation>Časové omezení zdroje UDP</translation>
    </message>
    <message>
        <location line="+471"/>
        <source>Media is loaded as a playlist</source>
        <translation>Média jsou nahrána jako seznam skladeb</translation>
    </message>
</context>
<context>
    <name>QGstreamerRecorderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamerrecordercontrol.cpp" line="+160"/>
        <location line="+21"/>
        <source>Service has not been started</source>
        <translation>Služba nebyla spuštěna</translation>
    </message>
    <message>
        <location line="+103"/>
        <source>Not compatible codecs and container format.</source>
        <translation>Neslučitelný formát kodeků a kontejneru.</translation>
    </message>
</context>
<context>
    <name>QGstreamerVideoInputDeviceControl</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/gsttools_headers/qgstreamervideoinputdevicecontrol_p.h" line="+61"/>
        <source>Main camera</source>
        <translation>Hlavní kamera</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Front camera</source>
        <translation>Čelní kamera</translation>
    </message>
</context>
<context>
    <name>QMediaPlayer</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/playback/qmediaplayer.cpp" line="+743"/>
        <source>The QMediaPlayer object does not have a valid service</source>
        <translation>Objekt QMediaPlayer nemá platnou službu</translation>
    </message>
</context>
<context>
    <name>QMediaPlaylist</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/playback/qmediaplaylist.cpp" line="+447"/>
        <location line="+61"/>
        <source>Could not add items to read only playlist.</source>
        <translation>Nepodařilo se přidat položky do seznamu skladeb pouze pro čtení.</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+61"/>
        <source>Playlist format is not supported</source>
        <translation>Formát seznamu skladeb není podporován</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>The file could not be accessed.</source>
        <translation>K souboru se nepodařilo přistoupit.</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Playlist format is not supported.</source>
        <translation>Formát seznamu skladeb není podporován.</translation>
    </message>
</context>
<context>
    <name>QMultimediaDeclarativeModule</name>
    <message>
        <location filename="../../qtmultimedia/src/imports/multimedia/multimedia.cpp" line="+80"/>
        <source>CameraCapture is provided by Camera</source>
        <translation>CameraCapture (snímání) je poskytováno kamerou</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CameraRecorder is provided by Camera</source>
        <translation>CameraRecorder (záznamník) je poskytováno kamerou</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CameraExposure is provided by Camera</source>
        <translation>CameraExposure (osvit) je poskytováno kamerou</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CameraFocus is provided by Camera</source>
        <translation>CameraFocus (zaostření) je poskytováno kamerou</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CameraFlash is provided by Camera</source>
        <translation>CameraFlash (blesk) je poskytováno kamerou</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+17"/>
        <source>CameraImageProcessing is provided by Camera</source>
        <translation>CameraImageProcessing (zpracování obrázku) je poskytováno kamerou</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>CameraViewfinder is provided by Camera</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPlaylistFileParser</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/playback/playlistfileparser.cpp" line="+478"/>
        <source>%1 does not exist</source>
        <translation>%1 neexistuje</translation>
    </message>
</context>
<context>
    <name>QPlaylistFileParserPrivate</name>
    <message>
        <location line="-185"/>
        <source>%1 playlist type is unknown</source>
        <translation>%1 typ seznam skladeb je neznámý</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>invalid line in playlist file</source>
        <translation>Neplatný řádek v souboru se seznamem skladeb</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Empty file provided</source>
        <translation>Poskytnut prázdný soubor</translation>
    </message>
</context>
<context>
    <name>QWinRTCameraImageCaptureControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/winrt/qwinrtcameraimagecapturecontrol.cpp" line="+156"/>
        <source>Camera not ready</source>
        <translation type="unfinished">Kamera není připravena</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Invalid photo data length.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Image saving failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
