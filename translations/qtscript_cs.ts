<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>QScriptBreakpointsModel</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptbreakpointsmodel.cpp" line="+447"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Location</source>
        <translation>Umístění</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Condition</source>
        <translation>Podmínka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore-count</source>
        <translation>Kolikrát nechat projít</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Single-shot</source>
        <translation>Spustit jednou</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hit-count</source>
        <translation>Počet spuštění</translation>
    </message>
</context>
<context>
    <name>QScriptBreakpointsWidget</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptbreakpointswidget.cpp" line="+290"/>
        <source>New</source>
        <translation>Nový</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
</context>
<context>
    <name>QScriptDebugger</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebugger.cpp" line="+878"/>
        <location line="+1033"/>
        <source>Go to Line</source>
        <translation>Jít na řádek</translation>
    </message>
    <message>
        <location line="-1032"/>
        <source>Line:</source>
        <translation>Řádek:</translation>
    </message>
    <message>
        <location line="+791"/>
        <source>Interrupt</source>
        <translation>Přerušit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shift+F5</source>
        <translation>Shift+F5</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Step Into</source>
        <translation>Krok do</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F11</source>
        <translation>F11</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Step Over</source>
        <translation>Krok přes</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F10</source>
        <translation>F10</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Step Out</source>
        <translation>Krok ven</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shift+F11</source>
        <translation>Shift+F11</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Run to Cursor</source>
        <translation>Provést po kurzor</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ctrl+F10</source>
        <translation>Ctrl+F10</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Run to New Script</source>
        <translation>Provést po nový skript</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Toggle Breakpoint</source>
        <translation>Přepnout bod přerušení</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Clear Debug Output</source>
        <translation>Smazat výstup ladění</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Clear Error Log</source>
        <translation>Smazat výstupní zápis s chybami</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Clear Console</source>
        <translation>Smazat konzoli</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Find in Script...</source>
        <translation>&amp;Hledat ve skriptu...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Find &amp;Next</source>
        <translation>Najít &amp;další</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Find &amp;Previous</source>
        <translation>Najít &amp;předchozí</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shift+F3</source>
        <translation>Shift+F3</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Debug</source>
        <translation>Ladit</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerCodeFinderWidget</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebuggercodefinderwidget.cpp" line="+133"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Previous</source>
        <translation>Předchozí</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Next</source>
        <translation>Další</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Case Sensitive</source>
        <translation>Rozlišující velká a malá písmena</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Whole words</source>
        <translation>Celá slova</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;img src=&quot;:/qt/scripttools/debugging/images/wrap.png&quot;&gt;&amp;nbsp;Search wrapped</source>
        <translation>&lt;img src=&quot;:/qt/scripttools/debugging/images/wrap.png&quot;&gt;&amp;nbsp;Hledání dosáhlo konce</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerLocalsModel</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebuggerlocalsmodel.cpp" line="+889"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerStackModel</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebuggerstackmodel.cpp" line="+153"/>
        <source>Level</source>
        <translation>Úroveň</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Location</source>
        <translation>Umístění</translation>
    </message>
</context>
<context>
    <name>QScriptEdit</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptedit.cpp" line="+413"/>
        <source>Toggle Breakpoint</source>
        <translation>Přepnout bod přerušení</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disable Breakpoint</source>
        <translation>Vypnout bod přerušení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable Breakpoint</source>
        <translation>Zapnout bod přerušení</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Breakpoint Condition:</source>
        <translation>Podmínka přerušení:</translation>
    </message>
</context>
<context>
    <name>QScriptEngineDebugger</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptenginedebugger.cpp" line="+516"/>
        <source>Loaded Scripts</source>
        <translation>Nahrané skripty</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Breakpoints</source>
        <translation>Body přerušení</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Stack</source>
        <translation>Zásobník</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Locals</source>
        <translation>Místní proměnné</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Console</source>
        <translation>Konzole</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Debug Output</source>
        <translation>Výstup ladění</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Error Log</source>
        <translation>Výstupní zápis s chybami</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Search</source>
        <translation>Hledat</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>View</source>
        <translation>Pohled</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Qt Script Debugger</source>
        <translation>Qt ladění skriptů</translation>
    </message>
</context>
<context>
    <name>QScriptNewBreakpointWidget</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptbreakpointswidget.cpp" line="-223"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
</context>
</TS>
